---
title: "Rejected Trip Analyis"
subtitle: "Monitoring rejected plates via image request review process."
author: "Mike Gaunt"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float:
      collapsed: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  cache = FALSE, cache.lazy = FALSE, autodep = TRUE, warning = FALSE, 
  message = FALSE, echo = TRUE, dpi = 180,
  fig.width = 5, fig.height = 3, echo = FALSE
  )
```

<!--#general comments===========================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is [[insert description here - what it does/solve]]
#
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: [[insert brief readme here]]
#-------- [[insert brief readme here]]
#
# *please use 80 character margins
# *please go to https://pkgs.rstudio.com/flexdashboard/articles/layouts.html
# to explore the different layouts available for the flexdashboard framework
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<!--#library set-up=============================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->
```{r}
library(magrittr)
library(tidyverse)
library(plotly)
library(data.table)
library(DT)
library(lubridate)
library(crosstalk)
library(here)
library(kableExtra)
library(gauntlet)
```

<!--#source helpers/utilities===================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->
```{r}
# source(here("code/helpers_general.r"))
# source(here("code/helpers_aggregation.r"))
source(here("code/helpers_plotly.r"))
source(here("code/helpers_DT.r"))
# source(here("code/helpers_data_import.r"))


#placing this here, mike can run if he's lazy and doesn't want to run two scripts
#should always be commented out
# source(here("code/script_process_icrs.r"))
```

```{js}

function filter_default(){
  document.getElementById("res_created_irr_disp").getElementsByClassName("selectized")[0].selectize.setValue("4. Counts greater than 10,000",false) 
  document.getElementById("res_updatedat").getElementsByClassName("selectized")[0].selectize.setValue("4. Counts greater than 10,000",false) 
}
    
$(document).ready(filter_default);
```

```{r}
group_wtd_quantiles = function(df, value, weight = "count", quantiles = c(0, .25, .5, .75, 1)){
  #this function creates creates qauntiles from data that has been pre-aggregated
  map(quantiles, ~DescTools::Quantile(df[[value]], df[[weight]], .x, na.rm = T)) %>%
    reduce(bind_cols) %>%
    set_names(map_chr(quantiles, ~paste0(value, "_", .x*100, "%")))
}
```

```{r}
fix_facility = function(data){
  data %>%
    mutate(facility = parse_number(facility),
           facility = case_when(facility == 16~"TNB"
                                ,T~as.character(facility)))
}
```


<!--#source data================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev 
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts -->

```{r cache=FALSE}
# bring in latest_icrs data
# -- icrs: contains all data from 08/01/2021 through query date
icrs_list = read_rds_allFiles(data_location = "data/icrs_data",
                              specifically = "icrs_agg_", latest = F) %>%
  rev() 
  # map(~.x %>%
  #       mutate(disposition = case_when(disposition == "1-ISS"~"1-AUTOPASS"
  #                                      ,T~disposition)))
#probably should be implemented at some point but some data dont have column
#need to make a safe version
  # map(~.x %>%
  #       mutate(facility = extract_numeric(facility),
  #              facility = case_when(facility == 16~"TNB"
  #                                   ,T~as.character(facility))))

icrs = icrs_list %>% .[[1]] %>%  data.table()

icrs_safe = icrs

old_data = icrs_list %>% .[[2]] %>%  data.table()
  
```

```{r}
# icrs_aggShort = icrs %>%
#   count_percent_zscore(
#     grp_c = c(trip_date,diff_created_trip,diff_created_ireq,diff_created_irr_disp,
#       time_kapsch,time_etan,time_qfree),
#     grp_p = c()
#   )
# 
# icrs_aggShort_stats = icrs_aggShort %>%
#   mutate(across(c(time_kapsch, time_etan, time_qfree), 
#                 ~dgt2(.x/diff_created_irr_disp), .names = "{.col}_per")) %>% 
#   pivot_longer(cols = starts_with(c("diff", "time")),
#                names_to = "time_period",
#                values_to = "days") %>% 
#   na.omit() %>% 
#   group_by(trip_date, time_period) %>%
#   nest() %>%
#   mutate(
#     mean = map(data, ~weighted.mean(.x$days, .x$count, na.rm = T)),
#     qauntiles = map(data, ~group_wtd_quantiles(.x, value = "days", weight = "count", quantiles = c(.5, .95)))) %>%
#   unnest(cols = c(mean, qauntiles)) %>%
#   select(!data) %>%  
#   mutate(time_period = fct_relevel(time_period,
#                                   c('diff_created_trip', 'diff_created_ireq', 'diff_created_irr_disp',
#                                     'time_kapsch', 'time_etan', 'time_qfree',
#                                     'time_kapsch_per', 'time_etan_per', 'time_qfree_per')))
# 
# icrs_aggShort_stats_result = icrs %>%  
#   filter(!is.na(result)) %>% 
#   group_by(trip_date, result, time_qfree) %>%  
#   summarise(count = sum(count), .groups = "drop") %>% 
#   group_by(trip_date, result) %>%
#   nest() %>%
#   mutate(
#     mean = map(data, ~weighted.mean(.x$time_qfree, .x$count, na.rm = T)),
#     qauntiles = map(data, ~group_wtd_quantiles(.x, value = "time_qfree", quantiles = c(.5, .95), weight = "count"))) %>%
#   unnest(cols = c(mean, qauntiles)) %>%
#   select(!data) 
# 
# icrs_aggShort_strat = icrs_aggShort %>%
#     mutate(trip_week = floor_date(trip_date, "week"),
#            across(starts_with(c("diff", "time")),
#                   ~cut(.x, c(0, 2, 5, 10, 25, 50, 500), right = F)))
```


## Intro

This report details rejected trips only. 

The data used for this analysis extends from ``r min(icrs$trip_date)`` to ``r max(icrs$trip_date)``.    
It was queried on ``r min(icrs$queried_at)``.

## Summary/Observations

<details>
<summary>Click to Expand</summary>

::: {style="padding: 1em; background: #DCDCDC; border-radius: 10px"}
 
Removed. 

:::

</details> 

## Data Overview

The data used for this report is pre-aggregated count data. The data is aggregated by a number of trip attributes - e.g. when and where the trip occurred, etc. This aggregation was performed to limit the size of the data and make it easier to manage across different platforms.

Variable Summary:

<details>
<summary>Click to Expand</summary>

Grouping Attributes: 

+ `trip_date`: trip occurrence date used for trip aggregation 
+ `isrequested`: flag indicating if a IRR request has been submitted 
+ `isresponded`: flag indicating if QFree has acknowledged the submitted IRR
+ `disputeNullFlag`: indicates if a review was disputed
+ `disposition`: the outcome of the image review
+ `result`: what type of IRR was performed
+ `dispute`: type of the IRR dispute (NA indicates no dispute)  
+ `created_intrip`: date trip was created in*lance trip* table
+ `created_intpid `: date trip was created in *lance tripPassage* table
+ `created_irr_req `: date trip was created in *qfree request* table
+ `created_irr_disp `: date trip was created *qfree disposition* table
+ `diff_created_trip`: number of days it took trip to enter *lance tables* from when the trip occurred 
+ `diff_created_ireq`: number of days it took trip to enter *qfree tables* from when the trip occurred 
+ `diff_created_irr_disp`: number of days it took trip to be assigned a disposition from when the trip occurred 
+ `time_kapsch`: number of days Kapsch was responsible for a trip
  - formula: `created_intpid - trip_date`
+ `time_etan`: number of days ETAN was responsible for a trip
  - formula: `created_irr_req - created_intpid`
+ `time_qfree`: number of days QFree was responsible for a trip
  - formula: `created_irr_disp - created_irr_req`

Count Attributes:

+ `count`: count of trips given unique combinations of the above attributes

This table displays a sample of the raw trip level data. 
```{r}
icrs %>% 
  sample_n(200) %>% 
  mutate(id = dplyr::row_number()) %>% 
  dt_common(y = 200, pl = 1000, dom = "Bftr")
```

</details> 

## Rejected Trips Status {#yolo}

This section details the status of the trip image review process. Trips in the `image review request table` but not in the `image review results table` have yet to be processed by QFREE and do not have a disposition associated with it. 

## Current Status {.tabset}

### Total (All Dispositions)
```{r}
icrs %>%  
  mutate(trip_date = floor_date(trip_date, "day")) %>% 
  count_percent_zscore(
    grp_c = c(trip_date, disposition),
    grp_p = c(trip_date), 
    rnd = 2) %>% 
  pivot_longer(cols = c(count, percent)) %>% 
  arrange(trip_date, disposition) %>% 
  plot_ly(x=~trip_date, y = ~value, color = ~replace(disposition, is.na(disposition), "NA"), type = "bar",
          transforms = list(
            list(type = 'filter', target = ~name, 
                 operation = '=', value = "count")
          )) %>%
layout(
  updatemenus = make_menu_item(name_list = c("count", "percent"), filter_pos = 0,
                               direction = "right", x = -.1, y = 1.2),
  barmode = "stack"
    ,xaxis = make_range_select_buttons(month = c(1, 3, 6)
                                       ,rng = 3
                                       ,ttl = "Trip Occurance (day)")
    ,yaxis = list(title = "Trip Count"))

```

### By Roadway (Type-24 Only)
```{r}
temp = icrs %>%  
  # fix_facility() %>% 
  mutate(trip_date = floor_date(trip_date, "day")) %>% 
  count_percent_zscore(
    grp_c = c(trip_date, facility, disposition),
    grp_p = c(trip_date, facility), 
    rnd = 2) %>% 
  pivot_longer(cols = c(count, percent), names_to = "value_type") %>% 
  mutate(disposition = replace(disposition, is.na(disposition), "NA")) %>% 
  filter(str_detect(disposition, "24"))  %>% 
  group_by(facility, value_type) %>% 
  ts_mad()
  # group_by(facility, name) %>%
  # mutate(test_m = roll::roll_mean(value, width = 28)
  #        ,test_sd = roll::roll_sd(value, width = 28)) %>%  
  # ungroup() %>%  
  # mutate(test2 = (value-test_m)/test_sd)


```

```{r fig.height=4}
temp %>% 
  group_by(facility) %>% 
  group_map(~{
    plot_ly(data = .x, x=~trip_date, y = ~value, type = "bar"
            ,legendgroup = ~disposition, showlegend = (.y == "520E")
            ,transforms = list(
              list(type = 'filter', target = ~name, 
                   operation = '=', value = "count")
            )) %>%
      layout(
        updatemenus = make_menu_item(name_list = c("count", "percent"), filter_pos = 0,
                                     direction = "right", x = -.1, y = 1.2)
        ,barmode = "stack"
        ,yaxis = list(
          title = paste0(
            c(rep("&nbsp;", 20),
              paste("<b>", as.character(.y), "</b>"),
              rep("&nbsp;", 20)),
            collapse = "")))
  }) %>% 
  subplot(nrows = NROW(.), margin = .01, shareX = T, shareY = T, titleY = T) %>%
  layout(showlegend = F,
         xaxis = make_range_select_buttons(month = c(1,3,6)
                                           ,rng = 3, ttl = "Trip Occurance (day)"))
```


```{r fig.height=4}
# temp %>% 
#   group_by(facility) %>% 
#   group_map(~{
#     plot_ly(data = .x, x=~trip_date, y = ~test2, type = "bar"
#             ,legendgroup = ~disposition, showlegend = (.y == "520E")
#             ,transforms = list(
#               list(type = 'filter', target = ~name, 
#                    operation = '=', value = "count")
#             )) %>%
#       layout(
#         updatemenus = make_menu_item(name_list = c("count", "percent"), filter_pos = 0,
#                                      direction = "right", x = -.1, y = 1.2)
#         ,barmode = "stack"
#         ,yaxis = list(
#           title = paste0(
#             c(rep("&nbsp;", 20),
#               paste("<b>", as.character(.y), "</b>"),
#               rep("&nbsp;", 20)),
#             collapse = "")))
#   }) %>% 
#   subplot(nrows = NROW(.), margin = .01, shareX = T, shareY = T, titleY = T) %>%
#   layout(showlegend = F,
#          xaxis = make_range_select_buttons(month = c(1,3,6)
#                                            ,rng = 3, ttl = "Trip Occurance (day)"))
```

## {-}

## Plate Rejection Rates: {.tabset}

### By Completion Date

```{r}
icrs_safe %>% 
  .[disposition == "24-REJECTED_UNREADABLE",] %>% 
  .[,.(Count = sum(count)), by = .(queried_at, created_irr_disp)] %>% 
  .[order(created_irr_disp)] %>% 
  .[,`:=`(`Rolling 2\nWeek Avg.` = zoo::rollmean(Count, 14, "center"))] %>%
  pivot_longer(cols = c(Count, `Rolling 2\nWeek Avg.`)) %>%
  plot_ly(x = ~created_irr_disp, y = ~value, color = ~name,
          type = "scatter", mode = "line", showlegend=T) %>%
  layout(xaxis = make_range_select_buttons(month = c(1,3,6), rng = 3
                                           ,ttl = "Date ETAN received from Qfree")
         ,yaxis = list(title = "Count (trips rejected)"))
```


### Days to Reject (total agg.)
```{r}
temp = icrs %>%
  filter(!is.na(result)) %>%
  filter(disposition == "24-REJECTED_UNREADABLE") %>%
  fix_facility() %>%
  group_by(trip_date = floor_date(trip_date, "day"), time_qfree) %>%
  summarise(count = sum(count), .groups = "drop") %>%
  group_by(trip_date) %>%
  nest() %>%
  mutate(
    mean = map(data, ~weighted.mean(.x$time_qfree, .x$count, na.rm = T)),
    qauntiles = map(data, ~group_wtd_quantiles(.x, value = "time_qfree", quantiles = c(.5, .95), weight = "count"))) %>%
  unnest(cols = c(mean, qauntiles)) %>%
  select(!data) %>%
  rename(Median = 'time_qfree_50%', Average = 'mean'
         ,`95th\nPercentile` = "time_qfree_95%") %>%
  pivot_longer(cols = c(Average, Median, `95th\nPercentile`),
               names_to = "Metric") %>%  ungroup()

ylmt = temp %>%
  filter(trip_date > Sys.Date()-months(6)) %>%
  pull(value) %>%
  max()

##plot----
temp %>%
    plot_ly(x=~trip_date, y = ~value, color = ~Metric, 
            type = "scatter",  mode = 'lines') %>% 
  layout(showlegend = T
         ,yaxis = list(title = "Days", range = c(0, ylmt))
         ,xaxis = make_range_select_buttons(month = c(1,3,6), rng = 6, ttl = "Trip Occurance (day)"))
```

### Days to Reject (by roadway) 
```{r}
temp = icrs %>%
  filter(!is.na(result)) %>%
  filter(disposition == "24-REJECTED_UNREADABLE") %>%
  fix_facility() %>%
  group_by(trip_date = floor_date(trip_date, "day"), facility, time_qfree) %>%
  summarise(count = sum(count), .groups = "drop") %>%
  group_by(trip_date, facility) %>%
  nest() %>%
  mutate(
    mean = map(data, ~weighted.mean(.x$time_qfree, .x$count, na.rm = T)),
    qauntiles = map(data, ~group_wtd_quantiles(.x, value = "time_qfree", quantiles = c(.5, .95), weight = "count"))) %>%
  unnest(cols = c(mean, qauntiles)) %>%
  select(!data) %>%
  rename(Median = 'time_qfree_50%', Average = 'mean'
         ,`95th\nPercentile` = "time_qfree_95%") %>%
  pivot_longer(cols = c(Average, Median, `95th\nPercentile`),
               names_to = "Metric")

ylmt = temp %>%
  filter(trip_date > Sys.Date()-months(6)) %>%
  pull(value) %>%
  max()

##plot----
temp %>%
  group_by(facility) %>%
  group_map(~{
    plot_ly(.x, x=~trip_date, y = ~value, color = ~Metric, legendgroup = ~Metric,
            type = "scatter",  mode = 'lines', showlegend = (.y == "520")) %>%
      layout(
        # xaxis = list(range = quick_date_rng(3))
        yaxis = list(
          range = c(0, ylmt+ylmt*.10)
          ,title = paste0(
            c(rep("&nbsp;", 20),
              paste("<b>", as.character(.y), "</b>"),
              rep("&nbsp;", 20)),
            collapse = ""))
      )
  }) %>%
  subplot(nrows = NROW(.), margin = .01, shareX = T, shareY = T, titleY = T) %>%
  layout(showlegend = T,
         xaxis = make_range_select_buttons(month = c(1,3,6), rng = 6, ttl = "Trip Occurance (day)"))
```

## {-}

<!--end-->









