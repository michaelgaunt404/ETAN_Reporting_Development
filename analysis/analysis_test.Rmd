---
title: "ICRS Analysis"
subtitle: "Monitoring image request processing and status."
author: "Mike Gaunt & Kara Todd"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: TRUE
    toc_float:
      collapsed: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  cache = FALSE, cache.lazy = FALSE, autodep = TRUE, warning = FALSE, 
  message = FALSE, echo = TRUE, dpi = 180,
  fig.width = 5, fig.height = 3, echo = FALSE
  )
```

<!--#general comments===========================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is [[insert description here - what it does/solve]]
#
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: [[insert brief readme here]]
#-------- [[insert brief readme here]]
#
# *please use 80 character margins
# *please go to https://pkgs.rstudio.com/flexdashboard/articles/layouts.html
# to explore the different layouts available for the flexdashboard framework
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<!--#library set-up=============================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->

<!--#source helpers/utilities===================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->


<!--#source data================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev 
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts -->


## Intro

This is the first attempt at an ICRS report. The intent of this document is to provide some context to the data queried by the new ICRS query. 

This document is under development and will act as a report as well as a template that will be used for reoccurring analyses. Some of the content may be holdover analysis artifacts. 


## Summary/Observations

<details>
<summary>Click to Expand</summary>

::: {style="padding: 1em; background: #DCDCDC; border-radius: 10px"}
 
Removed. 

:::

</details> 

## Data Overview

The data used for this report is pre-aggregated count data. The data is aggregated by a number of trip attributes - e.g. when and where the trip occurred, etc. This aggregation was performed to limit the size of the data and make it easier to manage across different platforms.

Variable Summary:

<details>
<summary>Click to Expand</summary>

Grouping Attributes: 

+ `trip_date`: trip occurrence date used for trip aggregation 
+ `isrequested`: flag indicating if a IRR request has been submitted 
+ `isresponded`: flag indicating if QFree has acknowledged the submitted IRR
+ `disputeNullFlag`: indicates if a review was disputed
+ `disposition`: the outcome of the image review
+ `result`: what type of IRR was performed
+ `dispute`: type of the IRR dispute (NA indicates no dispute)  
+ `created_intrip`: date trip was created in*lance trip* table
+ `created_intpid `: date trip was created in *lance tripPassage* table
+ `created_irr_req `: date trip was created in *qfree request* table
+ `created_irr_disp `: date trip was created *qfree disposition* table
+ `diff_created_trip`: number of days it took trip to enter *lance tables* from when the trip occurred 
+ `diff_created_ireq`: number of days it took trip to enter *qfree tables* from when the trip occurred 
+ `diff_created_irr_disp`: number of days it took trip to be assigned a disposition from when the trip occurred 
+ `time_kapsch`: number of days Kapsch was responsible for a trip
  - formula: `created_intpid - trip_date`
+ `time_etan`: number of days ETAN was responsible for a trip
  - formula: `created_irr_req - created_intpid`
+ `time_qfree`: number of days QFree was responsible for a trip
  - formula: `created_irr_disp - created_irr_req`

Count Attributes:

+ `count`: count of trips given unique combinations of the above attributes

</details> 

* **ETAN Database Exploration Tool**          
This application is an interactive markdown which allows for high level exploration of the ETAN database.    
  - Link to the [tool (last updated 2021-07-06)](https://thxmo87.shinyapps.io/ETAN_database_guide/?_ga=2.161016189.1747854243.1626974155-202500809.1626974155) 

* **Periodic Reporting**    
This is an example of a report which is published every month. Each month the same analysis template is used to generate the report using the data from the previous month.

  + ICRS Analysis Report (published ~weekly)  
    - jdjd
    
  
  <details>    
  <summary>Current Reports</summary>

    - Test 1
    - Test 2

  </details>   

  <details>   
  <summary>Sept Reports</summary>  

- Test 1
- Test 2

</details>   
    
  + Assigned Toll Amount Changes (published ~weekly)
    - [Most recent toll monitoring report](toll_change_2021-10-01.html)

  + Trip Summary Report (published monthly)
    - [Trips Summary - March 2020](TSMR_April_2020-05-04.html)
    - [Trips Summary - April 2020](TSMR_June_2020-07-06.html)      
    - [Trips Summary - May 2020](TSMR_March_2020-04-01.html)
    - [Trips Summary - June 2020](TSMR_May_2020-06-02.html)

* **Dashboard Mock-Ups**   
The workflowr framework is capable of publishing dashboards created using flexdashboard. Shiny elements cannot be used with this framework, interactivity requires document to be hosted on ShinyIO. These dashboards are intended to very lightweight and used only for mock-ups for production dashboards and/or feature development.
  + [Trip Summary Dashboard](dashboard_trip_summary.html) 
  




