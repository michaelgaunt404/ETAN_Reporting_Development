--#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--#
--# This script contains SQL queries used for monitoring the status of tolls 
--#
--# By: mike gaunt, michael.gaunt@wsp.com
--#
--# README: mostly sql scripts
--#-------- [[description]]
--# *please use 80 character margins
--# *please save as helpers_[[informative description]]
--#
--#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


--QUERY #1 \\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
--//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\/\/
--get table that records state of trip amouts 
select 
ta.tripId
,tp.trippassageid
,t.ledgerItemInfoId
,tp.lanetime
,tp.axleCount 
,ta.amount 
,ta.updatedat as ta_updatedat
,t.created as t_created
,tp.updatedat as tp_updatedat
,datediff(day, tp.lanetime, ta.updatedat) as diff_ta_updatedat
,datediff(day, tp.lanetime, t.created) as diff_t_created
,datediff(day, tp.lanetime, tp.updatedat) as diff_tp_updatedat
,GETDATE() as query_date
into #temp_new_query
from lance.TripAmount ta
join lance.trip t on t.tripId = ta.tripId 
join lance.tripPassage tp on tp.tripPassageId = t.tripPassageId 
where cast(tp.laneTime as date) > '2021-09-01';

--QUERY #2 \\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
--//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\/\/
--put old data into sacrificial temp table 
select * into #temp_again_1 from WSDOT_STAGE.tmp.trip_amount_changes_1;

--QUERY #3 \\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
--//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\/\/
INSERT into #temp_again_1
select * from #temp_new_query;

--QUERY #4 \\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
--//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\/\/
--make lage amounts to perfrom filter operation on later
select *,
LAG(amount, 1, 0) OVER(PARTITION BY trippassageid 
       ORDER BY trippassageid, query_date ASC) AS amount_lag
into #temp_again_2
from #temp_again_1;

--exploration checks
select top 100 * from #temp_again_2 where amount != amount_lag and amount_lag != 0 
select top 100 * into #temp_index_changedAmount from #temp_again_2 where amount != amount_lag and amount_lag != 0 --make index
SELECT * from #temp_index_changedAmount

select count(*) from #temp_again_2 where amount != amount_lag and amount_lag != 0

select query_date, count(*) as count from #temp_again_2 group by query_date

--Questions
--records that are new and that need to be added to the table for future reference
--is there any easier way of doing this -- is there some ledger that records price reductions
--or changes to the data with some sort of tag asscoiated with them???!?!?!?!?
----->the ledger and transaction tables are really hard to interpret 
----->theres a lot going on in there -like different account ids and different additions/subtractions of amounts 


select top 100 * from #temp_again_2 where amount != amount_lag and amount_lag != 0 


--new records added since last download
select top 100 * from #temp_again_2 where lanetime > '2022-03-08 16:19:11.900'

--top records
select top 100 * from #temp_again_2 
select top 100 trippassageid into #temp_index_tr from #temp_again_2 

--new records added since last download
select top 100 * from #temp_again_2 where lanetime > '2022-03-08 16:19:11.900'
select top 100 trippassageid into #temp_index_nr from #temp_again_2 where lanetime > '2022-03-08 16:19:11.900' 

--records that have changed since last download
select top 100 * from #temp_again_2 where lanetime > '2022-03-01 16:19:11.900' AND ta_updatedat > '2022-03-08 16:19:11.900'
select top 100 trippassageid into #temp_index_cr from #temp_again_2 where lanetime > '2022-03-01 16:19:11.900' AND ta_updatedat > '2022-03-08 16:19:11.900'


select *,
case when amount != amount_lag then 0 else  1 end as flag
into #temp_again_3
from #temp_again_2
where trippassageid in (select * from #temp_index_tr)
or trippassageid in (select * from #temp_index_nr)
or trippassageid in (select * from #temp_index_cr)
order by trippassageid, ta_updatedat

SELECT
*,
    s_index = ROW_NUMBER() OVER(PARTITION BY trippassageid ORDER BY trippassageid, query_date)
into #temp_again_4
FROM #temp_again_3

select * from #temp_again_4
where (flag = 0 and s_index = 1) | (flag = 0 and s_index = 1)



      
--section looking at other tables an trying to see if they record the different changes to a toll amount 
--\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\
--//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\/\/
SELECT * from #temp_again_4 order by ledgerItemInfoId;
select * from xact.V_TransactionSummary where ledgerItemInfoId in (329897881, 290870511, 290870602, 291087927, 291087928, 291087929, 291087930)  order by ledgerItemInfoId, transactionId;

--select top 10 * from xact.V_TransactionSummary 
--xact.V_TransactionSummary and xact.ledger are super similar - all id attibutes and toll amounts are the same pretty much
select * from xact.V_TransactionSummary where ledgerItemInfoId in (SELECT ledgerItemInfoId from #temp_again_4) order by ledgerItemInfoId, transactionId 
select * from xact.ledger where ledgerItemInfoId in (SELECT ledgerItemInfoId from #temp_again_4) order by ledgerItemInfoId, transactionId 
select * from xact.EnuTransactionType ett
select * from xact.EnuTransactionCategory etc 

select top 1000 transactionTypeId, count(*) as count from xact.V_TransactionSummary group by transactionTypeId
select top 1000 transactionTypeId, count(*) as count from xact.V_TransactionSummary group by transactionTypeId
select top 1000 transactionTypeId, count(*) as count from xact.ledger group by transactionTypeId

select top 5 * from xact.Ledger l 
select top 5 * from xact.V_TransactionSummary
select top 10 * from xact.V_TransactionSummary WHERE ledgerentryid in (SELECT ledgerentryid from #temp_index_esCount_transactionId)
select top 10 * from rtoll.toll
select top 10 * from rtoll.TollStatus ts 