--#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--#
--# By: mike gaunt, michael.gaunt@wsp.com
--#
--# README: mostly sql scripts
--#-------- icrs sql scripts 
--#-------- newer version - needs the ability to write out to report
--# *please use 80 character margins
--# *please save as helpers_[[informative description]]
--#
--#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--QUERY_#1==================================================================================
--==========================================================================================
--makes icrs data - is eventually aggregated for report because it is really big
with icrs as(
select ireq.trippassageid
,ireq.imageReviewRequestId
,irrr.imageReviewResultsId
,ireq.isrequested,ireq.isresponded
,CASE
    WHEN irr.dispositionId IS NULL THEN 0
    ELSE 1
END AS dispositionNullFlag
,CASE
    WHEN ireq.disputetypeid IS NULL THEN 0
    ELSE 1
END AS disputeNullFlag
,(cast(irr.dispositionId as varchar(10)) + '-' + dispo.shortcode) as disposition
,(cast(irr.resulttypeid as varchar(10)) + '-' + res.shortcode) as result
,(cast(ireq.disputetypeid as varchar(10)) + '-' + dispu.shortcode) as dispute
,cast(ireq.updatedat as date) as created_irr_req
,cast(irrr.processedat as date) as created_irr_disp
from qfree.ImageReviewRequest ireq 
left join qfree.ImageReviewResult irr on irr.imageReviewRequestId = ireq.imageReviewRequestId
left join qfree.ImageReviewResultResponses irrr on irrr.imagereviewresultsid = irr.imagereviewresultsid
left join qfree.enudisposition dispo on dispo.dispositionId = irr.dispositionId
left join qfree.enudisputetype dispu on dispu.disputeTypeId = ireq.disputeTypeId
left join qfree.enuresulttype res on res.resulttypeid = irr.resulttypeid
)
select 
cast(tpass.lanetime as datetime) as trip_datetime 
,icrs.trippassageid, icrs.imageReviewRequestId,icrs.imageReviewResultsId,icrs.isrequested,icrs.isresponded
,icrs.dispositionNullFlag, icrs.disputeNullFlag,icrs.disposition,icrs.result,icrs.dispute
,trip.created as created_intrip
,tpass.updatedAt as created_intpid
,icrs.created_irr_req, icrs.created_irr_disp
,DATEDIFF(day, cast(tpass.lanetime as date), trip.created) as diff_created_trip
,DATEDIFF(day, cast(tpass.lanetime as date), icrs.created_irr_req) as diff_created_ireq
,DATEDIFF(day, cast(tpass.lanetime as date), icrs.created_irr_disp) as diff_created_irr_disp
,DATEDIFF(day, cast(tpass.lanetime as date), trip.created) as time_kapsch
,DATEDIFF(day, trip.created, icrs.created_irr_req) as time_etan
,DATEDIFF(day, icrs.created_irr_req, icrs.created_irr_disp) as time_qfree
,cast(GETDATE() as date) as queried_at
into #temp_icrs_1
from icrs
join lance.trippassage tpass on tpass.trippassageid = icrs.trippassageid
left join lance.trip trip on trip.tripPassageId = icrs.trippassageid
where (cast(tpass.lanetime as datetime) > '2021-08-01 00:00:00.000')

--select top 10 * from #temp_icrs_1

--QUERY_#2==================================================================================
--==========================================================================================
--selects singular rows for each trip_date and diff_% column grouping 
select top 2 with ties * from #temp_icrs_1
order by row_number() over (partition by 
cast(trip_datetime as date)
,diff_created_trip
,diff_created_ireq
,diff_created_irr_disp
,diff_etan
,diff_qfree order by trip_datetime)

--QUERY_#3==================================================================================
--==========================================================================================
--perfroming some aggregation so I can look at it in R
select 
cast(trip_datetime as date) as trip_date
,diff_created_trip
,diff_created_ireq
,diff_created_irr_disp
,diff_etan
,diff_qfree
,count(*) as count 
into #temp_icrs_agg
from #temp_icrs_1
group by 
cast(trip_datetime as date)
,diff_created_trip
,diff_created_ireq
,diff_created_irr_disp
,diff_etan
,diff_qfree

--select * from #temp_icrs_agg

--QUERY_END=================================================================================
--==========================================================================================




select 
trip_date
,DATEDIFF(day, trip_date, updatedat) as diff_updatedat_ireq
,DATEDIFF(day, trip_date, updated_tpid) as diff_updatedat_tpid
,DATEDIFF(day, trip_date, createdat) as diff_created_irrr
,DATEDIFF(day, trip_date, created_trip) as diff_created_trip
,DATEDIFF(day, updated_tpid, updatedat) as diff_updatedat_cols
,count(*) as count 
into #temp_icrs_agg
from #temp_icrs
group by 
trip_date
,DATEDIFF(day, trip_date, updatedat) 
,DATEDIFF(day, trip_date, updated_tpid) 
,DATEDIFF(day, trip_date, createdat) 
,DATEDIFF(day, trip_date, created_trip) 
,DATEDIFF(day, updated_tpid, updatedat) 

select top 6 * 
from #temp_icrs
where DATEDIFF(day, trip_date, updated_tpid) > 10 
and DATEDIFF(day, updated_tpid, updatedat) > 10

select count(*) 
from #temp_icrs
where DATEDIFF(day, trip_date, updated_tpid) > 80
and DATEDIFF(day, updated_tpid, updatedat) > 10

select DATEDIFF(day, updated_tpid, updatedat), count(*) 
from #temp_icrs
where DATEDIFF(day, trip_date, updated_tpid) >= 80 and DATEDIFF(day, trip_date, updated_tpid) < 86
group by DATEDIFF(day, updated_tpid, updatedat)
order by DATEDIFF(day, updated_tpid, updatedat)


and DATEDIFF(day, updated_tpid, updatedat) > 10



select count(*) from #temp_icrs_agg

select top 2 with ties * from #temp_icrs_agg
order by row_number() over (partition by trip_date, diff_updatedat_cols order by trip_date)

select top 9 * from #temp_icrs_2

select top 2 with ties * from #temp_icrs_2
order by row_number() over (partition by cast(trip_datetime as date)
,diff_created_trip
,diff_created_ireq
,diff_created_irr_disp
,diff_etan
,diff_qfree order by trip_datetime)



,diff_updatedat_ireq
,diff_updatedat_tpid
,diff_created_irrr
,diff_created_trip
,diff_updatedat_cols