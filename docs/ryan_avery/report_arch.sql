/*
-- ETAN database reporting architecture
-- Ryan P. Avery <ryan.avery@wsp.com>
*/

/********** DESIGN **********/
/*

generic prefixes:
 - normal table: no prefix
 - history table: h_ (mssql temporal tables, not generic history)
 - scratch table: x_ (often used in reporting to store latest result)
 - index: ix_
 - function: f_
 - user stored procedure: usp_
 - view: v_

logging - look at ETCC logging to see what might be useful

temporal tracking tables:
 - trip
   - trip_id
   - latest (by posting date, ordered by priority?) TollLedgerEntry transaction id (represents current state of trip)
   - latest (by posting date) isdebit, non-fee (transactionCategoryId<>2) TollLedgerEntry transaction id (debit removes TOLLDISC and WRITEOFF types, non-fee removes FPBP, FCPP, FCVP)
   - [in separate table?] posting TollLedgerEntry transaction_id? (first txn after LATEST system account, can't do first non-system since it can get re-processed)
   - [in separate table?] may need to keep latest non-NOCP TollLedgerEntry transaction_id to get proper amount of toll - also consider late fees

 - account
   - customer_account_id
   - account_type (based on derived USP_AccountTypesAll, no need to keep other fields after investigation)
   
 - future:
   - if we ever geocode addresses to coordinates, we will want to track address details to know when we need to update coords

reports:
 - leakage (incorporated into disposition?)
 - trips by payment method (incorporated into disposition at monthly level, do we still need daily?)
 - TB/NOCP summary - investigate
 - trip disposition
 - accounts by account type, account status (open, pending close, closed, etc), and last trip month
 - tags by account type, account status (open, pending close, closed, etc), tag agency and tag type, and last trip month

write docs as markdown

*/

USE WSDOT_STAGE;

CREATE SCHEMA log;		-- logging schema
CREATE SCHEMA misc;		-- misc schema
CREATE SCHEMA rpt;		-- reporting schema
CREATE SCHEMA tmp;		-- temp schema (promote objects to other schemas upon validation)
CREATE SCHEMA util; 	-- util schema (procedures, tracking, etc)
CREATE SCHEMA wsdot;	-- schema for custom wsdot data
CREATE SCHEMA xold;		-- schema for deprecated work

-- user schemas
CREATE SCHEMA rpa;	-- schema for ryan
CREATE SCHEMA meg;	-- schema for michael
--CREATE SCHEMA kgt;	-- schema for kara

/*######################################################################*/
/*##########                     LOGGING                      ##########*/
/*######################################################################*/

-- create log level priority if needed for sorting
-- DROP TABLE WSDOT_STAGE.log.level_priority;
CREATE TABLE WSDOT_STAGE.log.level_priority (
	log_level varchar(8) NOT NULL PRIMARY KEY
	,priority smallint NOT NULL
);

INSERT INTO WSDOT_STAGE.log.level_priority VALUES
('DEBUG',10),
('INFO',20),
('WARN',30),
('ERROR',40),
('FATAL',50);

-- activity types
-- DROP TABLE WSDOT_STAGE.log.activities;
CREATE TABLE WSDOT_STAGE.log.activities (
	activity_id smallint NOT NULL PRIMARY KEY
	,activity_desc varchar(64) NOT NULL
);

-- TODO: define hierarchy around various activities?
-- for now, use 100s for state/history tracking
INSERT INTO WSDOT_STAGE.log.activities VALUES
(100,'Account Type Tracking'),
(101,'Trip State Tracking');

-- create message logging table
-- table name, action and item count are optional depending on circumstances
-- DROP TABLE WSDOT_STAGE.log.messages;
CREATE TABLE WSDOT_STAGE.log.messages (
	system_time datetime2(2) NOT NULL
	,activity_id smallint NOT NULL
	,log_level varchar(8) NOT NULL
	,message varchar(96) NOT NULL
	,table_name varchar(64)
	,table_action varchar(20)
	,item_count integer
);

-- view linking messages with activities
-- DROP VIEW log.v_activity_messages;
CREATE OR ALTER VIEW log.v_activity_messages AS
SELECT  m.system_time
		,m.activity_id
		,a.activity_desc
		,m.log_level
		,m.message
		,m.table_name
		,m.table_action
		,m.item_count
  FROM  WSDOT_STAGE.log.messages m 
  JOIN  WSDOT_STAGE.log.activities a ON a.activity_id = m.activity_id;

-- simple run logging table for scheduled reporting
-- 2022-08-03: update to add process and server fields
-- DROP TABLE WSDOT_STAGE.log.runinfo;
CREATE TABLE WSDOT_STAGE.log.runinfo (
	message varchar(96)
	,process_name varchar(32)
	,server_name varchar(32)
	,system_time datetime2(2) NOT NULL DEFAULT getdate()
);

-- simple debug logging table for scheduled reporting
-- scheduled reporting will truncate records older than 7 days
-- DROP TABLE WSDOT_STAGE.log.debug_info;
/*CREATE TABLE WSDOT_STAGE.log.debug_info (
	jobstep smallint
	,message varchar(96)
	,system_time datetime2(2) NOT NULL DEFAULT getdate()
);
*/

/*######################################################################*/
/*##########                UTILITY FUNCTIONS                 ##########*/
/*######################################################################*/

/********** BEGINNING OF MONTH **********/
-- get beginning of month (return NULL if NULL)
-- DROP FUNCTION util.f_get_month;
CREATE OR ALTER FUNCTION util.f_get_month (@dtm datetime2(2))
RETURNS date AS
BEGIN
	DECLARE @ret datetime2;
	SELECT @ret = dateadd(month, datediff(month, 0, @dtm), 0);
	RETURN @ret;
END;

/********** BEGINNING OF QUARTER **********/
-- get beginning of quarter (return NULL if NULL)
-- DROP FUNCTION util.f_get_quarter;
CREATE OR ALTER FUNCTION util.f_get_quarter (@dtm datetime2(2))
RETURNS date AS
BEGIN
	DECLARE @ret datetime2;
	SELECT @ret = dateadd(quarter, datediff(quarter, 0, @dtm), 0);
	RETURN @ret;
END;

/********** REPORTING TYPES **********/
-- check if performing monthly reporting (allow time for trip building, run on 10th)
-- DROP FUNCTION util.f_is_report_monthly;
CREATE OR ALTER FUNCTION util.f_is_report_monthly (@dtm datetime2(2))
RETURNS bit AS
BEGIN
	DECLARE @ret bit;
	IF datepart(day, @dtm) = 10 SET @ret = 1
	ELSE SET @ret = 0
	RETURN @ret
END;

-- check if performing weekly reporting (on Mondays)
-- DROP FUNCTION util.f_is_report_weekly;
CREATE OR ALTER FUNCTION util.f_is_report_weekly (@dtm datetime2(2))
RETURNS bit AS
BEGIN
	DECLARE @ret bit;
	IF datename(weekday, @dtm) = N'Monday' SET @ret = 1
	ELSE SET @ret = 0
	RETURN @ret
END;

/********** TIME DIFFERENCE IN HH:MM:SS **********/
-- get difference between two timestamps
-- TODO: check for large time differences or add days?
-- DROP FUNCTION util.f_timediff_hhmmss;
CREATE OR ALTER FUNCTION util.f_timediff_hhmmss (@start_dtm datetime2(2), @end_dtm datetime2(2))
RETURNS varchar(10) AS
BEGIN
	DECLARE @ret varchar(10);
	WITH q AS (
		SELECT datediff(second, @start_dtm, @end_dtm) t
	), hms AS (
		SELECT t/60/60 AS hh,  (t/60) % 60 AS mm , t % 60 AS ss
		FROM q
	)
	SELECT @ret = concat(format(hh,'00'),':',format(mm,'00'),':',format(ss,'00') )
	FROM hms;
	
	RETURN @ret;
END;

/********** CSV SANITIZE **********/
-- sanitize text for csv output by converting commas to semicolons and removing char 10 and 13 (\r\n)
-- default to converting comma to blank, but user can choose another character to replace to
-- DROP FUNCTION util.f_csv_sanitize;
CREATE OR ALTER FUNCTION util.f_csv_sanitize (@in_text varchar(max), @comma_to varchar(1) = '')
RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max);
	SELECT @ret = replace(replace(replace(@in_text, ',', @comma_to), char(10), ''), char(13), '')
	RETURN @ret;
END;

/********** SERVER AVAILABILITY GROUPS AND ROLES **********/
-- NOTE: info on AGs: https://docs.microsoft.com/en-us/sql/database-engine/availability-groups/windows/monitor-availability-groups-transact-sql?view=sql-server-ver16
-- first create utility views for AG servers and roles

-- create detailed view for checking status of all server roles
-- DROP VIEW util.v_server_ag_roles;
CREATE OR ALTER VIEW util.v_server_ag_roles AS
SELECT  ag.name AS ag_name, ar.replica_server_name, ars.is_local, ars.role_desc, ars.operational_state_desc
  FROM  sys.dm_hadr_availability_replica_states ars
  JOIN  sys.availability_replicas ar ON ar.replica_id = ars.replica_id 
  JOIN  sys.availability_groups ag ON ars.group_id = ag.group_id;

-- create detailed view for checking status
-- use is_local = 1 to look at currently connected (local) server
-- DROP VIEW util.v_local_server_ag_roles;
CREATE OR ALTER VIEW util.v_local_server_ag_roles AS
SELECT  *
  FROM  WSDOT_STAGE.util.v_server_ag_roles sar
 WHERE  sar.is_local = 1;

-- list of servers in availability group
-- DROP VIEW util.v_ag_servers;
CREATE OR ALTER VIEW util.v_ag_servers AS
SELECT  *
  FROM  WSDOT_STAGE.util.v_server_ag_roles sar
 WHERE  sar.ag_name = 'MCLUSTR9002AG1';

-- return role of server (e.g. PRIMARY, SECONDARY) in specified availability group
-- see https://dba.stackexchange.com/questions/129556/scheduling-jobs-on-a-sql-always-on-ha-cluster
-- NOTE: use AG1 listener (MCLUSTR9002AG1) as default value
-- see https://stackoverflow.com/questions/8358315/t-sql-function-with-default-parameters
-- DROP FUNCTION util.f_get_ag_role;
CREATE OR ALTER FUNCTION util.f_get_ag_role (@ag_name sysname = 'MCLUSTR9002AG1')
RETURNS nvarchar(60) AS
BEGIN
	DECLARE @ret nvarchar(60);
	SELECT  @ret = lsr.role_desc 
	  FROM  WSDOT_STAGE.util.v_local_server_ag_roles lsr
	 WHERE  lsr.ag_name = @ag_name;
	RETURN @ret;
END;

/********** JOB REPORTING RESULTS & HISTORY **********/
-- functions to build dynamic SQL strings to include server name for running on specified target server
-- TODO: consider alternative approaches (but have not worked so far):
--		 sp_prepare passing a handle does not work because server prefix before database does not work as a parameter 
--		 openquery avoids server.database.schema.table aliasing, but you can't pass variables to the call
--		 EXEC (sql) AT server and :CONNECT also won't work
-- NOTE: use four (4) quotes to quote a variable (casn't use @@servername since it is evaluated in advance?)

-- get job info by server
-- DROP FUNCTION util.f_build_query_server_job_detail;
CREATE OR ALTER FUNCTION util.f_build_query_server_job_detail (@server nvarchar(256), @jobname nvarchar(128))
RETURNS nvarchar(max) AS
BEGIN
	DECLARE @sql nvarchar(max)
	DECLARE @col_list nvarchar(max)
	SET @col_list = 'j.name AS job_name, j.enabled, j.version_number AS job_version, st.step_id, st.step_name, st.subsystem, st.command, st.last_run_outcome
		,st.last_run_duration, st.last_run_date, st.last_run_time, js.next_run_date, js.next_run_time, s.name AS schedule_name, hs.latest_run_date, hs.hist_msg_count'
	SET @sql = 'SELECT ' + '''' + @server + '''' + ' AS server_name,' + @col_list + '
	  FROM  ' + @server + '.msdb.dbo.sysjobs j
	  JOIN  ' + @server + '.msdb.dbo.sysjobsteps st ON st.job_id = j.job_id
	  LEFT  JOIN ' + @server + '.msdb.dbo.sysjobschedules js ON js.job_id = j.job_id
	  LEFT  JOIN ' + @server + '.msdb.dbo.sysschedules s ON s.schedule_id = js.schedule_id
	  LEFT  JOIN (SELECT h.job_id, max(h.run_date) AS latest_run_date, count(*) AS hist_msg_count FROM ' + @server + '.msdb.dbo.sysjobhistory h	GROUP BY h.job_id) hs ON hs.job_id = j.job_id
	 WHERE  j.name = ' + @jobname
	RETURN @sql
END;

-- get job history by server
-- DROP FUNCTION util.f_build_query_server_job_history;
CREATE OR ALTER FUNCTION util.f_build_query_server_job_history (@server nvarchar(256), @jobname nvarchar(128), @result_limit int)
RETURNS nvarchar(max) AS
BEGIN
	DECLARE @sql nvarchar(max)
	DECLARE @col_list nvarchar(max)
	SET @col_list = 'h.server AS server_name, h.instance_id, h.step_id, h.step_name, h.sql_message_id, h.sql_severity, h.message, h.run_status, h.run_date, h.run_time, h.run_duration'
	SET @sql = 'SELECT TOP ' + str(@result_limit) + ' ' + @col_list + '
	  FROM  ' + @server + '.msdb.dbo.sysjobs j
	  JOIN  ' + @server + '.msdb.dbo.sysjobhistory h ON h.job_id = j.job_id
	 WHERE  j.name = ' + @jobname + ' ORDER BY h.run_date DESC, h.instance_id;'
	RETURN @sql
END;

-- procedure to iterate over servers and get server job information
-- TODO: look into extracting iteration over servers to separate procedure, but don't know how to pass dynamic function
--		 for now, use variable to select type 
-- NOTE: must use stored procedure in order to execute dynamic SQL
-- for cursor example, see https://stackoverflow.com/a/11852882
-- for dynamic table creation, see https://dba.stackexchange.com/a/243371
-- 2023-02-07: new 'Z' servers seem to have been added, but don't have job info - change to ignore these for now
-- DROP PROCEDURE util.usp_get_server_job_info;
CREATE OR ALTER PROCEDURE util.usp_get_server_job_info (
	@info varchar(16)
	,@job_name nvarchar(128) = '''WSDOT_REPORTING'''
	,@result_limit int = 10
) AS
BEGIN
	DECLARE @sql nvarchar(max)
	DECLARE @cur cursor
	DECLARE @server nvarchar(256)
	DECLARE @alter nvarchar(MAX) = N'ALTER TABLE #ret ADD '
	
	-- check that info type exists - if not, exit
	IF charindex(@info,'detail,history') = 0
	BEGIN
		PRINT 'Invalid job info type: ' + @info
		RETURN 1
	END
	
	-- create base table with just server name
	DROP TABLE IF EXISTS #ret
	CREATE TABLE #ret (server_name nvarchar(128))
	
	-- select sql based on information type to build correct table structure (use @@servername to select server)
	IF @info = 'detail'		SELECT @sql = WSDOT_STAGE.util.f_build_query_server_job_detail(@@servername, @job_name)
	IF @info = 'history'	SELECT @sql = WSDOT_STAGE.util.f_build_query_server_job_history(@@servername, @job_name, @result_limit)
	
	-- get table structure from result
	-- then use STUFF and FOR XML with PATH to build alter table statement with all columns except server_name
	SELECT column_ordinal, name, system_type_name INTO #dfr FROM sys.dm_exec_describe_first_result_set(@sql, NULL, 0)
	SET @alter += STUFF((SELECT nchar(10) + d.name + N' ' + d.system_type_name + N','
		FROM #dfr AS d
		WHERE d.name <> N'server_name'
		ORDER BY d.column_ordinal
		FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(4000)'), 1, 1, N'')
	-- finally, remove the last trailing comma and execute alter table statement
	SET @alter = LEFT(@alter, LEN(@alter) - 1)
	EXEC (@alter)
	
	-- set cursor to iterate over servers in ag
	SET @cur = CURSOR FOR SELECT replica_server_name FROM WSDOT_STAGE.util.v_ag_servers vas ORDER BY replica_server_name
	-- open cursor and fetch first result
	OPEN @cur
	FETCH NEXT FROM @cur INTO @server
	WHILE @@fetch_status = 0
	BEGIN
		-- 20230207: check if server name does not contain 'Z'; if so, continue
		IF charindex('Z', @server) = 0
		BEGIN
			-- select sql based on information type
			IF @info = 'detail'		SELECT @sql = WSDOT_STAGE.util.f_build_query_server_job_detail(@server, @job_name)
			IF @info = 'history'	SELECT @sql = WSDOT_STAGE.util.f_build_query_server_job_history(@server, @job_name, @result_limit)
			INSERT #ret
			EXEC (@sql)
		END
		-- fetch next server
		FETCH NEXT FROM @cur INTO @server
	END
	
	-- clean up other items
	DROP TABLE #dfr
	
	-- return table results
	SELECT * FROM #ret
END;

-- testing procedure runs
-- EXEC WSDOT_STAGE.util.usp_get_server_job_info @info = 'detail';
-- EXEC WSDOT_STAGE.util.usp_get_server_job_info @info = 'history', @result_limit = 10;
-- EXEC WSDOT_STAGE.util.usp_get_server_job_info @info = 'nonexist';

/********** HASH STRINGS CHARACTER COMPARISONS **********/
----- get number of different characters
-- this is the Hamming distance: https://en.wikipedia.org/wiki/Hamming_distance
-- see https://stackoverflow.com/a/18575942
-- see also https://docs.microsoft.com/en-us/sql/t-sql/statements/create-function-transact-sql?view=sql-server-ver15
-- DROP FUNCTION util.f_hamming_dist;
CREATE OR ALTER FUNCTION util.f_hamming_dist (@s1 varchar(max), @s2 varchar(max))
RETURNS smallint AS
BEGIN
	DECLARE @diff smallint;
	-- dynamically get recursion limit as max len of the two input strings
    DECLARE @r integer = (SELECT max(v) FROM (VALUES (len(@s1)), (len(@s2)) ) AS val(v));
	WITH cte(n) AS (
		SELECT 1
		UNION ALL
		SELECT n + 1 FROM cte WHERE n <= @r
	)
	SELECT  --t.s1, t.s2,
			@diff = sum(CASE WHEN substring(t.s1, c.n, 1) <> substring(t.s2, c.n, 1) THEN 1 ELSE 0 END)
	  FROM  (SELECT @s1 AS s1, @s2 AS s2) AS t
	  CROSS JOIN cte AS c
	GROUP BY t.s1, t.s2
	RETURN @diff
END;

-- testing
-- SELECT  WSDOT_STAGE.util.f_hamming_dist('B00000000204D2625138B4FE', 'B00000000204D2625138B4FE');	-- 0
-- SELECT  WSDOT_STAGE.util.f_hamming_dist('B00000000204D2625138B4FE', 'B00000000204D0625138B4FE');	-- 1
-- SELECT  WSDOT_STAGE.util.f_hamming_dist('B00000000204D2625138B4FE', 'B00000000204D06841A3066E');	-- 8

----- get position of first character difference
-- see https://dba.stackexchange.com/a/232832
-- DROP FUNCTION util.f_str_diff_position;
CREATE OR ALTER FUNCTION util.f_str_diff_position (@_str1 varchar(300), @_str2 varchar(300))
RETURNS int AS
BEGIN
	DECLARE @_value int = NULL;
	WITH cte AS
	(
		SELECT  1 AS num
				,@_str1 AS str1
				,@_str2 AS str2
		UNION ALL
		SELECT  num + 1
				,str1
				,str2
		  FROM  cte
		 WHERE  num < (SELECT CASE WHEN len(@_str1) > len(@_str2) THEN len(@_str1) ELSE len(@_str2) END)
	)
	SELECT  TOP 1 @_value = num
	  FROM  cte
	 WHERE  substring(str1, num, 1) <> substring(str2, num, 1)
	ORDER BY num ASC
	OPTION (MAXRECURSION 300);

	RETURN(@_value);
END;

-- testing
-- SELECT  WSDOT_STAGE.util.f_str_diff_position('B00000000204D2625138B4FE', 'B00000000204D2625138B4FE');	-- -1 (no diff)
-- SELECT  WSDOT_STAGE.util.f_str_diff_position('B00000000204D2625138B4FE', 'B00000000204D0625138B4FE');	-- 14
-- SELECT  WSDOT_STAGE.util.f_str_diff_position('B00000000204D2625138B4FE', 'B00000000204D06841A3066E');	-- 14

----- get count of true bits in binary value
-- NOTE: SQL Server 2022 has bit_count function (https://learn.microsoft.com/en-us/sql/t-sql/functions/bit-count-transact-sql?view=sql-server-ver16)
-- handle an arbitrary length binary value by using a recursive CTE to split the data into a 
-- table of 1-byte values and counting all of the bits that are true in each byte of that table
-- see https://stackoverflow.com/a/51756217
-- NOTE: a solution like https://stackoverflow.com/a/21311153 does not handle large binary values
-- DROP FUNCTION util.f_true_bit_count;
CREATE OR ALTER FUNCTION util.f_true_bit_count (@_data varbinary(max))
RETURNS int AS
BEGIN
	IF @_data IS NULL RETURN NULL;
	DECLARE @_cnt int = 0;
	WITH each ( byte, pos ) AS (
		SELECT substring(@_data, len(@_data), 1), len(@_data)-1 WHERE len(@_data) > 0
		UNION ALL
		SELECT substring(@_data, pos, 1), pos-1 FROM each WHERE pos > 0
	)
	SELECT  @_cnt = count(*)
	  FROM  each
	 CROSS  JOIN (VALUES (1),(2),(4),(8),(16),(32),(64),(128)) [bit](flag)
	 WHERE  each.byte & [bit].flag = [bit].flag
	OPTION (MAXRECURSION 0);
	
	RETURN @_cnt;
END;

----- convert varchar to varbinary
-- NOTE: use convert style = 2 for hex to varbinary conversion to avoid prefixing 0x to string
--       also, text must be even number of caracters, so pad with 0 if not even
--       see https://learn.microsoft.com/en-us/sql/t-sql/functions/cast-and-convert-transact-sql?redirectedfrom=MSDN&view=sql-server-ver15#binary-styles
-- DROP FUNCTION util.f_text_to_binary;
CREATE OR ALTER FUNCTION util.f_text_to_binary (@_str varchar(64))
RETURNS varbinary(max) AS
BEGIN
	IF len(@_str) % 2 = 1 SET @_str = concat('0', @_str);	-- prefix zero if length is not even
	RETURN convert(varbinary(max), @_str, 2);
END;

----- get difference between two small binary values
-- int range: 2,147,483,647 --> 7FFFFFFF
-- bigint range: 9,223,372,036,854,775,807 --> 7FFFFFFFFFFFFFFF
-- DROP FUNCTION util.f_binary_subtract;
CREATE OR ALTER FUNCTION util.f_binary_subtract (@_b1 varbinary(16), @_b2 varbinary(16))
RETURNS int AS
BEGIN
	RETURN abs(cast(@_b1 AS bigint) - cast(@_b2 AS bigint));
END;

-- TODO: query to get history on a single toll tag?

/********** ACCOUNT TYPE TRACKING **********/
-- create temporal table for account type tracking (use lower precision for datetime2)
-- DROP TABLE WSDOT_STAGE.util.account_type;
CREATE TABLE WSDOT_STAGE.util.account_type (
	customer_account_id int NOT NULL PRIMARY KEY CLUSTERED
	,account_type_id bigint NOT NULL
	,sys_start_time datetime2(2) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL
	,sys_end_time datetime2(2) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL
	,PERIOD FOR SYSTEM_TIME (sys_start_time, sys_end_time)
) WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = util.h_account_type));

-- add index on account_type_id in main and history tables
-- 2022-08-10: drop indices on history table as they may be corrupt: https://stackoverflow.com/q/51844442
CREATE NONCLUSTERED INDEX ix_account_type_id ON util.account_type (account_type_id);
-- CREATE NONCLUSTERED INDEX ix_h_account_type_id ON util.h_account_type (account_type_id);

-- view for pulling all history
-- TODO: add join with separate table that provides historic account types at ETAN switch-over as well as account conversions between July and Dec 2021
-- DROP VIEW util.v_ah_account_type;
CREATE OR ALTER VIEW util.v_ah_account_type AS
SELECT *, sys_start_time, sys_end_time FROM WSDOT_STAGE.util.account_type FOR SYSTEM_TIME ALL;

-- view for more detail
-- TODO: add more details as needed
-- DROP VIEW util.v_customer_account;
CREATE OR ALTER VIEW util.v_customer_account AS
SELECT  t.*
		,eat.shortCode AS acct_short_code
		,eat.description AS acct_type_desc
		,ca.customerId 
		,ca.legacyAccountNumber 
		,ca.postingDate 
		,ca.expiryDate 
		,ca.balanceTypeId 
		,t.sys_start_time
		,t.sys_end_time
  FROM  fastlane.util.CustomerAccount ca
  JOIN  WSDOT_STAGE.util.account_type t ON t.customer_account_id = ca.customerAccountId
  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = t.account_type_id;

-- view for latest account type update
-- DROP VIEW util.v_acct_type_updated_at;
CREATE OR ALTER VIEW util.v_acct_type_updated_at AS
SELECT max(sys_start_time) AS latest_update FROM WSDOT_STAGE.util.account_type act;

----- procedure to update account types
USE WSDOT_STAGE;

-- DROP PROCEDURE util.usp_update_account_types;
CREATE OR ALTER PROCEDURE util.usp_update_account_types AS
BEGIN
	-- variable to store latest update system time
	DECLARE @sys_time datetime2(2)
	
	DROP TABLE IF EXISTS #acct_types
	DROP TABLE IF EXISTS #changes

	-- 2022-01-10: remove hasLpOrTransponder
	CREATE TABLE #acct_types (
	    customerAccountId int NOT NULL PRIMARY KEY
    	,revenueTypeId int NOT NULL
	    ,customerTypeId int NOT NULL
    	,primaryPoC char NOT NULL
	    ,isPrepaid bit NOT NULL
    	,hasReplenishment bit NOT NULL
	    ,isExpiryGreaterThan90Days bit NOT NULL
    	--,hasLpOrTransponder bit NOT NULL
	    ,isSystemGenerated bit NOT NULL
    	,accountTypeId INT NOT NULL
	    ,shortcode varchar(64) NOT NULL
	)
	
	CREATE TABLE #changes (
		change_action varchar(20)
	)

	-- use procedure to get latest derived account status
	-- updated 2022-08-01: USP_AccountTypesAll now requires asOf datetime parameter, default to NULL for current time
	INSERT #acct_types
	EXEC fastlane.util.USP_AccountTypesAll @asOf = NULL;

	-- use merge to insert/update account types
	-- use output to store action taken for each inserted/updated/deleted record
	MERGE INTO WSDOT_STAGE.util.account_type AS t
	USING   (SELECT customerAccountId
					,accountTypeId
			   FROM #acct_types v
			) AS src (customerAccountId, accountTypeId)
		ON  t.customer_account_id = src.customerAccountId
	WHEN MATCHED AND t.account_type_id <> src.accountTypeId THEN
		-- matching acct id but account type changed - update account
		UPDATE SET t.account_type_id = src.accountTypeId
	WHEN NOT MATCHED BY TARGET THEN
		-- new acct - insert record
		INSERT (customer_account_id, account_type_id) VALUES (src.customerAccountId, src.accountTypeId)
	WHEN NOT MATCHED BY SOURCE AND t.customer_account_id IN (SELECT ca.customerAccountId FROM fastlane.util.CustomerAccount ca WHERE ca.isDefunct = 1) THEN
		-- existing account now marked defunct - delete from tracking table (but will remain in history)
		DELETE
	OUTPUT $action INTO #changes;

	-- get latest add time (from beginning of transaction)
	SET @sys_time = (SELECT max(sys_start_time) FROM WSDOT_STAGE.util.account_type)
	
	-- log result
	INSERT INTO WSDOT_STAGE.log.messages
	SELECT  @sys_time AS system_time
			,100 AS activity_id	-- account type tracking
			,'INFO' AS log_level
			,concat_ws(' ', s.change_action, s.rec_count, 'rows in table WSDOT_STAGE.util.account_type') AS message
			,'WSDOT_STAGE.util.account_type' AS table_name
			,s.change_action AS table_action
			,s.rec_count AS item_count
	  FROM  (SELECT c.change_action
					,count(*) AS rec_count
			  FROM  #changes c
			GROUP BY c.change_action
			) s

	DROP TABLE #acct_types
	DROP TABLE #changes

END;

-- further investigation (if needed)
SELECT sys_start_time, count(*) FROM WSDOT_STAGE.util.account_type GROUP BY sys_start_time ORDER BY sys_start_time;
SELECT sys_start_time, sys_end_time, count(*) FROM WSDOT_STAGE.util.v_ah_account_type GROUP BY sys_start_time, sys_end_time ORDER BY sys_start_time, sys_end_time;

-- current state of accounts (consider adding defunct accounts?)
SELECT  ca.account_type_id
        ,ca.acct_type_desc
        ,count(*) AS count
  FROM  WSDOT_STAGE.util.v_customer_account ca
-- WHERE  ca.postingDate > convert(date,'2021-07-10')
GROUP BY ca.account_type_id
        ,ca.acct_type_desc
ORDER BY ca.account_type_id;

-- defunct accounts
SELECT count(*) FROM fastlane.util.CustomerAccount ca WHERE isDefunct = 1;

-- TODO: create recent tracking table for accounts like WSDOT_STAGE.util.recent_trip_state

/********** TRIP STATE TRACKING **********/

-- create table for linking sys_start_time with cutoff date from processing
-- DROP TABLE WSDOT_STAGE.util.trip_state_updates;
CREATE TABLE WSDOT_STAGE.util.trip_state_updates (
    as_of_date_time datetime2(2) PRIMARY KEY
    ,sys_start_time datetime2(2) NOT NULL
    ,description varchar(64) NOT NULL
);

-- insert inital record into updates 
INSERT INTO WSDOT_STAGE.util.trip_state_updates VALUES
(convert(datetime2, '2000-01-01'),convert(datetime2, '2021-07-01'),'initial date for tracking');

-- create temporal table for toll ledger entry tracking (use lower precision for datetime2)
-- NOTE: use bigint for trip and transaction ids since they will overflow in about 7 years
-- DROP TABLE WSDOT_STAGE.util.trip_state;
CREATE TABLE WSDOT_STAGE.util.trip_state (
	trip_id bigint NOT NULL PRIMARY KEY CLUSTERED
	,last_trip_txn_id bigint NOT NULL
	,last_toll_txn_id bigint NOT NULL
	,sys_start_time datetime2(2) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL
	,sys_end_time datetime2(2) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL
	,PERIOD FOR SYSTEM_TIME (sys_start_time, sys_end_time)
) WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = util.h_trip_state));

-- add indices for main and history table
-- 2022-08-10: drop indices on history table as they may be corrupt: https://stackoverflow.com/q/51844442
CREATE NONCLUSTERED INDEX ix_last_trip_txn_id ON util.trip_state (last_trip_txn_id);
CREATE NONCLUSTERED INDEX ix_last_toll_txn_id ON util.trip_state (last_toll_txn_id);
-- CREATE NONCLUSTERED INDEX ix_h_last_trip_txn_id ON util.h_trip_state (last_trip_txn_id);
-- CREATE NONCLUSTERED INDEX ix_h_last_toll_txn_id ON util.h_trip_state (last_toll_txn_id);

-- view for pulling all history
-- DROP VIEW util.v_ah_trip_state;
CREATE OR ALTER VIEW util.v_ah_trip_state AS
SELECT *, sys_start_time, sys_end_time FROM WSDOT_STAGE.util.trip_state FOR SYSTEM_TIME ALL;

-- TODO: EDIT
-- if we set vts.balancetypeid=1 and vts.isDebit=1, does this avoid need for transaction priority?
-- they simply sort by max(postingdate) for each trip
SELECT * FROM fastlane.xact.EnuTransactionType ett WHERE ett.balancetypeid=1 AND ett.isDebit=1;
SELECT DISTINCT vts.transactionTypeId
		,ett.shortCode 
		,vts.isDebit AS vtsDebit
		,ett.isDebit AS ettDebit
		,vts.balanceTypeId AS vtsBalanceType
		,ett.balanceTypeId AS ettBalanceType
  FROM  fastlane.xact.V_TransactionSummary vts
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId;

-- create function to return distinct trip_id and latest TollLedgerEntry/V_TransactionSummary transcation ids in several categories:
-- latest overall trip transaction (represents current state of trip): by posting date and priority
-- latest toll type transaction (to get actual trip value without fees or discounts): by transactionCategoryId, posting date)
--   - use CASE to select only those with transactionCategoryId = 1, but keep in ORDER clause to get NULL when there aren't any
--     this hides category 2 fees [FPBP,FCPP,FCVP] and category 5 other (non-debit) [TOLLDISC,WRITEOFF] types
-- NOTE: do not use isDebit = 1 as ETAN does since this eliminates TOLLDISC and WRITEOFF txn types; may consider adding balanceTypeId = 1 condition
-- @end_dtm is end of extent to get updates
-- DROP FUNCTION util.f_get_trip_state;
CREATE OR ALTER FUNCTION util.f_get_trip_state (@end_dtm datetime2(2))
RETURNS TABLE AS
RETURN (
	SELECT  DISTINCT tle.tollId AS trip_id
			,first_value(vts.transactionId) OVER (PARTITION BY tle.tollId ORDER BY vts.postingDate DESC, ttp.priority DESC, vts.transactionId) AS last_trip_txn_id
			,first_value(CASE WHEN ett.transactionCategoryId = 1 THEN vts.transactionId ELSE NULL END) OVER (PARTITION BY tle.tollId ORDER BY ett.transactionCategoryId, vts.postingDate DESC) AS last_toll_txn_id
	  FROM  fastlane.xact.TollLedgerEntry tle
	  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
	  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
	  LEFT  JOIN  WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transaction_type_id = vts.transactionTypeId
	 WHERE  vts.postingDate >= (SELECT max(tsu.as_of_date_time) FROM WSDOT_STAGE.util.trip_state_updates tsu)
	   AND  vts.postingDate < @end_dtm
);

-- also look, some trips do not show in TLE, and others have several entries?
SELECT count(*) FROM fastlane.lance.trip t;				-- 260955747
SELECT count(DISTINCT tle.tollId) FROM fastlane.xact.TollLedgerEntry tle;	-- 355563137, distinct 260931013
SELECT count(*) FROM fastlane.xact.TollLedgerEntry tle JOIN fastlane.lance.trip t ON t.tripId = tle.tollId;	-- 355563137
SELECT count(*) FROM fastlane.lance.trip t LEFT JOIN fastlane.xact.TollLedgerEntry tle ON tle.tollId = t.tripId WHERE tle.tollId IS NULL;	-- 29105

----- procedure to insert records into trip_state
-- see issue with dBeaver: https://github.com/dbeaver/dbeaver/issues/1482
USE WSDOT_STAGE;

-- must highlight all to create procedure for some reason!
-- DROP PROCEDURE util.usp_update_trip_state;
CREATE OR ALTER PROCEDURE util.usp_update_trip_state (
	@end_dtm datetime2(2)
	,@desc varchar(64)
) AS
BEGIN
	-- get start date from previous run
	DECLARE @sys_time datetime2(2)
	
	DROP TABLE IF EXISTS #changes
	
	CREATE TABLE #changes (
		change_action varchar(20)
	)
	
	-- use merge to insert/update recent trip state
	MERGE INTO WSDOT_STAGE.util.trip_state AS t
	USING   (SELECT *
			   FROM WSDOT_STAGE.util.f_get_trip_state(@end_dtm)
			) AS src (trip_id, last_trip_txn_id, last_toll_txn_id)
		ON  t.trip_id = src.trip_id
	WHEN MATCHED THEN
		-- new activity on existing trip - update trip
		-- use coalesce on toll_txn_id in case there is not a new toll-type txn in the period
		UPDATE SET t.last_trip_txn_id = src.last_trip_txn_id
				  ,t.last_toll_txn_id = coalesce(src.last_toll_txn_id, t.last_toll_txn_id)
	WHEN NOT MATCHED BY TARGET THEN
		-- new trip - insert record
		INSERT (trip_id, last_trip_txn_id, last_toll_txn_id) VALUES (src.trip_id, src.last_trip_txn_id, src.last_toll_txn_id)
	-- do nothing when not matched by source since we expect source to have less than full history
	OUTPUT $action INTO #changes;

	-- get transaction start time that temporal tables use for inserting into trip_state_updates (https://dba.stackexchange.com/a/243889)
	-- don't have permissions to run below query, so select max(sys_start_time) instead
	-- SELECT dtat.transaction_begin_time FROM sys.dm_tran_active_transactions dtat WHERE dtat.transaction_id = CURRENT_TRANSACTION_ID();
	SET @sys_time = (SELECT max(sys_start_time) FROM WSDOT_STAGE.util.trip_state)
	
	-- insert new record into trip state updates
	INSERT INTO WSDOT_STAGE.util.trip_state_updates VALUES (@end_dtm, @sys_time, @desc)
	
	-- log results
	INSERT INTO WSDOT_STAGE.log.messages
	SELECT  @sys_time AS system_time
			,101 AS activity_id	-- trip state tracking
			,'INFO' AS log_level
			,concat_ws(' ', s.change_action, s.rec_count, 'rows in table WSDOT_STAGE.util.trip_state') AS message
			,'WSDOT_STAGE.util.trip_state' AS table_name
			,s.change_action AS table_action
			,s.rec_count AS item_count
	  FROM  (SELECT c.change_action
					,count(*) AS rec_count
			  FROM  #changes c
			GROUP BY c.change_action
			) s

	-- truncate recent trip state table since this update supercedes it
	TRUNCATE TABLE WSDOT_STAGE.util.recent_trip_state

	-- drop temp tables
	DROP TABLE #changes

END;

-- run past updates for 2021 and before
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-07-10', @desc = 'trips ETAN imported from ETCC';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-08-10', @desc = '2021-07 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-09-10', @desc = '2021-08 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-10-10', @desc = '2021-09 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-11-10', @desc = '2021-10 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2021-12-10', @desc = '2021-11 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2022-01-10', @desc = '2021-12 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2022-02-10', @desc = '2022-01 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2022-03-10', @desc = '2022-02 trip updates';
EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = '2022-04-11', @desc = '2022-03 trip updates';
-- starting 2022-05 run automatically!

-- further investigation (if needed)
SELECT * FROM WSDOT_STAGE.log.v_activity_messages m WHERE m.system_time > cast(getdate() AS date) ORDER BY m.system_time DESC;
SELECT sys_start_time, count(*) FROM WSDOT_STAGE.util.trip_state GROUP BY sys_start_time ORDER BY sys_start_time;
SELECT sys_start_time, sys_end_time, count(*) FROM WSDOT_STAGE.util.v_ah_trip_state GROUP BY sys_start_time, sys_end_time ORDER BY sys_start_time, sys_end_time;
SELECT count(*) FROM WSDOT_STAGE.util.trip_state ts;
SELECT * FROM WSDOT_STAGE.util.trip_state_updates tsu;

/********** RECENT TRIP STATE TRACKING **********/
-- create recent trip state; similar to monthly trip state but without history/temporal fields
-- NOTE: last toll txn in recent table CAN be null since it may not be in recent data
-- DROP TABLE WSDOT_STAGE.util.recent_trip_state;
CREATE TABLE WSDOT_STAGE.util.recent_trip_state (
	trip_id bigint NOT NULL PRIMARY KEY
	,last_trip_txn_id bigint NOT NULL
	,last_toll_txn_id bigint
);

-- add indices for recent trip state txns
CREATE NONCLUSTERED INDEX ix_recent_last_trip_txn_id ON util.recent_trip_state (last_trip_txn_id);
CREATE NONCLUSTERED INDEX ix_recent_last_toll_txn_id ON util.recent_trip_state (last_toll_txn_id);

-- procedure to update recent trip state (since last monthly run)
-- end_dtm will usually be current date at midnight, e.g. cast(getdate() AS date)
-- DROP PROCEDURE util.usp_update_recent_trip_state;
CREATE OR ALTER PROCEDURE util.usp_update_recent_trip_state (
	@end_dtm datetime2(2)
) AS
BEGIN
	-- truncate existing table; then insert new records since last monthly run
	TRUNCATE TABLE WSDOT_STAGE.util.recent_trip_state;
	INSERT INTO WSDOT_STAGE.util.recent_trip_state
	SELECT * FROM WSDOT_STAGE.util.f_get_trip_state(@end_dtm);
END;

-- view of existing monthly trip state with recent changes
-- full outer join since most trips will be in monthly trip sstate, new trips in recent, and only updated trips in both 
-- select recent first to get any updates (as others will be null except on update)
-- DROP VIEW util.v_all_trip_state;
CREATE OR ALTER VIEW util.v_all_trip_state AS
SELECT  coalesce(r.trip_id, ts.trip_id) AS trip_id
        ,coalesce(r.last_trip_txn_id, ts.last_trip_txn_id) AS last_trip_txn_id
        ,coalesce(r.last_toll_txn_id, ts.last_toll_txn_id) AS last_toll_txn_id
  FROM  WSDOT_STAGE.util.trip_state ts
  FULL OUTER JOIN WSDOT_STAGE.util.recent_trip_state r ON r.trip_id = ts.trip_id;

/********** TRIP DETAIL **********/
---------- base trip detail used for most queries
-- NOTE: previously directed by ETAN to use annotation in ledger to link with reason code
--       however, this misses T-codes and other processing that is written to endAnnotationId in the TollLedgerEntry
--       thus, use TollLedgerEntry annotation unless it is NULL, then use ledger annotation
-- 2023-01-30: renamed postingDate to etan_received_date, added etan_received_month, update subsequent views
-- DROP VIEW util.v_trip_detail_base;
CREATE OR ALTER VIEW util.v_trip_detail_base AS
SELECT  ts.trip_id
		,ts.last_trip_txn_id 
		,ts.last_toll_txn_id 
		,t.tripPassageId 
		,t.created AS etan_received_time
		,WSDOT_STAGE.util.f_get_month(t.created) AS etan_received_month
		,tp.roadGantryId 
		,tp.laneTime 
		,WSDOT_STAGE.util.f_get_month(tp.laneTime) AS trip_month
		,tp.roadsideTollType AS roadside_toll_type
		,tp.axleCount AS axle_count
		,vfl.wsdot_facility AS facility
		,vfl.wsdot_roadway AS roadway
		,vfl.explazaCode AS exit_plaza
		,coalesce(tle.endAnnotationId, ld.annotationid) AS annotation_id
		,ett.shortCode AS transaction_type
		,eet.shortCode AS enforcement_type
		,epp.shortCode AS pricing_plan
		,vts.transactionId AS vts_txn_id
		,vts.prefix 
		,vts.basecode 
		,vts.suffix
		,vts.customerAccountid
		,vts.amount AS latest_amount
		,vts.isCancelled
		,vts.isFullyAllocated
		,vts.isUnpaid
  FROM  WSDOT_STAGE.util.v_all_trip_state ts
--  FROM  WSDOT_STAGE.util.trip_state ts
  JOIN  fastlane.lance.trip t ON t.tripId = ts.trip_id 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_trip_txn_id 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ts.last_trip_txn_id AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.ledger ld ON ld.transactionId = ts.last_trip_txn_id	-- get other annotationId if TLE endAnnotationId is NULL
  JOIN  fastlane.xact.EnuEnforcementType eet ON eet.enforcementTypeId = tle.enforcementTypeId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId;

---------- full trip detail for disposition
-- view with hierarchical disposition classification, used for later disposition reporting; may add NOCP info later or create separate table
/* general disposition category questions: [resolution in brackets]
what are duplicates? [system account 16 or 120, as before]
 - sys account 16 and 120 are supposedly duplicates, but many have a reason code which is currently leak 1 [changed reason code mappings to adjustment from leak]
 - others have no reason code - are these the duplicates we should count? [no, count all duplicates now that leak was changed to adjust]
where do type 98's get counted? are these duplicates? [no, not dupes, but add type 98 to disposition summary]
why do type 99's not have a reason code? [since there is no fare, doesn't hit GL, hence no reason code]
why do trips with transaction type WRITEOFF not have a reason code? [most were prior to transition so no GL entry, but some are after - research]
why do trips with system account 252 not have a reason code? [mapping during transtion (disposition code F from ETCC); later process will likely add reason code]
why do some type 90's have pricing plan PBM? should they still be HOV trips? [these are likely misidentified as type 90, count as PBM]
why do some type 90's have transaction type TOLL? should they still be HOV trips? [these all appear to be duplicates]
why are some type 95's assigned to sys account 12 but not others? [sys account 12 is for valid motorcycle tags not assigned to an account]
why are some type 92's TOLL with no charge or a charge and others are TOLLDISC? [count type 92's after NRV/TOLLDISC processing]
are unpaid PAS trips normal (~1.5%)? [yes, no money in account, unable to reload]
are unpaid PBP trips normal (~2.0%)? [yes, no money in account, unable to reload]
why do some pricing plan PAS trips have transaction type ETB instead of TOLL? [probably mis-assigned to image review, count as unpaid tag]
how does a PBM have a transaction type of TOLL? [this might be normal, perhaps flow is TPENDING > ETB > TOLL > INVTOLL1 > INVTOLL2 > TCPP > FCPP]
what is the significance of fully allocated vs unpaid when pending in sys account 121? [ask ETAN]
how are there fully allocated pending trips that are not in a system account? [these are likely pending DOL lookup]

Hierarchy:
 - duplicates (system account 16 or 120)
 - leakage by reason code (not adjustment)
 - type 98 trips (adjustment, not leakage)
 - type 99 trips (leakage)
 - transaction type writeoff (leakage)
 - sys account 252 (leakage, later might have reason code)
 - HOV (pricing plan HOV, should never have a reason code) (some type 90s are pricing plan PBM, so looking at pricing plan avoids these)
 - MOT (pricing plan MOT, should never have a reason code) (all type 95s are MOT but some MOT are type 2, so use pricing plan)
 - NRV (pricing plan NRV, should never have a reason code)
 - custom tolldisc (not in HOV, MOT, NRV) - these are often custom passes
 - type 92 trips (SR167 unidentifiable tag)
 - paid tag (pricing plan PAS, fully allocated)
 - paid pbp (pricing plan PBP, fully allocated)
 - paid pbm (txn type ETB, TOLL, or INVTOLL1/2, pricing plan PBM, fully allocated)
 - paid nocp (txn type TCPP, FCPP, pricing plan PBM, fully allocated)
 - unpaid tag (pricing plan PAS, unpaid) (includes some ETB)
 - unpaid pbp (pricing plan PBP, unpaid)
 - unpaid pbm (txn type ETB, TOLL, or INVTOLL1/2, pricing plan PBM, unpaid)
 - unpaid nocp (txn type TCPP, FCPP, pricing plan PBM, unpaid)
 - pending image review (pricing plan PEN, system account 121)
 - pending DOL (pricing plan PEN, not sys account but does have customer account)
 - unknown - catch-all for unmapped trips
*/
-- NOTE: SYS_252 dropped 2022-07-06 since all trips were re-processed
-- 2023-01-30: added latest_txn_posting_date
-- DROP VIEW util.v_trip_detail_full;
CREATE OR ALTER VIEW util.v_trip_detail_full AS
SELECT  tdb.*
		,ta.amount AS roadside_amount
		,ta.roadsideFareCategoryId
        ,tl.licensePlateId
        ,tpt.transponderId 
        ,tpt.transponderStatusId 
        ,tpt.occupancyCodeId 
        ,CASE WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NULL THEN 'no_lp_or_pass'
        	  WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NOT NULL THEN 'pass_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NULL THEN 'lp_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NOT NULL THEN 'both_lp_and_pass'
        	END AS identifiers
        ,enca.customerAccountId AS system_account_id
        ,eer.shortCode AS entry_reason
        ,vlr.leakage_cat_id
        ,vlr.leakage_code
        ,vts2.amount AS latest_toll_amount
        ,vts2.postingDate AS latest_txn_posting_date
        ,CASE WHEN enca.customerAccountId IN (16, 120) THEN 'DUPLICATE_TRIP'				-- duplicates (both LP and tag)
        	  WHEN vlr.leakage_cat_id > 0 THEN vlr.leakage_code								-- leakage by reason code (not adjustment)
              WHEN tdb.roadside_toll_type = 98 THEN 'TYPE_98'								-- adjustment (type 98 op overrides, after accounting for dupes)
              WHEN tdb.roadside_toll_type = 99 THEN 'TYPE_99'								-- leakage with no reason code (type 99)
              WHEN tdb.transaction_type = 'WRITEOFF' THEN 'WRITEOFF'						-- leakage with no reason code (transaction type writeoff)
              --WHEN enca.customerAccountId = 252 THEN 'SYS_252'								-- leakage with no reason code (system account 252, may get reason later)
              WHEN tdb.pricing_plan = 'HOV' THEN 'HOV'										-- HOV (don't assume all transaction type 90s are actually HOV)
              WHEN tdb.pricing_plan = 'MOT' THEN 'MOTORCYCLE'								-- motorcycles
              WHEN tdb.pricing_plan = 'NRV' THEN 'NONREV'									-- non-revenue vehicles
              WHEN tdb.transaction_type = 'TOLLDISC' THEN 'CUSTOM_TOLLDISC'					-- other tolldisc (not in HOV, MOT, NRV)
              WHEN tdb.roadside_toll_type = 92 THEN 'TYPE_92' 								-- adjustment (type 92, but after nonrev)
              WHEN tdb.pricing_plan = 'PAS' AND tdb.isFullyAllocated = 1 THEN 'PAID_TAG'	-- paid tag trip
              WHEN tdb.pricing_plan = 'PBP' AND tdb.isFullyAllocated = 1 THEN 'PAID_PBP'	-- paid pbp trip
              WHEN tdb.pricing_plan = 'PBM' AND tdb.transaction_type NOT IN ('TCPP', 'FCPP') AND tdb.isFullyAllocated = 1 THEN 'PAID_PBM'	-- paid pbm trip
              WHEN tdb.pricing_plan = 'PBM' AND tdb.transaction_type IN ('TCPP', 'FCPP') AND tdb.isFullyAllocated = 1 THEN 'PAID_NOCP'		-- paid nocp trip
              WHEN tdb.pricing_plan = 'PAS' AND tdb.isUnpaid = 1 THEN 'UNPAID_TAG'															-- unpaid tag trip
              WHEN tdb.pricing_plan = 'PBP' AND tdb.isUnpaid = 1 THEN 'UNPAID_PBP'															-- unpaid pbp trip
              WHEN tdb.pricing_plan = 'PBM' AND tdb.transaction_type NOT IN ('TCPP', 'FCPP') AND tdb.isUnpaid = 1 THEN 'UNPAID_PBM'			-- unpaid pbm trip
              WHEN tdb.pricing_plan = 'PBM' AND tdb.transaction_type IN ('TCPP', 'FCPP') AND tdb.isUnpaid = 1 THEN 'UNPAID_NOCP'			-- unpaid nocp trip
              WHEN tdb.pricing_plan = 'PEN' AND enca.customerAccountId = 121 THEN 'PENDING_IR'												-- pending image review
              WHEN tdb.pricing_plan = 'PEN' AND enca.customerAccountId IS NULL THEN 'PENDING_DOL'											-- pending DOL lookup?
              ELSE 'UNKNOWN'																-- unknown category
            END AS disposition_category
  FROM  WSDOT_STAGE.util.v_trip_detail_base tdb
  JOIN  fastlane.lance.TripAmount ta ON ta.tripId = tdb.trip_id
  JOIN  fastlane.xact.TollLedgerEntry tle2 ON tle2.ledgertransactionId = tdb.last_toll_txn_id 
  JOIN  fastlane.xact.V_TransactionSummary vts2 ON vts2.transactionId = tdb.last_toll_txn_id AND vts2.customerAccountid = tle2.ledgeraccountId
  JOIN  fastlane.xact.Annotation ann ON ann.annotationid = tdb.annotation_id
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
  LEFT  JOIN  fastlane.xact.EnuNonCustomerAccounts enca ON enca.customerAccountId = tdb.customerAccountid
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_leakage_reasons vlr ON vlr.entry_reason_id = ann.entryReasonId
  LEFT  JOIN  fastlane.lance.TripLp tl ON tl.tripId = tdb.trip_id
  LEFT  JOIN  fastlane.lance.TripPassageTransponder tpt ON tpt.tripPassageId = tdb.tripPassageId;

---------- trip detail for image review
-- NOTE: TSQL does not have boolean; use bit instead (really? c'mon microsoft!)
-- 2023-01-30: removed reason description for now since they seem to have deleted the column, but relationship still shows on EnuReason
-- DROP VIEW util.v_trip_detail_mir;
CREATE OR ALTER VIEW util.v_trip_detail_mir AS
SELECT  tdb.*
		,reqres.insightId 
		,cast(isnull(reqres.insightId, 0) AS bit) AS requested
		,cast(isnull(reqresps.insightId, 0) AS bit) AS qfree_acked
		,cast(isnull(res.insightId, 0) AS bit) AS result_received
		,erc.shortCode AS request_result_code
		--,er.description AS request_result_reason 
		,ert.shortCode AS result_type
		,ed.shortCode AS disposition_type
		,irr.lpNumber 
		,irr.jurisdiction 
		,irr.plateType 
  FROM  WSDOT_STAGE.util.v_trip_detail_base tdb 
  JOIN  fastlane.qfree.ImageReviewRequest req ON req.tripPassageId = tdb.tripPassageId 
  LEFT  JOIN fastlane.qfree.ImageReviewRequestResponse reqres ON reqres.imageReviewRequestId = req.imageReviewRequestId 
  LEFT  JOIN fastlane.qfree.ImageReviewRequestResponses reqresps ON reqresps.imageReviewRequestId = req.imageReviewRequestId 
  LEFT  JOIN fastlane.qfree.ImageReviewResultResponses res ON res.imageReviewRequestId = req.imageReviewRequestId 
  LEFT  JOIN fastlane.qfree.ImageReviewResult irr ON irr.imageReviewRequestId = req.imageReviewRequestId 
  LEFT  JOIN fastlane.qfree.EnuResultCode erc ON erc.resultCodeId = res.resultCodeId 
  --LEFT  JOIN fastlane.qfree.EnuReason er ON er.reasonId = res.reasonId 		-- reasonId appears to be deleted from fastlane.qfree.ImageReviewResultResponses
  LEFT  JOIN fastlane.qfree.EnuResultType ert ON ert.resultTypeId = irr.resultTypeId 
  LEFT  JOIN fastlane.qfree.EnuDisposition ed ON ed.dispositionId = irr.dispositionId;

/********** PASS FULFILLMENT DETAIL **********/
-- detail view of pass requests
-- DROP VIEW util.v_pass_requests;
CREATE OR ALTER VIEW util.v_pass_requests AS
SELECT  trl.customerAccountId 
		,hitr.req_dtm AS request_datetime
		,ins.shortCode AS req_source
		,etrt.description AS request_type
		,tdt.description AS pass_type
		,eitrs.description AS request_status
		,CASE WHEN itr.invTagRequestStatusId = 3 THEN 1 ELSE 0 END AS fulfilled
		--,count(*) AS pass_count
  FROM  fastlane.inv.InvTagRequest itr
  JOIN  fastlane.inv.EnuTagRequestType etrt ON etrt.tagRequestTypeId = itr.tagRequestTypeId
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = itr.tagDeviceTypeId
  JOIN  fastlane.inv.EnuInvTagRequestStatus eitrs ON eitrs.invTagRequestStatusId = itr.invTagRequestStatusId 
  JOIN  fastlane.xact.TagRequestLedger trl ON trl.invTagRequestId = itr.invTagRequestId
--  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = trl.customerAccountId
  JOIN  (SELECT DISTINCT invTagRequestId
  				,first_value(updatedAt) OVER (PARTITION BY invTagRequestId ORDER BY updatedAt) AS req_dtm		-- get original request date
  				,first_value(inputSourceId) OVER (PARTITION BY invTagRequestId ORDER BY updatedAt) AS src_id	-- get original request source
		   FROM fastlane.inv.InvTagRequest FOR system_time ALL
		  WHERE invTagRequestStatusId = 1
		) hitr ON hitr.invTagRequestId = itr.invTagRequestId
  JOIN  fastlane.info.InputSource ins ON ins.inputSourceId = hitr.src_id
 WHERE  itr.isDefunct = 0		-- remove invalid/cancelled requests
   AND  hitr.req_dtm >= convert(date, '2021-07-10');

-- detail of unfulfilled requests more than five days old
-- DROP VIEW util.v_old_unfulfilled_pass_requests;
CREATE OR ALTER VIEW util.v_old_unfulfilled_pass_requests AS
SELECT * FROM WSDOT_STAGE.util.v_pass_requests pr WHERE pr.fulfilled = 0 AND pr.request_datetime < dateadd(day, -5, cast(getdate() AS date));

/********** LATEST TRIP BY ACCOUNT **********/
-- get latest trip for each account
-- NOTE: consider an indexed view instead (similar to psql materialized view); see https://stackoverflow.com/a/3986540
-- DROP VIEW util.v_acct_latest_trip;
CREATE OR ALTER VIEW util.v_acct_latest_trip AS
SELECT  tle.ledgeraccountId AS customer_account_id 
		,max(tp.laneTime) AS latest_trip
  FROM  WSDOT_STAGE.util.v_all_trip_state ts 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_trip_txn_id 
  JOIN  fastlane.lance.trip t ON t.tripId = ts.trip_id 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
GROUP BY tle.ledgeraccountId;

/********** ACCOUNT CONTACT INFO **********/
-- view of concatenated phone numbers, removing pure duplicates with DISTINCT statement
-- DROP VIEW util.v_phone_contact_list;
CREATE OR ALTER VIEW util.v_phone_contact_list AS
SELECT  d.customerAccountId 
		,d.phone_type
		,string_agg(d.phone_num, ';') WITHIN GROUP (ORDER BY d.phone_num) AS phone_numbers
  FROM  (SELECT DISTINCT icac.customerAccountId 
				,ept.description AS phone_type
				,pd.[number] AS phone_num
		   FROM fastlane.util.IV_CustomerAccountContacts icac
		   LEFT JOIN fastlane.info.PhoneDevice pd ON pd.personOrCompanyId = icac.personOrCompanyId AND pd.isDefunct = 0
		   LEFT JOIN fastlane.info.EnuPhoneType ept ON ept.phoneTypeId = pd.phoneTypeId
		) AS d
GROUP BY d.customerAccountId 
		,d.phone_type;

-- view of phone number list with 'pivot' to convert additional rows (e.g. Home, Mobile) to columns
-- DROP VIEW util.v_phone_contact_pivot;
CREATE OR ALTER VIEW util.v_phone_contact_pivot AS
SELECT  customerAccountId
		,[Home], [Work], [Mobile], [Other]
  FROM  WSDOT_STAGE.util.v_phone_contact_list
 PIVOT  (
		max(phone_numbers)
		FOR phone_type IN ([Home], [Work], [Mobile], [Other])
) pvt;

-- view of standard account contact information
-- contact types: 
--   1-main (only one per account, 6604714 as of 2022-09-20)
--   2-secondary (can have many, highest is 36 on account 453293 as of 2022-09-20; 1427567 accounts have a secondary contact as of 2022-09-20)
--   3-companyname (currently unused as of 2022-09-20)
--   4-companycontact (very few of these; 5 as of 2022-09-20)
-- 2022-08-15: add isDefunct checks and preserve unique identifiers for email and postal address
-- DROP VIEW util.v_acct_contact_info;
CREATE OR ALTER VIEW util.v_acct_contact_info AS
SELECT  icac.customerAccountId
		,icac.personOrCompanyId 
		,icac.isCompany
		,icac.salutation 
		,icac.firstName
		,icac.middleName 
		,icac.lastNameOrCompanyName
		,icac.suffix 
		,icac.designation 
		,icac.displayname
		,pcp.Home AS home_phone
		,pcp.Mobile AS mobile_phone
		,ea.emailAddress AS email_address
		,ea.emailAddressId AS email_address_id
		,ea.updatedAt AS email_updated_at
		,pa.address1
		,pa.address2
		,pa.city
		,es.abbr AS state
		,pa.postalCode
		,ec.description AS country
		,pa.otherState
		,pa.coordinates 
		,ec.countryId AS country_id
		,eas.shortCode AS address_source
		,pa.postalAddressId AS postal_address_id
		,pa.updatedAt AS postal_address_updated_at
  FROM  fastlane.util.IV_CustomerAccountContacts icac
  LEFT  JOIN fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId AND pa.isDefunct = 0
  LEFT  JOIN fastlane.info.EnuAddressSource eas ON eas.addressSourceId = pa.addressSourceId 
  LEFT  JOIN fastlane.locale.EnuState es ON es.stateId = pa.stateId 
  LEFT  JOIN fastlane.locale.EnuCountry ec ON ec.countryId = es.countryId
  LEFT  JOIN fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId AND ea.isDefunct = 0
  LEFT  JOIN WSDOT_STAGE.util.v_phone_contact_pivot pcp ON pcp.customerAccountId = icac.customerAccountId
 WHERE  icac.contacttype = 'MAIN';		-- only main contacts

-- verify no duplicates by account: verified
SELECT aci.customerAccountId, count(*) FROM WSDOT_STAGE.util.v_acct_contact_info aci GROUP BY aci.customerAccountId HAVING count(*) > 1;

-- secondary account email contact information based on latest updated_at and greatest personOrCompanyId
-- NOTE: isDefunct also removes emailAddressId 0, which is unknown@etantolling.com
-- DROP VIEW util.v_acct_contact_secondary_email;
CREATE OR ALTER VIEW util.v_acct_contact_secondary_email AS
SELECT  DISTINCT icac.customerAccountId
		,first_value(icac.personOrCompanyId) OVER (PARTITION BY icac.customerAccountId ORDER BY icac.updatedAt DESC, icac.personOrCompanyId DESC) AS secondary_personOrCompanyId
		,first_value(icac.emailAddressId) OVER (PARTITION BY icac.customerAccountId ORDER BY icac.updatedAt DESC, icac.personOrCompanyId DESC) AS secondary_email_address_id
		,first_value(ea.emailAddress) OVER (PARTITION BY icac.customerAccountId ORDER BY icac.updatedAt DESC, icac.personOrCompanyId DESC) AS secondary_email_address
		,first_value(ea.updatedAt) OVER (PARTITION BY icac.customerAccountId ORDER BY icac.updatedAt DESC, icac.personOrCompanyId DESC) AS secondary_email_updated_at
  FROM  fastlane.util.IV_CustomerAccountContacts icac 
  JOIN  fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId AND ea.isDefunct = 0
 WHERE  icac.contacttype = 'SECONDARY';

/********** LAST LOGIN TO CWP **********/
-- DROP VIEW util.v_account_last_cwp_login;
CREATE OR ALTER VIEW util.v_account_last_cwp_login AS 
SELECT  ca.customerAccountId 
		,cca.updatedAt AS last_cwp_login_date
  FROM  fastlane.info.CustomerCWPLoginAttempts cca 
  JOIN  fastlane.info.Customer c ON c.CustomerCredentialsId = cca.customerCredentialsId 
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId;

/********** GEOCODED ADDRESSES **********/
-- since multiple accounts may map to same address, use two tables:
-- one to store account info and one to store unique address results

-- temporal table of account numbers with address id, updated time, and binary hash
-- DROP TABLE WSDOT_STAGE.util.account_postal_address_hash;
CREATE TABLE WSDOT_STAGE.util.account_postal_address_hash (
	customer_account_id int NOT NULL PRIMARY KEY CLUSTERED
	,postal_address_id int NOT NULL
	,postal_address_updated_at datetime2 NOT NULL
	,address_hash varbinary(64) NOT NULL
	,sys_start_time datetime2(2) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL
	,sys_end_time datetime2(2) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL
	,PERIOD FOR SYSTEM_TIME (sys_start_time, sys_end_time)
) WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = util.h_account_postal_address_hash));

-- other table is in WSDOT_STAGE.wsdot schema since it is WSDOT-processed data

-- view for pulling all history
-- DROP VIEW util.v_ah_account_postal_address_hash;
CREATE OR ALTER VIEW util.v_ah_account_postal_address_hash AS
SELECT *, sys_start_time, sys_end_time FROM WSDOT_STAGE.util.account_postal_address_hash FOR SYSTEM_TIME ALL;

/********** CASE QUEUE LIST AND SUMMARY **********/
-- replicate counts on case queues bos page: https://csc.wsprod.etantolling/en/bos/case/queue
-- see notes in adhoc for 2022-07-20 identify case queue
-- summary: use min case queue for case queue < 1015; 1015 is closed with workflowId 25; 1019-1021 use CaseQueueTypeCaseSource
-- also, defunct cases appear to be counted
-- DROP VIEW util.v_case_queue_list;
CREATE OR ALTER VIEW util.v_case_queue_list AS
SELECT  uc.ucaseId AS case_id
		,ws.isClosing AS is_closing
		,CASE WHEN cqct.caseQueueId < 1015 THEN cqct.caseQueueId 	-- 2022-08-08 do not require ws.isClosing = 0
			  WHEN cqct.caseQueueId = 1015 AND ws.isClosing = 1 AND uc.workflowStateId = 25 THEN cqct.caseQueueId
			  ELSE NULL END AS case_queue_id
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId
 WHERE  cqct.caseQueueId <= 1015
--   AND  uc.isDefunct = 0	-- case not defunct (comment out; they appear to count open but defunct cases on that page)
UNION ALL	-- queues 1019-1021 (subquery to remove NULLs for those cases that do not meet criteria)
SELECT  s.case_id
		,s.is_closing
		,s.case_queue_id
  FROM  (SELECT  uc.ucaseId AS case_id 
  				,ws.isClosing AS is_closing
				,CASE WHEN cqct.caseQueueId = 1019 AND uc.caseEscalationId > 1 THEN 1019
					  WHEN cqct.caseQueueId = 1020 AND uc.workflowStateId = 1 THEN 1020
				      WHEN cqct.caseQueueId = 1021 AND uc.workflowStateId = 0 AND uc.caseSourceId = 2 THEN 1021
				      ELSE NULL END AS case_queue_id
		  FROM  fastlane.usercase.[UCase] uc
		  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
		  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
		  JOIN  fastlane.usercase.CaseQueueTypeCaseSource cqtcs ON cqtcs.caseQueueId = cqct.caseQueueId AND cqtcs.caseSourceId = uc.caseSourceId 
		 WHERE  ws.isClosing = 0	-- open cases only
		) s 
 WHERE  s.case_queue_id IS NOT NULL;

-- user case view
-- DROP VIEW util.v_user_case_queue;
CREATE OR ALTER VIEW util.v_user_case_queue AS
SELECT  cql.case_id
		,string_agg(cql.case_queue_id, ';') WITHIN GROUP (ORDER BY cql.case_queue_id) AS case_queue_ids
  FROM  WSDOT_STAGE.util.v_case_queue_list cql
GROUP BY cql.case_id;

-- summary view
-- DROP VIEW util.v_case_queue_summary;
CREATE OR ALTER VIEW util.v_case_queue_summary AS
SELECT  cq.caseQueueId AS case_queue_id
		,cq.name AS queue_name
		,count(cql.case_id) AS case_count
  FROM  fastlane.usercase.CaseQueue cq 
  LEFT  JOIN  WSDOT_STAGE.util.v_case_queue_list cql ON cql.case_queue_id = cq.caseQueueId AND (cql.is_closing = 0 OR cql.case_queue_id = 1015)	-- case open or in queue 1015
 WHERE  cq.isDefunct = 0	-- no defunct queues
GROUP BY cq.caseQueueId
		,cq.name;

-- view results
SELECT * FROM WSDOT_STAGE.util.v_case_queue_summary cqs ORDER BY cqs.case_queue_id;

/********** CSR CASE WORK DETAIL **********/
-- TODO: consider adding web messages and staff notes later, need to group by other fields
-- 2022-08-08: add case queue ids column from util.v_user_case_queue view above
-- DROP VIEW util.v_csr_case_work_detail;
CREATE OR ALTER VIEW util.v_csr_case_work_detail AS
SELECT  uc.ucaseId AS case_id
		,coalesce(ect3.description, ect2.description, ect.description) AS case_type
		,ect.caseTypeId AS case_type_id
		,ect.description AS case_category
		,ucq.case_queue_ids 
		,ws.workflowStateId AS wf_state_id
		,ws.description AS case_status
		,ws.isClosing AS ws_is_closing
		,uc.created AS case_open_date
		,uc.updatedAt AS case_last_update
		,uc.inputUserId AS input_user_id
		,concat(iu.firstName, ' ', iu.lastName) AS modified_by
		--,wm.msgBody AS web_message
		--,hist.created_by
		,a.customerAccountIds
--SELECT count(*)
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = uc.caseTypeId 
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId 
  JOIN  fastlane.info.InputUser iu ON iu.inputUserId = uc.inputUserId 
  LEFT  JOIN WSDOT_STAGE.util.v_user_case_queue ucq ON ucq.case_id = uc.ucaseId		-- added 2022-08-08
  LEFT  JOIN fastlane.usercase.EnuCaseType ect2 ON ect2.caseTypeId = ect.parentCaseTypeId 
  LEFT  JOIN fastlane.usercase.EnuCaseType ect3 ON ect3.caseTypeId = ect2.parentCaseTypeId 
--  LEFT  JOIN  fastlane.usercase.WebMessage wm ON wm.ucaseId = uc.ucaseId 
--  LEFT  JOIN  fastlane.usercase.CaseNote cn ON cn.ucaseId = uc.ucaseId
--  LEFT  JOIN  fastlane.usercase.Note n ON cn.noteId = n.noteId
  LEFT  JOIN  (SELECT  cca.ucaseId
				,count(*) AS customerAccount_count
				,string_agg(cca.customerAccountId, ';') WITHIN GROUP (ORDER BY cca.customerAccountId) AS customerAccountIds
		  FROM  fastlane.usercase.CaseCustomerAccount cca
		GROUP BY ucaseId
		) a ON a.ucaseId = uc.ucaseId
  /*CROSS APPLY
        (SELECT TOP 1
        		concat(iu.firstName, ' ', iu.lastName) AS created_by
           FROM fastlane.usercase.hUCase huc 
           JOIN fastlane.info.InputUser iu ON iu.inputUserId = huc.inputUserId 
          WHERE huc.ucaseId = uc.ucaseId 
        ORDER BY huc.startTime ASC
        ) AS hist */
 WHERE  uc.created >= convert(datetime2,'2021-07-12');	-- only since ETAN started running

/********** PASS DETAIL **********/
-- DROP VIEW util.v_pass_detail;
CREATE OR ALTER VIEW util.v_pass_detail AS
SELECT  t.transponderNumber AS transponder_number
		,fa.kapschCode AS pass_agency 
		,tdt.description AS pass_type 
		,etis.description AS pass_inv_status 
		,tr.first_read 
		,tr.latest_read 
		,tca.customerAccountId AS customer_account_id
		,vca.acct_type_desc 
		,vas.status AS acct_status
  FROM  fastlane.vehicle.Transponder t 
  JOIN  fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = t.transponderId 	-- left join?
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  JOIN  fastlane.inv.InventoryTag it ON it.inventoryTagId = t.inventoryTagId 
  JOIN  fastlane.inv.EnuTagInventoryStatus etis ON etis.tagInventoryStatusId = it.tagInventoryStatusId 
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = it.tagDeviceTypeId 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tca.customerAccountId 
--  LEFT  JOIN fastlane.info.V_AccountStatus vas ON vas.customerAccountId = tca.customerAccountid 
  LEFT  JOIN WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = tca.customerAccountid
  LEFT  JOIN
		(SELECT tpt.transponderId 
				,min(tp.laneTime) AS first_read
				,max(tp.laneTime) AS latest_read
		   FROM fastlane.lance.TripPassageTransponder tpt
		   JOIN fastlane.lance.trip trp ON trp.tripPassageId = tpt.tripPassageId	-- use to ensure only exit passages
		   JOIN fastlane.lance.tripPassage tp ON tp.tripPassageId = tpt.tripPassageId 
		GROUP BY tpt.transponderId 
		) tr ON tr.transponderId = t.transponderId;

/********** ACCOUNT DISCOUNTS **********/
-- NOTE: most info comes from function fastlane.xact.F_Get_DiscountPlansByCustomerAccountId(@cust_id)
-- DROP VIEW util.v_account_discounts;
CREATE OR ALTER VIEW util.v_account_discounts AS
SELECT  d.customerAccountId AS customer_account_id
		,d.discountPlanId AS discount_plan_id
		,dp.shortCode AS discount_plan_code
		,dp.name AS discount_plan_name
		,edpt.description AS discount_type
		,edpc.description AS discount_category 
		,edat.description AS applied_to
		,edl.description AS attached_to
		,d.lp_or_pass_number
		,d.status AS attached_status
  FROM  (SELECT cadp.customerAccountId	-- account discounts (currently empty)
				,cadp.discountPlanId 
				,cadp.discountPlanScopeId 
				,NULL AS lp_or_pass_number
				,vas.status
		  FROM  fastlane.xact.CustomerAccountDiscountPlan cadp
		  --JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = cadp.customerAccountId
		  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = cadp.customerAccountId 
		 WHERE  cadp.isDefunct = 0
		UNION ALL	-- transponder plan
  		SELECT tca.customerAccountId 
				,tdp.discountPlanId 
				,tdp.discountPlanScopeId 
				,concat(convert(varchar, fa.kapschCode),' ',t.transponderNumber) AS lp_or_pass_number
				,etis.description AS status
		  FROM  fastlane.xact.TransponderDiscountPlan tdp
		  JOIN  fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = tdp.transponderId
		  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = tca.transponderId 
		  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
		  JOIN  fastlane.inv.InventoryTag it ON it.inventoryTagId = t.inventoryTagId 
		  JOIN  fastlane.inv.EnuTagInventoryStatus etis ON etis.tagInventoryStatusId = it.tagInventoryStatusId 
		 WHERE  tdp.isDefunct = 0
		UNION ALL	-- license plate discounts
		SELECT  lpca.customerAccountId 
				,lpdp.discountPlanId 
				,lpdp.discountPlanScopeId 
				,concat(es.shortCode,' ',lp.lpnumber) AS lp_or_pass_number 
				-- license plate status as defined in fastlane.util.V_licensePlate 
				,CASE WHEN fastlane.config.getpostingdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
				      THEN (SELECT shortcode FROM fastlane.displayenum.enuLicenseplateStatus WHERE licenseplatestatusid = 1) 
					  ELSE (SELECT shortcode FROM fastlane.displayenum.enuLicenseplateStatus WHERE licenseplatestatusid = 0)
				END AS status
		  FROM  fastlane.xact.LicensePlateDiscountPlan lpdp
		  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lpdp.licensePlateId 
		  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = lpca.licensePlateId 
		  JOIN  fastlane.util.EnuLpJurisdiction lpj on lpj.lpJurisdictionId=lp.lpJurisdictionId
		  JOIN  fastlane.locale.EnuState es on lpj.stateId = es.stateId
		 WHERE  lpdp.isDefunct = 0
		) AS d
  JOIN  fastlane.xact.DiscountPlan dp ON dp.discountPlanId = d.discountPlanId 
  JOIN  fastlane.xact.EnuDiscountLevel edl ON edl.discountLevelId = dp.discountLevelId 
  JOIN  fastlane.xact.EnuDiscountApplyType edat ON edat.discountApplyTypeId = dp.discountApplyTypeId
  LEFT JOIN fastlane.xact.EnuDiscountPlanType edpt ON edpt.discountPlanTypeId = dp.discountPlanTypeId 
  LEFT JOIN fastlane.xact.EnuDiscountPlanCategory edpc ON edpc.discountPlanCategoryId = dp.discountPlanCategoryId 
 WHERE  dp.isDefunct = 0;

/********** UNIQUE VEHICLE USAGE SUMMARIES **********/
-- view for preparing unique vehile usage frequency analysis
-- TODO: add roadside toll type if certain exclusions or types are needed
-- 		 or switch to v_trip_detail_full to get current amount and other factors
-- DROP VIEW util.v_daily_facility_usage;
CREATE OR ALTER VIEW util.v_daily_facility_usage AS
SELECT  cast(t.exitTime AS date) AS trip_date
		,WSDOT_STAGE.util.f_get_month(t.exitTime) AS trip_month
		,vfl.wsdot_facility AS facility
		,vfl.wsdot_roadway AS roadway
		,CASE WHEN t.transponderId IS NOT NULL THEN concat('tag_',t.transponderId)
			  WHEN t.licensePlateId IN (1000, 182329, 184145) THEN 'none'	-- lump high-frequncy bad plate trips into none category
			  WHEN t.licensePlateId IS NOT NULL THEN concat('lp_',t.licensePlateId)
			  ELSE 'none' END AS veh_id
		,sum(t.amount) AS sum_roadside_toll_charges
		,count(*) AS toll_trips
  FROM  fastlane.rtoll.Toll t
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId 
-- WHERE  (t.transponderId IS NOT NULL OR t.licensePlateId IS NOT NULL)	-- at least one vehicle identifier
GROUP BY cast(t.exitTime AS date)
		,WSDOT_STAGE.util.f_get_month(t.exitTime)
		,vfl.wsdot_facility
		,vfl.wsdot_roadway
		,CASE WHEN t.transponderId IS NOT NULL THEN concat('tag_',t.transponderId)
			  WHEN t.licensePlateId IN (1000, 182329, 184145) THEN 'none'	-- lump high-frequncy bad plate trips into none category
			  WHEN t.licensePlateId IS NOT NULL THEN concat('lp_',t.licensePlateId)
			  ELSE 'none' END;

/********** IMAGE REVIEW SUPPORT **********/
-- 2022-09-01 incorporated queries from Mike Gaunt
-- icrs detail
-- DROP VIEW util.v_icrs_detail;
CREATE OR ALTER VIEW util.v_icrs_detail AS
SELECT 	t.tripId AS trip_id
		,tp.tripPassageId 
		,tp.laneTime AS trip_datetime
		,cast(tp.laneTime AS date) AS trip_date
		,vfl.wsdot_facility AS facility
		,ireq.imageReviewRequestId
		,irrr.imageReviewResultsId
		,ireq.isrequested
		,ireq.isresponded
		,CASE WHEN irr.dispositionId IS NULL THEN 0 ELSE 1 END AS dispositionNullFlag
		,CASE WHEN ireq.disputetypeid IS NULL THEN 0 ELSE 1 END AS disputeNullFlag
		,(cast(irr.dispositionId AS varchar(10)) + '-' + dispo.shortcode) AS disposition
		,(cast(irr.resulttypeid AS varchar(10)) + '-' + res.shortcode) AS ir_result
		,(cast(ireq.disputetypeid AS varchar(10)) + '-' + dispu.shortcode) AS ir_dispute
		,cast(t.created AS date) AS created_intrip
		,cast(tp.updatedAt AS date) AS created_intpid
		,cast(ireq.updatedat AS date) AS created_irr_req
		,cast(irrr.processedat AS date) AS created_irr_disp
		,datediff(day, tp.lanetime, t.created) AS diff_created_trip
		,datediff(day, tp.lanetime, ireq.updatedat) AS diff_created_ireq
		,datediff(day, tp.lanetime, irrr.processedat) AS diff_created_irr_disp
		,datediff(day, tp.lanetime, t.created) AS time_kapsch
		,datediff(day, t.created, ireq.updatedat) AS time_etan
		,datediff(day, ireq.updatedat, irrr.processedat) AS time_qfree
		,cast(getdate() AS date) AS queried_at
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
  JOIN  fastlane.qfree.ImageReviewRequest ireq ON ireq.tripPassageId = tp.tripPassageId  
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  LEFT  JOIN fastlane.qfree.ImageReviewResult irr ON irr.imageReviewRequestId = ireq.imageReviewRequestId
  LEFT  JOIN fastlane.qfree.ImageReviewResultResponses irrr ON irrr.imagereviewresultsid = irr.imagereviewresultsid AND irrr.processedAt < convert(datetime2, '2099-01-01')
  LEFT  JOIN fastlane.qfree.enudisposition dispo ON dispo.dispositionId = irr.dispositionId
  LEFT  JOIN fastlane.qfree.enudisputetype dispu ON dispu.disputeTypeId = ireq.disputeTypeId
  LEFT  JOIN fastlane.qfree.enuresulttype res ON res.resulttypeid = irr.resulttypeid;

-- image download detail
-- DROP VIEW util.v_tp_image_download_detail;
CREATE OR ALTER VIEW util.v_tp_image_download_detail AS
SELECT  d.tripPassageId
		,t.tripId AS trip_id
		,tp.laneTime AS trip_datetime
		,cast(tp.lanetime AS date) AS trip_date
		,vfl.wsdot_facility AS facility
		,vfl.wsdot_roadway AS roadway
		,ts.tollStatusCodeId 
		,tp.roadsideTollType
		,d.count_req
		,d.count_dl
		,d.cnt_null
		,d.cnt_no_dl
		,CASE WHEN d.count_dl > 0 THEN 1 ELSE 0 END AS flag_yes_dl
		,CASE WHEN d.count_dl = 0 THEN 1 ELSE 0 END AS flag_no_dl
		,img.flag_inLance
		,CASE WHEN img.flag_inLance IS NULL THEN 1 ELSE 0 END AS flag_notinImage
  FROM  (SELECT r.tripPassageId
  				,count(*) AS count_req
				,sum(flag_dl) AS count_dl
				,sum(flag_dl_null) AS cnt_null
				,sum(flag_dl_no) AS cnt_no_dl
		  FROM  (SELECT req.tripPassageId
						,CASE WHEN resp.errorcode = 2 THEN 1 ELSE 0 END AS flag_dl
						,CASE WHEN resp.errorcode = 1 THEN 1 ELSE 0 END AS flag_dl_no
						,CASE WHEN resp.errorcode IS NULL THEN 1 ELSE 0 END AS flag_dl_null
				  FROM  fastlane.lance.TripPassageImageDownloadRequest req
				  LEFT  JOIN fastlane.lance.TripPassageImageDownloadResponse resp ON resp.tripPassageImageDownloadRequestId = req.tripPassageImageDownloadRequestId
				 ) r
		GROUP BY r.tripPassageId
		) d
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = d.tripPassageId
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.lance.trip t ON t.tripPassageId = tp.tripPassageId
  JOIN  fastlane.rtoll.tollstatus ts ON ts.tollid = t.tripid
  LEFT  JOIN (SELECT DISTINCT tripPassageId, 1 AS flag_inLance FROM fastlane.lance.PassageImageURI) img
		  ON img.tripPassageId = d.tripPassageId;

/********** IMAGE REVIEW SUPPORT **********/
-- 2022-09-07 fix fastlane.info.V_AccountStatus
-- 2022-11-23 add expectedStateTransitionDate to get when account closed; also, accounts seem to have only one case closing case
-- DROP VIEW util.v_account_status;
CREATE OR ALTER VIEW util.v_account_status AS
SELECT  c.customerAccountId
		,c.statusId
		,eal.description AS status
		,c.expectedStateTransitionDate
  FROM  (SELECT ca.customerAccountId 
  				--,iif(isClosing IS NULL, 'ACTIVE', iif(isClosing = 1 , 'CLOSED', 'PENDINGCLOSE')) AS statusId
  				,CASE WHEN case_close.isClosing IS NULL THEN 'ACTIVE'
  					  WHEN case_close.isClosing = 0 THEN 'PENDINGCLOSE'
  					  WHEN case_close.isClosing = 1 THEN 'CLOSED'
  					  ELSE 'UNKNOWN' END AS statusId	-- not technically needed since isClosing is a bit type
  				,case_close.expectedStateTransitionDate
		  FROM  fastlane.util.CustomerAccount ca 
		  LEFT  JOIN	-- get any cases related to account closure
				(SELECT DISTINCT cca.customerAccountId
		  				,wfs.isClosing
		  				,uc.expectedStateTransitionDate 
				  FROM  fastlane.usercase.CaseCustomerAccount cca
				  JOIN  fastlane.usercase.[UCase] uc ON uc.ucaseId = cca.ucaseId 
				  JOIN  fastlane.usercase.EnucaseType ect ON ect.caseTypeId = uc.caseTypeId 
				  JOIN  fastlane.userCase.WorkflowState wfs ON wfs.workflowStateId = uc.workflowStateId 
				 WHERE  cca.isDefunct = 0	-- no defunct cases
				   AND  ect.shortcode = 'CLOSEACCOUNT'	-- only account closure cases
				) case_close
		 		 ON case_close.customerAccountId = ca.customerAccountId
		) c
  LEFT  JOIN fastlane.displayenum.EnuAccountLifecycle eal ON eal.shortCode = c.statusId;

/********** SCHEDULED REPORTING PROCEDURES **********/
-- procedure to summarize negative balance accounts for escalation go-live
-- see Tyler's tracking spreadsheet for conditions
-- 2023-01-27: updated email syntax check from %@% to %_@%_._%
CREATE OR ALTER PROCEDURE util.usp_escalation_golive AS
BEGIN
	DECLARE @runtime datetime2(2) = getdate()
	
	-- clear existing table and drop temp tables if they exist
	TRUNCATE TABLE WSDOT_STAGE.rpt.x_negative_balance_detail;
	DROP TABLE IF EXISTS #recd_statement_date;
	DROP TABLE IF EXISTS #legacy_vtoll2_nocp_accts;

	-- get all negative balance accounts with account type and other fields for later update (fast, ~5 seconds)
	INSERT INTO WSDOT_STAGE.rpt.x_negative_balance_detail 
	SELECT  tb.customerAccountId
			,CASE WHEN vca.acct_short_code LIKE 'PAYBYMAIL%' THEN 'pbm'
				  WHEN vca.acct_short_code LIKE 'ZBA%' THEN 'payg'
				  WHEN vca.acct_short_code IN ('INDIVIDUAL', 'COMMERCIAL') THEN 'prepaid'
				  ELSE 'other' END AS acct_category
			,CASE WHEN amount <= -5000 THEN 'over_$5k'
				  WHEN amount <= -200 THEN '$200-5k'
				  ELSE '$0-200' END AS amount_class
			,datediff(day, tb.lastSignChangeDate, @runtime) AS days_since_negative
			,NULL AS latest_recd_statement_date
			,NULL AS cc_failed
			,NULL AS valid_email
			,NULL AS category_id
	  FROM  fastlane.xact.TransactionsBalance tb 
	  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
	 WHERE  tb.isCurrentActivity = 1	-- current activity
	   AND  tb.signValue = -1;			-- negative balance

	-- get latest received statement date for each account (~2 minutes)
	SELECT  i.customerAccountId
			,max(i.postingDate) AS latest_recd_statement_date
	  INTO  #recd_statement_date
	  FROM  fastlane.xact.Invoice i 
	  JOIN  WSDOT_STAGE.rpt.x_negative_balance_detail nb ON nb.customerAccountId = i.customerAccountId 
	 WHERE  i.communicationId IS NOT NULL	-- sent statement
	   AND  fastlane.util.F_IsMailReturned(i.storageFileId) <> 1	-- not returned
	GROUP BY i.customerAccountId;

	-- update base table with latest statement date (~10 seconds)
	UPDATE  WSDOT_STAGE.rpt.x_negative_balance_detail
	   SET  latest_recd_statement_date = c.latest_recd_statement_date
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nbd
	  JOIN  #recd_statement_date c ON c.customerAccountId = nbd.customerAccountId;

	-- update only pay-as-you-go accounts with failed cc flag (~2 minutes)
	UPDATE  WSDOT_STAGE.rpt.x_negative_balance_detail
	   SET  cc_failed = fastlane.util.F_AccountFlag_CreditCardFailed(customerAccountId)
	 WHERE  acct_category = 'payg';

	-- check for any legacy unpaid VTOLL2 and NOCP amounts (~1 minute)
	SELECT  vts.customerAccountid
			,vts.transactionTypeId 
			,count(*) AS trip_cnt
	  INTO  #legacy_vtoll2_nocp_accts
	  FROM  fastlane.xact.V_TransactionSummary vts 
	  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
	 WHERE  vts.isCurrentActivity = 1	-- current activity
	   AND  vts.isUnpaid = 1			-- not paid
	   AND  vts.transactionTypeId IN (23, 70, 83)
	GROUP BY vts.customerAccountid
			,vts.transactionTypeId;

	-- add accounts from legacy INVTOLL2 and NOCP trips - should be none
	INSERT  INTO WSDOT_STAGE.rpt.x_negative_balance_detail
	SELECT  leg.customerAccountId
			,CASE WHEN vca.acct_short_code LIKE 'PAYBYMAIL%' THEN 'pbm'
				  WHEN vca.acct_short_code LIKE 'ZBA%' THEN 'payg'
				  WHEN vca.acct_short_code IN ('INDIVIDUAL', 'COMMERCIAL') THEN 'prepaid'
				  ELSE 'other' END AS acct_category
			,CASE WHEN amount <= -5000 THEN 'over_$5k'
				  WHEN amount <= -200 THEN '$200-5k'
				  WHEN amount < 0 THEN '$0-200'
				  ELSE 'not_neg' END AS amount_class	-- add to allow for edge case?
			,datediff(day, tb.lastSignChangeDate, @runtime) AS days_since_negative
			,NULL AS latest_recd_statement_date
			,NULL AS cc_failed
			,NULL AS valid_email
			,CASE WHEN ett.shortCode = 'INVTOLL2' THEN '5'	-- adjust for email later
				  WHEN ett.shortCode = 'TCPP' THEN '6'
				  END AS category_id
	  FROM  (SELECT v2n.customerAccountId
  					,min(v2n.transactionTypeId) AS txn_type_id	-- prioritize INVTOLL2 (23) over NOCP (83)
	  		   FROM #legacy_vtoll2_nocp_accts v2n
	  		GROUP BY v2n.customerAccountId
  			) leg
	  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = leg.txn_type_id
	  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = leg.customerAccountId AND tb.isCurrentActivity = 1
	  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
	  LEFT  JOIN WSDOT_STAGE.rpt.x_negative_balance_detail nb ON nb.customerAccountId = leg.customerAccountid 
	 WHERE  nb.customerAccountId IS NULL;	-- accounts not in existing summary

	-- update with valid email (~5 seconds)
	UPDATE  WSDOT_STAGE.rpt.x_negative_balance_detail
	   SET  valid_email = 1
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
	  JOIN  WSDOT_STAGE.util.v_acct_contact_info aci ON aci.customerAccountId = nb.customerAccountId 
	 WHERE  aci.email_address LIKE '%_@%_._%';

	-- update accounts with final categories (~10 seconds)
	--  0: pbm, payg, or prepaid acct with balance not yet 30 days late
	-- 1A: pbm acct over 30 days late balance between 0 and $200 with valid email
	-- 1B: pbm acct over 30 days late balance between 0 and $200 without valid email
	-- 2A: pbm acct over 30 days late balance between $200 and $5k with valid email
	-- 2B: pbm acct over 30 days late balance between $200 and $5k without valid email and statement received since 2022-05-01
	-- 2C: pbm acct over 30 days late balance between $200 and $5k without valid email and statement received before 2022-05-01 without revised address from skip tracing
	-- 2D: pbm acct over 30 days late balance between $200 and $5k without valid email and statement received before 2022-05-01 *with* revised address from skip tracing
	--  3: pbm acct over 30 days late balance over $5k
	--  4: prepaid acct over 30 days late with any negative balance
	-- 5A: acct with legacy VTOLL2 trips with valid email (should be none as all accts show above)
	-- 5B: acct with legacy VTOLL2 tripswithout valid email (should be none as all accts show above)
	--  6: acct with legacy NOCP trips (should be none as all accts show above)
	--  7: pay-as-you-go acct over 30 days late with failed credit card
	--  8: other, mostly non prepaid, pbm, or payg accts with negative balances
	--  negative balance (over 30 days for PAYG and PBM)
	WITH cte AS (
		SELECT  nb.customerAccountId 
				,CASE WHEN nb.category_id = '5' AND nb.valid_email = 1 THEN '5A'	-- first adjust legacy category_ids
					  WHEN nb.category_id = '5' THEN '5B'
					  WHEN nb.category_id = '6' THEN '6'
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = '$0-200' AND nb.valid_email = 1 THEN '1A'
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = '$0-200' THEN '1B'
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = '$200-5k' AND nb.valid_email = 1 THEN '2A'
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = '$200-5k' AND nb.latest_recd_statement_date >= convert(date,'2022-05-01') THEN '2B'
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = '$200-5k' THEN '2C'	-- need to add skip tracing identification
					  WHEN nb.acct_category = 'pbm' AND nb.days_since_negative > 30 AND nb.amount_class = 'over_$5k' THEN '3'
					  WHEN nb.acct_category = 'prepaid' AND nb.days_since_negative > 30 THEN '4'
					  WHEN nb.acct_category = 'payg' AND nb.days_since_negative > 30 AND nb.cc_failed = 1 THEN '7'
					  WHEN nb.acct_category IN ('pbm', 'payg', 'prepaid') AND nb.days_since_negative <= 30 THEN '0'	-- not yet a concern
					  ELSE '8'	-- catch-all other category
					  END AS category_id
		  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
		)
	UPDATE  WSDOT_STAGE.rpt.x_negative_balance_detail
	   SET  category_id = c.category_id
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
	  JOIN  cte c ON c.customerAccountId = nb.customerAccountId;

	-- update for skip tracing of addresses to category 2D
	UPDATE  WSDOT_STAGE.rpt.x_negative_balance_detail
	   SET  category_id = '2D'
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
	  JOIN  WSDOT_STAGE.util.v_acct_contact_info vaci ON vaci.customerAccountId = nb.customerAccountId 
	 WHERE  category_id = '2C'
	   AND  vaci.address_source = 'SKIPTRACING';

	-- add data to history table
	INSERT INTO WSDOT_STAGE.rpt.negative_balance_history
	SELECT *, cast(@runtime AS date) FROM WSDOT_STAGE.rpt.v_escalation_negative_balance_status nbs;
	
	-- drop temp tables
	DROP TABLE IF EXISTS #recd_statement_date;
	DROP TABLE IF EXISTS #legacy_vtoll2_nocp_accts;

END;

-- procedure to run reporting nightly
-- 2022-08-10: added debug log to better identify where failures occur
-- TODO: look at adding transactions to commit intermediate results and check for errors?
-- DROP PROCEDURE util.usp_wsdot_reporting;
CREATE OR ALTER PROCEDURE util.usp_wsdot_reporting AS
BEGIN
	DECLARE @runtime datetime2(2) = getdate()				-- start datetime
	DECLARE @rundate datetime2(2) = cast(@runtime AS date)	-- midnight datetime
	
	-- exit if not running on primary (read-write) node on availability group
	IF WSDOT_STAGE.util.f_get_ag_role(default) <> 'PRIMARY' RETURN;

	/*----- tracking updates -----*/
	-- INSERT INTO WSDOT_STAGE.log.debug_info (jobstep, message) VALUES (10,'start scheduled reporting');
	INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: update tracking tables', 'tracking_tables', @@servername);
	-- trip state tracking
	IF WSDOT_STAGE.util.f_is_report_monthly(@runtime) = 1
	BEGIN
		-- update monthly trip state; get previous month in yyyy-mm format and create description
		DECLARE @d varchar(64) = concat(left(convert(varchar, dateadd(month, -1, WSDOT_STAGE.util.f_get_month(@runtime)), 120),7), ' trip updates');
		EXEC WSDOT_STAGE.util.usp_update_trip_state @end_dtm = @rundate, @desc = @d;
	END;
	ELSE EXEC WSDOT_STAGE.util.usp_update_recent_trip_state @end_dtm = @rundate;

	/*----- daily reporting -----*/
	INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: daily', 'daily_reporting', @@servername);
	-- summarize pass fulfillment
	TRUNCATE TABLE WSDOT_STAGE.rpt.x_pass_fulfillment;
	INSERT INTO WSDOT_STAGE.rpt.x_pass_fulfillment
	SELECT * FROM WSDOT_STAGE.rpt.v_pass_fulfillment;
	-- summarize trips by posting and trip date
	TRUNCATE TABLE WSDOT_STAGE.rpt.x_trip_posting_summary;
	INSERT INTO WSDOT_STAGE.rpt.x_trip_posting_summary
	SELECT * FROM WSDOT_STAGE.rpt.v_trip_posting_summary
	WHERE posting_date >= dateadd(month, -2, WSDOT_STAGE.util.f_get_month(@runtime));	-- look at two full months prior to current
	-- summarize icrs
	INSERT INTO WSDOT_STAGE.rpt.icrs_summary 
	SELECT * FROM WSDOT_STAGE.rpt.v_icrs_summary
	WHERE trip_date >= dateadd(day, -45, cast(getdate() AS date));
	-- summarize image download
	INSERT INTO WSDOT_STAGE.rpt.image_download_summary 
	SELECT * FROM WSDOT_STAGE.rpt.v_image_download_summary
	WHERE trip_date >= dateadd(day, -45, cast(getdate() AS date));
	
	/*----- weekly reporting -----*/
	IF WSDOT_STAGE.util.f_is_report_weekly(@runtime) = 1
	BEGIN
		INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: weekly', 'weekly_reporting', @@servername);
		-- update account types
		EXEC WSDOT_STAGE.util.usp_update_account_types;
		-- update posting performance
		TRUNCATE TABLE WSDOT_STAGE.rpt.x_posting_performance;
		INSERT INTO WSDOT_STAGE.rpt.x_posting_performance
		SELECT * FROM WSDOT_STAGE.rpt.v_posting_performance;
		-- summarize new account creation
		-- summarize account trends
	END;
	
	/*----- monthly reporting (no delay, run on 1st) -----*/
	/* -- for now csr query runs quickly and is only monthly report, so run manually
	IF datepart(day, @runtime) = 1
	BEGIN
		INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: monthly (no delay)', 'eo_monthly_reporting', @@servername);
		-- csr case closure
	END;
	*/
	
	/*----- monthly reporting -----*/
	IF WSDOT_STAGE.util.f_is_report_monthly(@runtime) = 1
	BEGIN
		INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: monthly', 'monthly_reporting', @@servername);
		-- disposition summary
		INSERT INTO WSDOT_STAGE.rpt.disposition_summary
		SELECT *, cast(@rundate AS date) FROM WSDOT_STAGE.rpt.v_disposition_summary vds
		WHERE vds.trip_month >= convert(date, '2021-01-01');
		-- account last trip month
		-- pass last activity
	END;

	/*----- additional daily tasks -----*/
	-- update escalation go-live tracking
	INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES ('start scheduled reporting: escalation go-live tracking', 'escalation_golive', @@servername);
	EXEC WSDOT_STAGE.util.usp_escalation_golive;

	-- log run time in hh:mm:ss
	INSERT INTO WSDOT_STAGE.log.runinfo (message, process_name, server_name) VALUES (concat('scheduled reporting completed in ', WSDOT_STAGE.util.f_timediff_hhmmss(@runtime, getdate())), 'reporting_completed', @@servername);
END;

-- procedure to run custom ad-hoc queries (modify as needed)
-- DROP PROCEDURE util.usp_wsdot_custom;
CREATE OR ALTER PROCEDURE util.usp_wsdot_custom AS
BEGIN
	EXEC WSDOT_STAGE.tmp.usp_pull_equity_trip_data;
END;

/*######################################################################*/
/*##########                    WSDOT DATA                    ##########*/
/*######################################################################*/

/********** FACILITY AND ROADWAY NAMES **********/
-- create table of proper roadway names
-- based on fastlane.lance.EnuRoadway
-- DROP TABLE WSDOT_STAGE.wsdot.roadway;
CREATE TABLE WSDOT_STAGE.wsdot.roadway (
    roadway_id tinyint NOT NULL
    ,wsdot_short_code varchar(16) NOT NULL
    ,wsdot_route_no varchar(16) NOT NULL
    ,etan_short_code varchar(16) NOT NULL
    ,hours_to_post_goal smallint NOT NULL
    CONSTRAINT pk_roadway PRIMARY KEY (roadway_id)
);

-- add data to table
INSERT INTO WSDOT_STAGE.wsdot.roadway VALUES
(2,'SR167','SR167','SR167',120),
(3,'SR520','SR520','SR520',48),
(5,'TNB','SR016','SR16',48),
(6,'I405','I405','I405',120),
(13,'SR99','SR99','S99',48);

-- create table of proper facility names
-- based on fastlane.lance.EnuRoadFacility and fastlane.lance.EnuDirection
-- DROP TABLE WSDOT_STAGE.wsdot.facility;
CREATE TABLE WSDOT_STAGE.wsdot.facility (
    facility_id tinyint NOT NULL
    ,wsdot_short_code varchar(16) NOT NULL
    ,etan_short_code varchar(16) NOT NULL
    ,direction varchar(4) NOT NULL
    ,facility_desc varchar(64) NOT NULL
    ,roadway_id tinyint NOT NULL
    ,CONSTRAINT pk_facility PRIMARY KEY (facility_id)
);

-- add data to table
INSERT INTO WSDOT_STAGE.wsdot.facility VALUES
(1,'167N','167N','N','SR167 Northbound',2),
(2,'167S','167S','S','SR167 Southbound',2),
(3,'520W','520W','W','SR520 Westbound',3),
(5,'TNB','SR16','E','TNB Eastbound',5),
(6,'520E','520E','E','SR520 Eastbound',3),
(7,'405BL','405BL','N','I405 Bellevue - Lynnwood',6),
(8,'405LB','405LB','S','I405 Lynnwood - Bellevue',6),
(12,'99N','99N','N','SR99 Northbound',13),
(13,'99S','99S','S','SR99 Southbound',13);

-- create new view using fastlane.lance.V_AllLanesForFacility, but adding corrected items
-- DROP VIEW wsdot.v_facility_lanes;
CREATE OR ALTER VIEW wsdot.v_facility_lanes AS
SELECT  valff.*
		,f.wsdot_short_code AS wsdot_facility
		,r.wsdot_short_code AS wsdot_roadway
		,f.direction AS wsdot_direction
		,r.hours_to_post_goal
  FROM  fastlane.lance.V_AllLanesForFacility valff
  JOIN  WSDOT_STAGE.wsdot.facility f ON f.facility_id = valff.roadFacilityId
  JOIN  WSDOT_STAGE.wsdot.roadway r ON r.roadway_id = f.roadway_id;

-- create table of txn type priority for simultaneous transactions
-- DROP TABLE WSDOT_STAGE.wsdot.transaction_type_priority;
CREATE TABLE WSDOT_STAGE.wsdot.transaction_type_priority (
    transaction_type_id tinyint NOT NULL
    ,shortcode varchar(64) NOT NULL
    ,priority smallint NOT NULL
    ,CONSTRAINT pk_txn_type_priority PRIMARY KEY (transaction_type_id)
    ,CONSTRAINT uq_txn_type_priority_shortCode UNIQUE (shortCode)
);

-- insert data for known and some suspected simultaneous transaction types
-- NOTE: observed concurrent entries as of 2021-07-01:
--      FPBP,TOLL
--      TOLL,TOLLDISC
--      FCVP,TCVP
--      FPBP,TOLL,TOLLDISC
--      FPBP,TOLL,WRITEOFF
--      FPBP,TOLL,TOLLDISC,TOLLDISC
--      WRITEOFF,WRITEOFF
-- TODO: FCPP status is later than TCPP, so this gets FCPP instead - does that need to be addressed?
INSERT INTO WSDOT_STAGE.wsdot.transaction_type_priority VALUES
(1,'TOLL',1),
(19,'TOLLDISC',5),
(25,'FPBP',0),
(55,'FCVP',0),
(70,'TCVP',1),
(83,'TCPP',1),
(84,'FCPP',0),
(122,'WRITEOFF',10);

/********** LEAKAGE DEFINITIONS **********/
-- create table of leakage categories
-- DROP TABLE WSDOT_STAGE.wsdot.leakage_categories;
CREATE TABLE WSDOT_STAGE.wsdot.leakage_categories (
    leakage_cat_id tinyint NOT NULL
    ,short_code varchar(16) NOT NULL
    ,category_description varchar(64) NOT NULL
    ,CONSTRAINT pk_leakage_categories PRIMARY KEY (leakage_cat_id)
);

-- add data to table
INSERT INTO WSDOT_STAGE.wsdot.leakage_categories VALUES
(0,'ADJ','Revenue Adjustment per Policy or Business Rule'),
(1,'LEAK_I','Leak (I): Unidentifiable Vehicle'),
(2,'LEAK_II','Leak (II): Unidentifiable Owner'),
(3,'LEAK_III','Leak (III): Dismissals'),
(4,'LEAK_IV','Leak (IV): Uncollectible');

-- create table linking reason codes to leakage/adjustment types
-- allow leakage category id to be NULL for unmapped reason codes
-- TODO: consider using a temporal table or adding validity dates to allow changes over time
-- DROP TABLE WSDOT_STAGE.wsdot.leakage_reason_codes;
CREATE TABLE WSDOT_STAGE.wsdot.leakage_reason_codes (
    entry_reason_id tinyint NOT NULL
    ,er_short_code varchar(16) NOT NULL
    ,leakage_cat_id tinyint
    ,CONSTRAINT pk_leakage_reason_codes PRIMARY KEY (entry_reason_id)
);

-- add data to table
-- initially developed from meetings with WSDOT and ETAN in Oct 2021
-- see email to Tyler, Ami, Yang, and Marie on 2021-10-12 about missing codes
INSERT INTO WSDOT_STAGE.wsdot.leakage_reason_codes VALUES
(0,'UNKNOWN',NULL),
(1,'d-ORIG',NULL),
(2,'c-ORIG',NULL),
(3,'x-CHOUT',NULL),
(4,'d-DUNDO',NULL),
(5,'c-CUNDO',NULL),
(6,'d-CANCL',NULL),
(7,'c-CANCL',NULL),
(10,'C-ACCHG',0),
(11,'C-LPCHG',0),
(12,'C-NOREV',0),
(13,'C-2FLET',NULL),
(20,'D-AUTHC',0),
(21,'D-CP167',0),
(22,'D-CP405',0),
(23,'D-DUPTR',0),
(24,'D-ERROR',3),
(25,'D-GENPL',0),
(26,'D-NTHRS',0),
(27,'D-PSNR1',3),
(28,'D-PSNR2',3),
(29,'D-RENT2',0),
(30,'D-SOLDF',0),
(31,'D-STRVS',3),
(32,'D-MIGDS',3),
(33,'D-OTHER',NULL),
(34,'D-MSKLP',NULL),
(35,'D-ADMHR',3),
(36,'D-MOTOC',NULL),
(40,'M-ADUTY',0),
(41,'M-DEADO',0),
(42,'M-DEADS',0),
(43,'M-EVICT',0),
(44,'M-HMLES',0),
(45,'M-HOSPT',0),
(46,'M-NOBL1',0),
(47,'M-NOBL2',0),
(48,'M-VERTP',0),
(49,'M-VSTOL',0),
(50,'M-DVSEP',0),
(51,'M-EMERG',NULL),
(53,'M-MIGWO',NULL),
(54,'M-OTHER',NULL),
(60,'P-BDACH',NULL),
(61,'P-CCCCB',NULL),
(62,'P-CKNSF',NULL),
(63,'P-CKSTP',NULL),
(64,'P-DATER',NULL),
(65,'P-MIGRV',NULL),
(66,'P-UNREA',NULL),
(67,'P-SPLNK',NULL),
(80,'R-ATOLL',0),
(82,'R-RENT1',0),
(83,'R-SOLDR',0),
(84,'R-STADJ',0),
(85,'R-WRLPR',0),
(90,'S-ESCAL',0),
(91,'S-LPDUP',1),
(92,'S-LPREC',1),
(93,'S-NOBIL',3),
(94,'S-NOREV',0),
(95,'S-MCYCL',0),
(96,'S-CARPL',NULL),
(97,'S-PENRP',NULL),
(98,'S-TGDUP',1),
(99,'S-UBAGE',3),
(100,'S-OPOVR',0),
(101,'S-MIGDS',NULL),
(102,'S-ISRFD',0),
(103,'S-BKRPY',NULL),
(104,'S-LPADD',0),
(110,'W-BKRPY',0),
(111,'W-CPDAG',3),
(112,'W-LGYDR',3),
(113,'W-TRNAG',3),
(114,'W-UNCOL',4),
(115,'W-CLOSD',0),
(116,'D-NODMV',NULL),
(120,'X-DATER',0),
(121,'X-NSFFE',NULL),
(122,'C-PINOP',0),
(123,'C-PLOST',0),
(124,'X-PNOTN',0),
(125,'X-STMFE',NULL),
(126,'Q-RCAST',NULL),
(127,'P-CKACT',NULL),
(128,'P-BKADJ',NULL),
(129,'C-PRVSL',0),
(130,'C-PLSFD',0),
(131,'S-RADCL',0),
(150,'1-CPR4G',0),
(151,'2-CPR4A',0),
(152,'2-CPR4G',0),
(154,'T-ICREJ',1),
(155,'M-NRVDS',NULL),
(156,'1-CPR4U',NULL),
(160,'T-IMGTB',1),
(161,'T-IMGTD',1),
(162,'T-IMGBR',1),
(163,'T-WEATH',1),
(164,'T-CAMER',1),
(165,'T-NOVCH',1),
(166,'T-PLUNR',1),
(167,'T-PLOVS',1),
(168,'T-TRNOF',1),
(169,'T-PLMIS',2),
(170,'T-PTUNK',1),
(171,'T-NOJUR',1),
(172,'T-NOPLT',1),
(173,'T-PTCAN',2),
(174,'T-PTMEX',2),
(175,'T-PTITL',2),
(176,'T-PTTRB',2),
(177,'T-VCHMM',1),
(180,'S-STMT1',0),
(181,'S-STMT2',0),
(182,'S-STMT3',NULL),
(183,'C-WSDR1',0),
(184,'C-WSDR2',0),
(185,'C-WSDR3',0),
(186,'C-WSDR4',0),
(187,'S-MATCH',0),
(188,'S-MKSLP',0),
(189,'S-ROVTR',0),
(190,'S-2FLET',0),
(191,'S-LPIRV',0),
(192,'X-NSFFD',0),
(193,'X-NSFFW',0);

-- updates 
UPDATE  WSDOT_STAGE.wsdot.leakage_reason_codes SET  leakage_cat_id = 3 WHERE  entry_reason_id = 33;	-- 2021-10-18 set D-OTHER to leak type 3
UPDATE  WSDOT_STAGE.wsdot.leakage_reason_codes SET  leakage_cat_id = 0 WHERE  entry_reason_id = 91;	-- 2021-10-28 set S-LPDUP to adjustment 0
UPDATE  WSDOT_STAGE.wsdot.leakage_reason_codes SET  leakage_cat_id = 0 WHERE  entry_reason_id = 98;	-- 2021-10-28 set S-TGDUP to adjustment 0
UPDATE  WSDOT_STAGE.wsdot.leakage_reason_codes SET  leakage_cat_id = 2 WHERE  entry_reason_id = 83;	-- 2021-10-28 set R-SOLDR to leak type 2
UPDATE  WSDOT_STAGE.wsdot.leakage_reason_codes SET  leakage_cat_id = 2 WHERE  entry_reason_id = 30;	-- 2021-10-28 set D-SOLDF to leak type 2

-- view to link leakage with reason codes
-- DROP VIEW wsdot.v_leakage_reasons;
CREATE OR ALTER VIEW wsdot.v_leakage_reasons AS
SELECT  lrc.entry_reason_id
		,lrc.er_short_code 
		,eer.description AS entry_reason_desc
		,lrc.leakage_cat_id
		,lc.short_code AS leakage_code
		,lc.category_description AS leakage_cat_desc
  FROM  WSDOT_STAGE.wsdot.leakage_reason_codes lrc
  LEFT  JOIN fastlane.xact.EnuEntryReason eer ON eer.entryReasonID = lrc.entry_reason_id
  LEFT  JOIN WSDOT_STAGE.wsdot.leakage_categories lc ON lc.leakage_cat_id = lrc.leakage_cat_id;

SELECT * FROM WSDOT_STAGE.wsdot.v_leakage_reasons vlr
-- WHERE vlr.entry_reason_id IN (91,98,83,30,33)
ORDER BY vlr.er_short_code;

/********** MANUAL TRIPS, AXLES & TOLL RATES **********/
-- TNB manual trips (lanes 1-6 at toll plaza)
-- NOTE: to convert tabs to commas in vim use :%s/\t/,/g
-- 2022-05-19: imported data from 2021-01-01 thru 2022-03-31
-- 2022-09-28: imported data from 2022-04-01 thru 2022-07-31
-- 2022-10-10: re-imported all data from 2021-01-01 thru 2022-08-31
-- TRUNCATE TABLE WSDOT_STAGE.wsdot.tnb_manual_trips;
-- DROP TABLE WSDOT_STAGE.wsdot.tnb_manual_trips;
CREATE TABLE WSDOT_STAGE.wsdot.tnb_manual_trips (
	trip_date date PRIMARY KEY
	,trip_count int NOT NULL DEFAULT 0
);

-- check current values
SELECT  min(trip_date) AS min_date
		,max(trip_date) AS max_date
		,count(*) AS total_days
		,datediff(day, min(trip_date), max(trip_date)) + 1 AS expected_days
		,sum(trip_count) AS total_trips
  FROM  WSDOT_STAGE.wsdot.tnb_manual_trips;

-- view for monthly summary
-- DROP VIEW wsdot.v_tnb_manual_monthly_trips;
CREATE OR ALTER VIEW wsdot.v_tnb_manual_monthly_trips AS
SELECT  WSDOT_STAGE.util.f_get_month(t.trip_date) AS trip_month
		,sum(t.trip_count) AS trip_count
  FROM  WSDOT_STAGE.wsdot.tnb_manual_trips t
GROUP BY WSDOT_STAGE.util.f_get_month(t.trip_date);

-- TNB monthly manual trips by number of axles - needed to get correct manual toll rate
-- NOTE: these do not always match trips; impute axles to match trips
-- 2022-09-28: imported data from 2021-01-01 thru 2022-07-31
-- 2022-10-10: re-imported all data from 2021-01-01 thru 2022-08-31
-- TRUNCATE TABLE WSDOT_STAGE.wsdot.tnb_manual_trips_by_axles;
-- DROP TABLE WSDOT_STAGE.wsdot.tnb_manual_trips_by_axles;
CREATE TABLE WSDOT_STAGE.wsdot.tnb_manual_trips_by_axles (
	trip_month date NOT NULL
	,axles smallint NOT NULL
	,trip_count int NOT NULL
	,CONSTRAINT pk_tnb_manual_trips_by_axles PRIMARY KEY (trip_month, axles)
);

-- check current values
SELECT  min(trip_month) AS min_month
		,max(trip_month) AS max_month
		,count(*) AS total_records
		,count(DISTINCT trip_month) * 5 AS expected_records
		,sum(trip_count) AS total_trips
  FROM  WSDOT_STAGE.wsdot.tnb_manual_trips_by_axles;

-- view to normalize axle counts to observed counts
-- first order approximation computes rounded proportional difference to match distribution of axles
-- due to rounding, sums may not match so final adjustment corrects 2-axle count to match
-- DROP VIEW wsdot.v_tnb_manual_adj_trips_by_axles;
CREATE OR ALTER VIEW wsdot.v_tnb_manual_adj_trips_by_axles AS
SELECT  adj.trip_month
		,adj.axles
		,adj.orig_count
		,adj.trip_count AS first_adj_count
		,adj.target_total
		,CASE WHEN adj.axles = 2 THEN adj.trip_count + adj.target_total - sum(adj.trip_count) OVER (PARTITION BY adj.trip_month) 
			ELSE adj.trip_count END AS trip_count
  FROM  (SELECT ta.trip_month
				,ta.axles
				,ta.trip_count AS orig_count
				,t.trip_count AS target_total
				-- adds proportion of correction following distribution of trip count by axle 
				,ta.trip_count + round(ta.trip_count*1.0/t.trip_count * (t.trip_count - sum(ta.trip_count) OVER (PARTITION BY ta.trip_month)),0) AS trip_count
		  FROM  WSDOT_STAGE.wsdot.tnb_manual_trips_by_axles ta
		  JOIN  WSDOT_STAGE.wsdot.v_tnb_manual_monthly_trips t ON ta.trip_month = t.trip_month
		) adj;

-- check that sums always match (should return no results)
SELECT  ta.trip_month
		,min(ta.target_total) AS target
		,sum(ta.trip_count) AS total
  FROM  WSDOT_STAGE.wsdot.v_tnb_manual_adj_trips_by_axles ta
GROUP BY ta.trip_month
HAVING  sum(ta.trip_count) <> min(ta.target_total);

-- TNB manual (cash) toll rates
-- DROP TABLE WSDOT_STAGE.wsdot.tnb_manual_toll_rates;
CREATE TABLE WSDOT_STAGE.wsdot.tnb_manual_toll_rates (
	axles smallint NOT NULL
	,toll_rate decimal(7,2) NOT NULL
	,start_date datetime2 NOT NULL
	,end_date datetime2 NOT NULL
	,CONSTRAINT pk_tnb_manual_toll_rates PRIMARY KEY (axles, toll_rate, start_date)
);

-- fill in data from https://apps.leg.wa.gov/WAC/default.aspx?cite=468-270-070
-- use web.archive.org for historical info
-- see also: https://wstc.wa.gov/programs/tolling/tacoma-narrows-bridge/
--           https://wsdot.wa.gov/travel/roads-bridges/toll-roads-bridges-tunnels/tacoma-narrows-bridge-tolling
-- TRUNCATE TABLE WSDOT_STAGE.wsdot.tnb_manual_toll_rates;
INSERT INTO WSDOT_STAGE.wsdot.tnb_manual_toll_rates VALUES
(2,6.00,'2015-07-01','2021-09-30 23:59:59.999'),
(3,9.00,'2015-07-01','2021-09-30 23:59:59.999'),
(4,12.00,'2015-07-01','2021-09-30 23:59:59.999'),
(5,15.00,'2015-07-01','2021-09-30 23:59:59.999'),
(6,18.00,'2015-07-01','2021-09-30 23:59:59.999'),
(2,6.25,'2021-10-01','2022-09-30 23:59:59.999'),
(3,9.40,'2021-10-01','2022-09-30 23:59:59.999'),
(4,12.50,'2021-10-01','2022-09-30 23:59:59.999'),
(5,15.65,'2021-10-01','2022-09-30 23:59:59.999'),
(6,18.75,'2021-10-01','2022-09-30 23:59:59.999'),
(2,5.50,'2022-10-01','9999-12-31 23:59:59.999'),
(3,8.25,'2022-10-01','9999-12-31 23:59:59.999'),
(4,11.00,'2022-10-01','9999-12-31 23:59:59.999'),
(5,13.75,'2022-10-01','9999-12-31 23:59:59.999'),
(6,16.50,'2022-10-01','9999-12-31 23:59:59.999');

SELECT * FROM WSDOT_STAGE.wsdot.tnb_manual_toll_rates ORDER BY 3,1;

-- create manual disposition summary for union with etan disposition
-- NOTE: cast NULL to avoid type mismatch on UNION ALL: https://stackoverflow.com/a/19616412
-- 2023-01-30: added received month, which is always month after trip month for manual trips
-- DROP VIEW wsdot.v_tnb_manual_disposition AS
CREATE OR ALTER VIEW wsdot.v_tnb_manual_disposition AS
SELECT  t.trip_month
		,dateadd(month, 1, t.trip_month) AS etan_received_month
		,'TNB' AS roadway
		,3 AS roadside_toll_type	-- 3 is 'CASH' in fastlane.rtoll.EnuRoadsideTollType 
		,t.axles AS axle_count
		,'TOLL' AS transaction_type	-- best fit for manual paid
		,'MAN' AS pricing_plan		-- made-up plan; not in fastlane.xact.EnuPricingPlan
		,'d-ORIG' AS entry_reason	-- use d-ORIG as entry reason
		,cast(NULL AS varchar(16)) AS leak_or_adj
		,'PAID_MANUAL' AS disposition_category
		,-1 * (t.trip_count * r.toll_rate) AS sum_roadside_amount
		,-1 * (t.trip_count * r.toll_rate) AS sum_toll_amount
		,0 AS cancelled_count
		,t.trip_count AS fully_allocated_count
		,0 AS unpaid_count
		,t.trip_count AS trip_count
		,t.trip_count AS record_count
  FROM  WSDOT_STAGE.wsdot.v_tnb_manual_adj_trips_by_axles t
  JOIN  WSDOT_STAGE.wsdot.tnb_manual_toll_rates r
		 ON r.axles = t.axles
		AND t.trip_month BETWEEN r.start_date AND r.end_date;

/********** WSDOT TOLL HOLIDAYS **********/
-- table to identify holidays for toll rates
-- used in monthly avg toll rate query
-- TODO: may need edits to accommodate future info
-- DROP TABLE WSDOT_STAGE.wsdot.holidays;
CREATE TABLE WSDOT_STAGE.wsdot.holidays (
	holiday_date date PRIMARY KEY
	,holiday_name varchar(32) NOT NULL
	,weekend_rates bit NOT NULL DEFAULT 0
);

-- holidays from WSDOT and observed dates checked with https://www.officeholidays.com/countries/usa/washington/2018
INSERT INTO WSDOT_STAGE.wsdot.holidays VALUES
('2017-01-01','New Year''s Day',1),
('2017-01-02','New Year''s Day (observed)',1),
('2017-05-29','Memorial Day',1),
('2017-07-04','Independence Day',1),
('2017-09-04','Labor Day',1),
('2017-11-23','Thanksgiving',1),
('2017-12-25','Christmas Day',1),
('2018-01-01','New Year''s Day',1),
('2018-05-28','Memorial Day',1),
('2018-07-04','Independence Day',1),
('2018-09-03','Labor Day',1),
('2018-11-22','Thanksgiving',1),
('2018-12-25','Christmas Day',1),
('2019-01-01','New Year''s Day',1),
('2019-05-27','Memorial Day',1),
('2019-07-04','Independence Day',1),
('2019-09-02','Labor Day',1),
('2019-11-28','Thanksgiving',1),
('2019-12-25','Christmas Day',1),
('2020-01-01','New Year''s Day',1),
('2020-05-25','Memorial Day',1),
('2020-07-03','Independence Day (observed)',1),
('2020-07-04','Independence Day',1),
('2020-09-07','Labor Day',1),
('2020-11-26','Thanksgiving',1),
('2020-12-25','Christmas Day',1),
('2021-01-01','New Year''s Day',1),
('2021-05-31','Memorial Day',1),
('2021-07-04','Independence Day',1),
('2021-07-05','Independence Day (observed)',1),
('2021-09-06','Labor Day',1),
('2021-11-25','Thanksgiving',1),
('2021-12-24','Christmas Day (observed)',1),
('2021-12-25','Christmas Day',1),
('2021-12-31','New Year''s Day (observed)',1),
('2022-01-01','New Year''s Day',1),
('2022-05-30','Memorial Day',1),
('2022-07-04','Independence Day',1),
('2022-09-05','Labor Day',1),
('2022-11-24','Thanksgiving',1),
('2022-12-25','Christmas Day',1),
('2022-12-26','Christmas Day (observed)',1),
('2023-01-01','New Year''s Day',1),
('2023-01-02','New Year''s Day (observed)',1),
('2023-05-29','Memorial Day',1),
('2023-07-04','Independence Day',1),
('2023-09-04','Labor Day',1),
('2023-11-23','Thanksgiving',1),
('2023-12-25','Christmas Day',1),
('2024-01-01','New Year''s Day',1),
('2024-05-27','Memorial Day',1),
('2024-07-04','Independence Day',1),
('2024-09-02','Labor Day',1),
('2024-11-28','Thanksgiving',1),
('2024-12-25','Christmas Day',1);

/********** ANALYSIS TIME PERIODS **********/
-- table of typical time periods for time-of-day analysis
-- used in monthly avg toll rate query
-- use full precesion for time: https://learn.microsoft.com/en-us/sql/t-sql/data-types/time-transact-sql?view=sql-server-ver16
-- DROP TABLE WSDOT_STAGE.wsdot.time_periods;
CREATE TABLE WSDOT_STAGE.wsdot.time_periods (
	start_time time(7) PRIMARY KEY
	,end_time time(7) NOT NULL
	,period_name varchar(16) NOT NULL
	,sort_order smallint NOT NULL
);

INSERT INTO WSDOT_STAGE.wsdot.time_periods VALUES
('00:00:00.0000000','04:59:59.9999999','night',5),
('05:00:00.0000000','08:59:59.9999999','am_peak',1),
('09:00:00.0000000','14:59:59.9999999','midday',2),
('15:00:00.0000000','18:59:59.9999999','pm_peak',3),
('19:00:00.0000000','22:59:59.9999999','evening',4),
('23:00:00.0000000','23:59:59.9999999','night',5);

/********** HASH KEYS **********/
-- preserve anonymization keys used for hashing data to enable future re-hashing
-- NOTE: these are not technically salts as they are not unique for each record
--		 this is necessary because in this case we are only anonymizing, but need to 
--		 preserve same acct/tag/plate info for matching: https://stackoverflow.com/q/21561516
--		 the upside is that the acct/tag/plate info is quite unique for each user
-- NOTE: use IDENTITY (seed,increment) to create an auto-incrementing field
-- DROP TABLE WSDOT_STAGE.wsdot.hashkeys;
CREATE TABLE WSDOT_STAGE.wsdot.hashkeys (
	id int IDENTITY(1,1) PRIMARY KEY
    ,purpose varchar(64) NOT NULL
    ,hashkey varchar(64) NOT NULL
    ,created_at datetime2(2) NOT NULL DEFAULT getdate()
);

-- insert values
INSERT INTO WSDOT_STAGE.wsdot.hashkeys (purpose, hashkey) VALUES ('TRAC Toll Equity Study', 'correcthorsebatterystaple');

/********** CENSUS RESULTS FROM GEOCODING **********/
-- results geocoded from geocodio (and perhaps other providers in the future)
-- NOTE: could also import full list from census, but only needed for linking to geocoding results

-- fips places
-- DROP TABLE WSDOT_STAGE.wsdot.census_fips_places;
CREATE TABLE WSDOT_STAGE.wsdot.census_fips_places (
	fips_place varchar(7) NOT NULL
	,census_year smallint NOT NULL
	,place_name varchar(128) NOT NULL
	,CONSTRAINT pk_census_fips_place PRIMARY KEY (fips_place, census_year)
);

-- metro/micro statistical areas
-- DROP TABLE WSDOT_STAGE.wsdot.census_metro_micro_statistical_areas;
CREATE TABLE WSDOT_STAGE.wsdot.census_metro_micro_statistical_areas (
	code varchar(5) NOT NULL
	,census_year smallint NOT NULL
	,name varchar(128) NOT NULL
	,area_type varchar(32) NOT NULL
	,CONSTRAINT pk_census_metro_micro_statistical_areas PRIMARY KEY (code, census_year)
);

-- combined statistical areas
-- DROP TABLE WSDOT_STAGE.wsdot.census_combined_statistical_areas;
CREATE TABLE WSDOT_STAGE.wsdot.census_combined_statistical_areas (
	code varchar(3) NOT NULL
	,census_year smallint NOT NULL
	,name varchar(128) NOT NULL
	,CONSTRAINT pk_census_combined_statistical_areas PRIMARY KEY (code, census_year)
);

-- metropolitan division areas
-- DROP TABLE WSDOT_STAGE.wsdot.census_metropolitan_division_areas;
CREATE TABLE WSDOT_STAGE.wsdot.census_metropolitan_division_areas (
	code varchar(5) NOT NULL
	,census_year smallint NOT NULL
	,name varchar(128) NOT NULL
	,CONSTRAINT pk_census_metropolitan_division_areas PRIMARY KEY (code, census_year)
);

/********** GEOCODING RESULTS **********/
-- lookups and results of geocoding

-- enumeration of geocoding providers
-- DROP TABLE WSDOT_STAGE.wsdot.geocoding_providers;
CREATE TABLE WSDOT_STAGE.wsdot.geocoding_providers (
	provider_id smallint NOT NULL PRIMARY KEY
	,name varchar(32) NOT NULL
	,website varchar(128) NOT NULL
);

INSERT INTO WSDOT_STAGE.wsdot.geocoding_providers VALUES
(0, 'geocodio', 'https://www.geocod.io/');

-- enumeration of accuracy types by provider
-- DROP TABLE WSDOT_STAGE.wsdot.geocoding_accuracy_types;
CREATE TABLE WSDOT_STAGE.wsdot.geocoding_accuracy_types (
	provider_id smallint NOT NULL
	,accuracy_type_id smallint NOT NULL
	,short_code varchar(32) NOT NULL
	,description varchar(128) NOT NULL
	,CONSTRAINT pk_geocoding_accuracy_types PRIMARY KEY (provider_id, accuracy_type_id)
);

INSERT INTO WSDOT_STAGE.wsdot.geocoding_accuracy_types VALUES
(0, 0, 'uncoded', 'unable to geocode address'),
(0, 1, 'state', 'state centroid'),
(0, 2, 'county', 'county centroid'),
(0, 3, 'nearest_place', 'closest city/town/place'),
(0, 4, 'place', 'zip code or city centroid'),
(0, 5, 'nearest_street', 'nearest match for a specific street with estimated street number'),
(0, 6, 'street_center', 'a central point on the street'),
(0, 7, 'intersection', 'an intersection between two streets'),
(0, 8, 'nearest_rooftop_match', 'the nearest rooftop point if the exact point is unavailable'),
(0, 9, 'range_interpolation', 'generally, in front of the parcel on the street'),
(0, 10, 'point', 'generally, in front of the parcel on the street'),
(0, 11, 'rooftop', 'on the exact parcel');

-- geocoding results table
-- binary sha256 is 32 char long; allow for future sha512 hash length as well
-- DROP TABLE WSDOT_STAGE.wsdot.geocoding_results;
CREATE TABLE WSDOT_STAGE.wsdot.geocoding_results (
	address_hash varbinary(64) NOT NULL
	,provider_id smallint NOT NULL
	,lookup_date date NOT NULL
	,address varchar(256) NOT NULL
	,city varchar(100) NOT NULL
	,state varchar(13) NOT NULL
	,postal_code varchar(16) NOT NULL
	,country varchar(256) NOT NULL
	,latitude numeric(9,6)
	,longitude numeric(9,6)
	,accuracy_score numeric(4,3) NOT NULL
	,accuracy_type_id smallint NOT NULL
	,gc_number varchar(32)
	,gc_street varchar(128)
	,gc_unit_type varchar(16)
	,gc_unit_number varchar(64)
	,gc_city varchar(96)
	,gc_state varchar(16)
	,gc_county varchar(96)
	,gc_zip varchar(16)
	,gc_country varchar(64)
	,gc_source varchar(128)
	,census_year smallint
	,fips_state varchar(2)
	,fips_county varchar(5)
	,fips_place varchar(7)
	,census_tract_code varchar(6)
	,census_block_code varchar(4)
	,census_block_group varchar(1)
	,fips_block varchar(15)
	,fips_tract varchar(11)
	,metro_micro_statistical_area_code varchar(5)
	,combined_statistical_area_code varchar(3)
	,metropolitan_division_area_code varchar(5)
	,CONSTRAINT pk_geocoding_results PRIMARY KEY (address_hash, provider_id, lookup_date)
);

-- add indices on lookup fields
CREATE NONCLUSTERED INDEX ix_accuracy_type_id ON wsdot.geocoding_results (accuracy_type_id);
CREATE NONCLUSTERED INDEX ix_census_year ON wsdot.geocoding_results (census_year);
CREATE NONCLUSTERED INDEX ix_fips_place ON wsdot.geocoding_results (fips_place);
CREATE NONCLUSTERED INDEX ix_metro_micro_statistical_area_code ON wsdot.geocoding_results (metro_micro_statistical_area_code);
CREATE NONCLUSTERED INDEX ix_combined_statistical_area_code ON wsdot.geocoding_results (combined_statistical_area_code);
CREATE NONCLUSTERED INDEX ix_metropolitan_division_area_code ON wsdot.geocoding_results (metropolitan_division_area_code);

/*######################################################################*/
/*##########                    REPORTING                     ##########*/
/*######################################################################*/

/********** TABLE SHELLS FOR SCHEDULED REPORTING **********/
-- pass fulfillment
-- DROP TABLE WSDOT_STAGE.rpt.x_pass_fulfillment;
CREATE TABLE WSDOT_STAGE.rpt.x_pass_fulfillment (
	request_date date
	,req_source varchar(32)
	,request_type varchar(255)
	,pass_type varchar(64)
	,request_status varchar(64)
	,fulfilled_count smallint
	,pass_count smallint
);

-- trip posting summary
-- DROP TABLE WSDOT_STAGE.rpt.x_trip_posting_summary;
CREATE TABLE WSDOT_STAGE.rpt.x_trip_posting_summary (
	trip_date date
	,posting_date date
	,roadway varchar(16)
	,roadside_toll_type varchar(16)
	,dup_flag smallint
	,sum_roadside_amount decimal(16,2)
	,trip_count integer
);

-- disposition summary
-- DROP TABLE WSDOT_STAGE.rpt.disposition_summary;
CREATE TABLE WSDOT_STAGE.rpt.disposition_summary (
	trip_month date NOT NULL
	,etan_received_month date NOT NULL
	,roadway varchar(16) NOT NULL
	,roadside_toll_type int NOT NULL
	,axle_count tinyint NOT NULL
	,transaction_type varchar(32) NOT NULL
	,pricing_plan varchar(16) NOT NULL
	,entry_reason varchar(16) NOT NULL
	,leak_or_adj varchar(16)
	,disposition_category varchar(16) NOT NULL
	,sum_roadside_amount decimal(16,2)
	,sum_toll_amount decimal(16,2)
	,cancelled_count integer
	,fully_allocated_count integer
	,unpaid_count integer
	,trip_count integer
	,record_count integer
	,asof_date date
);

-- posting performance
-- DROP TABLE WSDOT_STAGE.rpt.x_posting_performance;
CREATE TABLE WSDOT_STAGE.rpt.x_posting_performance (
	trip_month date NOT NULL
	,roadway varchar(16) NOT NULL
	,hours_to_post_goal smallint NOT NULL
	,min_hours_to_post smallint
	,max_hours_to_post smallint
	,avg_hours_to_post smallint
	,met_avg_hours_to_post smallint
	,unmet_avg_hours_to_post smallint
	,met_goal_count integer
	,trip_count integer
	,pct_met_goal real
);

-- icrs summary
-- DROP TABLE WSDOT_STAGE.rpt.icrs_summary;
CREATE TABLE WSDOT_STAGE.rpt.icrs_summary (
	trip_date date
	,facility varchar(16)
	,isrequested bit
	,isresponded bit
	,dispositionNullFlag bit
	,disputeNullFlag bit
	,disposition varchar(64)
	,ir_result varchar(32)
	,ir_dispute varchar(48)
	,created_intrip date
	,created_intpid date
	,created_irr_req date
	,created_irr_disp date
	,diff_created_trip smallint
	,diff_created_ireq smallint
	,diff_created_irr_disp smallint
	,time_kapsch smallint
	,time_etan smallint
	,time_qfree smallint
	,queried_at date
	,trip_count int
);

-- image download summary
-- DROP TABLE WSDOT_STAGE.rpt.image_download_summary;
CREATE TABLE WSDOT_STAGE.rpt.image_download_summary (
	trip_date date
	,roadway varchar(16)
	,roadsidetolltype int
	,tollStatusCodeId tinyint
	,ttl_requested int
	,ttl_downloaded_t int
	,ttl_nullstatus int
	,ttl_downloaded_f int
	,ttl_yes_dl_image int
	,ttl_no_dl_image int
	,ttl_inLance int
	,ttl_notinImage int
	,tp_count int
	,queried_at date
);

-- negative balance escalation preparation
-- 2022-11-07: created to track categories of negative balances
-- DROP TABLE WSDOT_STAGE.rpt.x_negative_balance_detail;
CREATE TABLE WSDOT_STAGE.rpt.x_negative_balance_detail (
	customerAccountId int PRIMARY KEY NOT NULL
	,acct_category varchar(8)
	,amount_class varchar(8)
	,days_since_negative smallint
	,latest_recd_statement_date datetime2
	,cc_failed smallint
	,valid_email smallint
	,category_id varchar(4)
);

-- negative balance escalation history
-- 2022-12-14: created to track history of negative balances for accounts
-- DROP TABLE WSDOT_STAGE.rpt.negative_balance_history;
CREATE TABLE WSDOT_STAGE.rpt.negative_balance_history (
	customerAccountId int NOT NULL
	,acct_category varchar(8)
	,amount_class varchar(8)
	,days_since_negative smallint
	,latest_recd_statement_date datetime2
	,cc_failed smallint
	,valid_email smallint
	,category_id varchar(4)
	,acct_type_desc varchar(64)
	,acct_status varchar(64)
	,balance_due decimal(26,2) NOT NULL
	,neg_balance_since datetime NOT NULL
	,balance_last_updated datetime NOT NULL
	,run_date date NOT NULL
	,PRIMARY KEY (customerAccountId, run_date)
);

-- view to get latest result from disposition summary
-- DROP VIEW rpt.v_latest_disposition;
CREATE OR ALTER VIEW rpt.v_latest_disposition AS
SELECT * FROM WSDOT_STAGE.rpt.disposition_summary
 WHERE asof_date = (SELECT max(asof_date) FROM WSDOT_STAGE.rpt.disposition_summary);

/********** PASS FULFILLMENT **********/
-- report for Jenny Ugale, Maria Flesher, and Bobbie Jean Stanley
-- updated 2021-11-30 to add new vs existing account info
-- updated 2023-02-06 to use new pass request view

-- DROP VIEW rpt.v_pass_fulfillment;
CREATE OR ALTER VIEW rpt.v_pass_fulfillment AS
SELECT  cast(pr.request_datetime AS date) AS request_date
		,pr.req_source
		,pr.request_type
		,pr.pass_type
		,pr.request_status
		,sum(pr.fulfilled) AS fulfilled_count
		,count(*) AS pass_count
  FROM  WSDOT_STAGE.util.v_pass_requests pr
GROUP BY cast(pr.request_datetime AS date)
		,pr.req_source
		,pr.request_type
		,pr.pass_type
		,pr.request_status;

/* legacy version using bad ETAN view:
CREATE OR ALTER VIEW rpt.v_pass_fulfillment AS
SELECT  cast(vitr.requestDate AS date) AS request_date
		,vitr.isSource AS req_source
		,CASE WHEN ca.legacyAccountNumber IS NULL THEN 'New ETAN Account' ELSE 'Converted ETCC Account' END AS account_desc
		,CASE WHEN vitr.invTagRequestStatusId = 3 THEN 'Fulfilled' ELSE 'Unfulfilled' END AS fulfillment_status
		--,vitr.transponderType AS transponder_type
		,vitr.transponderRequestStatus AS request_status
		--,ca.customerAccountId
		,count(*) AS pass_count
  FROM  fastlane.inv.V_InvTagRequest vitr
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = vitr.customerAccountId
 WHERE  vitr.requestDate >= convert(date, '2021-07-10')
--   AND  invTagRequestStatusId <> 3
GROUP BY cast(vitr.requestDate AS date)
		,vitr.isSource
		,CASE WHEN ca.legacyAccountNumber IS NULL THEN 'New ETAN Account' ELSE 'Converted ETCC Account' END
		,CASE WHEN vitr.invTagRequestStatusId = 3 THEN 'Fulfilled' ELSE 'Unfulfilled' END
		--,vitr.transponderType
		,vitr.transponderRequestStatus
		--,ca.customerAccountId
;
*/

/********** TRIP POSTING SUMMARY **********/
-- report for Tyler, Marie, Sarah, and Ami
-- summarize trips by trip and posting date, counting duplicates (system accounts 16 and 120)
-- updated 2022-05-17 to improve performance and get duplicates older than 7 days
-- updated 2022-07-27 (see ad-hoc test) to add roadside toll amount
-- DROP VIEW rpt.v_trip_posting_summary;
CREATE OR ALTER VIEW rpt.v_trip_posting_summary AS
SELECT  cast(tp.laneTime AS date) AS trip_date
		,cast(t.created AS date) AS posting_date
		,vfl.wsdot_roadway AS roadway
		,ertt.shortCode AS roadside_toll_type
		,CASE WHEN d.tollId IS NOT NULL THEN 1 ELSE 0 END AS dup_flag	-- added to filter duplicates
		,sum(ta.amount) AS sum_roadside_amount		-- added to get roadside toll amount
		--,count(d.tollId) AS dup_count				-- removed with flag added above
		,count(*) AS trip_count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tp.roadsideTollType
  JOIN  fastlane.lance.TripAmount ta ON ta.tripId = t.tripId	-- added to get roadside toll amount
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  LEFT  JOIN (SELECT DISTINCT tollId FROM fastlane.xact.TollLedgerEntry WHERE ledgeraccountId IN (16,120)) d ON d.tollId = t.tripId 
GROUP BY cast(tp.laneTime AS date)
		,cast(t.created AS date)
		,vfl.wsdot_roadway
		,ertt.shortCode
		,CASE WHEN d.tollId IS NOT NULL THEN 1 ELSE 0 END;	-- added: filter by duplicates rather than count

/********** ACCOUNT CREATION IN ETAN **********/
-- account creation since 2021-07-10
-- DROP VIEW rpt.v_account_creation;
CREATE OR ALTER VIEW rpt.v_account_creation AS
SELECT  cast(ca.postingDate AS date) AS posting_date
		,ca.acct_type_desc AS account_type
		,count(*) AS acct_count
  FROM  WSDOT_STAGE.util.v_customer_account ca
 WHERE  ca.postingDate > convert(date,'2021-07-10')
GROUP BY cast(ca.postingDate AS date)
		,ca.acct_type_desc;

----- account type trends
/********** ACCOUNT TYPES AS OF DATE **********/
-- use function to provide as-of date (remember system_time is in UTC!)
-- default is current time in UTC
-- NOTE: unable to create function with default set to getutcdate()
-- DROP FUNCTION rpt.f_account_types_asof;
CREATE OR ALTER FUNCTION rpt.f_account_types_asof(@dtm datetime2)
RETURNS @ret_table TABLE (
	as_of_date datetime2
	,account_type_id bigint
	,account_type varchar(1000)
	,account_count int
) AS
BEGIN
	IF @dtm IS NULL SET @dtm = getutcdate();
	INSERT INTO @ret_table
	SELECT  @dtm as_of_date
			,at2.account_type_id 
			,eat.description AS account_type
			,count(*) AS account_count
	  FROM  WSDOT_STAGE.util.account_type FOR SYSTEM_TIME AS OF @dtm at2
	  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = at2.account_type_id
	GROUP BY at2.account_type_id 
			,eat.description;
	RETURN;
END;

/********** POSTING PERFORMANCE **********/
-- this measures time to post, time from when a trip occurs until it is sent to ETAN
-- poor performance reflects on Kapsch/TransCore, not ETAN
-- DROP VIEW rpt.v_posting_performance;
CREATE OR ALTER VIEW rpt.v_posting_performance AS
SELECT  d.trip_month
		,d.roadway
		,d.hours_to_post_goal
		,min(d.posting_delay) AS min_hours_to_post
		,max(d.posting_delay) AS max_hours_to_post
		,avg(d.posting_delay) AS avg_hours_to_post
		,avg(CASE WHEN d.posting_delay < d.hours_to_post_goal THEN d.posting_delay ELSE NULL END) AS met_avg_hours_to_post
		,avg(CASE WHEN d.posting_delay < d.hours_to_post_goal THEN NULL ELSE d.posting_delay END) AS unmet_avg_hours_to_post
		,sum(CASE WHEN d.posting_delay < d.hours_to_post_goal THEN 1 ELSE 0 END) AS met_goal_count
		,count(*) AS trip_count
		,sum(CASE WHEN d.posting_delay < d.hours_to_post_goal THEN 1.0 ELSE 0 END) / count(*) AS pct_met_goal
  FROM  (SELECT vfl.wsdot_roadway AS roadway
				,vfl.hours_to_post_goal 
				,WSDOT_STAGE.util.f_get_month(tp.laneTime) AS trip_month
				,datediff(hour, tp.laneTime, t.created) AS posting_delay	-- truncates fractional hour
		   FROM fastlane.lance.Trip t 
		   JOIN fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
		   JOIN WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId
		  WHERE tp.laneTime > convert(date,'2021-07-10')
		) d
GROUP BY d.trip_month
		,d.roadway
		,d.hours_to_post_goal;

/********** DISPOSITION SUMMARY **********/
-- DROP VIEW rpt.v_disposition_summary;
CREATE OR ALTER VIEW rpt.v_disposition_summary AS
SELECT  t.trip_month
		,t.etan_received_month 
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,t.entry_reason
		,t.leakage_code AS leak_or_adj
		,t.disposition_category
		,sum(t.roadside_amount) AS sum_roadside_amount
		,sum(t.latest_toll_amount) AS sum_toll_amount	-- okay for now since transaction types seperate positive (WRITEOFF, TOLLDISC) from others
		,sum(cast(t.isCancelled AS integer)) AS cancelled_count
		,sum(cast(t.isFullyAllocated AS integer)) AS fully_allocated_count
		,sum(cast(t.isUnpaid AS integer)) AS unpaid_count
		,count(DISTINCT t.trip_id) AS trip_count
		,count(*) AS record_count
  FROM  WSDOT_STAGE.util.v_trip_detail_full t
GROUP BY t.trip_month
		,t.etan_received_month 
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,t.entry_reason
		,t.leakage_code
		,t.disposition_category
UNION ALL
SELECT * FROM  wsdot.v_tnb_manual_disposition m;

/********** FORECASTING SUMMARY **********/
-- redacted summary for forecasting - coalesce tolldisc and leaks
-- NOTE: uses latest data in rpt.v_latest_disposition
-- DROP VIEW rpt.v_forecasting_summary;
CREATE OR ALTER VIEW rpt.v_forecasting_summary AS
SELECT  t.trip_month
		,t.etan_received_month
		,datediff(month, t.trip_month, t.etan_received_month) AS month_delay
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
              WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
              ELSE t.disposition_category
              END AS disposition_category
		,sum(t.sum_roadside_amount) AS sum_roadside_amount
		,sum(t.sum_toll_amount) AS sum_toll_amount
        ,sum(t.trip_count) AS trip_count
  FROM  WSDOT_STAGE.rpt.v_latest_disposition t
GROUP BY t.trip_month
		,t.etan_received_month
		,datediff(month, t.trip_month, t.etan_received_month)
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
			  WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
			  ELSE t.disposition_category
			  END;

-- recent forecasting summary for off-cycle updates
-- DROP VIEW rpt.v_recent_forecasting_summary;
CREATE OR ALTER VIEW rpt.v_recent_forecasting_summary AS
SELECT  t.trip_month
		,t.etan_received_month
		,datediff(month, t.trip_month, t.etan_received_month) AS month_delay
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
              WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
              ELSE t.disposition_category
              END AS disposition_category
		,sum(t.sum_roadside_amount) AS sum_roadside_amount
		,sum(t.sum_toll_amount) AS sum_toll_amount
        ,sum(t.trip_count) AS trip_count
  FROM  WSDOT_STAGE.rpt.v_disposition_summary t
 WHERE  t.trip_month >= convert(date, '2021-01-01')		-- add in start month since running manually
GROUP BY t.trip_month
		,t.etan_received_month
		,datediff(month, t.trip_month, t.etan_received_month)
		,t.roadway
		,t.roadside_toll_type
		,t.axle_count
		,t.transaction_type
		,t.pricing_plan
		,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
			  WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
			  ELSE t.disposition_category
			  END;

/********** ACCT LAST TRIP MONTH **********/
-- indirectly depends on util.v_all_trip_state
-- use left join to keep accounts that have no trips
-- could do full outer for trips no longer associated to a non-defunt account, but we don't care about defunct accounts?
-- consider adding count of tags on account to match ETCC version, though no idea why we should do this
-- DROP VIEW rpt.v_acct_last_trip_month;
CREATE OR ALTER VIEW rpt.v_acct_last_trip_month AS
SELECT  WSDOT_STAGE.util.f_get_month(alt.latest_trip) AS last_trip_month
		,vca.acct_type_desc 
		,vas.status AS acct_status
		,count(*) AS acct_count
  FROM  WSDOT_STAGE.util.v_customer_account vca 
  --JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = vca.customer_account_id
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = vca.customer_account_id
  LEFT  JOIN  WSDOT_STAGE.util.v_acct_latest_trip alt ON alt.customer_account_id = vca.customer_account_id
GROUP BY WSDOT_STAGE.util.f_get_month(alt.latest_trip)
		,vca.acct_type_desc 
		,vas.status;

-- get data
SELECT  *
  FROM  WSDOT_STAGE.rpt.v_acct_last_trip_month valtm
ORDER BY valtm.last_trip_month
		,valtm.acct_type_desc
		,valtm.acct_status;  

/********** PASS LAST ACTIVITY SUMMARY **********/
-- does not depend on util.v_all_trip_state
-- consider adding flag for if used in last two years?
-- DROP VIEW rpt.v_pass_last_activity_summary;
CREATE OR ALTER VIEW rpt.v_pass_last_activity_summary AS
SELECT  vpd.pass_agency 
		,vpd.pass_type 
		,vpd.pass_inv_status 
		,vpd.acct_type_desc 
		,vpd.acct_status
		,WSDOT_STAGE.util.f_get_month(vpd.latest_read) AS latest_trip_month 
		,count(*) AS pass_count
  FROM  WSDOT_STAGE.util.v_pass_detail vpd
GROUP BY vpd.pass_agency 
		,vpd.pass_type 
		,vpd.pass_inv_status 
		,vpd.acct_type_desc 
		,vpd.acct_status
		,WSDOT_STAGE.util.f_get_month(vpd.latest_read);

-- get data
SELECT  *
  FROM  WSDOT_STAGE.rpt.v_pass_last_activity_summary vplas 
-- WHERE  vplas.pass_status = 'Active - Assigned Customer'
ORDER BY vplas.latest_trip_month
		,vplas.pass_count DESC;

/********** ACCOUNTS BY CYCLE DATE **********/
-- DROP VIEW rpt.v_acct_cycle_day_summary;
CREATE VIEW rpt.v_acct_cycle_day_summary AS
SELECT  CASE WHEN vca.acct_type_desc IS NULL THEN 'New Account' ELSE vca.acct_type_desc END AS acct_type_desc
		,vas.status AS acct_status
		,ccd.CycleDay AS cycle_day
		,count(*) AS acct_count
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = ca.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ccd.customeraccountid
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ccd.customerAccountId 
 WHERE  ca.customerAccountId >= 1000	-- not a system account
   AND  ca.isDefunct = 0		-- not defunct
GROUP BY CASE WHEN vca.acct_type_desc IS NULL THEN 'New Account' ELSE vca.acct_type_desc END
		,vas.status
		,ccd.CycleDay;

/********** CSR TRACKING **********/
-- monthly case closure summary for Patricia
-- 2022-08-08: added case queue ids
-- DROP VIEW rpt.v_csr_case_closure_summary;
CREATE OR ALTER VIEW rpt.v_csr_case_closure_summary AS
SELECT  cw.modified_by
		,cw.case_type 
		,cw.case_category
		,cw.case_queue_ids 
		,cw.case_status
		,WSDOT_STAGE.util.f_get_month(cw.case_open_date) AS case_created_month
		,WSDOT_STAGE.util.f_get_month(cw.case_last_update) AS case_updated_month
		,count(*) AS case_count
  FROM  WSDOT_STAGE.util.v_csr_case_work_detail cw
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = cw.wf_state_id
 WHERE  cw.input_user_id <> 2925	-- exclude DB_SCRIPT_USER
   AND  ws.isClosing = 1			-- only closing states
GROUP BY cw.modified_by
		,case_type
		,cw.case_category
		,cw.case_queue_ids 
		,cw.case_status
		,WSDOT_STAGE.util.f_get_month(cw.case_open_date)
		,WSDOT_STAGE.util.f_get_month(cw.case_last_update);

-- daily account closure summary for Tyler
-- DROP VIEW rpt.v_account_closure_summary;
CREATE OR ALTER VIEW rpt.v_account_closure_summary AS
SELECT  cw.modified_by
		,cw.case_status
		,cast(cw.case_last_update AS date) AS last_updated_date
		,count(*) AS case_count
  FROM  WSDOT_STAGE.util.v_csr_case_work_detail cw
 WHERE  cw.case_type_id = 11	-- close account
   AND  cw.wf_state_id = 4		-- closed resolved
GROUP BY cw.modified_by
		,cw.case_status
		,cast(cw.case_last_update AS date);

/********** IMAGE REVIEW REPORTING **********/
-- queries via Michael Gaunt
-- for standard reporting, summarize previous 45 days

-- icrs summary
-- DROP VIEW rpt.v_icrs_summary;
CREATE OR ALTER VIEW rpt.v_icrs_summary AS
SELECT  icrs.trip_date
		,icrs.facility
		,icrs.isrequested
		,icrs.isresponded
		,icrs.dispositionNullFlag
		,icrs.disputeNullFlag
		,icrs.disposition
		,icrs.ir_result
		,icrs.ir_dispute
		,icrs.created_intrip
		,icrs.created_intpid
		,icrs.created_irr_req
		,icrs.created_irr_disp
		,icrs.diff_created_trip
		,icrs.diff_created_ireq
		,icrs.diff_created_irr_disp
		,icrs.time_kapsch
		,icrs.time_etan
		,icrs.time_qfree
		,icrs.queried_at
		,count(*) AS trip_count
  FROM  WSDOT_STAGE.util.v_icrs_detail icrs
GROUP BY icrs.trip_date
		,icrs.facility
		,icrs.isrequested
		,icrs.isresponded
		,icrs.dispositionNullFlag
		,icrs.disputeNullFlag
		,icrs.disposition
		,icrs.ir_result
		,icrs.ir_dispute
		,icrs.created_intrip
		,icrs.created_intpid
		,icrs.created_irr_req
		,icrs.created_irr_disp
		,icrs.diff_created_trip
		,icrs.diff_created_ireq
		,icrs.diff_created_irr_disp
		,icrs.time_kapsch
		,icrs.time_etan
		,icrs.time_qfree
		,icrs.queried_at;

-- image download summary
-- DROP VIEW rpt.v_image_download_summary;
CREATE OR ALTER VIEW rpt.v_image_download_summary AS
SELECT  trip_date
		,roadway
		,roadsidetolltype
		,tollStatusCodeId
		,sum(count_req) AS ttl_requested
		,sum(count_dl) AS ttl_downloaded_t
		,sum(cnt_null) AS ttl_nullstatus
		,sum(cnt_no_dl) AS ttl_downloaded_f
		,sum(flag_yes_dl) AS ttl_yes_dl_image
		,sum(flag_no_dl) AS ttl_no_dl_image
		,sum(flag_inLance) AS ttl_inLance
		,sum(flag_notinImage) AS ttl_notinImage
		,count(*) AS tp_count
		,cast(getdate() AS date) AS queried_at
  FROM  WSDOT_STAGE.util.v_tp_image_download_detail
GROUP BY trip_date
		,roadway
		,roadsidetolltype
		,tollstatuscodeid;

/********** NEGATIVE BALANCE/ESCALATION TRACKING **********/
-- tracking summary for Tyler/Tim Arnold
-- DROP VIEW rpt.v_escalation_go_live_population;
CREATE OR ALTER VIEW rpt.v_escalation_go_live_population AS
SELECT  nb.category_id 
		,count(*) AS acct_count
  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
GROUP BY nb.category_id;

-- account detail view
-- NOTE: if altering, need to update rpt.negative_balance_history table too
-- DROP VIEW rpt.v_escalation_negative_balance_status;
CREATE OR ALTER VIEW rpt.v_escalation_negative_balance_status AS
SELECT  nb.*
		,vca.acct_type_desc 
		,vas.status AS acct_status
		,tb.amount AS balance_due
		,tb.lastSignChangeDate AS neg_balance_since
		,tb.updatedAt AS balance_last_updated
  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb 
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = nb.customerAccountId AND tb.isCurrentActivity = 1
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = nb.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = nb.customerAccountId;

-- detail view with contact info
-- DROP VIEW rpt.v_escalation_negative_balance_detail;
CREATE OR ALTER VIEW rpt.v_escalation_negative_balance_detail AS
SELECT  nbs.*
		,ccd.CycleDay AS cycle_date
		,vaci.isCompany
		,vaci.firstName
		,vaci.lastNameOrCompanyName
		,vaci.mobile_phone
		,vaci.email_address
		,vaci.address1
		,vaci.address2
		,vaci.city
		,vaci.state
		,vaci.postalCode
		,vaci.country
		,vaci.address_source
  FROM  WSDOT_STAGE.rpt.v_escalation_negative_balance_status nbs
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = nbs.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info vaci ON vaci.customerAccountId = nbs.customerAccountId;

-- view of earliest history as reference group for comparison
-- DROP VIEW rpt.v_negative_balance_reference_group;
CREATE OR ALTER VIEW rpt.v_negative_balance_reference_group AS
SELECT  *
  FROM  WSDOT_STAGE.rpt.negative_balance_history nbh  
 WHERE  nbh.run_date = (SELECT min(run_date) FROM WSDOT_STAGE.rpt.negative_balance_history)
   AND  nbh.category_id <> '0';

-- view of latest history for comparison
-- DROP VIEW rpt.v_negative_balance_latest_group;
CREATE OR ALTER VIEW rpt.v_negative_balance_latest_group AS
SELECT  *
  FROM  WSDOT_STAGE.rpt.negative_balance_history nbh  
 WHERE  nbh.run_date = (SELECT max(run_date) FROM WSDOT_STAGE.rpt.negative_balance_history)
   AND  nbh.category_id <> '0';
  
-- comparison between groups
-- DROP VIEW rpt.v_negative_balance_comparison;
CREATE OR ALTER VIEW rpt.v_negative_balance_comparison AS
SELECT  s.ref_category_id
		,s.new_category_id 
		,s.compare
		,count(*) AS account_cnt
  FROM  (SELECT nbr.category_id AS ref_category_id
			,nbl.category_id AS new_category_id
			,CASE WHEN nbr.category_id = nbl.category_id AND nbl.balance_due <= nbr.balance_due THEN 'no_action'
				  WHEN nbr.category_id = nbl.category_id THEN 'owes_less_still_negative'
				  WHEN nbl.category_id IS NULL THEN 'no_longer_negative'
				  WHEN nbr.category_id IS NULL THEN 'new_since_original_run'
				  WHEN nbl.category_id <> nbr.category_id THEN 'changed_category'
				END AS compare
	  FROM  WSDOT_STAGE.rpt.v_negative_balance_reference_group nbr
	  FULL  OUTER JOIN WSDOT_STAGE.rpt.v_negative_balance_latest_group nbl ON nbl.customerAccountId = nbr.customerAccountId
) s
GROUP BY s.ref_category_id
		,s.new_category_id 
		,s.compare;

/********** DOCUMENTATION **********/
-- view to record table names and metadata for documentation
-- ignore constraints and indices for now
-- DROP VIEW rpt.v_obj_documentation;
CREATE OR ALTER VIEW rpt.v_obj_documentation AS
SELECT  schema_name(o.schema_id) AS schema_name
		,o.name
		,o.type
		,o.type_desc
		,o.create_date
		,o.modify_date
  FROM  sys.objects o
 WHERE  schema_name(o.schema_id) NOT IN ('dbo','sys','WSPROD\mrogers')	-- ignore dbo, sys, marie's schemas
   AND  o.type NOT IN ('D','PK','UQ');
--ORDER BY 1,2;

/*######################################################################*/
/*##########                  MISCELLANEOUS                   ##########*/
/*######################################################################*/

/********** NEOLOGY PASS DATA **********/
-- create tables for neology tag info
-- DROP TABLE WSDOT_STAGE.misc.neology_flexpass;
CREATE TABLE WSDOT_STAGE.misc.neology_flexpass (
	barcode varchar(12) PRIMARY KEY
	,epc_sov varchar(24) NOT NULL UNIQUE
	,tid_sov varchar(24) NOT NULL UNIQUE
	,epc_hov varchar(24) NOT NULL UNIQUE
	,tid_hov varchar(24) NOT NULL UNIQUE
	,encoded datetime2(2) NOT NULL
);

-- DROP TABLE WSDOT_STAGE.misc.neology_single_pass;
CREATE TABLE WSDOT_STAGE.misc.neology_single_pass (
	barcode varchar(12) PRIMARY KEY
	,epc varchar(24) NOT NULL UNIQUE
	,tid varchar(24) NOT NULL UNIQUE
	,encoded datetime2(2) NOT NULL
);

-- DROP TABLE WSDOT_STAGE.misc.neology_pass_types;
CREATE TABLE WSDOT_STAGE.misc.neology_pass_types (
	start_barcode integer NOT NULL
	,end_barcode integer NOT NULL
	,pass_type varchar(4) NOT NULL
	,pass_desc varchar(20) NOT NULL
);

INSERT INTO WSDOT_STAGE.misc.neology_pass_types VALUES
(1,4999999,'FLEX','Flexpass'),
(5000000,5499999,'MOT','Motorcycle Pass'),
(5500000,5999999,'LPP','License Plate Pass'),
(6000000,9999999,'STCK','Sticker Pass');

--TRUNCATE TABLE tmp.pass;
--INSERT INTO WSDOT_STAGE.misc.neology_flexpass SELECT * FROM tmp.pass;

-- view for all neology passes (but without epc or tid info)
-- DROP VIEW misc.v_neology_passes;
CREATE OR ALTER VIEW misc.v_neology_passes AS
SELECT  p.barcode
		,p.encoded
		,p.epc
		,p.tid
		,p.is_hov
		,pt.pass_type
		,pt.pass_desc
  FROM  (SELECT barcode, encoded, epc, tid, 0 AS is_hov FROM WSDOT_STAGE.misc.neology_single_pass nsp 
		 UNION ALL
		 SELECT barcode, encoded, epc_sov AS epc, tid_sov AS tid, 0 AS is_hov FROM WSDOT_STAGE.misc.neology_flexpass nf
		 UNION ALL
		 SELECT barcode, encoded, epc_hov AS epc, tid_hov AS tid, 1 AS is_hov FROM WSDOT_STAGE.misc.neology_flexpass nf
		) p
  JOIN  WSDOT_STAGE.misc.neology_pass_types pt
  		 ON cast(right(p.barcode, 10) AS integer) BETWEEN pt.start_barcode AND pt.end_barcode;

/********** TRANSCORE UII VALIDATION DATA **********/
-- data from email from Michael Severance 2022-01-04 15:48
-- additional data from email from Michael Severance 2022-01-06 10:13
-- also watch for email from Thomas Opheim (TransCore)

-- barcode from neology tables is concat(tag_authority, right('000000' + tag_id, 10))
-- NOTE: tsql does not have LPAD function, use concat with right instead

-- transcore uii data
-- DROP TABLE WSDOT_STAGE.misc.transcore_uii;
CREATE TABLE WSDOT_STAGE.misc.transcore_uii (
	internal_id int PRIMARY KEY
	,lane_time datetime2(2) NOT NULL
	,tag_id varchar(10) NOT NULL
	,tag_agency varchar(2) NOT NULL
	,tag_status varchar(16) NOT NULL
	,validation varchar(16) NOT NULL
	,lane_num smallint NOT NULL
);

-- check counts on import
-- NOTE: BE SURE TO SET DATE FORMAT to 'mm-dd-yyyy'
SELECT  WSDOT_STAGE.util.f_get_month(lane_time) AS trip_month
		,count(*) AS cnt
  FROM  WSDOT_STAGE.misc.transcore_uii
GROUP BY WSDOT_STAGE.util.f_get_month(lane_time)
ORDER BY WSDOT_STAGE.util.f_get_month(lane_time);

-- transcore validation log entries (precessed via shell script)
-- TODO: consider varbinary for epc, tid, and result_8_hash?
-- DROP TABLE WSDOT_STAGE.misc.transcore_validation_log;
CREATE TABLE WSDOT_STAGE.misc.transcore_validation_log (
	tag_read bigint NOT NULL
	,tag_read_dtm datetime2 NOT NULL
	,tag_type varchar(8) NOT NULL
	,tag_agency varchar(4)
	,key_len smallint
	,epc varchar(24) NOT NULL
	,tid varchar(24) NOT NULL
	,result_8_hash varchar(40) NOT NULL
	,CONSTRAINT pk_validation_log PRIMARY KEY (tag_read, tag_read_dtm)
);

-- check counts on import
SELECT  WSDOT_STAGE.util.f_get_month(tag_read_dtm) AS trip_month
		,count(*) AS cnt
  FROM  WSDOT_STAGE.misc.transcore_validation_log
GROUP BY WSDOT_STAGE.util.f_get_month(tag_read_dtm)
ORDER BY WSDOT_STAGE.util.f_get_month(tag_read_dtm);

/*-- TODO: check this
-- all legacy types seem to be 78 so fill in as type 78
UPDATE  WSDOT_STAGE.misc.transcore_validation_log
   SET  tag_agency = 78
 WHERE  tag_agency = ''
   AND  tag_read < 10000000;	-- only valid tags

-- fill in nonvalid as NULL
UPDATE  WSDOT_STAGE.misc.transcore_validation_log
   SET  tag_agency = NULL
 WHERE  tag_agency = ''
   AND  tag_read > 10000000;	-- only invalid tags
*/

-- view to link uii with validation
-- NOTE: join with UII needed since a handful in the log are valid, though perhaps we'd see these as no issue then?
-- DROP VIEW misc.v_transcore_uii_validation;
CREATE OR ALTER VIEW misc.v_transcore_uii_validation AS
SELECT  tvl.*
		,tu.internal_id 
		,tu.lane_time 
		,tu.tag_id 
		,tu.tag_agency AS tu_tag_agency
		,tu.tag_status 
		,tu.validation 
		,tu.lane_num 
		,m_epc.epc AS matched_epc
		,m_epc.tid AS matched_epc_tid
		,m_tid.tid AS matched_tid
		,m_tid.epc AS matched_tid_epc
		,CASE WHEN m_epc.epc IS NOT NULL AND m_tid.tid IS NOT NULL THEN 'both'
			  WHEN m_epc.epc IS NOT NULL THEN 'epc'
			  WHEN m_tid.tid IS NOT NULL THEN 'tid'
			  ELSE 'none' END AS match_type
--SELECT count(*)
  FROM  WSDOT_STAGE.misc.transcore_uii tu 
  JOIN  WSDOT_STAGE.misc.transcore_validation_log tvl
  		 ON tvl.tag_read = tu.tag_id
  		AND datediff(second, tvl.tag_read_dtm, tu.lane_time) BETWEEN -5 AND 5
  LEFT  JOIN WSDOT_STAGE.misc.v_neology_passes m_epc ON left(m_epc.epc,20) = left(tvl.epc,20)
  LEFT  JOIN WSDOT_STAGE.misc.v_neology_passes m_tid ON m_tid.tid = tvl.tid
 WHERE  tu.validation = 'Valid';

-- view to investigate differences
-- DROP VIEW misc.v_transcore_error_summary;
CREATE OR ALTER VIEW misc.v_transcore_error_summary AS
SELECT  t.match_type
		,CASE WHEN match_type = 'both' THEN 'epc'
			  WHEN match_type = 'epc' THEN 'tid'
			  WHEN match_type = 'tid' THEN 'epc'
			  WHEN match_type = 'none' THEN 'none'
			  END AS compare_field						-- field to compare
		,t.txn_count
		,CASE WHEN match_type = 'both' THEN t.epc
			  WHEN match_type = 'epc' THEN t.tid
			  WHEN match_type = 'tid' THEN t.epc
			  WHEN match_type = 'none' THEN NULL
			  END AS read_value							-- value read during validation
		,CASE WHEN match_type = 'both' THEN t.matched_epc
			  WHEN match_type = 'epc' THEN t.matched_tid
			  WHEN match_type = 'tid' THEN t.matched_epc
			  WHEN match_type = 'none' THEN NULL
			  END AS matched_neology_value				-- matching value from neology data, if it exists
		,CASE WHEN match_type = 'both' THEN t.matched_tid_epc
			  WHEN match_type = 'epc' THEN t.matched_epc_tid
			  WHEN match_type = 'tid' THEN t.matched_tid_epc
			  WHEN match_type = 'none' THEN NULL
			  END AS other_identifier_neology_value		-- matching value for other identifier in neology data
		,CASE WHEN match_type = 'both' THEN WSDOT_STAGE.util.f_hamming_dist(left(t.epc,20), left(t.matched_tid_epc,20))
			  WHEN match_type = 'epc' THEN WSDOT_STAGE.util.f_hamming_dist(t.tid, t.matched_epc_tid)
			  WHEN match_type = 'tid' THEN WSDOT_STAGE.util.f_hamming_dist(left(t.epc,20), left(t.matched_tid_epc,20))
			  WHEN match_type = 'none' THEN NULL
			  END AS read_vs_other_id_hamming_dist		-- number of different characters (hamming distance) between read and other
		,CASE WHEN match_type = 'both' THEN WSDOT_STAGE.util.f_hamming_dist(right(t.epc,4), right(t.matched_tid_epc,4))
			  WHEN match_type = 'epc' THEN NULL
			  WHEN match_type = 'tid' THEN WSDOT_STAGE.util.f_hamming_dist(right(t.epc,4), right(t.matched_tid_epc,4))
			  WHEN match_type = 'none' THEN NULL
			  END AS epc_hash_hamming_dist				-- number of different characters (hamming distance) for hash value (only on both or tid match)
		,CASE WHEN match_type = 'both' THEN WSDOT_STAGE.util.f_str_diff_position(left(t.epc,20), left(t.matched_tid_epc,20))
			  WHEN match_type = 'epc' THEN WSDOT_STAGE.util.f_str_diff_position(t.tid, t.matched_epc_tid)
			  WHEN match_type = 'tid' THEN WSDOT_STAGE.util.f_str_diff_position(left(t.epc,20), left(t.matched_tid_epc,20))
			  WHEN match_type = 'none' THEN NULL
			  END AS read_vs_other_id_first_hex_diff_position_from_left
  FROM  (SELECT tuv.epc 
  				,tuv.matched_epc
				,tuv.matched_tid_epc
				,tuv.tid 
				,tuv.matched_tid
				,tuv.matched_epc_tid 
				,tuv.match_type 
				,count(*) AS txn_count
		   FROM misc.v_transcore_uii_validation tuv
		GROUP BY tuv.epc 
  				,tuv.matched_epc
				,tuv.matched_tid_epc
				,tuv.tid 
				,tuv.matched_tid
				,tuv.matched_epc_tid 
				,tuv.match_type 
		) t;

/*######################################################################*/
/*##########                       XOLD                       ##########*/
/*######################################################################*/

-- 2023-01-30: transfer existing disposition date to archive table to add new column
ALTER SCHEMA xold TRANSFER rpt.disposition_summary;
EXEC sp_rename 'xold.disposition_summary', '20230125_disposition_summary';

/*######################################################################*/
/*##########                  JOB SCHEDULING                  ##########*/
/*######################################################################*/
/* notes:
 * https://docs.microsoft.com/en-us/sql/ssms/agent/create-a-job?view=sql-server-ver15
 * https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-delete-job-transact-sql?view=sql-server-ver15
 */

-- 2023-02-07: we may no longer have access to create jobs, but this is fine since we would need to contact Alexey
--             to ensure that any new job is replicated to the other primary nodes in the availability group

-- 2022-08-02: Alexei copied the job to all four nodes in availability group; only set to run on the two 'P' nodes, not 'R' nodes
-- add the job
EXEC msdb.dbo.sp_add_job
	@job_name = N'WSDOT_REPORTING'
	,@enabled = 1
	,@description = N'job for running reporting nightly';

-- add a job step
EXEC msdb.dbo.sp_add_jobstep
	@job_name = N'WSDOT_REPORTING'
	,@step_name = N'run WSDOT reporting'
	,@subsystem = N'TSQL'
	,@command = N'EXEC WSDOT_STAGE.util.usp_wsdot_reporting'
	--,@retry_attempts = 5
	--,@retry_interval = 5
	;

-- add the schedule
EXEC msdb.dbo.sp_add_schedule
	@schedule_name = N'NIGHTLY'
	,@freq_type = 4					-- daily run
	,@freq_interval = 1				-- number of days between runs  
	,@active_start_time = 050000;	-- HHmmss format

-- attach the schedule
EXEC msdb.dbo.sp_attach_schedule
	@job_name = N'WSDOT_REPORTING'
	,@schedule_name = N'NIGHTLY';

-- add to a job server
EXEC msdb.dbo.sp_add_jobserver
	@job_name = N'WSDOT_REPORTING';

/*-- clean up
-- delete the job (also deletes jobsteps)
EXEC msdb.dbo.sp_delete_job
	@job_name = N'WSDOT_REPORTING';

-- delete the schedule (no need if not attached to any jobs)
EXEC msdb.dbo.sp_delete_schedule  
	@schedule_name = N'NIGHTLY'
	,@force_delete = 1;
*/

-- look at jobs
SELECT  j.name
		,max(h.run_date) AS latest_run_date
		,count(*) AS msg_count
  FROM  msdb.dbo.sysjobs j
  JOIN  msdb.dbo.sysjobhistory h ON h.job_id = j.job_id
 WHERE  enabled = 1
--   AND j.name = 'WSDOT_REPORTING'
GROUP BY j.name
ORDER BY count(*) DESC;

SELECT * FROM  msdb.dbo.sysjobs j
--WHERE j.name = 'WSDOT_REPORTING'
;

-- history of job runs
SELECT  *
  FROM  msdb.dbo.sysjobs j
  JOIN  msdb.dbo.sysjobhistory h ON h.job_id = j.job_id
 WHERE  j.name = 'WSDOT_REPORTING'
ORDER BY run_date DESC, run_time DESC, step_id DESC;

-- next scheduled
SELECT  *
  FROM  msdb.dbo.sysjobs j
  JOIN  msdb.dbo.sysjobschedules s ON s.job_id = j.job_id
 WHERE  j.name = 'WSDOT_REPORTING';

SELECT  *
  FROM  msdb.dbo.sysschedules s
 WHERE  s.name = N'NIGHTLY';

-- running on AlwaysON Availability Groups - tables of interest:
SELECT * FROM sys.availability_replicas ar;
SELECT * FROM sys.dm_hadr_availability_replica_states ars;
SELECT * FROM sys.availability_groups ag;
SELECT * FROM sys.dm_hadr_availability_group_states ags;
SELECT * FROM sys.availability_group_listeners li;

/*############################################################################################################################################*/
-- standard reporting

-- test write access to WSDOT_STAGE
SELECT * INTO WSDOT_STAGE.tmp.test_write FROM WSDOT_STAGE.wsdot.leakage_categories lc;
DROP TABLE WSDOT_STAGE.tmp.test_write;

/*######################################################################*/
/*##########                  OUTPUT QUERIES                  ##########*/
/*######################################################################*/

-- NOTE: use ctrl+alt+shift+A to get all results

/********** DAILY QUERIES **********/
----- check job details and history; on history, run_status = 0 indicates failure
EXEC WSDOT_STAGE.util.usp_get_server_job_info @info = 'detail';
EXEC WSDOT_STAGE.util.usp_get_server_job_info @info = 'history', @result_limit = 10;

----- check that we are only PRIMARY on AG1; if not PRIMARY on AG1 or also PRIMARY on AG3, notify ETAN
SELECT * FROM WSDOT_STAGE.util.v_local_server_ag_roles ORDER BY ag_name, role_desc, replica_server_name;

----- review log messages
SELECT * FROM WSDOT_STAGE.log.runinfo r ORDER BY r.system_time DESC;
SELECT * FROM WSDOT_STAGE.log.v_activity_messages m ORDER BY m.system_time DESC;

----- account closures
-- SELECT * FROM WSDOT_STAGE.rpt.v_account_closure_summary ORDER BY last_updated_date ASC;

----- pass fulfillment
SELECT * FROM WSDOT_STAGE.rpt.x_pass_fulfillment ORDER BY request_date, req_source, request_type, pass_type, request_status;

----- trips by posting and trip date
SELECT * FROM WSDOT_STAGE.rpt.x_trip_posting_summary ORDER BY posting_date, trip_date, roadway, roadside_toll_type;

----- icrs summary
SELECT * FROM WSDOT_STAGE.rpt.icrs_summary ORDER BY queried_at DESC, trip_date, facility, trip_count DESC;

----- image download
SELECT * FROM WSDOT_STAGE.rpt.image_download_summary ORDER BY queried_at DESC, roadway, trip_date, roadsidetolltype, tollstatuscodeid;

-- escalation go-live tracking summary
SELECT * FROM WSDOT_STAGE.rpt.v_escalation_go_live_population WHERE category_id <> '0' ORDER BY category_id;

-- escalation go-live tracking detail
SELECT * FROM WSDOT_STAGE.rpt.v_escalation_negative_balance_detail venbd WHERE category_id <> '0' ORDER BY category_id;

-- escalation go-live tracking comparison
SELECT * FROM WSDOT_STAGE.rpt.v_negative_balance_comparison nbc ORDER BY ref_category_id, new_category_id, compare;

/********** WEEKLY QUERIES **********/
----- account creation since 2021-07-10
SELECT * FROM WSDOT_STAGE.rpt.v_account_creation ORDER BY posting_date, acct_count;

----- account type trends (remember system_time is in UTC!)
SELECT * FROM WSDOT_STAGE.rpt.f_account_types_asof(getutcdate()) ORDER BY account_type;

-- posting performance summary
SELECT * FROM WSDOT_STAGE.rpt.x_posting_performance ORDER BY trip_month, roadway;

-- account cycle day summary
SELECT * FROM WSDOT_STAGE.rpt.v_acct_cycle_day_summary ORDER BY acct_type_desc, acct_status, cycle_day;

/********** MONTHLY QUERIES **********/
----- CSR case closure summary
SELECT * FROM WSDOT_STAGE.rpt.v_csr_case_closure_summary ORDER BY modified_by, case_created_month, case_updated_month;

----- disposition summary
SELECT * FROM WSDOT_STAGE.rpt.v_latest_disposition ORDER BY trip_month, roadway, record_count DESC;

----- forecasting summary (do not include partial current month)
SELECT * FROM WSDOT_STAGE.rpt.v_forecasting_summary
WHERE trip_month < WSDOT_STAGE.util.f_get_month(getdate())
ORDER BY trip_month, roadway, trip_count DESC;

/********** OFF-CYCLE QUERIES **********/
-- disposition summary
SELECT *, cast(getdate() AS date) AS asof_date FROM WSDOT_STAGE.rpt.v_disposition_summary vds
WHERE vds.trip_month >= convert(date, '2021-01-01')
ORDER BY trip_month, roadway, record_count DESC;

-- forecasting summary
SELECT * FROM WSDOT_STAGE.rpt.v_recent_forecasting_summary
WHERE trip_month < WSDOT_STAGE.util.f_get_month(getdate())
ORDER BY trip_month, roadway, trip_count DESC;

/********** READONLY QUERIES **********/
-- use these for daily reporting when the database is readonly for some reason
-- pass fulfillment
SELECT * FROM WSDOT_STAGE.rpt.v_pass_fulfillment ORDER BY request_date, req_source, request_type, pass_type, request_status;
-- daily trip summary
SELECT * FROM WSDOT_STAGE.rpt.v_trip_posting_summary
WHERE posting_date >= dateadd(month, -2, WSDOT_STAGE.util.f_get_month(getdate()))
ORDER BY posting_date, trip_date, roadway, roadside_toll_type;

/********** DOCUMENTATION QUERIES **********/
SELECT * FROM WSDOT_STAGE.rpt.v_obj_documentation d ORDER BY d.schema_name, d.name;

/*
----- TODO:
 - add to documentation: adding wsdot data: holidays, manual tnb rates, etc
 - clean up temp tables
 - create view for  fastlane.info.CommunicationPreference items
 - set up query to drop old data out of icrs_summary and image_download_summary?
 - create procedure to build dynamic monthly trip frequency data
 - create permanent views for exported trip data from TRAC toll equity pull
 - modify most views to include unique identifiers
 - add debug messages to job to have better specificity on failure?
 - add indication of which adjustments are 'terminal' - no further processing happens (get from Ami)
   - add line to disposition in resolved area, above leakage, which counts these
   - also awaiting ORDER notes from lance for ledger priority
 - look into using ETAN rtoll.TollStatus and rtoll.EnuTollStatusCode in disposition
   - also add any other relevant ids to base and full trip details
 - consider updating account type view to somehow include defunct as a type?
 - create recent_account_type like recent_trip_state to summarize recent accounts?
 - update disposition to add prepaid/pay-as-you-go/short-term account types to PAS, PBP trips
 - add if pass used within last 24 months to pass type summary?
 - add conversion table history to account types history
 - consider adding log of events from monthly MTR reports for all facilities?
    - start_dtm, end_dtm, roadway, direction, category, milepost, comments
 - look into using linux version or scheduling connection in windows (or Power BI?)

txn counts by payment category (for latest month)
 - trip date
 - day of week
 - facility
 - category (PAS, nonrev pass, PBP, PBM, PAYG, non-viable)
 - trip count

TB/NOCP reporting for pay-by-mail txns once escalation turns on

future questions:
 - how many license plates are registered to accounts?
 - how many specialty license plates there are?

*/

/*
-- endpoint notes
-- 2022-05-26: listener address: mclustr9002ag1.wsprod.etantolling, connect to WSDOT_STAGE with applicationIntent=readwrite
--             AG = availability group; fastlane is writable on AG3, WSDOT_STAGE is writable on AG1
-- 2022-04-07: listener address: mclustr9002ag3.wsprod.etantolling (points to 10.1.8.9)
-- 2022-04-03: production prior to this date was on 10.3.8.5, backup reporting was on 10.3.8.4

*/
