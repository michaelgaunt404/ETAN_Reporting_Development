/*######################################################################*/
/*##########                  BIT FLIP ISSUE                  ##########*/
/*######################################################################*/
-- do I need manifests? (email from Michael Severance 2021-11-23 10:00)
-- later data from neology seems to indicate I have what I need (email from Michael Severance 2021-12-22 11:47)

-- base tables in report arch in misc schema

----- validation log extracts
-- NOTE: processing now done via script which also removes spaces from epc, tid, and result_8_hash; code left here for record
/* vim directions to convert validation log extracts to csv format:
add header row: tag_read,tag_read_dtm,tag_type,tag_agency,key_len,epc,tid,result_8_hash
:%s/^TagRead: EpcTidTagTime //gc
:%s/ RealTime: /,/gc
:%s/\n^6C tag Agency: /,6C,/gc
:%s/\n^XXX WSDOT legacy tag/,legacy,,/gc
:%s/ KeyLength:/,/gc
:%s/\n^Epc:  /,/gc
:%s/ \n^Tid:  /,/gc
:%s/ \n^Result: 8 Hash:  /,/gc
:%s/ ,/,/gc
:g/^--/d
*/

/*
-- DROP TABLE WSDOT_STAGE.tmp.transcore_validation_log;
CREATE TABLE WSDOT_STAGE.tmp.transcore_validation_log (
	tag_read integer NOT NULL
	,tag_read_dtm datetime2 NOT NULL
	,tag_type varchar(8) NOT NULL
	,tag_agency varchar(4)
	,key_len smallint
	,epc varchar(40)
	,tid varchar(40)
	,result_8_hash varchar(80)
);

-- all legacy types seem to be 78 so fill in as type 78
UPDATE  WSDOT_STAGE.tmp.transcore_validation_log
   SET  tag_agency = 78
 WHERE  tag_agency = ''
   AND  tag_read < 10000000;	-- only valid tags

-- fill in nonvalid as NULL
UPDATE  WSDOT_STAGE.tmp.transcore_validation_log
   SET  tag_agency = NULL
 WHERE  tag_agency = ''
   AND  tag_read > 10000000;	-- only invalid tags

INSERT INTO WSDOT_STAGE.misc.transcore_validation_log
SELECT  tag_read
		,tag_read_dtm
		,tag_type
		,tag_agency
		,key_len
		,replace(epc,' ','') AS epc
		,replace(tid,' ','') AS tid
		,replace(result_8_hash,' ','') AS result_8_hash
  FROM  WSDOT_STAGE.tmp.transcore_validation_log t;
*/
-- verify transcore counts
--TRUNCATE WSDOT_STAGE.misc.transcore_uii tu;
SELECT WSDOT_STAGE.util.f_get_month(tu.lane_time) AS trip_month, count(*) FROM WSDOT_STAGE.misc.transcore_uii tu GROUP BY WSDOT_STAGE.util.f_get_month(tu.lane_time) ORDER BY 1;

SELECT WSDOT_STAGE.util.f_get_month(tvl.tag_read_dtm) AS trip_month, count(*) FROM WSDOT_STAGE.misc.transcore_validation_log tvl
GROUP BY WSDOT_STAGE.util.f_get_month(tvl.tag_read_dtm) ORDER BY 1;

--DELETE
--SELECT count(*)
-- FROM WSDOT_STAGE.misc.transcore_validation_log WHERE tag_read_dtm > convert(date, '2022-08-01');

-- verify counts after loading neology data
SELECT count(*) FROM WSDOT_STAGE.misc.neology_single_pass;
SELECT count(*) FROM WSDOT_STAGE.misc.neology_flexpass;
--SELECT count(*) FROM WSDOT_STAGE.tmp.pass;

--SELECT count(*) FROM tmp.pass WHERE len(tid_sov) = 24;
--SELECT * FROM WSDOT_STAGE.tmp.pass WHERE tid_sov = 'E200680B00004006A12074CE';

-- results:	single		flexpass
--------------------------------------------
-- good		1,259,909	621,168
-- bad		    2,000	     31
-- 
-- single pass bad are all duplicate barcodes 77000000503[2-4] from 2018-08-16 10:57:59 - 2018-08-16 12:02:27
-- flexbass bad are 28 malformed barcodes (too long), 2 with NULL epc or tid, and 1 collision on tid_sov, see below
-- collision on tid_sov - second one (barcode (770003490830) not imported
SELECT * FROM WSDOT_STAGE.misc.neology_flexpass nf WHERE tid_sov = 'E200680B00004006A12074CE';

-- 2,502,245 passes, of which 621,168 are hov
SELECT count(*) FROM WSDOT_STAGE.misc.v_neology_passes vnp WHERE vnp.is_hov = 1;

-- summarize pass encoded dates by month and type
SELECT  WSDOT_STAGE.util.f_get_month(p.encoded) AS encoded_month
		,p.pass_desc
		,count(*) AS pass_count
		,min(p.barcode) AS min_barcode
		,max(p.barcode) AS max_barcode
  FROM  WSDOT_STAGE.misc.v_neology_passes p
 WHERE  p.is_hov = 0	-- ignore duplicate HOV info
GROUP BY WSDOT_STAGE.util.f_get_month(p.encoded)
		,p.pass_desc
ORDER BY WSDOT_STAGE.util.f_get_month(p.encoded)
		,p.pass_desc;

----- TransCore UII Validation data (see tables in report arch)
-- check data
SELECT count(*) FROM WSDOT_STAGE.tmp.transcore_uii;
SELECT convert(date, TxnDateTime) AS date, count(*) FROM WSDOT_STAGE.tmp.transcore_uii tu GROUP BY convert(date, TxnDateTime) ORDER BY convert(date, TxnDateTime);
SELECT DISTINCT t.TagAuthority FROM WSDOT_STAGE.tmp.transcore_uii t;
SELECT min(len(t.TagId)) AS min_len, max(len(t.TagId)) AS max_len FROM WSDOT_STAGE.tmp.transcore_uii t;

SELECT  convert(date, tu.lane_time) AS date
		,tu.lane_num 
		,count(*)
  FROM  WSDOT_STAGE.misc.transcore_uii tu 
GROUP BY convert(date, tu.lane_time)
		,tu.lane_num
ORDER BY convert(date, tu.lane_time)
		,tu.lane_num;

-- some passes are beyond neology ranges (and all except 43067914 are invalid):
-- 139 are 8 digits long
--  46 are 9 digits long
SELECT * FROM WSDOT_STAGE.misc.transcore_uii t WHERE len(t.tag_id) = 8 ORDER BY t.tag_id;

-- look at invalid states (we care about invalid validation):
-- 6033 have invalid tag_status, 3099 have invalid validation, 400 have both invalid (remaining 314,630 are both valid)
SELECT t.tag_status, t.validation, count(*) FROM WSDOT_STAGE.misc.transcore_uii t GROUP BY t.tag_status, t.validation;

----- validation log extracts (see report arch)

SELECT DISTINCT tvl.tag_agency FROM WSDOT_STAGE.tmp.transcore_validation_log tvl;
SELECT DISTINCT tvl.tag_read FROM WSDOT_STAGE.tmp.transcore_validation_log tvl WHERE tvl.tag_agency = '';

-- fastlane.util.FacilityAgency.kapschCode contains types 77,78,218,219
SELECT  t.transponderId
		,fa.kapschCode 
		,t.transponderNumber 
		,tca.customerAccountId 
  FROM  fastlane.vehicle.Transponder t 
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  LEFT  JOIN fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = t.transponderId 
 WHERE  convert(int, t.transponderNumber) IN (SELECT DISTINCT tvl.tag_read FROM WSDOT_STAGE.misc.transcore_validation_log tvl WHERE tvl.tag_agency = '');

-- look at epc, tid, and hash lengths
SELECT min(len(item)) AS minlen, max(len(item)) AS maxlen FROM (SELECT replace(result_8_hash,' ','') AS item FROM WSDOT_STAGE.misc.transcore_validation_log tvl) AS d;

SELECT count(*) FROM WSDOT_STAGE.misc.transcore_validation_log;

-- 185 invalid validations on 2022-01-05, 158 match with validation log
-- also none are flexpass, so can join with single pass
-- TODO: add plate OCR confidence since plate could come from bad source?
SELECT  tu.tag_id AS tc_uii_tag_id
		,tu.tag_agency AS tc_uii_tag_agency
		,tu.tag_status AS tc_uii_tag_status
		,tu.validation AS tc_uii_validation
		,tu.lane_time AS tc_uii_lane_time
		,tu.lane_num AS tc_uii_lane_num
		,vfl.roadGantryId AS road_gantry_id
		,tp.tripPassageId AS trip_passage_id
		--,tp.roadsideUnusualOccurenceId	-- join to get values
		,t.tripId AS trip_id
		,fa.kapschCode AS trip_pass_tag_agency
		,tr.transponderNumber AS trip_pass_tag_number
		,tca.customerAccountId AS trip_pass_cust_acct_id
		,lpca.customerAccountId AS trip_lp_cust_acct_id
		,es.shortCode AS trip_lp_state
		,lp.lpnumber AS trip_lp_number
		,tvl.epc AS tvl_txn_epc
		,tvl.tid AS tvl_txn_tid
		--,tvl.result_8_hash AS tvl_result_hash
		,nsp.barcode AS uii_tag_id_match_barcode
		,nsp.epc AS neology_barcode_epc
		,nsp.tid AS neology_barcode_tid
		,nsptid.tid AS tvl_tid_match
		,nsptid.barcode AS tvl_tid_barcode
		,nsptid.epc AS tvl_tid_epc
		,nspepc.epc AS tvl_epc_match
		,nspepc.barcode AS tvl_epc_barcode
		,nspepc.tid AS tvl_epc_tid
		,lp_acct.epc AS lp_acct_closest_epc
		,lp_acct.tid AS lp_acct_tid
		,lp_acct.barcode AS lp_acct_tag
		,lp_acct.diff AS lp_acct_bit_difference
		,CASE WHEN tvl.epc = nsp.epc AND tvl.tid = nsp.tid THEN 'EPC and TID match; no issue'		-- doesn't appear to happen when validation is invalid
			  WHEN tvl.epc = nsp.epc AND nsp.barcode = concat(fa.kapschCode, tr.transponderNumber) THEN 'Group 1 - TID mismatch, EPC match, correct acct'
			  -- note if EPC doesn't match, we can't trust tag id, verify through other means?
			  WHEN tvl.tid = nsp.tid AND nsp.barcode = concat(fa.kapschCode, tr.transponderNumber) THEN 'Group 1.5 - EPC mismatch, TID match, correct acct (error in EPC not affecting barcode)'
			  WHEN nsptid.barcode = concat(tu.tag_agency, right('000000' + tu.tag_id, 10)) THEN 'Group X - match on TVL TID corresponds with barcode - correct acct'
			  WHEN lp_acct.barcode = concat(tu.tag_agency, right('000000' + tu.tag_id, 10)) THEN 'match on LP plate account nearest EPC - correct acct'
			  WHEN nsptid.barcode = lp_acct.barcode AND lp_acct.barcode <> concat(tu.tag_agency, right('000000' + tu.tag_id, 10)) THEN 'Group 2/3/4 - LP acct matches TID but wrong acct'
			  ELSE 'Group 4 - no match, charged to incorrect account' END AS category
		/*,CASE WHEN nsp.barcode = concat(fa.kapschCode, tr.transponderNumber) THEN 'trip pass match'
			  WHEN lpca.customerAccountId = tca.customerAccountId AND lp_acct.barcode = concat(fa.kapschCode, tr.transponderNumber) THEN 'lp acct tag match'*/
--SELECT  count(*)
  FROM  WSDOT_STAGE.misc.transcore_uii tu 
  JOIN  WSDOT_STAGE.misc.transcore_validation_log tvl
  		  ON tvl.tag_read = tu.tag_id
  		 AND datediff(second, tvl.tag_read_dtm, tu.lane_time) BETWEEN -5 AND 5
  LEFT  JOIN WSDOT_STAGE.misc.neology_single_pass nsp ON nsp.barcode = concat(tu.tag_agency, right('000000' + tu.tag_id, 10))
  LEFT  JOIN WSDOT_STAGE.misc.neology_single_pass nsptid ON nsptid.tid = tvl.tid 
  LEFT  JOIN WSDOT_STAGE.misc.neology_single_pass nspepc ON nspepc.epc = tvl.epc
  LEFT  JOIN WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.wsdot_roadway = 'TNB' AND vfl.exLaneId = tu.lane_num 
  LEFT  JOIN fastlane.lance.tripPassage tp ON tp.laneTime = tu.lane_time AND tp.roadGantryId = vfl.roadGantryId  
  LEFT  JOIN fastlane.lance.trip t ON t.tripPassageId = tp.tripPassageId 
  LEFT  JOIN fastlane.rtoll.Toll tt ON tt.TollID = t.tripId 
  LEFT  JOIN fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = tt.transponderId 
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = tt.licensePlateId AND tt.exitTime BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate AND lpca.customerAccountId >= 1000
  LEFT  JOIN fastlane.vehicle.Transponder tr ON tr.transponderId = tca.transponderId 
  LEFT  JOIN fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = tr.facilityAgencyId 
  LEFT  JOIN fastlane.util.LicensePlate lp ON lp.licensePlateId = tt.licensePlateId 
  LEFT  JOIN fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId	-- NOTE: is this okay or need to go through JurisdictionLicensePlateMap?
  LEFT  JOIN fastlane.locale.EnuState es ON es.stateId = lpj.stateId
  OUTER APPLY (		-- get transponders from lp account for matching
  		SELECT  TOP 1
				nsp2.epc
				,nsp2.barcode 
				,nsp2.tid
				,WSDOT_STAGE.util.f_hamming_dist(tvl.epc, nsp2.epc) AS diff
		  FROM  fastlane.rtoll.TransponderCustomerAccount tca2 
		  JOIN  fastlane.vehicle.Transponder tr2 ON tr2.transponderId = tca2.transponderId 
		  JOIN  fastlane.util.FacilityAgency fa2 ON fa2.facilityAgencyId = tr2.facilityAgencyId 
		  JOIN  WSDOT_STAGE.misc.neology_single_pass nsp2 ON nsp2.barcode = concat(fa2.kapschCode, tr2.transponderNumber)
		 WHERE  tca2.customerAccountId = lpca.customerAccountId
		ORDER BY diff ASC
		) AS lp_acct
 WHERE  tu.validation = 'Invalid'
--   AND  tu.tag_id = 39997752
ORDER BY tu.tag_id;

-- look for trips on TNB lane X
SELECT  t.tripId, tp.*
  FROM  fastlane.lance.tripPassage tp 
  JOIN  fastlane.lance.trip t ON t.tripPassageId = tp.tripPassageId 
 WHERE  tp.roadGantryId = 160032 AND tp.laneTime BETWEEN '2022-01-05 11:52:35' AND '2022-01-05 11:52:55'
ORDER BY laneTime;

SELECT  nsp.epc
  FROM  fastlane.rtoll.TransponderCustomerAccount tca 
  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = tca.transponderId 
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  JOIN  WSDOT_STAGE.misc.neology_single_pass nsp ON nsp.barcode = concat(fa.kapschCode, t.transponderNumber)
 WHERE  tca.customerAccountId = 57041;

SELECT DISTINCT convert(date, tvl.tag_read_dtm) FROM WSDOT_STAGE.misc.transcore_validation_log tvl;

-- test character compare - see function WSDOT_STAGE.util.f_hamming_dist

SELECT  'B00000000204D2625138B4FE'
		,nsp.epc
		,WSDOT_STAGE.util.f_hamming_dist('B00000000204D2625138B4FE', nsp.epc) AS diff
  FROM  fastlane.rtoll.TransponderCustomerAccount tca 
  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = tca.transponderId 
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  JOIN  WSDOT_STAGE.misc.neology_single_pass nsp ON nsp.barcode = concat(fa.kapschCode, t.transponderNumber)
 WHERE  tca.customerAccountId = 57041;

--------------------------------------------------------------------------------
-- 2022-10-18 new analysis

-- for tags which fail validation, find if there is a matching TID or EPC in neology data
SELECT  count(DISTINCT tuv.tag_id) AS tag_id_count
		,count(DISTINCT tuv.epc) AS epc_count
		,count(DISTINCT tuv.tid) AS tid_count
		,count(DISTINCT tuv.matched_epc) AS match_epc
		,count(DISTINCT tuv.matched_tid) AS match_tid
		,count(DISTINCT concat(tuv.matched_epc,tuv.matched_tid)) AS match_epc_or_tid
		,count(*) AS read_count
  FROM  misc.v_transcore_uii_validation tuv;

-- where di the bitflip occur? check epc first
-- 20221108: 687 (jul-sep data only)
SELECT  count(DISTINCT tuv.epc) AS unmatched_epc
  FROM  misc.v_transcore_uii_validation tuv
 WHERE  tuv.matched_epc IS NULL;

/*-- search for a single difference - too slow! (over 15 min with no result for a single search)
SELECT  *
  FROM  (SELECT 'B00000000304D062697D71A7' AS flip_epc
				,vnp.epc 
				,WSDOT_STAGE.util.f_hamming_dist(left('B00000000304D062697D71A7',20), left(vnp.epc,20)) AS diff
		  FROM  WSDOT_STAGE.misc.v_neology_passes vnp
		) d
 WHERE  d.diff = 1;	-- look for single bit flip
*/

-- look at bitflip distance, get position of flipped bit
WITH hex AS (	-- hex difference
	SELECT  es.*
			,CASE WHEN es.read_vs_other_id_hamming_dist = 1 THEN  
					WSDOT_STAGE.util.f_text_to_binary(substring(es.read_value, es.read_vs_other_id_first_hex_diff_position_from_left, 1))
				ELSE NULL END AS read_diff_hex_char
			,CASE WHEN es.read_vs_other_id_hamming_dist = 1 THEN 
					WSDOT_STAGE.util.f_text_to_binary(substring(es.other_identifier_neology_value, es.read_vs_other_id_first_hex_diff_position_from_left, 1))
				ELSE NULL END AS other_diff_hex_char
	  FROM  WSDOT_STAGE.misc.v_transcore_error_summary es
	),
	bin AS (	-- binary difference
	SELECT  hex.*
			,CASE WHEN hex.read_vs_other_id_hamming_dist = 1 THEN (hex.read_vs_other_id_first_hex_diff_position_from_left - 1) * 4 ELSE NULL END AS binary_diff_base_position
			,WSDOT_STAGE.util.f_binary_subtract(hex.read_diff_hex_char, hex.other_diff_hex_char) AS binary_subtract
	  FROM  hex
	),
	pos AS (
	SELECT  bin.*
			,WSDOT_STAGE.util.f_true_bit_count(bin.binary_subtract) AS binary_bit_diff
			,CASE WHEN bin.read_vs_other_id_hamming_dist <> 1 THEN NULL	-- don't compute for cases when more than one hex digit differs
				  WHEN bin.binary_subtract > 8 THEN bin.binary_diff_base_position + 1
				  WHEN bin.binary_subtract > 4 THEN bin.binary_diff_base_position + 2
				  WHEN bin.binary_subtract > 2 THEN bin.binary_diff_base_position + 3
				  ELSE bin.binary_diff_base_position + 4 END AS binary_first_diff_position_from_left
	  FROM  bin
	)
SELECT  pos.*
		,CASE WHEN pos.compare_field = 'none' THEN 'no match; unable to identify account; unknown cause'
			  WHEN pos.compare_field = 'tid' AND pos.read_vs_other_id_hamming_dist = 1 THEN 'tid single bitflip; can properly identify account'
			  WHEN pos.compare_field = 'tid' AND pos.read_vs_other_id_hamming_dist > 1 THEN 'tid multiple bitflip; can properly identify account; unknown cause'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 0 AND pos.epc_hash_hamming_dist = 1 THEN 'epc single bitflip in uii validation hash; can properly identify account'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 0 AND pos.epc_hash_hamming_dist > 1 THEN 'epc multiple bitflip in uii validation hash; can properly identify account'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN  1 AND  8 THEN 'epc single bitflip in dsfid; unobservable since not validated'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN  9 AND 33 THEN 'epc single bitflip in reserved/classification area; can properly identify account'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN 34 AND 36 THEN 'epc single bitflip in hov declaration; can properly identify account'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN 37 AND 40 THEN 'epc single bitflip in version code; can properly identify account'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN 41 AND 52 THEN 'epc single bitflip in agency code; unobservable since not validated'
			  WHEN pos.compare_field = 'epc' AND pos.read_vs_other_id_hamming_dist = 1 AND pos.binary_first_diff_position_from_left BETWEEN 53 AND 80 THEN 'epc single bitflip in serial number; potential charge to incorrect account'
			  ELSE 'other undefined issue'
			  END AS bitflip_description
  FROM  pos
ORDER BY pos.match_type;

/*-- devel notes
-- must use bitwise approach since large binary values exceed bigint capacity
SELECT convert(varbinary(1), cast(0xF AS int) - cast(0x5 AS int), 162);
SELECT cast(cast(0xB01000000204D05E7424143F AS bigint) - cast(0xB00000000204D05E7424143F AS bigint) AS varbinary);

SELECT util.f_binary_diff(0xF, 0xA);	-- 5
SELECT util.f_binary_diff(0xA, 0xF);	-- 5
SELECT util.f_binary_diff(0xF, NULL);	-- NULL

-- using a bit-wise approach
SELECT WSDOT_STAGE.util.f_true_bit_count(0xB00000000204D05E7424143F);
SELECT WSDOT_STAGE.util.f_true_bit_count(NULL);
SELECT WSDOT_STAGE.util.f_true_bit_count(util.f_binary_diff(0xF, 0xA));	-- 2
SELECT WSDOT_STAGE.util.f_true_bit_count(util.f_binary_diff(0xA, 0xF));	-- 2
SELECT WSDOT_STAGE.util.f_true_bit_count(util.f_binary_diff(0xF, 0xF));	-- 0
SELECT WSDOT_STAGE.util.f_true_bit_count(util.f_binary_diff(0xF, NULL));	-- NULL

SELECT util.f_binary_diff(0xF, 0x3);

-- https://www.rapidtables.com/convert/number/hex-to-binary.html
--    B   0   0   0   0   0   0   0   0   2   0   4   D   0   5   E   7   4   2   4   1   4   3   F
-- 101100000000000000000000000000000000001000000100110100000101111001110100001001000001010000111111
-- 101100000000000000000000000000000001001000000100110100000101111001110100001001000001010000111111
--    B   0   0   0   0   0   0   0   1   2   0   4   D   0   5   E   7   4   2   4   1   4   3   F
*/

----------
-- flipped bits
-- NOTE: a binary bit flip does result in only one changed hexadecimal digit mathematically
-- do we see more bit flips in EPC or TID
--   if there is a bitflip in the EPC, what position is the bitflip
/*
when comparing EPC, use everything except for last four hex digits (that's the checksum)
if EPC is correct then account is correct

EPC is UII info, is info plus checksum hash in last four digits
TID is unique tag identifier (UUID), but does not correspond to agency or account info, is not hashed

if bit flip is in last four hex digits of EPC, then tag should match first 80 EPC and TID but fail validation
we don't see flips in 1-8 or 41-52 because host does not even try to validate those if they don't match WSDOT agency
 - we could ask to add this, but would make other reports much more verbose

TODO:
 - indicate if bitflip is 0 -> 1 or 1 -> 0
 - for incorrect account, see if we can identify the incorrect account, and if it exists in ETAN
 - estimate percent of tags that are invalid/single bit flip by each day of validation data

Kapsch UII
 - UII is EPC as read, with READ hash value, can be longer than 24 char as well, use first 24 characters)
 - TID is longer than transcore
 - UII Calc is halculated hash value
 - UII read is the last four digits hash value from the tag (so 0 for longer UII values)

*/