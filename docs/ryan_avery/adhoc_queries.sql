USE fastlane;

-- diagnostics - get server name and user name
SELECT @@servername;
SELECT user_name();

-- database version: SQL Server 2019
SELECT @@VERSION;

-- check email config (requires sysadmin privileges)
--EXECUTE msdb.dbo.sysmail_help_status_sp;

--SELECT fastlane.config.getPostingDate();
--SELECT getdate();

-- searching for objects
-- get schema from id
SELECT SCHEMA_NAME(12);

-- search for table by name
SELECT * FROM sys.tables WHERE name LIKE '%payment%';

-- search for procedures
SELECT * FROM sys.procedures WHERE name LIKE '%standing%';

-- see bottom of file for searching for generic objects

--------------------------------------------------------------------------------
-- initial reporting queries to track movement through system
-- use June 1 as test date for reporting
-- NOTE: transactions in ETAN are different from transactions in Kapsch/ETCC
--       transactions are simply steps in processing, many can be linked to a trip or toll

-- check server vs local times
-- NOTE: server appears to be Pacific Time
SELECT CURRENT_TIMEZONE();
SELECT getdate() AS loc_dt, getutcdate() AS utc_dt;
SELECT SYSDATETIME(), SYSUTCDATETIME(), SYSDATETIMEOFFSET();
SELECT getdate(), getdate() AT TIME ZONE 'Pacific Standard Time';

-- get available time zones
SELECT * FROM sys.time_zone_info tzi;

/* -- rounding times in TSQL:
DECLARE @dt datetime
SET @dt = '2021-07-19 15:04:36'
--SELECT left(format(@dt,'yyyy-MM-dd HH:mm'),15) + '0';
--SELECT dateadd(ss, datediff_big(ss, 0, @dt), 0);
SELECT dateadd(mi, datediff(mi, convert(date,'2021-07-01'), @dt), '2021-07-01');
SELECT datediff(ss, convert(date,'2021-07-01'), @dt);
SELECT convert(date,'2021-07-01');
SELECT cast('2021-07-01' AS date);
*/

-- test temporal table support from DBeaver
-- first, look at history
SELECT * FROM fastlane.rtoll.htollstatus WHERE tollid = 209337249;

-- check current value
SELECT * FROM fastlane.rtoll.TollStatus WHERE tollid = 209337249;

-- get current value as of various date times
-- querying temporal tables: https://docs.microsoft.com/en-us/sql/relational-databases/tables/querying-data-in-a-system-versioned-temporal-table?view=sql-server-ver15
-- for time zones: https://docs.microsoft.com/en-us/sql/t-sql/queries/at-time-zone-transact-sql?view=sql-server-ver15
SELECT * FROM fastlane.rtoll.TollStatus FOR SYSTEM_TIME AS OF '2021-05-20 18:05:34' WHERE tollid = 209337249;

SELECT getdate() AT TIME ZONE 'Pacific Standard Time';

-- toll status as of date
-- TODO: convert to function to provide date as argument
-- NOTE: TSQL doesn't support positional syntax for group by
-- 2021-06-16: runs in 68 seconds
DECLARE @end_dt datetime2;
SET @end_dt = CAST('2021-06-02' AS datetime2) AT TIME ZONE 'Pacific Standard Time' AT TIME ZONE 'UTC';
SELECT  etsc.tollStatusCodeId
        ,etsc.shortCode
        ,etsc.description
        ,count(*) AS toll_count
  FROM  fastlane.rtoll.TollStatus FOR SYSTEM_TIME AS OF @end_dt AS ts
  LEFT  JOIN fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = ts.tollStatusCodeId
  JOIN  fastlane.lance.Trip t ON t.tripid = ts.tollid
  JOIN  fastlane.lance.tripPassage tp on tp.tripPassageId = t.tripPassageId
 WHERE  cast(tp.lanetime AS date) = convert(date,'2021-06-01')
GROUP BY etsc.tollStatusCodeId
        ,etsc.shortCode
        ,etsc.description
ORDER BY etsc.tollStatusCodeId;

-- alternative approach to getting status of trips on a certain date
-- querying on temporal tables with AS OF is *really* slow (2 secs for current, 1600+ no result with AS OF)
DECLARE @end_dt datetime2;
SET @end_dt = CAST('2021-06-02' AS datetime2) AT TIME ZONE 'Pacific Standard Time' AT TIME ZONE 'UTC';
SELECT  etsc.tollStatusCodeId
        ,etsc.shortCode
        ,etsc.description
        ,count(*) AS toll_count
-- SELECT count(*)
  FROM  fastlane.lance.Trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
  JOIN  fastlane.rtoll.TollStatus ts -- FOR SYSTEM_TIME AS OF @end_dt AS ts
         ON ts.tollId = t.tripId
  JOIN  fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = ts.tollStatusCodeId
 WHERE  cast(tp.laneTime AS date) = convert(date,'2021-06-01')
GROUP BY etsc.tollStatusCodeId
        ,etsc.shortCode
        ,etsc.description
ORDER BY etsc.tollStatusCodeId;

-- SELECT *, startTime, endTime FROM fastlane.rtoll.TollStatus FOR SYSTEM_TIME ALL WHERE tollid = 209337249 ORDER BY endTime;
SELECT * FROM fastlane.rtoll.V_ah_TollStatus vats WHERE tollid = 209337249 ORDER BY endTime;

-- summarize entry reason codes by lane date
-- NOTE: 282,950 records on 6/1, 178,719 distinct trips, but only 62,794 with annotation (as of 2021-06-22)
--       vts.isCancelled mostly represents trips moving off system account, but not ALWAYS
--       Fees (FPBP) and TollDisc types occur at same time as toll so also sort on type
-- first look at transaction type flows
SELECT  txn_pattern
        ,count(*)
  FROM  (SELECT t.tripid
                ,string_agg(ett.shortcode,  ',') WITHIN GROUP (ORDER BY vts.postingDate, ett.shortCode) AS txn_pattern
          FROM  fastlane.xact.TollLedgerEntry tle
          JOIN  fastlane.lance.Trip t ON t.tripId = tle.tollId
          JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
          JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
          JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
          JOIN  fastlane.xact.EnuTransactionCategory etc ON etc.transactionCategoryId = ett.transactionCategoryId
          --LEFT JOIN fastlane.lance.TripPassageExternalReference tper ON tper.tripPassageId = t.tripPassageId
          --LEFT JOIN fastlane.xact.Annotation ann ON ann.annotationid = tle.endAnnotationId
          --LEFT JOIN fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
         WHERE  cast(tp.laneTime AS date) = convert(date,'2021-06-01')
        GROUP BY t.tripId) AS x
GROUP BY txn_pattern
ORDER BY 2 DESC;

-- look at TCVP and FCVP (Toll and Fee NOCP Unearned) (280 sec)
-- 15479 are INVTOLL2,FCVP,TCVP
--    55 are INVTOLL2,FCVP,TCVP,WRITEOFF
SELECT  txn_pattern
        ,count(*)
  FROM  (SELECT tle.tollid
                ,string_agg(ett.shortcode,  ',') WITHIN GROUP (ORDER BY vts.postingDate, ett.shortCode) AS txn_pattern
          FROM  fastlane.xact.TollLedgerEntry tle
          JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
          JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
         WHERE  tle.tollId IN (SELECT tle2.tollId
                   FROM fastlane.xact.TollLedgerEntry tle2
                   JOIN fastlane.xact.V_TransactionSummary vts2 ON vts2.transactionId = tle2.ledgertransactionId
                   JOIN fastlane.xact.EnuTransactionType ett2 ON ett2.transactionTypeId = vts2.transactionTypeId
                  WHERE ett2.shortCode IN ('FCVP', 'TCVP')
                )
        GROUP BY tle.tollId) AS x
GROUP BY txn_pattern
ORDER BY 2 DESC;

-- look at all trips with three or more simultaneous txns in ledger
-- also look at trips with repeated same txn types
SELECT  tle.*
        ,vts.*
        ,ett.*
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
 WHERE  tle.tollId IN
        (SELECT  x.tollId
          FROM  (SELECT tle2.tollId
                        ,vts2.postingDate
                        ,vts2.transactionTypeId     -- comment this out for first check
                        ,count(*) AS txn_cnt
                  FROM  fastlane.xact.TollLedgerEntry tle2
                  JOIN  fastlane.xact.V_TransactionSummary vts2 ON vts2.transactionId = tle2.ledgertransactionId
                GROUP BY tle2.tollId
                        ,vts2.postingDate
                        ,vts2.transactionTypeId     -- comment this out for first check
                HAVING  count(*) > 1) AS x          -- change from 2 to 1 for second check
        )
ORDER BY tle.tollId
        ,vts.postingDate
        ,ett.shortCode;

/********** WSDOT_STAGE TABLES **********/
-- work in our database
USE WSDOT_STAGE;

-- look at mac time between trip and ledger posting (this can happen due to Kapsch issues)
-- also look at max time from trip post to ledger posting date
-- since July 10, largest delay in processing was less than a day (16 hours)
SELECT  TOP 20 x.*
		,datediff(second, x.postTime, x.minPostingDate) AS max_post_diff
        ,datediff(second, x.laneTime, x.minPostingDate) AS max_lane_diff
  FROM  (SELECT t.tripId
                ,tp.laneTime
                ,t.created AS postTime
                ,min(vts.postingDate) AS minPostingDate
          FROM  fastlane.xact.TollLedgerEntry tle
          JOIN  fastlane.lance.Trip t ON t.tripId = tle.tollId
          JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
          JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
         WHERE  tp.laneTime > convert(date,'2021-06-01')
        GROUP BY t.tripId, t.created, tp.lanetime
) AS x
 WHERE  minPostingDate > convert(date,'2021-07-10')	-- after new processing system
ORDER BY 5 DESC;

-- quick tle/vts investigation
SELECT  tle.tollid AS tripId
		,vts.customerAccountid
        ,vts.postingDate
        ,vts.transactionId
        ,ett.shortCode
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
 WHERE  tle.tollId = 218331726
ORDER BY vts.postingDate
        ,vts.transactionId;

/********** TRIP SUMMARIES FOR TYLER **********/
-- NOTE: improved query in automated reporting; keep the query for records
-- trips by trip date and posting date
-- ctrl-alt-shift A to select all rows

-- count duplicates
-- TODO: consider summarizing only past 45-60 days?
--       write procedure to save results in table nightly for latest processing (or use v_all_trip_state)
--       then write views to produce trip and posting date summaries
SELECT  cast(tr.laneTime AS date) AS trip_date
        ,cast(tr.postTime AS date) AS posting_date
        ,vfl.wsdot_roadway AS roadway
        ,ertt.shortCode AS roadside_toll_type
        ,sum(CASE WHEN x.customerAccountId IN (16,120) THEN 1 ELSE 0 END) AS dup_count
        ,count(*) AS trip_count
  FROM  (SELECT t.tripId
                ,t.tripPassageId
                ,tp.roadGantryId
                ,tp.laneTime
                ,t.created AS postTime
                ,tp.roadsideTollType
          FROM  fastlane.lance.Trip t
          JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
         WHERE  cast(tp.laneTime AS date) >= convert(date,'2021-01-01')
           AND  cast(t.created AS date) < getdate()
        ) AS tr
        OUTER APPLY (   -- used to be CROSS APPLY
                SELECT  TOP 1 vts.postingDate AS latestTLETime
                        ,vts.customerAccountid
                        ,vts.transactionId
                  FROM  fastlane.xact.TollLedgerEntry tle
                  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
                  LEFT JOIN WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transaction_type_id = vts.transactionTypeId
                 WHERE  tle.tollId = tr.tripId
                   AND  vts.postingDate < getdate()
                ORDER BY vts.postingDate DESC
                        ,ttp.priority DESC
                        ,vts.transactionId
        ) AS x
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tr.roadsideTollType
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tr.roadGantryId
 WHERE  cast(tr.postTime AS date) >= cast(dateadd(d, -7, getdate()) AS date)	-- look at previous 6 days
--   AND  vfl.wsdot_roadway = 'I405' AND ertt.shortCode = 'PHOTOENFORCED'
GROUP BY cast(tr.laneTime AS date)
        ,cast(tr.postTime AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode
ORDER BY cast(tr.postTime AS date)
        ,cast(tr.laneTime AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode;

-- check posting and lane times
SELECT max(created) FROM fastlane.lance.trip;
SELECT max(laneTime) FROM fastlane.lance.tripPassage;

-- trip posting activity
SELECT  cast(t.created AS date) AS post_date
		,min(t.created) min_post
		,max(t.created) max_post
  FROM  fastlane.lance.trip t
--  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
 WHERE  t.created >= convert(datetime2, '2022-03-01')
GROUP BY cast(t.created AS date)
ORDER BY cast(t.created AS date);

---------- new investiation of duplicates for optimization
-- account 16, 120 - do they ever move out of that account?
-- 52357 trips with duplicate status (no repeats in this state)
SELECT  tle.tollId 
  INTO  WSDOT_STAGE.tmp.dup_trips
  FROM  fastlane.xact.TollLedgerEntry tle 
 WHERE  tle.ledgeraccountId IN (16,120);

-- 54524 transactions in those 52357 trips
SELECT  tle.ledgeraccountId, count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  WSDOT_STAGE.tmp.dup_trips d ON d.tollId = tle.tollId
GROUP BY tle.ledgeraccountId;

-- now aggregate by tollID
-- yes, terminal state is 16 or 120 so far
SELECT  account_ids
        ,count(*)
  FROM  (SELECT tle.tollId
				,string_agg(tle.ledgeraccountId,  ',') WITHIN GROUP (ORDER BY vts.postingDate, tle.ledgeraccountId) AS account_ids
		   FROM fastlane.xact.TollLedgerEntry tle
		   JOIN WSDOT_STAGE.tmp.dup_trips d ON d.tollId = tle.tollId
		   JOIN fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
		GROUP BY tle.tollId
		) t
GROUP BY account_ids;

-- look at max time to identify duplicates
-- 732 took longer than 5 days
-- max is 5775 hours (240 days)!
SELECT  *
--SELECT  max(datediff(hour, min_post, max_post))
  FROM  (SELECT tle.tollId
  				,min(vts.postingDate) AS min_post
  				,max(vts.postingDate) AS max_post
				,string_agg(tle.ledgeraccountId,  ',') WITHIN GROUP (ORDER BY vts.postingDate, tle.ledgeraccountId) AS account_ids
		   FROM fastlane.xact.TollLedgerEntry tle
		   JOIN WSDOT_STAGE.tmp.dup_trips d ON d.tollId = tle.tollId
		   JOIN fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
		GROUP BY tle.tollId
		) t
 WHERE  datediff(hour, min_post, max_post) > 168
ORDER BY max_post DESC;

-- new duplicate summary by trip and posting date
-- 2022-05-17 - updated report arch query to match
SELECT  cast(tp.laneTime AS date) AS trip_date
		,cast(t.created AS date) AS posting_date
		,vfl.wsdot_roadway AS roadway
		,ertt.shortCode AS roadside_toll_type
		,count(d.tollId) AS dup_count
		,count(*) AS trip_count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tp.roadsideTollType
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  LEFT  JOIN (SELECT DISTINCT tollId FROM fastlane.xact.TollLedgerEntry WHERE ledgeraccountId IN (16,120)) d ON d.tollId = t.tripId 
 WHERE  t.created >= '2022-03-01'
GROUP BY cast(tp.laneTime AS date)
        ,cast(t.created AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode
ORDER BY cast(tp.laneTime AS date)
        ,cast(t.created AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode;

-- old model for posterity
-- 2023-10-30: change posting_time to etan_received_time
SELECT  cast(tdb.laneTime AS date) AS trip_date
		,cast(tdb.etan_received_time AS date) AS posting_date
		,tdb.roadway
		,ertt.shortCode AS roadside_toll_type
		,sum(CASE WHEN tdb.customerAccountid IN (16, 120) THEN 1 ELSE 0 END) AS dup_count
		,count(*) AS trip_count
  FROM  WSDOT_STAGE.util.v_trip_detail_base tdb
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tdb.roadside_toll_type
GROUP BY cast(tdb.laneTime AS date)
		,cast(tdb.etan_received_time AS date)
		,tdb.roadway
		,ertt.shortCode;

/*######################################################################*/
/*##########                  ACCOUNT TYPES                   ##########*/
/*######################################################################*/
-- see tables in report arch
CREATE TABLE #acct_types (
	customerAccountId int NOT NULL PRIMARY KEY
	,revenueTypeId int NOT NULL
	,customerTypeId int NOT NULL
	,primaryPoC char NOT NULL
	,isPrepaid bit NOT NULL
	,hasReplenishment bit NOT NULL
	,isExpiryGreaterThan90Days bit NOT NULL
	--,hasLpOrTransponder bit NOT NULL
	,isSystemGenerated bit NOT NULL
	,accountTypeId INT NOT NULL
	,shortcode varchar(64) NOT NULL
);

-- look at variation in types from temp result of fastlane.util.USP_AccountTypesAll
-- customerTypeId (1 or 3), primaryPoC (C, I, or X), and all bit categories vary for UNKNOWN type
-- isExpiryGreaterThan90Days also varies for COMMERCIAL
-- isSystemGenerated also varies for INDIVIDUAL and ZBAINDIVIDUAL (but only a handful are system-generated)
-- see fastlane.util.IV_AccountCond_IsSystemGenerated - is based on join with customer, opening type enum = 'SYSTEM'
-- based on review, only need to store customer_account_id and account type in temporal table
SELECT  t.accountTypeId
		,t.shortcode
		,revenueTypeId 
		,isSystemGenerated
		,count(*) AS count
  FROM  #acct_types t
GROUP BY t.accountTypeId
		,t.shortcode
		,revenueTypeId 
		,isSystemGenerated
ORDER BY t.accountTypeId
		,t.shortcode;

-- now look at declared type for migrated accounts only
SELECT  ca.customerAccountId
		,ca.legacyAccountNumber
		,eat.description AS declared_acct_type
		,t.shortcode AS derived_acct_type
		,catd.declaredAccountTypeId
		,hcac.fromAccountTypeId
		,hcac.toAccountTypeId 
		,t.accountTypeId AS derived_acct_type_id
        ,ebt.description AS balance_type
        ,vas.status
        ,ca.isDefunct 
  FROM  fastlane.util.CustomerAccount ca
  JOIN  fastlane.xact.EnuBalanceType ebt ON ebt.balanceTypeId = ca.balanceTypeId
  LEFT  JOIN  #acct_types t ON t.customerAccountId = ca.customerAccountId 
  LEFT  JOIN  fastlane.util.CustomerAccountTypeDetails catd ON catd.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.util.HlpCustomerAccountConversion hcac ON hcac.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = catd.declaredAccountTypeId
  LEFT  JOIN  fastlane.util.CustomerAccountFlag caf ON caf.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = ca.customerAccountId
 WHERE  ca.legacyAccountNumber  IS NOT NULL				-- legacy ETCC accounts
   AND  t.accountTypeId <> catd.declaredAccountTypeId	-- account types do not match
   AND  catd.declaredAccountTypeId <> isnull(hcac.fromAccountTypeId, 0)	-- initial types do not match
   AND  isnull(hcac.toAccountTypeId, 0) <> t.accountTypeId 				-- final types do not match
;

SELECT  * FROM fastlane.util.HlpCustomerAccountConversion hcac WHERE hcac.customerAccountId IN (1185,1251,1327,1524,1961);

/*######################################################################*/
/*##########                 TRIP DISPOSITION                 ##########*/
/*######################################################################*/
-- see info in report arch

-- test for getting latest posting date and 'correct' latest tranaction id for each trip
-- use CROSS APPLY (equivalent to LATERAL JOIN in postgres)
-- NOTE: use txn_type_priority to select transaction type of interest
--       for duplicate writeoffs, should get basecode TCPP or TPBP
--       for duplicate tolldiscs, should get amount <> 0.25
--       however, it seems all WRITEOFF and TOLLDISC duplicates should use first simultaneous transaction (lower txn id)
--       could use DATEDIFF to compare dates since direct equality misses some matches
--       however, adding posting date to cross apply works better and avoids datediff overflow
--       finally, be sure to use LEFT JOIN on priority since not all txn types are in priority table
-- TODO: create procedure to run this and store last date run (always at midnight pacific time)
--       re-write to update table only for transactions that occured on any trip after last run
-- use OUTER instead of CROSS for when there are no ledger entries yet!
SELECT  tr.tripId
		,tr.tripPassageId
        ,tr.roadGantryId
        ,tr.laneTime
        ,tr.postTime
        ,tr.roadsideTollType
        ,x.latestTLETime
        ,x.transactionId
        ,cast(getdate() AS datetime2) AS asOfDatetime
        --,0 AS isFinal
  FROM  (SELECT t.tripId
  				,t.tripPassageId
                ,tp.roadGantryId
                ,tp.laneTime
                ,t.created AS postTime
                ,tp.roadsideTollType
          FROM  fastlane.lance.Trip t
          JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
         --WHERE  cast(tp.laneTime AS date) BETWEEN convert(date,'2020-07-01') AND convert(date,'2020-12-31')
         WHERE  cast(tp.laneTime AS date) >= convert(date,'2021-01-01')
           AND  cast(t.created AS date) < getdate()
        ) AS tr
        OUTER APPLY (   -- used to be CROSS APPLY
                SELECT  TOP 1 vts.postingDate AS latestTLETime
                        ,vts.transactionId
                  FROM  fastlane.xact.TollLedgerEntry tle
                  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
                  LEFT JOIN WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transactionTypeId = vts.transactionTypeId
                 WHERE  tle.tollId = tr.tripId
                   AND  cast(vts.postingDate AS date) < getdate()
                ORDER BY vts.postingDate DESC
                        ,ttp.priority DESC
                        ,vts.transactionId
        ) AS x;

-- count of trips
SELECT  count(*)
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
 WHERE  tp.laneTime >= convert(datetime2, '2020-07-01')
   AND  tp.laneTime < convert(datetime2, '2021-01-01');

/* testing code

SELECT * FROM fastlane.lance.Trip WHERE tripId = 217688355; --217818348;
SELECT min(tripId) FROM fastlane.lance.Trip WHERE created > convert(date,'2021-07-01');   -- 217688355
SELECT min(created) FROM fastlane.lance.Trip WHERE tripId >= 217688355;     -- 2021-07-10 09:15:25

SELECT  DISTINCT vfl.wsdot_roadway
  FROM  #trip_info ti
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = ti.roadsideTollType
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = ti.roadGantryId
 WHERE  cast(laneTime AS date) = convert(date,'2021-06-30')
   AND  cast(postTime AS date) = convert(date,'2021-07-12');

SELECT  DISTINCT cast(laneTime AS date)
  FROM  fastlane.lance.Trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
 WHERE laneTime >= convert(date,'2021-07-01');

-- sample fee trips
-- Pay By Plate (PBP): 217780678
-- Pay By Mail (PBM):  217786711
-- Op override (OPO):  217847781

-- QUESTION: why is pay by plate a separate fee, but pay by mail is incorporated into toll amount?

-- look at PayByPlate (PBP)
SELECT DISTINCT vts.amount FROM fastlane.xact.V_TransactionSummary vts WHERE vts.transactionTypeId = 25;

SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
  --JOIN  fastlane.xact.V_TransactionSummary vts2 ON vts2.ledgerItemInfoId = vts.ledgerItemInfoId AND vts2.transactionTypeId <> 25
-- WHERE  vts.transactionTypeId = 25
 WHERE  tle.tollId = '217780678'
ORDER BY vts.postingDate DESC;

SELECT * FROM fastlane.xact.V_TransactionSummary vts WHERE vts.ledgerItemInfoId = 246860406;

*/

-- roadside summary
SELECT  cast(ti.laneTime AS date) AS trip_date
        ,cast(ti.postTime AS date) AS posting_date
        ,vfl.wsdot_roadway AS roadway
        ,ertt.shortCode AS roadside_toll_type
        ,count(*) AS trip_count
  FROM  #trip_info ti
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = ti.roadsideTollType
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = ti.roadGantryId
 WHERE  cast(ti.laneTime AS date) < convert(date,'2021-07-01')
GROUP BY cast(ti.laneTime AS date)
        ,cast(ti.postTime AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode
ORDER BY cast(ti.laneTime AS date)
        ,cast(ti.postTime AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode;

-- check if lance.trip contains same info as rtoll.Toll
-- in fact, rtoll.toll is a view based on lance.trip!
SELECT  CASE WHEN t.transponderId IS NULL THEN 0 ELSE 1 END AS tag_present
        ,CASE WHEN t.licensePlateId IS NULL THEN 0 ELSE 1 END AS lp_present
        ,count(*)
  FROM  fastlane.rtoll.Toll t
--  JOIN  fastlane.lance.Trip tr ON tr.tripId = t.TollID
--  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = tr.tripPassageId
 WHERE  cast(t.exitTime AS date) = convert(date,'2021-07-01')
--   AND  t.exitTime <> tp.laneTime
--   AND  t.exitPlazaCodeId = tp.roadGantryId
GROUP BY CASE WHEN t.transponderId IS NULL THEN 0 ELSE 1 END
        ,CASE WHEN t.licensePlateId IS NULL THEN 0 ELSE 1 END;

-- vts basecode, prefix and suffix investigation - also look at table definition
SELECT * FROM fastlane.xact.V_TransactionSummary vts WHERE concat(vts.basecode,vts.suffix) <> vts.tcode;
SELECT * FROM fastlane.xact.V_TransactionSummary vts WHERE trim(concat(vts.prefix,(CASE WHEN vts.prefix = '' THEN '' ELSE '-' END),vts.tcode)) <> trim(vts.ptcode);

/***** MONTHLY DISPOSITION SUMMARY *****/
-- begin attempt at monthly disposition summary
-- NOTE: left joins on tle and supporting tables since tle entry may not yet be available
-- NOTE: vts.isAllocated is 1 when any payment has been applied, even if not full amount
--       vts.isFullyAllocated is 1 when full amount is paid, but may also be applied in other cases?
--       vts.isUnpaid is 1 when full amount is not paid, e.g. inverse of isFullyAllocated
--       if cancelled and fully allocated, is usually still posting to account after MIR
SELECT  vfl.wsdot_roadway AS roadway
        --,cast(ti.laneTime AS date) AS trip_date
		,cast(dateadd(month, datediff(month, 0, ti.laneTime), 0) AS date) AS trip_month
        ,ti.roadsideTollType
        --,ertt.shortCode AS roadside_toll_type_desc
        ,ett.shortCode AS transaction_type
        --,etc.description AS transaction_category
        ,eet.shortCode AS enforcement_type
        ,CASE WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NULL THEN 'no_lp_or_pass'
        	  WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NOT NULL THEN 'pass_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NULL THEN 'lp_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NOT NULL THEN 'both_lp_and_pass'
        	END AS identifiers
        --,CASE WHEN tl.licensePlateId IS NULL THEN 0 ELSE 1 END AS lp_present
        --,CASE WHEN tpt.transponderId IS NULL THEN 0 ELSE 1 END AS pass_present
        ,enca.customerAccountId AS system_account_id
        --,enca.description AS sys_account_description
        ,epp.shortCode AS pricing_plan
        ,eer.shortCode AS entry_reason
        --,eer.description AS entry_reason_desc
        --,erp.ShortCode AS dol_lookup_provider
        --,eds.shortCode AS dol_status_code
        ,sum(CASE WHEN ett.shortCode = 'TOLLDISC' THEN 0 ELSE abs(vts.amount) END) AS sum_toll_charges
        ,sum(cast(vts.isCancelled AS integer)) AS cancelled_count
        ,sum(cast(vts.isFullyAllocated AS integer)) AS fully_allocated_count
        ,sum(cast(vts.isUnpaid AS integer)) AS unpaid_count
        ,count(DISTINCT ti.tripId) AS trip_count
        ,count(*) AS record_count
        --,count(eds.shortCode) AS dol_lookup_count
        --,sum(CASE WHEN vdlrs.dmvLookupId IS NULL THEN 1 ELSE 0 END) AS dol_norequest_count
  FROM  #trip_info ti
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = ti.roadGantryId
  --JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = ti.roadsideTollType
  --FROM  fastlane.xact.TollLedgerEntry tle
  --JOIN  fastlane.lance.Trip t ON t.tripId = tle.tollId
  --JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
  LEFT JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ti.transactionId
  LEFT JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ti.transactionId AND vts.customerAccountid = tle.ledgeraccountId
  LEFT JOIN  fastlane.xact.EnuEnforcementType eet ON eet.enforcementTypeId = tle.enforcementTypeId
  LEFT JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
  LEFT JOIN  fastlane.xact.EnuNonCustomerAccounts enca ON enca.customerAccountId = vts.customerAccountid
  --LEFT JOIN  fastlane.xact.EnuTransactionCategory etc ON etc.transactionCategoryId = ett.transactionCategoryId
  --LEFT JOIN fastlane.lance.TripPassageExternalReference tper ON tper.tripPassageId = ti.tripPassageId
  LEFT JOIN  fastlane.xact.Annotation ann ON ann.annotationid = tle.endAnnotationId
  LEFT JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
  LEFT JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId
  LEFT JOIN  fastlane.lance.TripLp tl ON tl.tripId = ti.tripId
  LEFT JOIN  fastlane.lance.TripPassageTransponder tpt ON tpt.tripPassageId = ti.tripPassageId
  --LEFT JOIN  fastlane.monitor.V_dmv_lookup_request_status vdlrs ON vdlrs.licensePlateId = tl.licensePlateId
  -- per fastlane.reports.rpt_daily_toll_transaction_processing_summary, use this table instead and check seq_order
  --LEFT JOIN  fastlane.vehicle.V_DMVLookUpStatus vdus ON vdus.licensePlateId = tl.licensePlateId AND vdus.seq_order = 0
  --LEFT JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = tl.licensePlateId
  --LEFT JOIN  fastlane.vehicle.ROVLookupRouter rr ON rr.lpJurisdictionId = lp.lpJurisdictionId AND rr.licensePlatePatternClassId = lp.licensePlatePatternClassId
  --LEFT JOIN  fastlane.vehicle.EnuROVLookupProvider erp ON erp.ROVLookupProviderid = rr.ROVLookupProviderid
  --LEFT JOIN  fastlane.vehicle.EnuDMVLookupState eds ON eds.dmvLookupStateId = vdus.dmvLookupStateId
 --WHERE  cast(ti.laneTime AS date) = convert(date,'2021-07-01')
GROUP BY vfl.wsdot_roadway
        --,cast(ti.laneTime AS date)
        ,cast(dateadd(month, datediff(month, 0, ti.laneTime), 0) AS date)
        ,ti.roadsideTollType
        --,ertt.shortCode
        ,ett.shortCode
        --,etc.description
        ,eet.shortCode
        ,CASE WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NULL THEN 'no_lp_or_pass'
        	  WHEN tl.licensePlateId IS NULL AND tpt.transponderId IS NOT NULL THEN 'pass_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NULL THEN 'lp_only'
        	  WHEN tl.licensePlateId IS NOT NULL AND tpt.transponderId IS NOT NULL THEN 'both_lp_and_pass'
        	END
        --,CASE WHEN tl.licensePlateId IS NULL THEN 0 ELSE 1 END
        --,CASE WHEN tpt.transponderId IS NULL THEN 0 ELSE 1 END
        ,enca.customerAccountId
        --,enca.description
        ,epp.shortCode
        ,eer.shortCode
        --,eer.description
        --,erp.ShortCode
        --,eds.shortCode 
ORDER BY vfl.wsdot_roadway
        --,cast(ti.laneTime AS date)
        ,cast(dateadd(month, datediff(month, 0, ti.laneTime), 0) AS date)
        ,count(*) DESC;

-- trip ids for Ami:
-- [done] writeoff transaction types for Ami
-- [done] type 90's with PBM pricing plan
-- [done] type 92's with PBM pricing plan
-- [done] custom tolldisc (not HOV, MOT, or NRV)
-- [done] get ETB tag trips on PAS pricing plan in sys account 121
-- [done] pricing plan PEN, no sys account but assigned to customer account
-- [done] 20211222 - new unknown types appearing!
SELECT  *
  FROM  WSDOT_STAGE.util.v_trip_detail t
-- WHERE  t.disposition_category = 'WRITEOFF' AND month(t.laneTime) >= 5
-- WHERE  t.pricing_plan = 'PBM' AND t.roadsideTollType IN (90, 92);
-- WHERE  t.disposition_category = 'CUSTOM_TOLLDISC' AND month(t.laneTime) = 10
-- WHERE  t.transaction_type = 'ETB' AND t.pricing_plan = 'PAS' AND t.system_account_id = 121
-- WHERE  t.pricing_plan = 'PEN' AND t.system_account_id IS NULL AND month(t.laneTime) = 10
  WHERE t.disposition_category = 'UNKNOWN'
;

-- look at sample unknown types:
SELECT * FROM fastlane.lance.trip t WHERE t.tripId = 244904129;
SELECT * FROM fastlane.xact.TollLedgerEntry tle WHERE tle.tollId = 244904129;
SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  LEFT JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId  AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  tle.tollId = 244904129;

-- meeting 2021-11-17 with DeeAnn and Ami - look at 'unbilled' trips
-- unbilled is PENDING_IR, PENDING_DOL, and SYS_252 (the latter will eventually go away)
-- also, TPENO (which is basically unbilled) should be lower as those with entry reasons should not be O, but D
-- almost a million trips are here (vast majority LEAK_1 with T-entry reasons based on image review)
-- this could be due to 120-day dismissal job not running yet, which may pick up MIR rejects as well
SELECT  t.disposition_category
		,t.prefix
		,t.basecode
		,t.suffix
		,t.entry_reason
		,count(*) AS count
  FROM  WSDOT_STAGE.util.v_trip_detail t
 WHERE  t.basecode = 'TPEN'
   AND  t.disposition_category NOT IN ('PENDING_IR', 'PENDING_DOL', 'SYS_252')
GROUP BY t.disposition_category
		,t.prefix
		,t.basecode
		,t.suffix
		,t.entry_reason
ORDER BY t.disposition_category
		,count(*) DESC;

/***** OTHER INVESTIGATIONS *****/
SELECT  ti.tripId
  FROM  #trip_info ti
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ti.transactionId
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ti.transactionId
  LEFT JOIN  fastlane.xact.EnuEnforcementType eet ON eet.enforcementTypeId = tle.enforcementTypeId
  LEFT JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId
  LEFT JOIN  fastlane.xact.Annotation ann ON ann.annotationid = tle.endAnnotationId
  LEFT JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
 WHERE  cast(ti.laneTime AS date) = convert(date,'2021-07-01')
   AND  eet.shortCode = 'NONE'
   AND  epp.shortCode = 'OPO';
   --AND  eer.shortCode = 'S-MATCH';

SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
 WHERE  tle.tollId = '217847781'
ORDER BY vts.postingDate DESC;

-- look at NOCP certification
SELECT  nq.nocpCertificationId
        ,t.tripId
        ,nq.customerAccountId
        ,ens.shortCode AS nocpCertificationStatus
        ,enrc.shortCode AS nocpCertRejectCode
  FROM  fastlane.xact.NOCPCertificationQueue nq
  JOIN  fastlane.lance.Trip t ON t.ledgerItemInfoId = nq.ledgerItemInfoId
  JOIN  fastlane.xact.EnuNOCPCertificationStatus ens ON ens.nocpCertificationStatusId = nq.nocpCertificationStatusId
  LEFT JOIN fastlane.xact.EnuNOCPCertificationRejectCode enrc ON enrc.nocpCertificationRejectCodeId = nq.nocpCertificationRejectCodeId
 WHERE  nq.isDefunct = 0;

-- Mariana's attempt to work backward from NOCP certify to get the trip date 
-- with some (but maybe not all) transactions associated with the trip
-- NOTE: transaction ids change with every update - use ledgerentryid?
SELECT  *
  FROM  fastlane.xact.NOCPCertificationQueue t
  LEFT JOIN fastlane.xact.V_TransactionSummary u ON t.ledgerItemInfoId = u.ledgerItemInfoId
  LEFT JOIN fastlane.xact.TollLedgerEntry v ON u.ledgerItemInfoId = v.ledgertransactionId
  LEFT JOIN fastlane.lance.TripPassage w ON v.tollId = w.tripPassageId
  LEFT JOIN fastlane.lance.Trip x ON v.tollid = x.tripid
 WHERE  u.ledgerItemInfoId IN (SELECT TOP 10 ledgerItemInfoId FROM xact.NOCPCertificationQueue);

-- look at v_TripSearch
-- 276,140,832, of which 22,615,119 do not match, seems to be transactionId matches on coincidence?
SELECT count(*) FROM fastlane.util.v_TripSearch;
SELECT count(*) FROM fastlane.util.v_TripSearch WHERE transactionid <> ledgerentryid;
SELECT * FROM fastlane.util.v_TripSearch WHERE transactionid <> ledgerentryid;

-- status 2 appears, but not 6 - why?
SELECT DISTINCT transponderStatusId FROM fastlane.lance.TripPassageTransponder tpt;

/***** DOL LOOKUP STATUS *****/
SELECT  erp.ShortCode AS provider
        ,edlet.description AS event_type
        ,eds.dmvLookupStateId AS status_id
        ,eds.shortCode AS status_shortcode
        ,eds.description AS status_description
        ,sum(CASE WHEN vdlrs.dmvLookupEventReqId IS NOT NULL THEN 1 ELSE 0 END) AS lookup_request_count
        ,sum(CASE WHEN vdlrs.dmvLookupEventResId IS NOT NULL THEN 1 ELSE 0 END) AS lookup_result_count
        ,count(*) AS lookup_total_count
  FROM  fastlane.monitor.V_dmv_lookup_request_status vdlrs
  LEFT JOIN  fastlane.vehicle.EnuDmvLookupEventType edlet ON edlet.dmvLookupEventTypeId = vdlrs.dmvLookupEventTypeId
  LEFT JOIN  fastlane.vehicle.EnuROVLookupProvider erp ON erp.ROVLookupProviderid = vdlrs.ROVLookupProviderid
  LEFT JOIN  fastlane.vehicle.EnuDMVLookupState eds ON eds.dmvLookupStateId = vdlrs.dmvLookupRequestStatus
GROUP BY erp.ShortCode
        ,edlet.description
        ,eds.dmvLookupStateId
        ,eds.shortCode
        ,eds.description
ORDER BY 1,2,3,4,5;

-- earliest no-provider lookup
SELECT  min(queue_time) FROM  fastlane.monitor.V_dmv_lookup_request_status vdlrs WHERE errorid IS NULL;
SELECT  cast(queue_time AS date) AS queue_date, count(*) FROM  fastlane.monitor.V_dmv_lookup_request_status vdlrs WHERE errorid IS NULL GROUP BY cast(queue_time AS date) ORDER BY 1;
SELECT  max(queue_time) FROM  fastlane.monitor.V_dmv_lookup_request_status vdlrs WHERE errorid IS NOT NULL;

-- additional look at XML response for some plates
SELECT  erp.ShortCode AS provider
        ,edlet.description AS event_type
        ,eds.dmvLookupStateId AS status_id
        ,eds.shortCode AS status_shortcode
        ,eds.description AS status_description
        ,vdlrs.dmvLookupId
        ,lp.licensePlateId
        ,lp.lpnumber
        ,vdlrs.req_time
        ,vdlrs.req_payload
        ,vdlrs.resp_payload
  FROM  fastlane.monitor.V_dmv_lookup_request_status vdlrs
  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = vdlrs.licensePlateId
  LEFT JOIN  fastlane.vehicle.EnuDmvLookupEventType edlet ON edlet.dmvLookupEventTypeId = vdlrs.dmvLookupEventTypeId
  LEFT JOIN  fastlane.vehicle.EnuROVLookupProvider erp ON erp.ROVLookupProviderid = vdlrs.ROVLookupProviderid
  LEFT JOIN  fastlane.vehicle.EnuDMVLookupState eds ON eds.dmvLookupStateId = vdlrs.dmvLookupRequestStatus
  LEFT JOIN  fastlane.vehicle.DmvLookupEventRes dler ON dler.dmvLookupEventResId = vdlrs.dmvLookupEventResId
 WHERE  lp.lpnumber IN ('1499BK','7621AA','8041Q','2341CJ','3453N','BXV8347','BYC6066','BYR9362','BOE1909','C14391W','BQP6264','C81359W');

/***** LINK ADDRESS TO CUSTOMER ACCOUNT *****/
-- NOTE: customers can have more than one account? however, only account 0 has more, it has 13,692 as of 2021-07-19
--       only custermerId 199 does not have a customer account?
--       also look at rtoll.LicensePlateCustomerAccount, rtoll.TransponderCustomerAccount, and FleetAccountMap

SELECT count(*) FROM fastlane.util.CustomerAccount ca WHERE ca.customerId = 0;
SELECT count(*) FROM fastlane.info.Customer c;
SELECT count(*) FROM fastlane.info.PersonOrCompany poc;

SELECT  c.customerId
        --,ca.customerAccountId
        ,count(*)
  FROM  fastlane.util.CustomerAccount ca
  JOIN  fastlane.info.Customer c ON c.customerId = ca.customerId
GROUP BY c.customerId
        --,ca.customerAccountId
HAVING count(*) > 1;

SELECT  *
  FROM  fastlane.info.Customer c
  LEFT JOIN fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
 WHERE  ca.customerAccountId IS NULL;

SELECT count(DISTINCT customerId) FROM fastlane.util.CustomerAccount ca;

SELECT  count(*)
  FROM  fastlane.info.PostalAddress pa
  JOIN  fastlane.locale.EnuState es ON es.stateId = pa.stateId
  JOIN  fastlane.locale.EnuCountry ec ON ec.countryId = es.countryId
  JOIN  fastlane.info.EnuAddressSource eas ON eas.addressSourceId = pa.addressSourceId

/***** TRIP COUNT DISCREPANCY (Michael Gaunt) *****/
-- look at July 2021 trips
-- perhaps tripPassage has more records due to some sort of processing

-- https://stackoverflow.com/a/2639057
-- in lance.tripPassage: 9863506
-- in lance.trip: 5481190
-- in rtoll.Toll: 5481190
SELECT  count(*) as count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.trippassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  dateadd(month, datediff(month, 0, tp.lanetime), 0) = '2021-07-01 00:00:00';

SELECT  count(*) as count
  FROM  fastlane.rtoll.Toll t
 WHERE  WSDOT_STAGE.util.f_get_month(t.exittime) = '2021-07-01';

/***** NO SALES TAX ISSUE *****/
SELECT * FROM fastlane.inv.InvTagRequest itr;
SELECT * FROM fastlane.xact.TagRequestLedger trl;
SELECT * FROM fastlane.util.CustomerAccount ca;
SELECT * FROM fastlane.info.Customer c;
SELECT * FROM fastlane.monitor.SalesTaxEventRequest ster;

-- get entries with type 2 (pass sale) and type 31 (sales tax) at same time with same ledgerItemInfoId
SELECT  vts.postingDate
        ,vts.ledgerItemInfoId
        ,vts.customerAccountid
        ,pa.address1 AS shipping_address
        ,pa.city AS shipping_city
        ,es.shortCode AS shipping_state
        ,pa.postalCode AS shipping_zip
        ,ster.salesTaxEventRequestId
        ,ster2.salesTaxEventResponseId
        ,llc.locCode AS location_code
        ,vts.tcode AS pass_code
        ,vts.amount AS pass_amount
        ,vts2.tcode AS tax_code
        ,vts2.amount AS tax_amount
--SELECT  count(*)
  FROM  fastlane.xact.V_TransactionSummary vts
  JOIN  fastlane.xact.TagRequestLedger trl ON trl.ledgerItemInfoId = vts.ledgerItemInfoId
  JOIN  fastlane.inv.InvTagRequest itr ON itr.invTagRequestId = trl.invTagRequestId
  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = itr.shippingPostalAddressId
  JOIN  fastlane.locale.EnuState es ON es.stateId = pa.stateId
  LEFT JOIN fastlane.xact.V_TransactionSummary vts2 ON vts2.ledgerItemInfoId = vts.ledgerItemInfoId AND vts.postingDate = vts2.postingDate AND vts2.transactionTypeId <> 2
  LEFT JOIN fastlane.xact.LedgetLocationCode llc ON llc.customerAccountId = vts.customerAccountid AND llc.ledgerEntryId = vts.ledgerentryid
  LEFT JOIN fastlane.monitor.SalesTaxEventRequest ster ON ster.postalAddressId = pa.postalAddressId
  LEFT JOIN fastlane.monitor.SalesTaxEventResponse ster2 ON ster2.salesTaxEventRequestId = ster.salesTaxEventRequestId
 WHERE  vts.transactionTypeId = 2
   --AND  vts2.tcode IS NULL
   AND  vts.postingDate > convert(date,'2021-07-01')
ORDER BY vts.postingDate DESC;

/***** DOUBLE PAYMENT ISSUE *****/

-- NOTE: grouping by time is weird; use dateadd/datediff to round to nearest second
-- DROP TABLE WSDOT_STAGE.tmp.duplicate_payments;
SELECT  ipd.customerAccountId
        ,ipd.ptcode
        --,left(format(ipd.postingDate,'yyyy-MM-dd HH:mm'),15) + '0' AS postingDate     -- truncates to 10 min interval
        ,dateadd(ss, datediff(ss, convert(date,'2021-07-01'), ipd.postingDate), '2021-07-01') AS postingDate
        ,ipd.amount
        ,count(*) AS count
--  INTO  WSDOT_STAGE.tmp.duplicate_payments
  FROM  fastlane.payment.IV_PaymentDetails ipd
 WHERE  ipd.postingDate >= convert(date,'2021-07-01')
-- WHERE  ipd.tcode IN ('VMCDP','AMEX')
GROUP BY ipd.customerAccountId
        ,ipd.ptcode
        --,left(format(ipd.postingDate,'yyyy-MM-dd HH:mm'),15) + '0'
        ,dateadd(ss, datediff(ss, convert(date,'2021-07-01'), ipd.postingDate), '2021-07-01')
        ,ipd.amount
HAVING count(*) > 1
ORDER BY 3 DESC;

--SELECT count(*) FROM WSDOT_STAGE.tmp.duplicate_payments;

SELECT * FROM fastlane.payment.IV_PaymentDetails ipd WHERE customerAccountid = 83580 ORDER BY postingDate DESC;
SELECT * FROM fastlane.payment.Payment p WHERE p.paymentId IN (25487242, 25490934);
SELECT * FROM fastlane.payment.IV_PaymentDetails ipd WHERE ipd.paymentId IN (25487242, 25490934);

SELECT  ipd.customerAccountid
        ,ipd.ptcode
        ,ipd.amount
        ,ipd.postingDate AS IVPDpostingDate
        ,p.postingDate AS payPostingDate
  FROM  fastlane.payment.IV_PaymentDetails ipd
  JOIN  fastlane.payment.Payment p ON p.paymentId = ipd.paymentId
 WHERE  ipd.customerAccountid > 200
   AND  ipd.customerAccountid IN (SELECT customerAccountid FROM WSDOT_STAGE.tmp.duplicate_payments)
   AND  dateadd(ss, datediff(ss, convert(date,'2021-07-01'), p.postingDate), 0) <> dateadd(ss, datediff(ss, convert(date,'2021-07-01'), ipd.postingDate), 0)
ORDER BY 1, 4 DESC;

/***** (UN)CLOSED ACCOUNTS *****/
SELECT  ca.customerAccountId
        ,ca.legacyAccountNumber
        ,icac.contacttype
        ,icac.displayname
        ,concat_ws(' ', pa.address1, pa.address2) AS mailing_address
        ,catd.declaredAccountTypeId
        ,eat.description AS account_type_desc
        ,vas.status
  FROM  fastlane.util.CustomerAccount ca
  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = ca.customerAccountId
  JOIN  fastlane.xact.EnuBalanceType ebt ON ebt.balanceTypeId = ca.balanceTypeId
  LEFT  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId
  LEFT  JOIN  fastlane.util.CustomerAccountTypeDetails catd ON catd.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.util.HlpCustomerAccountConversion hcac ON hcac.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = catd.declaredAccountTypeId
  LEFT  JOIN  fastlane.util.CustomerAccountFlag caf ON caf.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = ca.customerAccountId
--  LEFT  JOIN  fastlane.util.EnuFlag ef ON caf.flagId = ef.flagId
--  LEFT  JOIN  fastlane.util.EnuFlagState efs ON efs.flagStateId = caf.flagStateId
--  LEFT  JOIN  fastlane.reports.Account_Status_Report_2021 asr ON asr.AccountNo = ca.customerAccountId
 WHERE  icac.contacttype = 'MAIN'
   AND  ca.customerAccountId IN (934188, 2226, 20355, 11953, 1143778);
  
-- look at address types
-- does seem to be only one MAIN per account
SELECT  *
  FROM  (SELECT icac.*
  				--,customerAccountId
				--,icac.contacttype
        		--,icac.displayname
		        --,icac.isCompany
        		,count(*) OVER (PARTITION BY icac.customerAccountId) AS contact_cnt
		   FROM fastlane.util.IV_CustomerAccountContacts icac
		) AS t
-- WHERE  t.contact_cnt > 1 --AND t.customerAccountId = 11953
 WHERE  t.contact_cnt = 1 AND t.contacttype <> 'MAIN'
ORDER BY t.customerAccountId;

-- look at address types
-- does seem to be only one MAIN per account
SELECT  icac.customerAccountId
		,count(*)
  FROM  fastlane.util.IV_CustomerAccountContacts icac
 WHERE  icac.contacttype = 'MAIN' 
GROUP BY icac.customerAccountId
HAVING count(*) > 1;

-- however, 72 accounts also have no main contact (many have only one contact total though)
SELECT  * FROM (
SELECT  icac.customerAccountId
		,sum(CASE WHEN contacttype = 'MAIN' THEN 1 ELSE 0 END) AS main_cnt
		,count(*) AS tot_cnt
  FROM  fastlane.util.IV_CustomerAccountContacts icac
GROUP BY icac.customerAccountId
--HAVING count(*) > 1
) d WHERE main_cnt = 0;

-- check procedure
-- see https://stackoverflow.com/a/34357785
-- INSERT INTO #tab ;
EXEC fastlane.util.USP_AccountTypesAll;

-- quick investigation into ghost account for Ami
SELECT  ca.customerAccountId 
		,ca.customerId 
		,ca.legacyAccountNumber 
		,ebt.shortCode AS balanceType
		,ect.shortCode AS customerType
		,icac.displayname 
  FROM  fastlane.util.CustomerAccount ca
  JOIN  fastlane.info.Customer c ON c.customerId = ca.customerId
  JOIN  fastlane.xact.EnuBalanceType ebt ON ebt.balanceTypeId = ca.balanceTypeId 
  JOIN  fastlane.info.EnuCustomerType ect ON ect.customerTypeId = c.customerTypeId 
  LEFT JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = ca.customerAccountId
 WHERE  ca.customerAccountId = 4409968;

SELECT  *
--  FROM  fastlane.rtoll.TransponderCustomerAccount c
  FROM  fastlane.rtoll.LicensePlateCustomerAccount c
 WHERE  c.customerAccountId = 4409968;

-- account opening date
SELECT  ca.*
		,ca.postingDate AS opening_date
  FROM  fastlane.util.CustomerAccount ca 
--  JOIN  fastlane.util.CustomerAccountOperational cao ON cao.customerAccountId = ca.customerAccountId 
  LEFT JOIN  fastlane.util.CustomerAccountTypeDetails catd ON catd.customerAccountId = ca.customerAccountId 
  LEFT JOIN  fastlane.xact.CustomerAccountPricingPlan capp ON capp.customerAccountId = ca.customerAccountId 
 WHERE  ca.customerAccountId IN (440967, 9778038)
-- WHERE  catd.postingDate < convert(date, '2021-11-28')
ORDER BY catd.postingDate DESC;

SELECT  count(*) FROM  fastlane.rtoll.TransponderCustomerAccount tca;
SELECT * FROM fastlane.util.LicensePlate lp WHERE lp.licensePlateId = 36943998;

-- look at person or company table
SELECT  *
  FROM  fastlane.util.IV_CustomerAccountContacts icac

SELECT  icac.contacttype
		,count(*) AS count
  FROM  fastlane.util.IV_CustomerAccountContacts icac
GROUP BY icac.contacttype;

/***** HIGH NUMBER OF DAILY TRIPS *****/
-- license plates with high numbers of daily trips (from Mike Gaunt)
SELECT  lp.licensePlateId
        ,lp.lpnumber
        ,lpca.customerAccountId
        ,lpca.effectiveStartDate
        ,lpca.effectiveExpirationDate
        ,lpca.isDefunct
  FROM  fastlane.util.LicensePlate lp
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
 WHERE  lp.licensePlateId IN (1000,182329,184145,4323283,30778495,34363513,39111889,41753326,41818616)
ORDER BY 1,6,4,5;

SELECT * FROM fastlane.rtoll.LicensePlateCustomerAccount lpca WHERE licensePlateId = 39111889;

/***** PAY-AS-YOU-GO (PAYG) PLATES *****/
-- look at pay-as-you-go accounts
-- NOTE: system creates 'shadow' accounts for all plates, labeled as pay-by-mail (system) for individuals, commercial, and pending

-- example accounts
-- PAYG � Converted Commercial (8963252, 8955951, 2323572)
-- PAYG � New Commercial (9289363, 9289697, 9297842)
-- PAYG � Converted Individual (2333190, 2333205, 9025172)
-- PAYG � New Individual (9263510, 9276406, 9260525)

SELECT  DISTINCT ca.customerAccountId
        ,ca.legacyAccountNumber
        ,CASE WHEN ca.legacyAccountNumber IS NULL THEN 'New Account' ELSE 'Converted Account' END AS new_or_converted
        --,icac.displayname
        --,concat_ws(' ', pa.address1, pa.address2) AS mailing_address
        ,catd.declaredAccountTypeId
        ,eat.description AS account_type_desc
        ,eat2.description AS conversion_from_account_type
        ,eat3.description AS conversion_to_account_type
  FROM  fastlane.util.CustomerAccount ca
  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = ca.customerAccountId
  JOIN  fastlane.xact.EnuBalanceType ebt ON ebt.balanceTypeId = ca.balanceTypeId
  LEFT  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId
  LEFT  JOIN  fastlane.util.CustomerAccountTypeDetails catd ON catd.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.util.HlpCustomerAccountConversion hcac ON hcac.customerAccountId = ca.customerAccountId
  LEFT  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = catd.declaredAccountTypeId
  LEFT  JOIN  fastlane.info.EnuAccountType eat2 ON eat2.accountTypeId = hcac.fromAccountTypeId
  LEFT  JOIN  fastlane.info.EnuAccountType eat3 ON eat3.accountTypeId = hcac.toAccountTypeId
 WHERE  ca.customerAccountId IN (2333190, 2333205, 9025172, 9263510, 9276406, 9260525, 8963252, 8955951, 2323572, 9289363, 9289697, 9297842);
-- WHERE  ca.customerAccountId IN (8963252, 8955951, 2323572, 9289363, 9289697, 9297842);
-- WHERE  ca.customerAccountId IN (440967, 9778038);

/***** SPLIT TRIPS ON I-405 *****/
-- see about permissions for long-running queries or to create jobs
-- https://dba.stackexchange.com/a/273629
-- https://docs.microsoft.com/en-us/sql/ssms/agent/create-a-job?view=sql-server-ver15

USE WSDOT_STAGE;
DROP TABLE WSDOT_STAGE.tmp.toll_fac;

SELECT  t.TollID 
		,t.exitTime 
		,t.licensePlateId 
		,t.transponderId 
		,vfl.wsdot_facility
  INTO  tmp.toll_fac
  FROM  fastlane.rtoll.Toll t
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId
 WHERE  vfl.wsdot_roadway = 'I405'
   AND  year(t.exitTime) = 2021;

-- look at transponderId and licensePlateId 
-- 6937468
SELECT  CASE WHEN tf.transponderId IS NULL AND tf.licensePlateId IS NULL THEN 'no_lp_or_tag'
			 WHEN tf.transponderId IS NULL AND tf.licensePlateId IS NOT NULL THEN 'lp_only'
			 WHEN tf.transponderId IS NOT NULL AND tf.licensePlateId IS NULL THEN 'tag_only'
			 WHEN tf.transponderId IS NOT NULL AND tf.licensePlateId IS NOT NULL THEN 'both_lp_and_tag'
			END AS id_type
		,count(*)
  FROM  WSDOT_STAGE.tmp.toll_fac tf
GROUP BY CASE WHEN tf.transponderId IS NULL AND tf.licensePlateId IS NULL THEN 'no_lp_or_tag'
			 WHEN tf.transponderId IS NULL AND tf.licensePlateId IS NOT NULL THEN 'lp_only'
			 WHEN tf.transponderId IS NOT NULL AND tf.licensePlateId IS NULL THEN 'tag_only'
			 WHEN tf.transponderId IS NOT NULL AND tf.licensePlateId IS NOT NULL THEN 'both_lp_and_tag'
			END;

-- number in brackets [] is after null
-- licensePlateId (lp_count > 1): 1,172,164 [1,163,523] toll_ids, but 1000 is observed a lot
-- transponderId (tag_count > 1): 2,751,369 [2,742,728] toll ids, but null transponder ids are observed often
-- both (tag_count > 1): 812,664 [804,024] toll_ids, use these going forward
-- txn count is always same as isnull version, so comment out
--SELECT  count(*)
SELECT  *
  INTO  WSDOT_STAGE.tmp.potential_split_trips
  FROM  (SELECT tf.*
				--,count(*) OVER (PARTITION BY tf.licensePlateId, cast(tf.exitTime AS date), tf.wsdot_facility) AS lp_count
				--,count(*) OVER (PARTITION BY tf.transponderId, cast(tf.exitTime AS date), tf.wsdot_facility) AS tag_count
				,count(*) OVER (PARTITION BY tf.transponderId, tf.licensePlateId, cast(tf.exitTime AS date), tf.wsdot_facility) AS txn_count
				--,count(*) OVER (PARTITION BY isnull(tf.transponderId,0), isnull(tf.licensePlateId,0), cast(tf.exitTime AS date), tf.wsdot_facility) AS nf_count
		   FROM WSDOT_STAGE.tmp.toll_fac tf
		    WHERE NOT (tf.licensePlateId IS NULL AND tf.transponderId IS NULL)
	     --   AND tf.TollID IN (226495825,241242273,227622972,241628002)
		) t
-- WHERE  txn_count <> nf_count
 WHERE  txn_count > 1;

SELECT * FROM WSDOT_STAGE.tmp.potential_split_trips t WHERE t.TollID IN (226495825,241242273,227622972,241628002);

-- recreate, doing tag match first, then lp match
DROP TABLE WSDOT_STAGE.tmp.split_trips;
INSERT INTO WSDOT_STAGE.tmp.split_trips
SELECT  t.TollID AS toll_id1
		,t2.TollID AS toll_id2
--  INTO  WSDOT_STAGE.tmp.split_trips
  FROM  WSDOT_STAGE.tmp.potential_split_trips t
  JOIN  WSDOT_STAGE.tmp.potential_split_trips t2
  			-- ON t2.transponderId = t.transponderId					-- same tag
  			 ON t2.licensePlateId = t.licensePlateId				-- same license plate
  			AND t.licensePlateId NOT IN (1000,182329,184145)		-- ignore frequent 'weird' plates (blank, ~, etc)
  			AND t2.TollID <> t.TollID 								-- don't self-match
  			AND t2.wsdot_facility = t.wsdot_facility				-- same facility
  			AND t2.exitTime > t.exitTime							-- sort in chronological order
  			AND datediff(second, t.exitTime, t2.exitTime) < 1200	-- up to 20 minutes time difference
-- WHERE  t.TollID IN (226495825,241242273,227622972,241628002)
;

SELECT * FROM WSDOT_STAGE.tmp.split_trips t WHERE t.toll_id1 IN (226495825,241242273,227622972,241628002) OR t.toll_id2 IN (226495825,241242273,227622972,241628002);

SELECT * FROM fastlane.rtoll.Toll t where t.TollId IN (228520226, 236341266, 226495825,241242273,227622972,241628002);

-- OLD total count: 1,326,386
-- OLD removing "bad" plates: 803,744
-- NEW total count: 14,608
SELECT  count(*) AS split_trip_count
  FROM  WSDOT_STAGE.tmp.split_trips s;
--  JOIN  fastlane.rtoll.Toll t ON t.TollID = s.toll_id1
--  LEFT  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = t.licensePlateId 
--  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId AND t.exitTime BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
-- remove bad plates ONLY when transponderId is also NULL
-- WHERE  NOT (t.licensePlateId IN (182329, 1000, 184145) AND t.transponderId IS NULL)
-- WHERE  lpca.customerAccountId IS NOT NULL;

-- summarize trips by month
SELECT  vfl.wsdot_facility
		,WSDOT_STAGE.util.f_get_month(tp.laneTime) AS trip_month
		,sum(CASE WHEN coalesce(t2.entryPlazaCodeId, t2.exitPlazaCodeId) > tp.roadGantryId AND tle.ledgeraccountId >= 1000 THEN 1 ELSE 0 END) AS logical_split_trips_on_acct
		,sum(CASE WHEN coalesce(t2.entryPlazaCodeId, t2.exitPlazaCodeId) > tp.roadGantryId THEN 1 ELSE 0 END) AS logical_split_trips
		,sum(CASE WHEN tle.ledgeraccountId >= 1000 THEN 1 ELSE 0 END) AS split_trips_on_account
		,count(*) AS tot_potential_split_trip_count
  FROM  WSDOT_STAGE.tmp.split_trips s
  JOIN  fastlane.lance.trip t ON t.tripId = s.toll_id1
  JOIN  fastlane.rtoll.Toll t2 ON t2.TollID = s.toll_id2
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  LEFT  JOIN  #trip_info ti ON ti.tripId = s.toll_id1
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ti.transactionId
 --WHERE  NOT (t.licensePlateId IN (182329, 1000, 184145) AND t.transponderId IS NULL);
GROUP BY vfl.wsdot_facility
		,WSDOT_STAGE.util.f_get_month(tp.laneTime)
ORDER BY vfl.wsdot_facility
		,WSDOT_STAGE.util.f_get_month(tp.laneTime);

-- OLD summarize trips by licensePlateId or transponderId
-- OLD licensePlateId: 182329 (1128363), 1000 (172511), 184145 (11905)
-- OLD transponderId: NULL (534724)
SELECT  t.licensePlateId
		--t.transponderId 
		,count(*) AS split_trip_count
  FROM  WSDOT_STAGE.tmp.split_trips s
  JOIN  fastlane.rtoll.Toll t ON t.TollID = s.toll_id1
GROUP BY t.licensePlateId 
		--t.transponderId
ORDER BY count(*) DESC;

-- look at licenseplateIds 182329, 1000, 184145, 51513777
SELECT  *
  FROM  fastlane.util.LicensePlate lp
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
 WHERE lp.licensePlateId IN (182329, 1000, 184145, 51513777);

-- detailed data
-- OLD 247,237 are on non-system accounts
-- NEW   8,281 are on non-system accounts, on same account, not ending at NB05 or SB06
SELECT  vfl1ex.wsdot_facility
		,t1.TollID AS trip_id1
		,t2.TollID AS trip_id2
		,ti.postTime AS post_time1
		,ti2.postTime AS post_time2
		,t1.externalId AS rts_id1
		,t2.externalId AS rts_id2
		,t1.licensePlateId
		,lp.lpnumber 
		,tle.ledgeraccountId AS customerAccountId
		,t1.transponderId
		,t.transponderNumber 
		,t1.amount AS amount1
		,t2.amount AS amount2
		,t1.entryTime AS entryTime1
		,t1.exitTime AS exitTime1
		,t2.entryTime AS entryTime2
		,t2.exitTime AS exitTime2
		,vfl1en.explazaCode AS entryPlaza1
		,vfl1ex.explazaCode AS exitPlaza1
		,vfl2en.explazaCode AS entryPlaza2
		,vfl2ex.explazaCode AS exitPlaza2
--SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.split_trips s 
  JOIN  fastlane.rtoll.Toll t1 ON t1.TollID = s.toll_id1
  JOIN  fastlane.rtoll.Toll t2 ON t2.TollID = s.toll_id2
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl1en ON vfl1en.roadGantryId = t1.entryPlazaCodeId
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl1ex ON vfl1ex.roadGantryId = t1.exitPlazaCodeId
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl2en ON vfl2en.roadGantryId = t2.entryPlazaCodeId
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl2ex ON vfl2ex.roadGantryId = t2.exitPlazaCodeId
  LEFT  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = t1.licensePlateId 
  LEFT  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = t1.transponderId 
  LEFT  JOIN  #trip_info ti ON ti.tripId = s.toll_id1
  LEFT  JOIN  #trip_info ti2 ON ti2.tripId = s.toll_id2 
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ti.transactionId
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle2 ON tle2.ledgertransactionId = ti2.transactionId
 WHERE  tle.ledgeraccountId >= 1000		-- only customer accounts
   AND  vfl1ex.explazaCode NOT IN ('NB05','SB06')	-- direct access ramps where trips should be split
   AND  tle.ledgeraccountId = tle2.ledgeraccountId	-- accounts match
   AND  t1.TollId > 225000000
;

--SELECT month(t.exitTime) FROM fastlane.rtoll.Toll t ORDER BY t.exitTime DESC;

-- counts match briefly until more accounts are created
SELECT count(*) FROM #types UNION ALL select count(*) FROM fastlane.util.CustomerAccount ca WHERE ca.isDefunct = 0;
SELECT count(*) FROM fastlane.util.CustomerAccount ca WHERE ca.isDefunct = 1;

SELECT * FROM #types WHERE customerAccountId IN (2333190, 2333205, 9025172, 9263510, 9276406, 9260525, 8963252, 8955951, 2323572, 9289363, 9289697, 9297842, 8963252, 8955951, 2323572, 9289363, 9289697, 9297842);
-- WHERE  ca.customerAccountId IN (440967, 9778038);

/***** account txns summary *****/
-- two approaches: first only non-defunct accounts, other (possibly easier) by ledger
-- latest trip by account id
SELECT  *
  FROM  fastlane.util.CustomerAccount ca
  JOIN  
 WHERE  ca.isDefunct = 0;

-- alternate approach
SELECT  tle.tollId 
		,
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.lance.trip t ON tle.tollId = t.tripId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  LEFT JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ti.transactionId AND vts.customerAccountid = tle.ledgeraccountId
;

-- look at posting date - what is on web BOS is vts.posting date when trip moved to customer account (but is still TPENO)
SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  LEFT JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  tle.tollId = 215211020
ORDER BY vts.postingDate DESC;

/***** LICENSE PLATE BORROW *****/
SELECT  *
--  FROM  fastlane.rtoll.LicensePlateBorrow lpb
  FROM  fastlane.rtoll.V_ah_LicensePlateBorrow lpb
  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = lpb.licensePlateId
  JOIN  fastlane.info.InputUser iu ON iu.inputUserId = lpb.inputUserId
 WHERE  lp.lpnumber = 'BYW5526';

/***** WSTC Queries 2021-11-06 *****/
-- how many drivers do we see using both 405 BTL and 167 in the same day? (tag only)
-- TODO: consider using PIVOT, but difficult to get working?
-- Jul-Oct 2021: 830,320 distinct users, of which 26,403 users use both I-405 and SR167 in the same day (3.2%)
-- NOTE: double-checked with HAVING on inner SELECT to be sure
-- 2019: 989,606 distinct users, of which 30,586 users used both I-405 and SR167 in the same day (3.09%), max 242 days, total 131,483 trip-days - avg 4.30 trips/user
-- 2020: 952,266 distinct users, of which 22,894 users used both I-405 and SR167 in the same day (2.40%), max 209 days, total  94,949 trip-days - avg 4.15 trips/user
-- 2021: 995,427 distinct users, of which 37,080 users used both I-405 and SR167 in the same day (3.73%), max 203 days, total 140,022 trip-days - avg 3.78 trips/user
-- since 2019: 1,383,302 distinct users, of which 70,960 users used both I-405 and SR167 in the same day (5.13%), max 604 days, total 366,455 trip-days - avg 5.16 trips/user
SELECT  sum(CASE WHEN used_167n405 > 0 THEN 1 ELSE 0 END) AS distinct_both_users
		,count(DISTINCT transponderId) AS distinct_users
		,count(*) AS count
		,max(used_167n405) AS max_167n405_days
		,sum(used_167n405) AS tot_167n405_days
  FROM  (SELECT  transponderId
				,sum(CASE WHEN trips_sr167 > 0 AND trips_i405 > 0 THEN 1 ELSE 0 END) AS used_167n405
		  FROM  (SELECT cast(t.exitTime AS date) AS trip_date
		  				--datepart(iso_week, t.exitTime) AS trip_week
						,t.transponderId 		-- tags only
						,sum(CASE WHEN vfl.wsdot_roadway = 'SR167' THEN 1 ELSE 0 END) AS trips_sr167
						,sum(CASE WHEN vfl.wsdot_roadway = 'I405' THEN 1 ELSE 0 END) AS trips_i405
						,count(*) AS trip_count
				  FROM  fastlane.rtoll.Toll t
				  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId
				 WHERE  t.transponderId IS NOT NULL
				   AND  datepart(year, t.exitTime) >= 2019
				 --  AND  cast(t.exitTime AS date) BETWEEN convert(date, '2021-07-01') AND convert(date, '2021-10-31')
				GROUP BY cast(t.exitTime AS date)
						--datepart(iso_week, t.exitTime)
						,t.transponderId
				--HAVING  sum(CASE WHEN vfl.wsdot_roadway = 'SR167' THEN 1 ELSE 0 END) > 0
				--   AND  sum(CASE WHEN vfl.wsdot_roadway = 'I405' THEN 1 ELSE 0 END) > 0
				) AS p
		GROUP BY transponderId
		) AS s;

-- investigate difference between iso_week and wk
SELECT  datepart(iso_week, dt) AS iso_wk, datepart(wk, dt) AS wk
  FROM  (SELECT convert(date, '2021-01-11') AS dt) AS p;

-- how many drivers have paid the max toll rate on 167 NB and then the 405 NB max toll rate 20 minutes apart? (tag only)
-- amounts are negative!
-- none since 2021-07 (not even over $11)
SELECT  t1.transponderId
		,t1.exitTime AS lanetime167
		,t1.amount AS amt167
		,t2.exitTime AS lanetime405
		,t2.amount AS amt405
		,t1.amount + t2.amount AS total_amount
  FROM  fastlane.rtoll.Toll t1
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl1 ON vfl1.roadGantryId = t1.exitPlazaCodeId
  JOIN  fastlane.rtoll.Toll t2 ON t2.transponderId = t1.transponderId AND datediff(minute, t1.exitTime, t2.exitTime) BETWEEN 0 AND 45
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl2 ON vfl2.roadGantryId = t2.exitPlazaCodeId
 WHERE  t1.transponderId IS NOT NULL
   AND  datepart(year, t1.exitTime) >= 2019
   AND  vfl1.wsdot_facility = '167N'
   AND  vfl2.wsdot_facility = '405BL'
   AND  ((t1.amount <= -7 AND t2.amount <= -8) OR (t1.amount + t2.amount) <= -15)
ORDER BY abs(t1.amount) + abs(t2.amount) DESC;

-- what about the reverse (SB 405 and SB 167) (tag only)
-- none since 2021-07 (not even over $11)
SELECT  t1.transponderId
		,t1.exitTime AS lanetime405
		,t1.amount AS amt405
		,t2.exitTime AS lanetime167
		,t2.amount AS amt167
		,t1.amount + t2.amount AS total_amount
  FROM  fastlane.rtoll.Toll t1
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl1 ON vfl1.roadGantryId = t1.exitPlazaCodeId
  JOIN  fastlane.rtoll.Toll t2 ON t2.transponderId = t1.transponderId AND datediff(minute, t1.exitTime, t2.exitTime) BETWEEN 0 AND 45
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl2 ON vfl2.roadGantryId = t2.exitPlazaCodeId
 WHERE  t1.transponderId IS NOT NULL
   AND  datepart(year, t1.exitTime) >= 2019
   AND  vfl1.wsdot_facility = '405LB'
   AND  vfl2.wsdot_facility = '167S'
   AND  ((t1.amount <= -8 AND t2.amount <= -7) OR (t1.amount + t2.amount) <= -15)
ORDER BY abs(t1.amount) + abs(t2.amount) DESC;

/***** TNB TRIPS BY ZIP CODE *****/
-- TODO: perhaps screen system accounts? only Payment Suspense, Bancpass, Bestpass, and Verra Mobility currently have addresses
SELECT  year(d.laneTime) AS txn_year
		,d.roadway
		,ec.description AS country
		,CASE WHEN ec.countryId <> 1 THEN NULL ELSE left(pa.postalCode,5) END AS postal_code
		,count(*) AS trip_count
  FROM  (SELECT t.tripId
  				,tp.laneTime
  				,vfl.wsdot_roadway AS roadway
				,max(tle.ledgertransactionId) AS maxledgerTransactionId
				--,count(*)
		   FROM fastlane.lance.trip t 
		   JOIN fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
		   JOIN WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
		   JOIN fastlane.xact.TollLedgerEntry tle ON tle.tollId = t.tripId 
		  WHERE tp.laneTime >= convert(date, '2022-01-01')
		   -- AND vfl.wsdot_roadway = 'TNB'	-- select roadway if desired 'I405,SR167,SR520,SR99,TNB'
		GROUP BY t.tripId
				,tp.laneTime 
				,vfl.wsdot_roadway 
		) d
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = d.maxledgerTransactionId
  LEFT  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = tle.ledgeraccountId AND icac.contacttype = 'MAIN'	-- only main contacts
  LEFT  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId
  LEFT  JOIN  fastlane.locale.EnuState es ON es.stateId = pa.stateId 
  LEFT  JOIN  fastlane.locale.EnuCountry ec ON ec.countryId = es.countryId
GROUP BY year(d.laneTime)
		,d.roadway
		,ec.description
		,CASE WHEN ec.countryId <> 1 THEN NULL ELSE left(pa.postalCode,5) END
ORDER BY year(d.laneTime)
		,d.roadway
		,ec.description DESC
		,CASE WHEN ec.countryId <> 1 THEN NULL ELSE left(pa.postalCode,5) END;

SELECT  icac.*
		,pa.*
  FROM  fastlane.util.IV_CustomerAccountContacts icac
  LEFT  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId
  LEFT  JOIN  fastlane.locale.EnuState es ON es.stateId = pa.stateId 
  LEFT  JOIN  fastlane.locale.EnuCountry ec ON ec.countryId = es.countryId
 WHERE  icac.contacttype = 'MAIN'	-- only main contacts
   AND  icac.customerAccountId < 1000;		-- system accounts

/***** ACCOUNTS NOT IN GOOD STANDING & OUTSTANDING ACCOUNT BALANCE *****/
-- 2021-12-30 email with Tyler, Ami, Patty Michaud
-- get pre-paid and payg accounts not in good standing, with balance (or range), number of trips? and email address if possible

/* good standing understanding (look at fastlane.util.F_AcctCond_IsInGoodStandingMany):

in good standing (1) when:
 - xact.transactionsbalance is positive and a prepaid account
 - is an internal account (util.IV_AccountCond_isInternal)
 - is a guaranteed revenue account (util.IV_AccountCond_GuaranteedIncome)
is not in good standing (0) when:
 - system generated and pending (util.IV_AccountCond_IsSystemGenerated, isPending = 1)
 - account with xact.IV_InvoiceLedgerEntryIDCount ilt joined with xact.V_TransactionSummary
     where ilt.number <> 1, vts.isFullyAllocated=0, vts.balanceTypeId=1, vts.isDebit=1, vts.isCancelled=0, 
     and vts.transactionTypeId is not TOLL, ETB, or FPBP
*/

-- functions/procedures: util.USP_Get_CustomerAccountTypeStanding, util.F_AcctCond_IsInGoodStanding, util.F_AcctCond_IsInGoodStandingMany
SELECT * FROM fastlane.rtoll.AccountDefaults ad;
SELECT * FROM fastlane.xact.EnuGoodStanding egs;

-- threshold is always zero - why?
SELECT  DISTINCT ca.goodStandingThreshold 
  FROM  fastlane.util.CustomerAccount ca;

-- account balances here? appears so
SELECT  *
  FROM  fastlane.xact.transactionsbalance tb
  JOIN  fastlane.xact.EnuBalanceType ebt ON ebt.balanceTypeId = tb.balanceTypeId
 WHERE  customeraccountid=356957;

-- prepaid and payg account types
SELECT  vca.acct_short_code
		,count(*) AS acct_count
  FROM  WSDOT_STAGE.util.v_customer_account vca
-- WHERE vca.acct_short_code IN ('INDIVIDUAL','COMMERCIAL','ZBACOMMERCIAL','ZBAINDIVIDUAL')
GROUP BY vca.acct_short_code
ORDER BY count(*) DESC;

-- test isingoodstanding function
SELECT fastlane.util.F_AcctCond_IsInGoodStanding(456368,'2022-01-04');

-- test isingoodstandingmany function
USE fastlane;
DECLARE @custs util.T_CustomerAccountList
--INSERT @custs VALUES (356957)
--INSERT INTO @custs SELECT ca.customerAccountId FROM fastlane.util.CustomerAccount ca WHERE ca.customerAccountId IN (356957,9778039)
INSERT INTO @custs SELECT ca.customerAccountId FROM fastlane.util.CustomerAccount ca WHERE ca.customerAccountId IN (356957,9778039)
--INSERT @custs VALUES (356957),(2333190),(2333205),(9025172),(9263510),(9276406),(9260525),(8963252),(8955951),(2323572),(9289363),(9289697),(9297842),(934188),(2226),(20355),(11953),(1143778),(440967),(9778039),(4409968)
SELECT  *
  FROM  fastlane.util.F_AcctCond_IsInGoodStandingMany(@custs,NULL) c
--  LEFT JOIN  fastlane.xact.EnuGoodStanding egs ON egs.goodStandingId = c.isInGoodStanding
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = c.customerAccountId AND tb.isCurrentActivity = 1
 WHERE  c.isInGoodStanding=0;

-- get data - only 2 zbaindividual accounts not in good standing? 8702984, 9114253
-- add manual/auto refreshment payment plan
DECLARE @custs util.T_CustomerAccountList
INSERT INTO @custs SELECT vca.customer_account_id FROM WSDOT_STAGE.util.v_customer_account vca WHERE vca.acct_short_code IN ('INDIVIDUAL','COMMERCIAL','ZBACOMMERCIAL','ZBAINDIVIDUAL')
SELECT  vca.acct_short_code
		,tb.signValue
		--,c.isInGoodStanding
		,count(*) AS acct_count
--SELECT  *
  FROM  fastlane.util.F_AcctCond_IsInGoodStandingMany(@custs,NULL) c
--  LEFT JOIN  fastlane.xact.EnuGoodStanding egs ON egs.goodStandingId = c.isInGoodStanding
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = c.customerAccountId AND tb.isCurrentActivity = 1
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = c.customerAccountId
-- WHERE  c.isInGoodStanding=0;
GROUP BY vca.acct_short_code
		,tb.signValue 
		,c.isInGoodStanding;

-- a list of pre-paid accounts with negative balances, including
-- date of the account opening, the date the balance went negative, current balance, names, email, 
-- mailing address, account number, date last payment received, linked cases
-- still need to add: manual or auto
-- 2022-05-23: added latest trip, over 30 days since went negative
SELECT  vca.customer_account_id
        ,vca.legacyAccountNumber
        ,vca.acct_type_desc
        ,vca.postingDate AS acct_creation_date
        ,tb.amount AS current_balance
        ,tb.lastSignChangeDate AS negative_balance_date
        ,datediff(day, tb.lastSignChangeDate, getdate()) AS days_since_negative
        ,valt.latest_trip
        ,pmt.latest_payment_date
        ,pmt.latest_payment_amount
        ,rs.zbaReplenishmentDayOfMonth1
        ,rs.zbaReplenishmentDayOfMonth2
        ,icac.isCompany
        ,icac.firstName
        ,icac.lastNameOrCompanyName
        ,icac.displayname
        ,ea.emailAddress
        ,pa.address1
        ,pa.address2
        ,pa.city
        ,es.abbr AS state
        ,pa.postalCode
        ,ec.description AS country
        ,pa.otherState
        ,uc.usercases
--SELECT  count(*)
  FROM  WSDOT_STAGE.util.v_customer_account vca
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = vca.customer_account_id AND tb.isCurrentActivity = 1
  LEFT  JOIN (SELECT customerAccountId
                ,string_agg(ucaseId,  ';') WITHIN GROUP (ORDER BY ucaseId) AS usercases
           FROM fastlane.usercase.V_ah_CaseCustomerAccount vacca
        GROUP BY customerAccountId
        ) uc ON uc.customerAccountId = vca.customer_account_id
  LEFT  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = vca.customer_account_id AND icac.contacttype = 'MAIN'   -- only main contacts
  LEFT  JOIN  fastlane.info.PostalAddress pa ON pa.postalAddressId = icac.mailingPostalAddressId
  LEFT  JOIN  fastlane.locale.EnuState es ON es.stateId = pa.stateId 
  LEFT  JOIN  fastlane.locale.EnuCountry ec ON ec.countryId = es.countryId
  LEFT  JOIN  fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId
  LEFT  JOIN  fastlane.info.ReplenishmentSettings rs ON rs.customerAccountId = vca.customer_account_id
  LEFT  JOIN  WSDOT_STAGE.util.v_acct_latest_trip valt ON valt.customerAccountid = vca.customer_account_id 
OUTER APPLY     (SELECT TOP 1 ts.customerAccountId
                        ,ts.postingDate AS latest_payment_date
                        ,ts.amount AS latest_payment_amount
                   FROM fastlane.xact.v_transactionsummary ts 
                   JOIN fastlane.xact.paymentcredit pc on pc.transactionId = ts.transactionId
                   JOIN fastlane.payment.Payment p on pc.paymentId = p.paymentId
                  WHERE ts.customerAccountid = vca.customer_account_id 
                ORDER BY ts.postingDate DESC
                ) pmt
 WHERE  tb.signValue = -1
   AND  tb.lastSignChangeDate < convert(date, '2022-04-23') 
--   AND  vca.acct_short_code IN ('INDIVIDUAL','COMMERCIAL','ZBACOMMERCIAL','ZBAINDIVIDUAL')
--   AND  vca.customer_account_id = 15510
;

/***** USER CASE CHARGE DISPUTES *****/
-- 2022-02-08 query for Catherine Larson (via Tyler)
-- get open usercases (workflow state is New [0] or Awaiting Customer Action [1]) for case queue 1006 Dispute a charge (case type id 1)
-- usercase links to own personOrCompanyId (never matches customer account) and emailAddressId (though is always NULL)
-- pull both account contact info and usercase contact info
SELECT  uc.ucaseId 
		,ect.description AS case_type_desc
		,ws.description AS workflow_state_desc
		,uc.created AS caseCreatedDate
		,ecs.description AS case_source
		,poc.isCompany AS case_isCompany
        ,poc.firstName AS case_firstName
        ,poc.lastNameOrCompanyName AS case_lastNameOrCompanyName 
        ,pocea.emailAddress AS case_emailAddress
		,count(cca.customerAccountId) OVER (PARTITION BY uc.ucaseId) AS case_customer_count
		,cca.customerAccountId 
		,vca.acct_type_desc 
		,icac.isCompany AS acct_isCompany
        ,icac.firstName AS acct_firstName
        ,icac.lastNameOrCompanyName AS acct_lastNameOrCompanyName
        ,ea.emailAddress AS acct_emailAddress
        ,ea.emailAddressId AS acct_emailAddressId
--SELECT  count(*)
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId 
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = uc.caseTypeId 
  JOIN  fastlane.usercase.EnuCaseSource ecs ON ecs.caseSourceId = uc.caseSourceId 
  LEFT  JOIN  fastlane.info.PersonOrCompany poc ON poc.personOrCompanyId = uc.personOrCompanyId 
  LEFT  JOIN  fastlane.info.EmailAddress pocea ON pocea.emailAddressId = poc.emailAddressId
  LEFT  JOIN  fastlane.usercase.CaseCustomerAccount cca ON cca.ucaseId = uc.ucaseId 
  LEFT  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = cca.customerAccountId AND icac.contacttype = 'MAIN'
  LEFT  JOIN  fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId
  LEFT  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = cca.customerAccountId 
 WHERE  uc.workflowStateId IN (0,1)
   AND  uc.caseTypeId = 1
ORDER BY uc.ucaseId;

SELECT count(*) FROM fastlane.usercase.[UCase] uc
 --WHERE  uc.workflowStateId IN (0,1)
  -- AND  uc.caseTypeId = 1
   WHERE  uc.emailAddressId IS NOT NULL;

-- 1196 usercases have multiple accounts linked
SELECT  uc.ucaseId 
		,uc.created AS caseCreatedDate
		,ect.description AS case_type_desc
		,ws.description AS workflow_state_desc
		,d.customerAccount_count
		,d.customerAccountIds
-- SELECT  count(*)
  FROM  (SELECT  cca.ucaseId
				,count(*) AS customerAccount_count
				,string_agg(cca.customerAccountId, ';') WITHIN GROUP (ORDER BY cca.customerAccountId) AS customerAccountIds
		  FROM  fastlane.usercase.CaseCustomerAccount cca
		GROUP BY ucaseId
		HAVING count(*) > 1
		) d
  JOIN  fastlane.usercase.[UCase] uc ON uc.ucaseId = d.ucaseId
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId 
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = uc.caseTypeId 
ORDER BY uc.ucaseId;

-- look at other sources
SELECT  ecs.description, count(*)
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.EnuCaseSource ecs ON ecs.caseSourceId = uc.caseSourceId 
GROUP BY ecs.description;

-- also look at main account contacts with unknown@etantolling.com address id - duplicates!
SELECT * FROM fastlane.info.EmailAddress ea WHERE ea.emailAddress = 'unknown@etantolling.com';

-- 
SELECT count(*) FROM (
SELECT ucaseId , count(*) AS cnt
  FROM fastlane.usercase.WebMessage wm 
  GROUP BY ucaseId 
  HAVING count(*) > 1) t;

/********** TRIP STATUS TESTING **********/
-- 2022-02-10 more detail on user case closures for Catherine (via Tyler) - see email 
-- TODO: promote to weekly run
-- NOTE: a case can be closed, then re-opened; see account 9546338, usercase 6514451 for example (also note error on contacts)
--       also, some cases shown below are not in sample list for unknown reason; see account 8567894, usercase 440703 for example
SELECT  uc.ucaseId 
		,coalesce(ect3.description, ect2.description, ect.description) AS case_type
		--,uc.workflowStateId 
		,ect.description AS case_category
		,ws.description AS case_status
		,uc.created AS case_open_date
		,uc.updatedAt AS case_last_update
		,hist.created_by
		,concat(iu.firstName, ' ', iu.lastName) AS modified_by
		,a.customerAccountIds
 --SELECT  count(*)
--SELECT DISTINCT concat(iu.firstName, ' ', iu.lastName, iu.inputUserId)
SELECT  concat(iu.firstName, ' ', iu.lastName) AS modified_by
		,ect.description AS case_category
		,ws.description AS case_status
		,WSDOT_STAGE.util.f_get_month(uc.created) AS case_created_month
		,WSDOT_STAGE.util.f_get_month(uc.updatedAt) AS case_updated_month
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId 
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = uc.caseTypeId 
--  JOIN  fastlane.usercase.WebMessage wm ON wm.
--  JOIN  fastlane.usercase.CaseNote cn ON cn.ucaseId = uc.ucaseId
--  JOIN  fastlane.usercase.Note n ON cn.noteId = n.noteId
  LEFT  JOIN fastlane.usercase.EnuCaseType ect2 ON ect2.caseTypeId = ect.parentCaseTypeId 
  LEFT  JOIN fastlane.usercase.EnuCaseType ect3 ON ect3.caseTypeId = ect2.parentCaseTypeId 
--  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = ect.caseTypeId 
--  JOIN  fastlane.usercase.CaseQueue cq ON cq.caseQueueId = cqct.caseQueueId 
  JOIN  fastlane.info.InputUser iu ON iu.inputUserId = uc.inputUserId 
  LEFT  JOIN  (SELECT  cca.ucaseId
				,count(*) AS customerAccount_count
				,string_agg(cca.customerAccountId, ';') WITHIN GROUP (ORDER BY cca.customerAccountId) AS customerAccountIds
		  FROM  fastlane.usercase.CaseCustomerAccount cca
		GROUP BY ucaseId
		) a ON a.ucaseId = uc.ucaseId
  CROSS APPLY
        (SELECT TOP 1
        		concat(iu2.firstName, ' ', iu2.lastName) AS created_by
           FROM fastlane.usercase.hUCase huc 
           JOIN fastlane.info.InputUser iu2 ON iu2.inputUserId = huc.inputUserId 
          WHERE huc.ucaseId = uc.ucaseId 
        ORDER BY huc.startTime ASC
        ) AS hist
 WHERE  ws.isClosing = 1
--   AND  uc.updatedAt BETWEEN convert(datetime2,'2021-12-13') AND convert(datetime2,'2021-12-28')
   AND  uc.created >= convert(datetime2,'2021-07-12') 
   AND  uc.inputUserId <> 2925		-- exclude DB_SCRIPT_USER
--   AND  iu.inputUserId NOT IN (2915,2642,2804)
--   WHERE  uc.ucaseId IN (6568594,6568578,6541666,6514451)
--ORDER BY uc.ucaseId
GROUP BY concat(iu.firstName, ' ', iu.lastName)
		,ect.description
		,ws.description
		,WSDOT_STAGE.util.f_get_month(uc.created)
		,WSDOT_STAGE.util.f_get_month(uc.updatedAt)
ORDER BY concat(iu.firstName, ' ', iu.lastName)
		,WSDOT_STAGE.util.f_get_month(uc.created)
		,WSDOT_STAGE.util.f_get_month(uc.updatedAt)
;

SELECT * FROM fastlane.info.InputUser iu WHERE iu.inputUserId IN (2915,2642,2804);
SELECT * FROM fastlane.info.InputUser iu WHERE iu.firstName = 'DB_SCRIPT_USER';

-- how do we determine correct case queue for a user case?
SELECT  cq.name AS case_queue
		,ect.description AS case_type
  FROM  fastlane.usercase.CaseQueueCaseType cqct 
  JOIN  fastlane.usercase.CaseQueue cq ON cq.caseQueueId = cqct.caseQueueId 
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = cqct.caseTypeId
-- WHERE  ect.caseTypeId = 43
 WHERE  cqct.caseQueueId = 1006
;

-- open cases
SELECT  count(*)
  FROM  WSDOT_STAGE.util.v_csr_case_work_detail cw
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = cw.wf_state_id
 WHERE  ws.isClosing = 0;  -- <> 1;

/********** TRIP STATUS TESTING **********/
-- FPBP is always -0.25
-- FCPP is always -40
-- TOLLDISC and WRITEOFF are only positive amount types
SELECT  DISTINCT ett.shortCode, ett.description, vts.isDebit, vts.balanceTypeId 
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
ORDER BY ett.shortCode;

-- look at trip with lots of transactions
SELECT  tle.tollId 
		,count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
 WHERE  cast(vts.postingDate AS date) BETWEEN convert(date, '2021-07-01') AND convert(date, '2021-09-01')
GROUP BY tle.tollId 
ORDER BY count(*) DESC;

-- look at FCPP
SELECT  tle.tollId 
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
-- WHERE  (cast(vts.postingDate AS date) BETWEEN convert(date, '2021-03-01') AND convert(date, '2021-05-01'))
   WHERE ett.shortCode = 'TCPP'
--   AND vts.isCancelled = 0
--   AND vts.isAllocated = 1
ORDER BY vts.postingDate DESC;

-- investigate high freq trip ids
-- FPBP seems to be reapplied to latest tle entry (except TCPP)
-- FCPP *should* be applied after TCPP entry, but processing of ETCC data may vary?
SELECT  vts.postingDate
		,tle.ledgertransactionId 
		,vts.customerAccountid 
		,epp.shortCode AS pricing_plan
		,vts.amount 
		,vts.isCancelled 
		,ett.shortCode AS txn_type
		,eer.shortCode AS entry_reason
		,eer.description AS entry_desc
--SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId 
  LEFT  JOIN  fastlane.xact.Annotation ann ON ann.annotationid = tle.endAnnotationId
  LEFT  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
 WHERE  tle.tollId = 185755300 --201422628 --214769680
ORDER BY vts.postingDate;

SELECT  *
  FROM  fastlane.xact.EnuTransactionType ett
  JOIN  fastlane.xact.EnuTransactionCategory etc ON etc.transactionCategoryId = ett.transactionCategoryId 
 WHERE  etc.transactionCategoryId = 1
   AND  ett.shortCode IN ('ETB','INVTOLL1','INVTOLL2','TPENDING','TOLL','TCVP','TCPP','FPBP','FCVP','FCPP','TOLLDISC','WRITEOFF');

-- look at FPBP - does it always coincide with another txn, and is it always PBP payment plan?
-- txn  type    plan	count
-- 25	NULL	NULL			 1
-- 25	TOLL-1	PBP-2	51,219,221
-- 25	TOLL-1	NRV-5		 1,891
-- 25	TOLL-1	MOT-6			 4
-- 25	TOLL-1	PBM-3			 1	
-- 25	FCPP-84	PBM-3			36
-- 2022-01-21: also look at FPBP on TNB
SELECT  vts.transactionTypeId AS fpbp_txn_type
		,t2.transactionTypeId AS other_txn_type
		,t2.pricingPlanId 
		--,t2.*
		,count(*) AS trip_count
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId
  LEFT  JOIN (SELECT tle2.tollId
  				,tle2.ledgertransactionId
  				,vts2.customerAccountid 
  				,vts2.postingDate 
  				,vts2.transactionTypeId 
  				,tle2.pricingPlanId 
		   FROM fastlane.xact.TollLedgerEntry tle2
		   JOIN fastlane.xact.V_TransactionSummary vts2 ON vts2.transactionId = tle2.ledgertransactionId AND vts2.customerAccountid = tle2.ledgeraccountId
		  WHERE vts2.transactionTypeId <> 25	-- not FPBP OR TOLL
		  	AND vts2.isDebit = 1				-- ignore TOLLDISC and WRITEOFF
		) AS t2
		 ON t2.tollId = tle.tollId							-- same trip
		AND t2.postingDate = vts.postingDate 				-- simultaneous txns
		AND t2.customerAccountId = vts.customerAccountid	-- same account
 WHERE  vts.transactionTypeId = 25 -- FPBP
   AND  vfl.wsdot_roadway = 'TNB'
   AND  tp.laneTime BETWEEN convert(datetime2, '2021-07-01') AND convert(datetime2, '2022-01-01')
--   AND  t2.transactiontypeId IS NULL
GROUP BY vts.transactionTypeId 
		,t2.transactionTypeId
		,t2.pricingPlanId;

-- pricing plan for FPBP:
-- 2 (PBP)	51219130
-- 5 (NRV)	1891
-- 6 (MOT)	4
-- 3 (PBM)	1
SELECT  tle.pricingPlanId 
		,epp.shortCode 
		,count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId 
 WHERE  vts.transactionTypeId = 25
GROUP BY tle.pricingPlanId
		,epp.shortCode;

-- do all trips have at least one toll type transaction? YES!
SELECT * FROM (
SELECT  tle.tollId
		,sum(CASE WHEN ett.transactionCategoryId = 1 THEN 1 ELSE 0 END) AS toll_txn_cnt
		,count(*) AS tot_cnt
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
GROUP BY tle.tollId
) t WHERE t.toll_txn_cnt = 0;

-- simultaneous transactions by transaction type (12 min to run)
SELECT  txn_pattern
        ,count(*) AS count
  FROM  (SELECT tle.tollId
                ,vts.postingDate
                ,string_agg(ett.shortcode,  ',') WITHIN GROUP (ORDER BY vts.postingDate, ett.shortCode) AS txn_pattern
          FROM  fastlane.xact.TollLedgerEntry tle
          JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId
          JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
        GROUP BY tle.tollId
                ,vts.postingDate) AS x
GROUP BY txn_pattern
ORDER BY 2 DESC;

-- get count of vts posting dates during transition for cutoff
SELECT  cast(vts.postingDate AS date)
		,count(*) AS tot_cnt
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  cast(vts.postingDate AS date) BETWEEN convert(date,'2021-06-20') AND convert(date,'2021-07-10')
GROUP BY cast(vts.postingDate AS date);

-- TOLLDISC seemed to always be concurrent with TOLL (and FPBP if present), but some are independent
-- WRITEOFF can be independent or simultaneous with TOLL and self

/********** TRIP STATUS TESTING **********/

-- look at occupancy code id and tag read 
-- NULL	0	- no occupancy code without a tag
-- 0 	1	- regular tag
-- 1	1	- hov tag
SELECT  DISTINCT t.occupancyCodeId
		,tagRead 
  FROM  fastlane.rtoll.Toll t;
 
 -- are all HOV $0? no, perhaps amount is the displayed rate, but some are in fact zero
 -- what about default rates (those ending in 7)? do not appear to be present
 -- also, do all tag reads have a transponderID? yes!
 SELECT t.*
  FROM  fastlane.rtoll.Toll t
-- WHERE  t.occupancyCodeId = 1
-- WHERE  t.amount = 0
-- WHERE  (100*t.amount % 10) = 7
 WHERE  t.tagRead = 1 AND t.transponderId IS NULL
;

-- get summary of trips by lane rate
SELECT  cast(t.exitTime AS date) AS trip_date
		,datepart(hour, t.exitTime) AS trip_hour
		,vfl.wsdot_facility AS facility
		,CASE WHEN t.occupancyCodeId = 1 THEN 'HOV'
			  WHEN t.tagRead = 1 THEN 'AVI'
			  ELSE 'IMG'
			END AS trip_type
		,t.amount AS lane_rate
		,count(*) AS trip_count
--SELECT count(*)
  FROM  fastlane.rtoll.Toll t
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId 
  JOIN  #trip_info ti ON ti.tripId = t.TollID
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ti.transactionId
 WHERE  t.exitTime >= convert(datetime2,'2020-07-01')
   AND  t.exitTime < convert(datetime2,'2021-07-01')
   AND  vfl.wsdot_roadway = 'SR167'
   AND  tle.ledgeraccountId <> 16
GROUP BY cast(t.exitTime AS date)
		,datepart(hour, t.exitTime)
		,vfl.wsdot_facility
		,CASE WHEN t.occupancyCodeId = 1 THEN 'HOV'
			  WHEN t.tagRead = 1 THEN 'AVI'
			  ELSE 'IMG'
			END
		,t.amount
ORDER BY cast(t.exitTime AS date)
		,datepart(hour, t.exitTime)
		,vfl.wsdot_facility 
		,CASE WHEN t.occupancyCodeId = 1 THEN 'HOV'
			  WHEN t.tagRead = 1 THEN 'AVI'
			  ELSE 'IMG'
			END
		,t.amount;

/********** NEGATIVE ACCOUNT BALANCE TAG TRIPS FOR FORECASTING **********/
-- look at balance on transactions page - where does this come from?
SELECT  *
  FROM  WSDOT_STAGE.rpt.v_trip_detail vtd  
  JOIN  fastlane.xact.v_runningBalance vrb ON vrb.customerAccountId = vtd.customerAccountid AND vrb.transactionId = vtd.last_trip_txn_id 
 WHERE  vtd.customerAccountid = 356957
ORDER BY vtd.laneTime DESC;

SELECT * FROM fastlane.xact.LedgerItemInfo lii WHERE lii.ledgerItemInfoId = 281038027;	-- 16:19:28
SELECT * FROM fastlane.lance.trip WHERE tripId = 249217328;

/********** NEW REPORTS FOR JENNY/MARIA **********/
-- 2022-01-31
-- toll-exempt trips where a toll was not charged after Jul 12
SELECT  vtd.customerAccountid 
		,vtd.facility 
		,vtd.roadway 
		,vtd.enforcement_type 
		,sum(vtd.roadside_amount) AS sum_roadside_amount
		,sum(vtd.latest_toll_amount) AS sum_toll_amount
		,count(*) AS trip_count
  FROM  WSDOT_STAGE.new.v_trip_detail vtd 
 WHERE  vtd.disposition_category IN ('CUSTOM_TOLLDISC','HOV','MOTORCYCLE','NONREV')
   AND  vtd.laneTime >= convert(datetime2, '2021-07-12')
GROUP BY vtd.customerAccountid 
		,vtd.facility 
		,vtd.roadway 
		,vtd.enforcement_type;

-- accounts with discount plan on active passes or plates
-- NOTE: most info comes from function fastlane.xact.F_Get_DiscountPlansByCustomerAccountId(@cust_id)
--       rewrite to get for all accounts with discount plans
--       also ignore account discounts since fastlane.xact.CustomerAccountDiscountPlan is empty
SELECT  d.customerAccountId 
		,vca.acct_type_desc 
		,icac.isCompany
        ,icac.firstName
        ,icac.lastNameOrCompanyName
        ,icac.displayname
        ,ea.emailAddress
		,d.discountPlanId 
		,dp.name AS discount_plan_name
		,edl.description AS attached_to
		,d.lp_or_pass_number
		,edat.description AS applied_to
		,d.status
  FROM  (SELECT tca.customerAccountId 
				,tdp.discountPlanId 
				,concat(convert(varchar, fa.kapschCode),' ',t.transponderNumber) AS lp_or_pass_number
				,etis.description AS status
		  FROM  fastlane.xact.TransponderDiscountPlan tdp
		  JOIN  fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = tdp.transponderId
		  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = tca.transponderId 
		  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
		  JOIN  fastlane.inv.InventoryTag it ON it.inventoryTagId = t.inventoryTagId 
		  JOIN  fastlane.inv.EnuTagInventoryStatus etis ON etis.tagInventoryStatusId = it.tagInventoryStatusId 
		 WHERE  tdp.isDefunct = 0
		UNION ALL
		SELECT  lpca.customerAccountId 
				,lpdp.discountPlanId 
				,concat(es.shortCode,' ',lp.lpnumber) AS lp_or_pass_number 
				-- license plate status as defined in fastlane.util.V_licensePlate 
				,CASE WHEN fastlane.config.getpostingdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
				      THEN (SELECT shortcode FROM fastlane.displayenum.enuLicenseplateStatus WHERE licenseplatestatusid = 1) 
					  ELSE (SELECT shortcode FROM fastlane.displayenum.enuLicenseplateStatus WHERE licenseplatestatusid = 0)
				END AS status
		  FROM  fastlane.xact.LicensePlateDiscountPlan lpdp
		  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lpdp.licensePlateId 
		  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = lpca.licensePlateId 
		  LEFT  JOIN  fastlane.util.EnuLpJurisdiction lpj on lpj.lpJurisdictionId=lp.lpJurisdictionId
		  LEFT  JOIN  fastlane.locale.EnuState es on lpj.stateId = es.stateId
		 WHERE  lpdp.isDefunct = 0
		) AS d
  JOIN  fastlane.xact.DiscountPlan dp ON dp.discountPlanId = d.discountPlanId 
  JOIN  fastlane.xact.EnuDiscountLevel edl ON edl.discountLevelId = dp.discountLevelId 
  JOIN  fastlane.xact.EnuDiscountApplyType edat ON edat.discountApplyTypeId = dp.discountApplyTypeId 
  LEFT  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = d.customerAccountId
  LEFT  JOIN  fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = vca.customer_account_id AND icac.contacttype = 'MAIN'
  LEFT  JOIN  fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId;

SELECT count(*) FROM fastlane.rtoll.TransponderCustomerAccount tca WHERE tca.customerAccountId = 356957;
SELECT count(*) FROM fastlane.rtoll.LicensePlateCustomerAccount lpca WHERE lpca.customerAccountId = 356957;

/********** UNKNOWN ACCOUNT CHANGES **********/
SELECT  vca.acct_short_code
        ,count(*) AS acct_count
--SELECT  vca.*
  FROM  WSDOT_STAGE.rpt.h_account_type hat
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = hat.customer_account_id
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = hat.customer_account_id
 WHERE  hat.account_type_id = 0
   AND  vca.account_type_id <> 0
   AND  cast(hat.sys_end_time AS date) = convert(date, '2022-01-10')    -- updated on 2022-01-10
   AND  ca.postingDate > convert(date, '2021-07-06')                    -- created after 2021-07-05
GROUP BY vca.acct_short_code;

-- count of trips
SELECT  count(*) 
  FROM  fastlane.lance.trip t 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  tp.laneTime BETWEEN convert(datetime2, '2021-07-01') AND convert(datetime2, '2022-01-01');

-- image requests (based on modified ETAN query)
SELECT  valff.Roadway
		,count(distinct t.tripId) AS trips
		,count(distinct tpidr.tripPassageId) AS tpidr_count
		,count(*) AS row_count
  FROM  fastlane.lance.Trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.lance.V_AllLanesForFacility valff ON valff.roadGantryId = tp.roadGantryId
  LEFT  JOIN  fastlane.lance.roadsideResponse rrsp ON rrsp.tripId = t.tripId 
  LEFT  JOIN  fastlane.lance.roadsideRequest rr ON rr.roadsideRequestId = rrsp.roadsideRequestId AND rrsp.roadEndPointId = rr.roadEndPointId
  LEFT  JOIN  fastlane.lance.TripPassageImageDownloadRequest tpidr ON t.tripPassageId = tpidr.tripPassageId 
  LEFT  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tp.roadsideTollType
  LEFT  JOIN  fastlane.rtoll.TollStatus tss ON tss.tollId = t.tripId
  LEFT  JOIN  fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = tss.tollStatusCodeId 
 WHERE  tp.laneTime BETWEEN convert(datetime2,'2021-07-01') AND convert(datetime2,'2022-01-01')
GROUP BY valff.roadway;

/********** ACCTS IN TRAINS **********/
-- 2022-02-01

-- accounts of interest: 0405 73, 0499 73
SELECT DISTINCT coa.glAccountNumber, coa.glAccountName FROM fastlane.gl.ChartOfAccounts coa;
SELECT count(*) FROM fastlane.gl.v_JournalEntryFullDetail vjefd;

-- items in account 040573:
--   6,785,675 in ledger:
--       5,707 as DISMISS
--   1,347,320 as PPDEBITCANCEL
--   5,432,648 in TollLedgerEntry:
--     3,221,468 as FCPP transaction type (40 with duplicate trip id) 
--     2,211,180 as TCPP transaction type (? with duplicate trip id)
-- mt.postingDate appears to be date of vts.postingDate
-- vts.amount and mt.amount are same but with signs reversed
-- gpr.glChartOfAccountsId matches mt.glChartOfAccountsId via fastlane.gl.GLPostingRule 
-- look at vts.isCurrentActivity?
SELECT  WSDOT_STAGE.util.f_get_month(tp.laneTime) AS trip_month
		,WSDOT_STAGE.util.f_get_month(vts.postingDate) AS posting_month
		--,WSDOT_STAGE.util.f_get_month(mt.postingDate) AS mt_post_month
		,ett.shortCode AS transaction_type
		,vfl.wsdot_facility 
		,epp.shortCode AS pricing_plan
		,egf.shortCode AS fund_code 
		,sum(vts.amount) AS vts_amount
		--,sum(mt.amount) AS mt_amount
		,count(DISTINCT mt.transactiondetailId) AS distinct_transactions
		,count(DISTINCT t.tripId) AS distinct_trips
		,count(*) AS row_count
--SELECT  count(*)
  FROM  fastlane.xact.ledger l 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  --JOIN  fastlane.gl.GLPostingRule gpr ON gpr.glPostingRuleId = mt.glPostingRuleId 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
  JOIN  fastlane.gl.EnuGlFund egf ON egf.glFundId = coa.glFundId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId		-- tle.ledgertransactionId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = l.transactionId 
  LEFT  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId
  LEFT  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  LEFT  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
 WHERE  coa.glAccountNumber = '0405 73'
--   AND  vts.customerAccountid = mt.customeraccountid
--   AND  tp.laneTime >= convert(datetime2, '2021-01-01')
GROUP BY WSDOT_STAGE.util.f_get_month(tp.laneTime)
		,WSDOT_STAGE.util.f_get_month(vts.postingDate)
		--,WSDOT_STAGE.util.f_get_month(mt.postingDate)
		,ett.shortCode 
		,vfl.wsdot_facility 
		,epp.shortCode
		,egf.shortCode 
;

/********** INACTIVE ACCOUNTS **********/
-- query for Ami (email 2022-02-18)
-- accounts with latest processing transaction prior to 2016
-- also add account status � Active, has at least one LP or Pass, pass type, discount plan
SELECT  d.customerAccountid
		,d.latest_post_date
		,vca.acct_type_desc 
		,vas.status 
		,tb.amount
		,icac.firstName AS acct_firstName
        ,icac.lastNameOrCompanyName AS acct_lastNameOrCompanyName
        ,ea.emailAddress AS acct_emailAddress
		,count(DISTINCT tca.transponderId) AS active_pass_count
		,count(DISTINCT lpca.licensePlateId) AS active_lp_count
		,count(DISTINCT tdp.discountPlanId) AS active_pass_discounts
		,count(DISTINCT lpdp.discountPlanId) AS active_lp_discounts
--SELECT  count(*)
  FROM  (SELECT vts.customerAccountId
  				,max(vts.postingDate) AS latest_post_date
		   FROM fastlane.xact.V_TransactionSummary vts
		GROUP BY vts.customerAccountId
		) d
  LEFT  JOIN fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = d.customerAccountid AND tb.isCurrentActivity = 1
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = d.customerAccountid
  LEFT  JOIN fastlane.info.V_AccountStatus vas ON vas.customerAccountId = d.customerAccountid 
  LEFT  JOIN fastlane.rtoll.TransponderCustomerAccount tca ON tca.customerAccountId = d.customerAccountid AND getdate() BETWEEN tca.effectiveStartDate AND tca.effectiveEndDate 
  LEFT  JOIN fastlane.xact.TransponderDiscountPlan tdp ON tdp.transponderId = tca.transponderId AND getdate() BETWEEN tdp.effectiveStartDate AND tdp.effectiveEndDate
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.customerAccountId = d.customerAccountid AND getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
  LEFT  JOIN fastlane.xact.LicensePlateDiscountPlan lpdp ON lpdp.licensePlateId = lpca.licensePlateId AND getdate() BETWEEN lpdp.effectiveStartDate AND lpdp.effectiveEndDate 
  LEFT  JOIN fastlane.util.IV_CustomerAccountContacts icac ON icac.customerAccountId = d.customerAccountId AND icac.contacttype = 'MAIN'
  LEFT  JOIN fastlane.info.EmailAddress ea ON ea.emailAddressId = icac.emailAddressId
 WHERE  d.latest_post_date < convert(date,'2016-01-01')
GROUP BY d.customerAccountid
		,d.latest_post_date 
		,vca.acct_type_desc
		,vas.status
		,tb.amount
		,icac.firstName
        ,icac.lastNameOrCompanyName
        ,ea.emailAddress
ORDER BY d.customerAccountId;

/********** DAILY DISPOSITION FOR MODELING **********/
-- 2022-02-25 - pull just october daily data for I-405, SR167
SELECT  cast(t.laneTime AS date) AS trip_date 
		,t.roadway
		,t.roadsideTollType AS roadside_toll_type
		--,t.axleCount
		,t.transaction_type
		,t.pricing_plan
        ,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
              WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
              ELSE t.disposition_category
              END AS disposition_category
		,sum(t.roadside_amount) AS sum_roadside_amount
		,sum(t.latest_toll_amount) AS sum_toll_amount	-- okay for now since transaction types seperate positive (WRITEOFF, TOLLDISC) from others
        ,count(DISTINCT t.trip_id) AS trip_count
  FROM  WSDOT_STAGE.rpt.v_trip_detail t
 WHERE  t.laneTime BETWEEN convert(datetime2,'2021-10-01') AND convert(datetime2,'2021-11-01')
   AND  t.roadway IN ('I405','SR167')
GROUP BY cast(t.laneTime AS date)
		,t.roadway
		,t.roadsideTollType
		--,t.axleCount
		,t.transaction_type
		,t.pricing_plan
        ,CASE WHEN t.disposition_category IN ('LEAK_I','LEAK_II','LEAK_III','LEAK_IV','SYS_252','TYPE_99','WRITEOFF') THEN 'LEAKAGE'
              WHEN t.disposition_category IN ('NONREV','CUSTOM_TOLLDISC') THEN 'NONREV'
              ELSE t.disposition_category END
ORDER BY t.roadway
		,cast(t.laneTime AS date)
        ,count(DISTINCT t.trip_id) DESC;

/********** WINDSHIELD FLAG **********/
-- 2022-03-01 request by text from Tyler
-- pull name, acct no, phone no, last time tag seen, last contact (if requested, leave off for now, see account note on acct 262444 for example)
-- use cross join (outer apply) to get latest trip by that transponder
SELECT  tca.customerAccountId
		,fa.kapschCode AS tag_agency
		,t.transponderNumber 
		,tdt.description AS tag_type
		,it.hasMetallizedWindshield 
		,etis.description AS inv_tag_status
		--,tr.laneTime AS last_tag_txn_time
		,ucd.latest_case_date
		,aci.isCompany
		,aci.firstName
		,aci.lastNameOrCompanyName
		,aci.displayname
		,aci.home_phone
		,aci.mobile_phone
		,aci.emailAddress
-- SELECT  count(DISTINCT it.inventoryTagId) 
  FROM  fastlane.inv.InventoryTag it
  JOIN  fastlane.vehicle.Transponder t ON t.inventoryTagId = it.inventoryTagId 
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  JOIN  fastlane.inv.EnuTagInventoryStatus etis ON etis.tagInventoryStatusId = it.tagInventoryStatusId 
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = it.tagDeviceTypeId 
--  LEFT  JOIN fastlane.rtoll.IV_tollDetailsTransponder itdt ON itdt.transponderId = t.transponderId
  JOIN  fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = t.transponderId 
  LEFT  JOIN WSDOT_STAGE.rpt.acct_contact_info aci ON aci.customerAccountId = tca.customerAccountId 
  LEFT  JOIN
		(SELECT cca.customerAccountId
				,max(uc.created) AS latest_case_date
		   FROM fastlane.usercase.[UCase] uc 
		   JOIN fastlane.usercase.CaseCustomerAccount cca ON cca.ucaseId = uc.ucaseId 
		GROUP BY cca.customerAccountId
		) ucd ON ucd.customerAccountId = tca.customerAccountId 
  /*OUTER APPLY
		(SELECT TOP 1 itdt.laneTime 
		   FROM fastlane.rtoll.IV_tollDetailsTransponder itdt 
		  WHERE itdt.transponderId = t.transponderId 
		ORDER BY itdt.laneTime DESC
		) tr */
 WHERE  it.hasMetallizedWindshield = 1;

SELECT  pd.personOrCompanyId 
		,pd.phoneTypeId 
		,count(*)
  FROM  fastlane.info.PhoneDevice pd 
GROUP BY pd.personOrCompanyId 
		,pd.phoneTypeId 
HAVING  count(*) > 2;

SELECT  icac.customerAccountId 
		,icac.personOrCompanyId 
		,pd.phoneTypeId 
		,pd.[number] 
  FROM  fastlane.util.IV_CustomerAccountContacts icac 
  JOIN  fastlane.info.PhoneDevice pd ON pd.personOrCompanyId = icac.personOrCompanyId 
 WHERE  icac.personOrCompanyId IN (266030, 318169, 1193913, 14928838);

SELECT * FROM fastlane.info.PhoneDevice pd WHERE pd.countryId <> 1;

/********** PASS TYPE DETAIL **********/
-- 2022-03-03 request by email from Tyler with pass type detail
-- see arch v_pass_detail

-- summary without pass/acct ids and read times
SELECT  vpd.pass_agency
		,vpd.pass_type
		,vpd.pass_status
		,vpd.acct_type_desc 
		,vpd.acct_status
		,count(*) AS pass_count
  FROM  WSDOT_STAGE.util.v_pass_detail vpd 
GROUP BY vpd.pass_agency
		,vpd.pass_type
		,vpd.pass_status
		,vpd.acct_type_desc 
		,vpd.acct_status;

/********** DO NOT BILL LIST **********/
-- 2022-03-07 request from Tyler and Ami
-- identify plates on do not bill list that have trips since 2022-01-01

-- create table of former do not bill list
-- data in _imports folder
DROP TABLE WSDOT_STAGE.tmp.do_not_bill_20210628;
CREATE TABLE WSDOT_STAGE.tmp.do_not_bill_20210628 (
	lp_state varchar(2) NOT NULL
	,lp_country varchar(2) NOT NULL
	,lp_type varchar(8) NOT NULL
	,lp_number varchar(10) NOT NULL
	,veh_make varchar(32)
	,veh_model varchar(32)
	,veh_year smallint
	,veh_color varchar(32)
	,start_dtm datetime2(2) NOT NULL
	,end_dtm datetime2(2) NOT NULL
);

-- 4378 plates on do not bill list
SELECT count(*) FROM WSDOT_STAGE.tmp.do_not_bill_20210628;

-- find lp id for these plates - 6 do not match, 4234 have one lpid, 117 have two lpids, 21 have three lpids
SELECT count(*) FROM (
SELECT lp.licensePlateId 
--SELECT  dnb.lp_state 
--		,dnb.lp_number 
--		,count(lp.licensePlateId) AS lpid_count
--SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.do_not_bill_20210628 dnb 
  JOIN  fastlane.locale.EnuCountry ec ON ec.isoCode = dnb.lp_country 
  JOIN  fastlane.locale.EnuState es ON es.shortCode = dnb.lp_state AND es.countryId = ec.countryId
  JOIN  fastlane.util.EnuLpJurisdiction elj ON elj.stateId = es.stateId 
  JOIN  fastlane.util.LicensePlate lp ON lp.lpnumber = dnb.lp_number AND lp.lpJurisdictionId = elj.lpJurisdictionId
--GROUP BY dnb.lp_state 
--		,dnb.lp_number
--HAVING count(*) > 1
) AS x;

-- trips made by do not bill plates since 2022-01-01
SELECT  dnb.lp_state 
		,dnb.lp_number
		,lp.licensePlateId
		,lpca.customerAccountId 
		,count(t.TollID) AS toll_trips
  FROM  WSDOT_STAGE.tmp.do_not_bill_20210628 dnb 
  JOIN  fastlane.locale.EnuCountry ec ON ec.isoCode = dnb.lp_country 
  JOIN  fastlane.locale.EnuState es ON es.shortCode = dnb.lp_state AND es.countryId = ec.countryId
  JOIN  fastlane.util.EnuLpJurisdiction elj ON elj.stateId = es.stateId 
  JOIN  fastlane.util.LicensePlate lp ON lp.lpnumber = dnb.lp_number AND lp.lpJurisdictionId = elj.lpJurisdictionId
  JOIN  fastlane.rtoll.Toll t ON t.licensePlateId = lp.licensePlateId 
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId AND t.exitTime BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
 WHERE  t.exitTime >= convert(datetime2, '2022-01-01')
GROUP BY dnb.lp_state 
		,dnb.lp_number
		,lp.licensePlateId
		,lpca.customerAccountId;

/********** SR167 TOLL ADJUST ACCOUNTS **********/
-- 2022-03-22 cases created for SR167 adjusted trips
-- create temp table to import data
-- data in _imports folder
DROP TABLE WSDOT_STAGE.tmp.sr167_toll_adjust;
CREATE TABLE WSDOT_STAGE.tmp.sr167_toll_adjust (
	customeraccountid integer
	,trip_count smallint
);

-- rename acct_count to trip_count
USE WSDOT_STAGE
EXEC sp_rename 'tmp.sr167_toll_adjust.acct_count', 'trip_count', 'COLUMN';

-- check data, should have 714 rows with 752 sum trip count
SELECT count(*), sum(trip_count) FROM WSDOT_STAGE.tmp.sr167_toll_adjust sta;

-- get any cases for these accounts created on or after Mar 3, 2022
SELECT  cca.customerAccountId 
		,sta.trip_count
		,u.ucaseId
--SELECT  count(*)
  FROM  fastlane.usercase.CaseCustomerAccount cca
  JOIN  fastlane.usercase.[UCase] u ON u.ucaseId = cca.ucaseId 
  JOIN  WSDOT_STAGE.tmp.sr167_toll_adjust sta ON sta.customeraccountid = cca.customerAccountId 
 WHERE  u.created >= convert(datetime2,'2022-03-03');

SELECT * FROM WSDOT_STAGE.util.v_trip_detail vtd WHERE vtd.transaction_type = 'WRITEOFF' AND laneTime > convert(date, '2022-01-01');

/********** INVESTIGATE NULL ENTRY REASONS **********/
-- 2022-03-29 call with Lance he clarified that you have to use annotation from xact.ledger!
SELECT  t.tripId
		,vts.postingDate 
		,vts.transactionTypeId 
		,ett.shortCode AS transactionType
		,tle.endAnnotationId 
		,ld.annotationid 
		,eer.shortCode 
		,eer2.shortCode 
  FROM  fastlane.lance.trip t 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.tollId = t.tripId
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  JOIN  fastlane.xact.ledger ld ON ld.transactionId = vts.transactionId 
  INNER  JOIN  fastlane.xact.Annotation ann ON ann.annotationid = ld.annotationid 
  INNER  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
 LEFT JOIN  fastlane.xact.Annotation ann2 ON ann2.annotationid = tle.endannotationid 
  LEFT   JOIN  fastlane.xact.EnuEntryReason eer2 ON eer2.entryReasonId = ann.entryReasonId
 WHERE  t.tripId = 227223335
ORDER BY vts.postingDate DESC;

-- also look at ledgeriteminfo (posting date = lanetime)
SELECT *
  FROM fastlane.lance.trip t 
  JOIN fastlane.xact.LedgerItemInfo lii ON lii.ledgerItemInfoId = t.ledgerItemInfoId 
  JOIN fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  t.tripId = 227223335;

-- verify that all ledgeriteminfo posting times for trips match lane time
SELECT  count(*)
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId
  JOIN  fastlane.xact.LedgerItemInfo lii ON lii.ledgerItemInfoId = t.ledgerItemInfoId 
 WHERE  tp.laneTime <> lii.postingDate;

/********** ACCOUNT TYPE QUESTIONS **********/
-- also check invoice accounts (currently 69560 and 1344863)
SELECT * FROM WSDOT_STAGE.util.v_customer_account vca WHERE vca.acct_short_code = 'INVOICE';

-- unknown accounts:
SELECT * FROM WSDOT_STAGE.util.v_customer_account vca WHERE vca.acct_short_code = 'UNKNOWN' ORDER BY vca.customer_account_id;

-- 2022-03-02 look at 6 formerly unknown accounts for Ami, now all are defunct
SELECT * FROM fastlane.util.CustomerAccount ca WHERE ca.customerAccountId IN (10106643,10106648,10106677,10106680,10106682,10106687);

/********** ACCT 121 TRIP DETAILS **********/
-- 2022-04-13 request from Ami, see code in email on 2022-04-08
-- 2023-01-30: remove request_result_reason for now since it is not in table
SELECT  tdm.trip_id 
		,tdm.tripPassageId 
		,tdm.laneTime 
		,tdm.roadway 
		,tdm.exit_plaza 
		,tdm.transaction_type 
		,tdm.customerAccountid 
		,tdm.insightId 
		,tdm.requested
		,tdm.qfree_acked
		,tdm.result_received
		,tdm.request_result_code
		--,tdm.request_result_reason 
		,tdm.result_type
		,tdm.disposition_type
		,tdm.lpNumber 
		,tdm.jurisdiction 
		,tdm.plateType 
--SELECT  count(*)
  FROM  WSDOT_STAGE.util.v_trip_detail_mir tdm 
 WHERE  tdm.transaction_type = 'TPENDING'
   AND  tdm.customerAccountid = 121;

SELECT  tdm.trip_month
		,tdm.roadway 
		,tdm.transaction_type 
		,CASE WHEN tdm.customerAccountid < 1000 THEN tdm.customerAccountid ELSE NULL END AS systemAccountId
		,tdm.requested
		,tdm.qfree_acked
		,tdm.result_received
		,tdm.request_result_code
		--,tdm.request_result_reason 
		,tdm.result_type
		,tdm.disposition_type 
		,count(*) AS trip_count
  FROM  WSDOT_STAGE.util.v_trip_detail_mir tdm 
-- WHERE  tdm.transaction_type = 'TPENDING'
--   AND  tdm.customerAccountid = 121
GROUP BY tdm.trip_month
		,tdm.roadway 
		,tdm.transaction_type 
		,CASE WHEN tdm.customerAccountid < 1000 THEN tdm.customerAccountid ELSE NULL END 
		,tdm.requested
		,tdm.qfree_acked
		,tdm.result_received
		,tdm.request_result_code
		--,tdm.request_result_reason 
		,tdm.result_type
		,tdm.disposition_type
ORDER BY tdm.trip_month
		,tdm.roadway
		,count(*) DESC;

--
SELECT * FROM fastlane.qfree.ImageReviewRequest irr WHERE isRequested = 0;

-- 706 resultCodeIds differ, 652 reason
SELECT count(*)
  FROM  fastlane.qfree.ImageReviewRequestResponses irrr
  JOIN  fastlane.qfree.ImageReviewResultResponses irrr2 ON irrr2.imageReviewRequestId = irrr.imageReviewRequestId 
 WHERE  irrr2.re <> irrr.reasonId ;

SELECT  *
  FROM  fastlane.qfree.ImageReviewRequest irr
 WHERE  irr.tripPassageId = 320368344;

SELECT * FROM fastlane.qfree.ImageReviewRequestResponse res WHERE res.imageReviewRequestId = 12317998;
SELECT * FROM fastlane.qfree.ImageReviewRequestResponses res WHERE res.imageReviewRequestId = 12317998;
SELECT * FROM fastlane.qfree.ImageReviewResult res WHERE res.imageReviewRequestId = 12317998;
SELECT * FROM fastlane.qfree.ImageReviewResultResponses res WHERE res.imageReviewRequestId = 12317998;

/********** RETURNED INVALID PASSES **********/
-- 2022-05-05 request from Ami
-- I am looking for a query on passes that are marked in the BOS as Returned. There was an issue at time of 
--   migration where these are considered �Ghost Pass� in ETCC. Which caused these passes to migrated to FL 
--   as �Returned� instead of �Active�. After we produce the query I am curious if we are able to see these 
--   pass recorded on an account prior to go live or reading in the lanes. Let me know if you have any questions.
-- Pass Inventory Status = Returned
-- Pass Status = Invalid
-- Date Found Lost/Stolen = This will have a date recorded in the database
-- Date Assigned = NA
-- History = No account LinkTo
SELECT  it.inventoryTagId 
		,fa.kapschCode AS pass_agency 
		,t.transponderNumber
		,tdt.description AS pass_type 
		,etis.description AS pass_inv_status 
		--,tr.first_read 
		--,tr.latest_read 
		--,tr.num_trips
		--,tr.status_ids
		,tr.tripId
		,tr.laneTime
		,tr.postTime
		,tr.roadway
		,tr.transponder_status 
		,tr.customerAccountId
		,tr.pricing_plan
-- SELECT  count(*)
  FROM  fastlane.inv.InventoryTag it 
  JOIN  fastlane.vehicle.Transponder t ON t.inventoryTagId = it.inventoryTagId 
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId 
  JOIN  fastlane.inv.EnuTagInventoryStatus etis ON etis.tagInventoryStatusId = it.tagInventoryStatusId 
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = it.tagDeviceTypeId 
  LEFT  JOIN fastlane.rtoll.TransponderCustomerAccount tca ON tca.transponderId = t.transponderId 
  --LEFT  JOIN
  JOIN
		(SELECT tpt.transponderId
				,trp.tripId
				,tp.laneTime
				,trp.created AS postTime
				,vfl.wsdot_roadway AS roadway
				,ets.description AS transponder_status 
				,vts.customerAccountid 
				,epp.shortCode AS pricing_plan
				--,min(tp.laneTime) AS first_read
				--,max(tp.laneTime) AS latest_read
				--,count(*) AS num_trips
				--,string_agg(tpt.transponderStatusId, ',') WITHIN GROUP (ORDER BY tp.laneTime) AS status_ids
		   FROM fastlane.lance.TripPassageTransponder tpt
		   JOIN fastlane.lance.trip trp ON trp.tripPassageId = tpt.tripPassageId	-- use to ensure only exit passages
		   JOIN fastlane.lance.tripPassage tp ON tp.tripPassageId = tpt.tripPassageId
		   JOIN WSDOT_STAGE.util.v_all_trip_state vats ON vats.trip_id = trp.tripId 
		   JOIN fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vats.last_trip_txn_id 
		   JOIN fastlane.xact.V_TransactionSummary vts ON vts.transactionId = vats.last_trip_txn_id AND vts.customerAccountid = tle.ledgeraccountId
		   JOIN fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId
		   JOIN fastlane.vehicle.EnuTransponderStatus ets ON ets.transponderStatusId = tpt.transponderStatusId
		   JOIN WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
		  WHERE trp.created >= convert(date, '2021-07-10')
		--GROUP BY tpt.transponderId 
		) tr ON tr.transponderId = t.transponderId
 WHERE  it.tagInventoryStatusId = 11	-- returned
   AND  it.lostOrStolenDate IS NOT NULL
   AND  tca.customerAccountId IS NULL
--   AND  tr.latest_read IS NOT NULL
--   AND  it.transponderNumber = '0003001054'
ORDER BY t.transponderNumber 
		,tr.laneTime
;

-- account status investigation
SELECT  vas.status, count(*)
  FROM  WSDOT_STAGE.util.v_customer_account ca
  LEFT  JOIN fastlane.info.V_AccountStatus vas ON vas.customerAccountId = ca.customer_account_id
GROUP BY vas.status;

-- account tag investigation
-- 2022-05-19 for Ami
SELECT * FROM fastlane.util.CustomerAccount ca WHERE ca.customerAccountId IN (10285389, 10312810, 10312813);
SELECT * FROM WSDOT_STAGE.util.v_pass_detail vpd WHERE vpd.customer_account_id IN (10285389, 10312810, 10312813);
SELECT * FROM fastlane.rtoll.V_ah_TransponderCustomerAccount tca WHERE tca.customerAccountId IN (10285389, 10312810, 10312813);

-- escalation investigation for Tyler
-- how many accounts have unpaid transactions that are older than (30, 90, 180) days?
-- what does the profile of the accounts look like (max of amounts due)?
SELECT  CASE WHEN debt_age >= 180 THEN 'over 180 days'
			 WHEN debt_age >= 90 THEN '90-180 days'
			 WHEN debt_age >= 30 THEN '30-90 days'
			 ELSE 'less than 30 days'
			END AS debt_age
		,sum(current_balance) AS total_due
		,min(current_balance) AS largest_individual_amt_due
		,count(*) AS account_count
  FROM  (SELECT ca.customer_account_id
  				,ca.acct_short_code
		        ,tb.amount AS current_balance
        		,tb.lastSignChangeDate AS negative_balance_date
		        ,datediff(day, tb.lastSignChangeDate, getdate()) AS debt_age
		--SELECT count(*)
		   FROM WSDOT_STAGE.util.v_customer_account ca
		   JOIN fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = ca.customer_account_id AND tb.isCurrentActivity = 1
		  WHERE tb.signValue = -1
		    AND ca.customer_account_id >= 1000
		    AND tb.amount < -10000
		) d 
GROUP BY CASE WHEN debt_age >= 180 THEN 'over 180 days'
			 WHEN debt_age >= 90 THEN '90-180 days'
			 WHEN debt_age >= 30 THEN '30-90 days'
			 ELSE 'less than 30 days' END
;

-- what are the number of unpaid transactions from each month?
-- note that most leaks/writoffs are marked fully allocated, but a small set are still considered unpaid?
SELECT  tdb.trip_month
		,count(*) AS trips
  FROM  WSDOT_STAGE.util.v_trip_detail_base tdb
 WHERE  tdb.customerAccountid >= 1000	-- non-system accounts
   AND  tdb.isUnpaid = 1				-- is unpaid
GROUP BY tdb.trip_month
ORDER BY tdb.trip_month;

-- how many accounts have not been paid since June 2021 or Dec 2021?
SELECT  count(*) AS account_count
  FROM  (SELECT tdb.customerAccountid 
				,sum(cast(tdb.isUnpaid AS int)) AS unpaid_trips
				,count(*) AS total_trips
		   FROM WSDOT_STAGE.util.v_trip_detail_base tdb
		  WHERE tdb.trip_month > convert(date, '2021-12-31')
		GROUP BY tdb.customerAccountid
		) d
 WHERE  d.unpaid_trips = d.total_trips;

/********** COMMUNICATION LETTERS **********/
-- 2022-05-24 Jeff Hall request to look at communications sent out by mail

-- batch_code is comm.EnuCommunicationType.documentTypeCode
SELECT  WSDOT_STAGE.util.f_get_month(c.createdAt) AS trip_month
		,vca.acct_type_desc 
		,ect.shortCode AS comm_type
		,ect.description 
		,count(*) AS comm_count
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.comm.EnuCommunicationType ect ON ect.communicationTypeId = c.communicationTypeId 
  JOIN  fastlane.comm.EnuCommunicationChannel ecc ON ecc.communicationChannelId = c.communicationChannelId
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
 WHERE  ect.documentTypeCode = 'A'
   AND  c.createdAt >= convert(datetime2, '2021-07-01')
   AND  c.communicationChannelId = 3	-- USMAIL
GROUP BY WSDOT_STAGE.util.f_get_month(c.createdAt)
		,vca.acct_type_desc
		,ect.shortCode 
		,ect.description 
ORDER BY WSDOT_STAGE.util.f_get_month(c.createdAt)
		,vca.acct_type_desc
		,ect.shortCode
		,ect.description;

-- investigate printhouse tables
-- NOTE: don't know how to select only those of interest (batch_code A) or identify the account!
SELECT  req.printHouseCommEventReqId 
		,req.fileRegistryId 
		,req.startTime 
		,fr.description 
		,eft.fileTypeGroupId 
		,res.incomingFileRegistryId 
		,et.shortCode AS comm_event_type
		,eft.shortCode AS file_type
		,efrs.shortCode AS filereg_status
--SELECT count(*)
  FROM  fastlane.comm.PrintHouseCommEventReq req
  JOIN  fastlane.comm.PrintHouseCommEventRes res ON res.printHouseCommEventReqid = req.printHouseCommEventReqId 
  JOIN  fastlane.monitor.FileRegistry fr ON fr.fileRegistryId = req.fileRegistryId 
  JOIN  fastlane.comm.EnuPrintHouseCommEventType et ON et.printHouseCommEventTypeId = req.printHouseCommEventTypeId 
  JOIN  fastlane.monitor.EnuFileType eft ON eft.fileTypeId = fr.fileTypeId 
  JOIN  fastlane.monitor.EnuFileRegistryStatus efrs ON efrs.fileRegistryStatusId = fr.fileRegistryStatusId 
 WHERE  WSDOT_STAGE.util.f_get_month(req.startTime) = convert(date, '2022-03-01')
;

/********** COMMUNICATION LETTERS **********/
-- 2022-05-24 get account type and case creation date for ~12k accounts

-- load account ids from Ami
-- data in _imports folder
DROP TABLE WSDOT_STAGE.tmp.escheatment_accts;
CREATE TABLE WSDOT_STAGE.tmp.escheatment_accts (customerAccountId int);

SELECT count(*) FROM WSDOT_STAGE.tmp.escheatment_accts ea;

SELECT  ea.customerAccountId 
		,vca.acct_short_code 
		,cca.ucaseId 
		,uc.created AS case_open_date
  FROM  WSDOT_STAGE.tmp.escheatment_accts ea
  JOIN  fastlane.usercase.CaseCustomerAccount cca ON cca.customerAccountId = ea.customerAccountId
  JOIN  fastlane.usercase.[UCase] uc ON uc.ucaseId = cca.ucaseId
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ea.customerAccountId 
 WHERE  uc.caseTypeId = 11;	-- close account

-- DROP TABLE WSDOT_STAGE.tmp.escheatment_accts;

/********** LICENSE PLATES ON OTHER ACCTS **********/
-- 2022-05-26 request from Bobbie Jean
-- data in _imports folder
DROP TABLE WSDOT_STAGE.tmp.lp_on_other_accts;
CREATE TABLE WSDOT_STAGE.tmp.lp_on_other_accts (lpnumber varchar(16), customerAccountId int);

SELECT customerAccountId, count(*) FROM WSDOT_STAGE.tmp.lp_on_other_accts GROUP BY customerAccountId;

-- get account number for these plates
-- 4487 based on lpnumber (so several states), 4645 with lpca, 3192 with current lp assigned to account
SELECT  looa.lpnumber 
		,lpj.shortCode AS lpjurisdiction 
		,lpca.customerAccountId 
		,lpca.effectiveStartDate 
		,lpca.effectiveExpirationDate 
--SELECT count(*)
  FROM  fastlane.util.LicensePlate lp 
  JOIN  WSDOT_STAGE.tmp.lp_on_other_accts looa ON looa.lpnumber = lp.lpnumber 
  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
 WHERE  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
ORDER BY looa.lpnumber 
		,lpj.shortCode 
		,lpca.effectiveStartDate;

-- get distinct counts of each plate
SELECT cnt, count(*) FROM (
SELECT looa.lpnumber, count(*) AS cnt
  FROM  fastlane.util.LicensePlate lp 
  JOIN  WSDOT_STAGE.tmp.lp_on_other_accts looa ON looa.lpnumber = lp.lpnumber 
  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
 WHERE  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
GROUP BY looa.lpnumber 
) d
GROUP BY cnt;

-- plates that did not match
SELECT  lpnumber FROM WSDOT_STAGE.tmp.lp_on_other_accts
 WHERE  lpnumber NOT IN (
SELECT  looa.lpnumber
  FROM  fastlane.util.LicensePlate lp 
  JOIN  WSDOT_STAGE.tmp.lp_on_other_accts looa ON looa.lpnumber = lp.lpnumber 
--  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
--  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
-- WHERE  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate
);

/********** TEMPORARY LICENSE PLATES **********/
SELECT  licensePlatePatternClassId
		,count(*)
  FROM  fastlane.util.LicensePlate lp
 WHERE  lpJurisdictionId = 47	-- WA
GROUP BY licensePlatePatternClassId;

-- get temp plate account assignemnts
SELECT  lp.lpnumber 
		,lpj.shortCode AS jurisdiction
		,lpca.customerAccountId 
		,lpca.effectiveStartDate 
		,lpca.effectiveExpirationDate 
-- SELECT  count(*)
  FROM  fastlane.util.LicensePlate lp 
  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId
 WHERE  lp.licensePlatePatternClassId = 3 	-- temporary
--   AND  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate 
--   AND  lp.lpJurisdictionId = 47
;

/********** T-NOPLT INVESTIGATION **********/
-- 2022-06-02 T-NOPLT investigation (and other missing T-type reason codes)
-- ETAN should write a new ledger entry after image review results with the proper reason code
-- instead, most T-codes still show as their single d-orig reason when the trip was created, even though they are cancelled and fully allocated
-- SELECT * FROM WSDOT_STAGE.util.v_trip_detail_full tdf WHERE trip_id = 264095616;
SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  tle.tollId = 215562790	--264095616
ORDER BY vts.postingDate;

-- t-codes do show up in fastlane.xact.IV_transactionsummary_base
SELECT itb.entryreasonCode, count(*) FROM fastlane.xact.IV_transactionsummary_base itb GROUP BY itb.entryreasonCode ORDER BY itb.entryreasonCode;

-- so apparently ledger entries get written, but they are not in the tollledgerentry?
-- see join in fastlane.reports.rpt_leakage_detail_report, selection into #Toll_Original
SELECT  *
  FROM  fastlane.xact.IV_transactionsummary_base itb
  JOIN  fastlane.rtoll.IV_tollDetails itd ON itd.ledgerItemInfoId = itb.ledgeriteminfoid 
 WHERE  itd.tripid = 215562790	--264095616
ORDER BY itb.postingdate;

SELECT  *
  FROM  fastlane.xact.V_TransactionSummary vts
--  JOIN  fastlane.rtoll.IV_tollDetails itd ON itd.ledgerItemInfoId = vts.ledgerItemInfoId  
  JOIN  fastlane.lance.Trip t ON t.ledgerItemInfoId = vts.ledgerItemInfoId
  JOIN  fastlane.xact.ledger ld ON ld.transactionId = vts.transactionId 
  JOIN  fastlane.xact.Annotation ann ON ann.annotationid = ld.annotationid 
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
 WHERE  t.tripid = 215562790	--264095616
ORDER BY vts.postingDate;

SELECT tle.tollId, count(*) FROM fastlane.xact.TollLedgerEntry tle GROUP BY tle.tollId HAVING count(*) > 10;

SELECT  DISTINCT tle.tollId AS trip_id
		,first_value(vts.transactionId) OVER (PARTITION BY tle.tollId ORDER BY vts.postingDate DESC, ttp.priority DESC, vts.transactionId) AS last_trip_txn_id
		,first_value(CASE WHEN ett.transactionCategoryId = 1 THEN vts.transactionId ELSE NULL END) OVER (PARTITION BY tle.tollId ORDER BY ett.transactionCategoryId, vts.postingDate DESC) AS last_toll_txn_id
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  LEFT  JOIN  WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transaction_type_id = vts.transactionTypeId
 WHERE  tle.tollId = 215562790;	--264095616;

SELECT  DISTINCT t.tripId AS trip_id
		,first_value(vts.transactionId) OVER (PARTITION BY t.tripId ORDER BY vts.postingDate DESC, ttp.priority DESC, vts.isCancellation, vts.transactionId) AS last_trip_txn_id
		,first_value(CASE WHEN ett.transactionCategoryId = 1 THEN vts.transactionId ELSE NULL END) OVER (PARTITION BY t.tripId ORDER BY ett.transactionCategoryId, vts.postingDate DESC) AS last_toll_txn_id
  FROM  fastlane.xact.V_TransactionSummary vts
  JOIN  fastlane.lance.Trip t ON t.ledgerItemInfoId = vts.ledgerItemInfoId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  LEFT  JOIN  WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transaction_type_id = vts.transactionTypeId
 WHERE  t.tripId = 264095616;	--215562790;	--264095616;

-- notes:
-- getting the T-codes also results in pulling cancellations on other trips
-- idea: perhaps only pull cancellation if there is not an associated item? could also use latest transactionid, but is that reliable
-- works, but using ledgeriteminfoid is MUCH slower, also tricky to include some cancellations but not others?
-- also, using isDebit=1 would remove tolldisc, writeoff from our summaries. should we add balanceTypeId condition though?
-- BUT! it looks like they now update the endAnnotationId on the TLE to give the correct t-code?!?
-- so if TLE anootation is null, use ledger annotation, otherwise use TLE annotation!

-- also, can TollLedgerEntry be replicated from Ledger info?
SELECT * FROM fastlane.xact.TollLedgerEntry tle WHERE tollId = 264095616;

SELECT * FROM fastlane.xact.ledger l WHERE l.transactionId = 440589671;

SELECT  DISTINCT t.tripId, l.transactionId, l.customerAccountId, itd.pricingPlanId, itd.enforcementTypeId, l.annotationid, itd.discountPlanId
  FROM  fastlane.xact.ledger l
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId 
  JOIN  fastlane.lance.Trip t ON t.ledgerItemInfoId = vts.ledgerItemInfoId
  JOIN  fastlane.rtoll.IV_tollDetails itd ON itd.tripid = t.tripId 
 WHERE  l.transactionId IN (374712748, 440589671, 448105816);

SELECT * FROM fastlane.xact.PaymentCredit pc WHERE pc.transactionId = 440589671;

SELECT * FROM fastlane.xact.Annotation a JOIN fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId WHERE a.annotationid IN (340512997, 345208737);

/********** RENTAL CASES WITH UNPAID TRIPS **********/
-- 2022-06-29 request from Bobbie Jean Stanley
-- 2022-08-24 similar request with new data
-- Would you be able to tell how many trips are associated �linked� to the case #s on the attached spreadsheet
-- If you could also determine the account number and how many unpaid trips are on the linked account that would be amazing too

CREATE TABLE WSDOT_STAGE.tmp.rental_cases (ucase_id int);

-- 2022-06-29: although there are 59 entries, 10 are duplicates
-- 2022-08-24: no duplicates
SELECT count(DISTINCT ucase_id) FROM WSDOT_STAGE.tmp.rental_cases;

-- remove duplicates (if needed)
SELECT ucase_id, count(*) AS cnt INTO WSDOT_STAGE.tmp.rental_case_dupes FROM WSDOT_STAGE.tmp.rental_cases GROUP BY ucase_id HAVING count(*) > 1;
DELETE FROM WSDOT_STAGE.tmp.rental_cases WHERE ucase_id IN (SELECT ucase_id FROM WSDOT_STAGE.tmp.rental_case_dupes);
INSERT INTO WSDOT_STAGE.tmp.rental_cases SELECT ucase_id FROM WSDOT_STAGE.tmp.rental_case_dupes;
SELECT ucase_id, count(*) AS cnt FROM WSDOT_STAGE.tmp.rental_cases GROUP BY ucase_id HAVING count(*) > 1;
DROP TABLE WSDOT_STAGE.tmp.rental_case_dupes;

-- get all trips linked to cases
-- re-do since multiple cases can link to same customer account, resulting in duplicate trips across different cases
SELECT  vtdb.*
  INTO  WSDOT_STAGE.tmp.rental_case_trips
  FROM  (SELECT DISTINCT cca.customerAccountId 
		   FROM WSDOT_STAGE.tmp.rental_cases rc
		   JOIN fastlane.usercase.CaseCustomerAccount cca ON cca.ucaseId = rc.ucase_id
		) c
  JOIN  WSDOT_STAGE.util.v_trip_detail_base vtdb ON vtdb.customerAccountid = c.customerAccountId;

-- get associated disputed charges - appears to be in caseledgerentry
SELECT  cle.ucaseId AS case_number
		,l.customerAccountId
		,l.transactionId 
		,ett.shortCode
		,l.amount 
		,cle.adjustToAmount
		,t.TollID 
		,t.exitTime 
  INTO  WSDOT_STAGE.tmp.rental_case_disputed_items
  FROM  fastlane.usercase.CaseLedgerEntry cle 
  JOIN  WSDOT_STAGE.tmp.rental_cases rc ON rc.ucase_id = cle.ucaseId 
  JOIN  fastlane.xact.ledger l ON l.transactionId = cle.transactionId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = l.transactionTypeId 
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = cle.transactionId 
  LEFT  JOIN  fastlane.rtoll.Toll t ON t.TollID = tle.tollId 
-- WHERE  ucaseId = 6532713
;

-- output all trip data and disputed charges
SELECT * FROM WSDOT_STAGE.tmp.rental_case_trips rct ORDER BY trip_id;
SELECT * FROM WSDOT_STAGE.tmp.rental_case_disputed_items rcdi ORDER BY case_number;
SELECT count(*) FROM WSDOT_STAGE.tmp.rental_case_trips rct JOIN WSDOT_STAGE.tmp.rental_case_disputed_items rcdi ON rcdi.TollID = rct.trip_id; --ORDER BY rct.case_number;

-- output summary
SELECT  d.case_number
		,d.customerAccountid
		,ct.unpaid_trips
		,ct.total_trips
		,d.disputed_trips
		,d.disputed_pbm_fees
		,d.disputed_total_amount
  FROM  (SELECT rcdi.case_number
  				,rcdi.customerAccountId
  				,count(rcdi.TollId) AS disputed_trips
  				,sum(CASE WHEN rcdi.shortCode = 'FPBM' THEN 1 ELSE 0 END) AS disputed_pbm_fees
  				,sum(rcdi.amount) AS disputed_total_amount
  		   FROM WSDOT_STAGE.tmp.rental_case_disputed_items rcdi
  		GROUP BY rcdi.case_number
  				,rcdi.customerAccountId
  		) d
  LEFT  JOIN  (SELECT rct.customerAccountid
				,sum(CASE WHEN rct.isUnpaid = 1 THEN 1 ELSE 0 END) AS unpaid_trips
				,count(*) AS total_trips
  		   FROM WSDOT_STAGE.tmp.rental_case_trips rct
  		GROUP BY rct.customerAccountid
		) ct ON ct.customerAccountId = d.customerAccountId
ORDER BY d.case_number;

-- cases without a customer account assigned
SELECT  rc.ucase_id AS case_number
  FROM  WSDOT_STAGE.tmp.rental_cases rc
  LEFT  JOIN WSDOT_STAGE.tmp.rental_case_disputed_items rcdi ON rcdi.case_number = rc.ucase_id
 WHERE  rcdi.case_number IS NULL
ORDER BY rc.ucase_id;

-- clean up:
DROP TABLE WSDOT_STAGE.tmp.rental_cases;
DROP TABLE WSDOT_STAGE.tmp.rental_case_trips;
DROP TABLE WSDOT_STAGE.tmp.rental_case_disputed_items;

/********** ESCHEATMENT INVESTIGATION **********/
-- 2022-06-29 for Tyler, via Mike G. in Teams chat
-- active accounts with no flex or motorcycle pass assigned, no discount assigned, with $0 or positive balance,
--   no toll charges or customer contact (case or communication) since 2018-01-01

-- no other forms of customer contact (History tab) since 1/1/2018

-- summarize exclusions to improve performance
SELECT DISTINCT vad.customer_account_id INTO #exc_discounts FROM WSDOT_STAGE.util.v_account_discounts vad;
SELECT DISTINCT vpd.customer_account_id INTO #exc_passes FROM WSDOT_STAGE.util.v_pass_detail vpd WHERE pass_type IN ('Flex','Motorcycle'));
SELECT DISTINCT valt.customer_account_id INTO #exc_trips FROM WSDOT_STAGE.util.v_acct_latest_trip valt WHERE valt.latest_trip >= convert(datetime2, '2018-01-01');
SELECT DISTINCT customer_account_id INTO #exc_accounts FROM (
	SELECT customer_account_id FROM #exc_discounts UNION
	SELECT customer_account_id FROM #exc_passes UNION
	SELECT customer_account_id FROM #exc_trips
);

SELECT  ca.customerAccountId 
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = ca.customerAccountId 
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = ca.customerAccountId AND tb.isCurrentActivity = 1
--  LEFT  JOIN WSDOT_STAGE.util.v_acct_latest_trip valt ON valt.customerAccountid = ca.customerAccountId 
 WHERE  vas.statusId = 'ACTIVE'	-- active status
   AND  tb.signValue <> -1		-- non-negative balance (0 or positive okay)
--   AND  valt.latest_trip < convert(date, '2018-01-01')	-- latest trip prior to 2018-01-01
--   AND  ca.customerAccountId NOT IN (SELECT DISTINCT vpd.customer_account_id FROM WSDOT_STAGE.util.v_pass_detail vpd WHERE pass_type IN ('Flex','Motorcycle'))	-- no flex or motorcycle pass
--   AND  ca.customerAccountId NOT IN (SELECT DISTINCT vad.customer_account_id FROM WSDOT_STAGE.util.v_account_discounts vad)	-- no discounts
 
SELECT DISTINCT pass_type FROM WSDOT_STAGE.util.v_pass_detail;

SELECT count(*) FROM fastlane.xact.F_Get_DiscountPlansByCustomerAccountId(8542);

/********** MULTI-FACILITY USE **********/
-- 2022-07-06 for Eric - how many users use multiple facilities in a day?
-- perform by transponder and license plate id (based on enforecement type?)

-- use datename to get weekday since datepart depends on DATEFIRST setting: https://stackoverflow.com/a/1113891
SELECT  WSDOT_STAGE.util.f_get_month(m.trip_date) AS trip_month
		,CASE WHEN datename(weekday, m.trip_date) IN (N'Saturday', N'Sunday') THEN 'weekend' ELSE 'weekday' END AS day_type
		--,m.facility_count 
		--,m.facilities 
		,m.roadway_count 
		,m.roadways 
		,count(m.daily_users) AS day_count
		,sum(m.daily_users) AS sum_daily_users
		,sum(m.sum_toll_charges) AS sum_toll_charges
		,sum(m.sum_toll_trips) AS sum_toll_trips
  FROM  (SELECT d.trip_date
				--,d.facility_count
				--,d.facilities
				,d.roadway_count
				,d.roadways
				,count(d.tag_or_plate_id) AS daily_users
				,sum(d.toll_charges) AS sum_toll_charges
				,sum(d.toll_trips) AS sum_toll_trips
		--  INTO  WSDOT_STAGE.tmp.multi_facility_2022
		--  INTO  WSDOT_STAGE.tmp.multi_roadway_2022
		  FROM  (SELECT f.trip_date
						--,f.customerAccountid 
						,f.tag_or_plate_id
						--,string_agg(f.facility,  ',') WITHIN GROUP (ORDER BY f.facility) AS facilities
						,string_agg(f.roadway,  ',') WITHIN GROUP (ORDER BY f.roadway) AS roadways
						--,count(f.facility) AS facility_count
						,count(f.roadway) AS roadway_count
						,sum(f.toll_charges) AS toll_charges
						,sum(f.toll_trips) AS toll_trips
				  FROM  (SELECT cast(tdf.laneTime AS date) AS trip_date
								--,tdf.customerAccountid 
								--,tdf.facility 
								,tdf.roadway
								,CASE WHEN tdf.enforcement_type = 'BYTAG' THEN concat('tag_',tdf.transponderId)
									  WHEN tdf.enforcement_type = 'BYLP' THEN concat('lp_',tdf.licensePlateId)
									  ELSE concat('none_',tdf.customerAccountid) END AS tag_or_plate_id
								,sum(tdf.latest_toll_amount) AS toll_charges
								,count(*) AS toll_trips
						  FROM  WSDOT_STAGE.util.v_trip_detail_full tdf
						 WHERE  tdf.laneTime >= convert(date, '2022-01-01')
						   AND  tdf.customerAccountid >= 1000	-- avoid system accounts?
						GROUP BY cast(tdf.laneTime AS date)
								--,tdf.customerAccountid 
								--,tdf.facility 
								,tdf.roadway  
								,CASE WHEN tdf.enforcement_type = 'BYTAG' THEN concat('tag_',tdf.transponderId)
									  WHEN tdf.enforcement_type = 'BYLP' THEN concat('lp_',tdf.licensePlateId)
									  ELSE concat('none_',tdf.customerAccountid) END
						) AS f 
				GROUP BY f.trip_date
						--,f.customerAccountid 
						,f.tag_or_plate_id
				) AS d
		GROUP BY d.trip_date
				--,d.facility_count
				--,d.facilities
				,d.roadway_count
				,d.roadways
		) AS m
GROUP BY WSDOT_STAGE.util.f_get_month(m.trip_date)
		,CASE WHEN datename(weekday, m.trip_date) IN (N'Saturday', N'Sunday') THEN 'weekend' ELSE 'weekday' END
		--,m.facility_count 
		--,m.facilities
		,m.roadway_count
		,m.roadways
ORDER BY WSDOT_STAGE.util.f_get_month(m.trip_date)
		,CASE WHEN datename(weekday, m.trip_date) IN (N'Saturday', N'Sunday') THEN 'weekend' ELSE 'weekday' END
		--,m.facility_count
		,m.roadway_count
		,sum(m.daily_users) DESC;

-- also stored intermediate result in tmp tables:
SELECT * FROM WSDOT_STAGE.tmp.multi_facility_2022 ORDER BY trip_date, facility_count, daily_users DESC;
SELECT * FROM WSDOT_STAGE.tmp.multi_roadway_2022 ORDER BY trip_date, roadway_count, daily_users DESC;
DROP TABLE WSDOT_STAGE.tmp.multi_facility_2022;
DROP TABLE WSDOT_STAGE.tmp.multi_roadway_2022;

/********** PENDING_DOL DISMISSALS **********/
-- 2022-07-13 look into trips sitting in PENDING DOL
SELECT * FROM WSDOT_STAGE.util.v_trip_detail_full tdf
 WHERE tdf.disposition_category = 'PENDING_DOL'
  AND tdf.laneTime BETWEEN convert(date,'2021-07-12') AND convert(date,'2021-07-15');

SELECT * FROM fastlane.xact.TollLedgerEntry tle WHERE tle.tollId = 221458510;

SELECT  *
  FROM  fastlane.xact.V_TransactionSummary vts
--  JOIN  fastlane.rtoll.IV_tollDetails itd ON itd.ledgerItemInfoId = vts.ledgerItemInfoId  
  JOIN  fastlane.lance.Trip t ON t.ledgerItemInfoId = vts.ledgerItemInfoId
  JOIN  fastlane.xact.ledger ld ON ld.transactionId = vts.transactionId 
  JOIN  fastlane.xact.Annotation ann ON ann.annotationid = ld.annotationid 
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = ann.entryReasonId
 WHERE  t.tripid = 221458510	--264095616
ORDER BY vts.postingDate;

/********** LEAKAGE SUMMARY FOR JEN **********/
-- 2022-07-14 provide summary of type 1 leaks by facility
SELECT  vld.trip_month 
		,vld.roadway 
		,vld.disposition_category 
		,vld.entry_reason 
		,sum(vld.sum_toll_amount) AS sum_toll_amount 
		,sum(vld.trip_count) AS trip_count
  FROM  WSDOT_STAGE.rpt.v_latest_disposition vld
 WHERE  vld.disposition_category = 'LEAK_I'
GROUP BY vld.trip_month 
		,vld.roadway 
		,vld.disposition_category 
		,vld.entry_reason 
ORDER BY vld.trip_month 
		,vld.roadway
		,sum(vld.trip_count) DESC;

SELECT  eer.shortCode AS entry_reason
		,eer.description
  FROM  fastlane.xact.EnuEntryReason eer
  JOIN  WSDOT_STAGE.wsdot.leakage_reason_codes lrc ON lrc.entry_reason_id = eer.entryReasonId 
 WHERE  lrc.leakage_cat_id = 1
ORDER BY eer.shortCode;

/********** PENDING_DOL DISMISSALS **********/
-- 2022-07-14 look into TollStatus for adding to disposition

SELECT  etsc.shortCode AS toll_status
		--,tdb.transaction_type 
		,ett.shortCode AS transaction_type
		,CASE WHEN vts.customerAccountid < 1000 THEN vts.customerAccountid ELSE NULL END AS system_account_id
		,count(*) AS cnt
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId  
  JOIN  fastlane.rtoll.TollStatus ts ON ts.tollId = t.tripId 
  JOIN  fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = ts.tollStatusCodeId
  --JOIN  WSDOT_STAGE.util.v_trip_detail_base tdb ON tdb.trip_id = t.tripId 
  JOIN  WSDOT_STAGE.util.v_all_trip_state vats ON vats.trip_id = t.tripId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = vats.last_trip_txn_id 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
 WHERE  tp.laneTime BETWEEN convert(datetime2, '2022-07-01') AND convert(datetime2, '2022-07-02')
GROUP BY etsc.shortCode
		,ett.shortCode
		,CASE WHEN vts.customerAccountid < 1000 THEN vts.customerAccountid ELSE NULL END;

-- also look at anomalies
SELECT  t.tripId 
		,tp.laneTime
		,etsc.shortCode AS toll_status
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId  
  JOIN  fastlane.rtoll.TollStatus ts ON ts.tollId = t.tripId 
  JOIN  fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = ts.tollStatusCodeId
 WHERE  tp.laneTime BETWEEN convert(datetime2, '2022-07-01') AND convert(datetime2, '2022-07-02')
   AND  etsc.shortCode IN ('DUPLICATE', 'REJECTED');

-- now look at toll ledger entry for same trips
SELECT  tle.tollid, vts.transactionId, vts.customerAccountid, tle.endAnnotationId, eer.shortCode, vts.postingDate 
  FROM  fastlane.xact.TollLedgerEntry tle 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  LEFT  JOIN  fastlane.xact.Annotation a ON a.annotationid = tle.endAnnotationId 
  LEFT  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
 WHERE  tle.tollId = 280197683; --279275062;

-- get toll status as of date
DECLARE @dtm date = getutcdate()
SELECT  t.tripId 
		,tp.laneTime
		,etsc.shortCode AS toll_status
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId  
  JOIN  fastlane.rtoll.TollStatus FOR SYSTEM_TIME AS OF @dtm ts ON ts.tollId = t.tripId 
  JOIN  fastlane.rtoll.EnuTollStatusCode etsc ON etsc.tollStatusCodeId = ts.tollStatusCodeId
 WHERE  t.tripId = 280197683;

/********** VEHICLE MAKE/MODEL INFO **********/
-- 2022-07-15 pull 2019 vehicle info for Standord students (via Eric)

-- get data
SELECT  concat(lpj.shortCode, '-', lp.lpnumber) AS state_lpn
		,lpca.customerAccountId AS customer_account_id
		,substring(ca.legacyAccountNumber, 1, len(ca.legacyAccountNumber) - 3) AS legacy_account_id 
		,vi.modelYear AS veh_model_year
		,upper(vi.make) AS veh_make
		,upper(vi.model) AS veh_model
		--,lpca.effectiveStartDate 
		--,lpca.effectiveExpirationDate 
--SELECT count(*)
  FROM  fastlane.vehicle.VehicleInfo vi 
  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = vi.licensePlateId 
  JOIN  fastlane.util.EnuLpJurisdiction lpj on lpj.lpJurisdictionId = lp.lpJurisdictionId
  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.licensePlateId = lp.licensePlateId AND lpca.effectiveStartDate < convert(date, '2020-01-01') AND lpca.effectiveExpirationDate > convert(date, '2020-01-01')
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = lpca.customerAccountId
-- WHERE  upper(vi.model) = 'UNKNOWN'
-- WHERE  left(ca.legacyAccountNumber, 1) = '0'
;

-- get suffixes from legacy accounts: -TS, -VP
SELECT  DISTINCT right(ca.legacyAccountNumber, 3)
  FROM  fastlane.util.CustomerAccount ca;

 -- check for no other -,P,S,T,V
SELECT  DISTINCT t.acct_id 
  FROM  (SELECT  substring(ca.legacyAccountNumber, 1, len(ca.legacyAccountNumber) - 3) AS acct_id FROM fastlane.util.CustomerAccount ca) t
WHERE t.acct_id LIKE '%V%';

/********** TRIP SUMMARY FOR EQUITY STUDY **********/
-- 2022-07-18 move to report arch when ready
-- pull SR99 from 2021-07-01 to 2022-06-30 (and other facilities?)
-- pull TNB from 2019-01-01 to 2019-12-31
-- get entry and exit gate (for I-405/SR167), account id, and other info
-- also consider vehicle year/make/model (find way to match transponders too? or at least accts with only one veh?)

SELECT  t.tollId AS trip_id
		,t.externalId AS external_trip_id
		,t.amount AS roadside_amount
		,t.axles
		,t.entryTime AS entry_datetime
		,t.exitTime AS exit_datetime
		,vfl.wsdot_facility AS facility
		,vfl.wsdot_roadway AS roadway
		,vflen.explazaCode AS entry_plaza
		,vfl.explazaCode AS exit_plaza
		,ertt.description AS roadside_toll_type
		,CASE WHEN t.transponderId IS NOT NULL THEN concat(convert(varchar, fa.kapschCode),'-',trn.transponderNumber) ELSE NULL END AS tag_transponder
		,CASE WHEN t.licensePlateId IS NOT NULL THEN concat(elj.shortCode, '-', lp.lpnumber) ELSE NULL END AS state_lp_number
		,ets.description AS transponder_status
		,t.tagRead AS tag_read_flag
		,t.occupancyCodeId AS occupancy_flag
		,vi.modelYear AS veh_model_year
		,upper(vi.make) AS veh_make
		,upper(vi.model) AS veh_model
		,vts.customerAccountid AS customer_account_id
		,vts.amount AS current_toll_amount
		,ett.description AS toll_type
		,epp.shortCode AS pricing_plan
  FROM  fastlane.rtoll.Toll t
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId 
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = t.roadsideTollType 
  JOIN  WSDOT_STAGE.util.v_all_trip_state vats ON vats.trip_id = t.TollID 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vats.last_trip_txn_id 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = vats.last_trip_txn_id AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId 
  LEFT  JOIN WSDOT_STAGE.wsdot.v_facility_lanes vflen ON vflen.roadGantryId = t.entryPlazaCodeId 
  LEFT  JOIN fastlane.vehicle.Transponder trn ON trn.transponderId = t.transponderId 
  LEFT  JOIN fastlane.vehicle.EnuTransponderStatus ets ON ets.transponderStatusId = t.transponderStatusId 
  LEFT  JOIN fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = trn.facilityAgencyId 
  LEFT  JOIN fastlane.util.LicensePlate lp ON lp.licensePlateId = t.licensePlateId 
  LEFT  JOIN fastlane.util.EnuLpJurisdiction elj ON elj.lpJurisdictionId = lp.lpJurisdictionId 
  LEFT  JOIN fastlane.vehicle.VehicleInfo vi ON vi.licensePlateId = t.licensePlateId 
-- WHERE  t.exitTime > convert(datetime2, '2021-07-01')
;

-- address data:
SELECT  ci.customerAccountId
		,ci.isCompany 
		,ci.address1 
		,ci.address2 
		,ci.city 
		,ci.state 
		,ci.postalCode 
		,ci.country 
		,ci.otherState 
  FROM  WSDOT_STAGE.util.v_acct_contact_info ci
 WHERE  ci.customerAccountId >= 1000	-- for testing, should probably remove later. what do we do for Verra Mobility?
;

/********** LEAKAGE REASON CODE COMPARE **********/
-- 2022-07-19 leakage code comparison for Ami

-- current summary of our codes
SELECT entry_reason_id, entry_reason_desc, er_short_code, leakage_code FROM WSDOT_STAGE.wsdot.v_leakage_reasons vlr ORDER BY vlr.er_short_code;

-- check for new codes:
SELECT * FROM fastlane.xact.EnuEntryReason eer ORDER BY eer.shortCode;

/********** OLD TAG ACCOUNTS/TRIPS **********/
-- 2022-07-19 old tags for DeeAnn (via Ami)
-- find if these tags are on an account or have any trips

-- data in _imports folder
DROP TABLE WSDOT_STAGE.tmp.old_tags_20220719;
CREATE TABLE WSDOT_STAGE.tmp.old_tags_20220719 (agency_code varchar(4), tag_id varchar(12));

SELECT  ot.agency_code 
		,ot.tag_id 
		,tr.transponderId
		,string_agg(tca.customerAccountId, ';') WITHIN GROUP (ORDER BY tca.customerAccountId) AS customerAccountIds
		,count(t.TollID) AS trips
		,max(t.exitTime) AS latest_trip
  FROM  fastlane.vehicle.Transponder tr
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = tr.facilityAgencyId 
  JOIN  WSDOT_STAGE.tmp.old_tags_20220719 ot ON ot.tag_id = tr.transponderNumber AND ot.agency_code = fa.kapschCode
  LEFT  JOIN fastlane.rtoll.Toll t ON t.transponderId = tr.transponderId
  LEFT  JOIN fastlane.rtoll.v_ah_TransponderCustomerAccount tca ON tca.transponderId = tr.transponderId 
GROUP BY ot.agency_code 
		,ot.tag_id 
		,tr.transponderId;

-- individual trips
SELECT  ot.agency_code 
		,ot.tag_id 
		,tr.transponderId
		,htca.customerAccountId
		,t.TollID
		,tle.ledgeraccountId 
		,epp.shortCode AS pricing_plan
		,ett.shortCode AS transaction_type
		,eer.shortCode 
		,t.exitTime
  FROM  fastlane.vehicle.Transponder tr
  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = tr.facilityAgencyId 
  JOIN  WSDOT_STAGE.tmp.old_tags_20220719 ot ON ot.tag_id = tr.transponderNumber AND ot.agency_code = fa.kapschCode
  JOIN  fastlane.rtoll.Toll t ON t.transponderId = tr.transponderId
  JOIN  WSDOT_STAGE.util.trip_state ts ON ts.trip_id = t.TollID 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_trip_txn_id
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId 
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  LEFT  JOIN  fastlane.xact.Annotation a ON a.annotationid = tle.endAnnotationId 
  LEFT  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
  LEFT  JOIN fastlane.rtoll.hTransponderCustomerAccount htca ON htca.transponderId = tr.transponderId AND t.exitTime BETWEEN htca.effectiveStartDate AND htca.effectiveEndDate 
ORDER BY t.exitTime;

/********** OLD TRIPS IN PENDING IR **********/
-- 2022-07-19 detail on old trips in PENDING_IR for Ami

SELECT * FROM WSDOT_STAGE.util.v_trip_detail_full vtdf WHERE vtdf.disposition_category = 'PENDING_IR' AND vtdf.laneTime BETWEEN convert(date,'2021-07-01') AND convert(date,'2021-10-01');

-- investigate individual trips
-- R-WRLPR (218113090, 230762485): often goes back to acct 121, still pending
-- S-MKSLP (224879932, 229137926): often goes back to acct 121, still pending
-- C-WSDR3 (222367125): never posted to an account, final state
-- S-LPIRV (219724602): posted to an account, then back to 121, then posted again, still pending
SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId 
 WHERE  tle.tollId = 219724602
ORDER BY vts.postingDate;

SELECT  *
  FROM  fastlane.xact.ledger l
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId 
 WHERE  l.ledgerItemInfoId = 248928788
ORDER BY vts.postingDate;

/********** COMMUNICATION TO PREPAID/PAY-AS-YOU-GO **********/
-- 2022-07-20 look at action taken on accounts (prepaid with negative balance and pay-as-you-go with negative balance over 30 days old) which received the communication

-- first find the communication - appears to be W_COM_1407, 64843 sent since July 1, but 53,372 on July 8
SELECT  ect.shortCode AS comm_code
		,ect.description 
		,count(*)
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.comm.EnuCommunicationType ect ON ect.communicationTypeId = c.communicationTypeId 
  --JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customerId = c.customerId 
 WHERE  c.createdAt BETWEEN convert(datetime2, '2022-07-08') AND convert(datetime2, '2022-07-09')
   AND  vca.account_type_id BETWEEN 2 AND 5 -- prepaid and pay-as-you-go
GROUP BY ect.shortCode 
		,ect.description 
ORDER BY count(*) DESC;

-- reminder of account types
SELECT * FROM fastlane.info.EnuAccountType eat;

-- summarize current balance by account type
-- re-run 2022-10-27, 2023-01-12
SELECT  cast(c.createdAt AS date) AS communication_date
		,ecc.shortCode AS comm_type
		,vca.acct_short_code 
		,CASE WHEN tb.signValue = -1 THEN 'negative' ELSE 'zero_or_positive' END AS balance_state
		,CASE WHEN tb.lastSignChangeDate > c.createdAt THEN 'yes' ELSE 'no' END AS sign_change_since_communication
		,count(*) AS acct_count
  FROM  fastlane.comm.Communication c
  JOIN  fastlane.comm.EnuCommunicationChannel ecc ON ecc.communicationChannelId = c.communicationChannelId 
  --JOIN  fastlane.comm.EnuCommunicationType ect ON ect.communicationTypeId = c.communicationTypeId 
  --JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customerId = c.customerId 
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = vca.customer_account_id AND tb.isCurrentActivity = 1
 WHERE  c.communicationTypeId = 1407
   AND  c.createdAt > convert(datetime2, '2022-07-01')
   --AND  tb.lastSignChangeDate > c.createdAt
GROUP BY cast(c.createdAt AS date)
		,ecc.shortCode 
		,vca.acct_short_code 
		,CASE WHEN tb.signValue = -1 THEN 'negative' ELSE 'zero_or_positive' END  
		,CASE WHEN tb.lastSignChangeDate > c.createdAt THEN 'yes' ELSE 'no' END
ORDER BY cast(c.createdAt AS date)
		,count(*) DESC
;

-- 2022-08-30 follow-up questions - how much was mail vs email? can we tell if it was returned?
SELECT  cast(c.createdAt AS date) AS comm_date
		,c.communicationChannelId
		,count(*)
--SELECT  c.communicationId
  FROM  fastlane.comm.Communication c 
--  JOIN  fastlane.comm.EnuCommunicationType ect ON ect.communicationTypeId = c.communicationTypeId 
--  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customerId = c.customerId
 WHERE  c.communicationTypeId = 1407
--   AND  vca.account_type_id BETWEEN 2 AND 5 -- prepaid and pay-as-you-go
GROUP BY cast(c.createdAt AS date)
		,c.communicationChannelId 
ORDER BY cast(c.createdAt AS date);

-- look at a single communication
-- then link to errors in email sending?
-- 92,733 sent
--SELECT  cast(c.createdAt AS date) AS comm_date
--		,ca.customerAccountId 
--		,rs.emailCommEventResPayloadStatusId 
SELECT  cast(c.createdAt AS date) AS comm_date
		,vca.acct_short_code AS account_type
		,ps.shortCode AS email_payload_status
		,ee.shortCode AS email_error_status
		,count(DISTINCT ca.customerAccountId) AS cust_acct_count
		,count(*) AS comm_cnt
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
  JOIN  fastlane.comm.EmailQueue eq ON eq.communicationId = c.communicationId 
  JOIN  fastlane.comm.EmailCommEventReq rq ON rq.emailQueueId = eq.emailQueueId 
  JOIN  fastlane.comm.EmailCommEventRes rs ON rs.emailCommEventReqId = rq.emailCommEventReqId 
  LEFT  JOIN fastlane.comm.EnuEmailCommEventError ee ON ee.emailCommEventErrorId = rs.emailCommEventErrorId 
  LEFT  JOIN fastlane.comm.EnuEmailCommEventResPayloadStatus ps ON ps.emailCommEventResPayloadStatusId = rs.emailCommEventResPayloadStatusId
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
 WHERE  c.communicationTypeId = 1407
-- WHERE  c.communicationId = 59489843; -- BETWEEN 59489843 AND 59489852;
GROUP BY cast(c.createdAt AS date)
		,vca.acct_short_code
		,ps.shortCode 
		,ee.shortCode 
ORDER BY cast(c.createdAt AS date)
		,count(*) DESC;
	
-- multiple communications to same account
SELECT  comm_cnt, count(*) AS cust_accts FROM (
SELECT  ca.customerAccountId 
		,count(*) AS comm_cnt
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
 WHERE  c.communicationTypeId = 1407
GROUP BY ca.customerAccountId
) d GROUP BY comm_cnt;

-- overall email result or error
SELECT  ps.shortCode AS email_payload_status
		,ee.shortCode AS email_error_status
		,count(*) AS comm_cnt
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.comm.EmailQueue eq ON eq.communicationId = c.communicationId 
  JOIN  fastlane.comm.EmailCommEventReq rq ON rq.emailQueueId = eq.emailQueueId 
  JOIN  fastlane.comm.EmailCommEventRes rs ON rs.emailCommEventReqId = rq.emailCommEventReqId 
  LEFT  JOIN fastlane.comm.EnuEmailCommEventError ee ON ee.emailCommEventErrorId = rs.emailCommEventErrorId 
  LEFT  JOIN fastlane.comm.EnuEmailCommEventResPayloadStatus ps ON ps.emailCommEventResPayloadStatusId = rs.emailCommEventResPayloadStatusId
 WHERE  c.communicationChannelId = 1	-- email
GROUP BY ps.shortCode 
		,ee.shortCode;

/********** IDENTIFY CORRECT CASE QUEUE **********/
-- 2022-07-20 *finally* look at linked cases to case queue

-- case queue links to cases via caseType
-- this sort of replicates case queue summary on the web portal, but 1015 is way lower and 1019-1021 are way higher

SELECT  cqct.caseQueueId 
		,cq.name AS case_queue_name
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  --JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId
  JOIN  (SELECT caseTypeId, min(caseQueueId) AS caseQueueId FROM fastlane.usercase.CaseQueueCaseType cqt GROUP BY caseTypeId) cqct ON cqct.caseTypeId = uc.caseTypeId
  JOIN  fastlane.usercase.CaseQueue cq ON cq.caseQueueId = cqct.caseQueueId 
 WHERE  ws.isClosing = 0	-- open cases only
   AND  cq.isDefunct = 0	-- not defunct
GROUP BY cqct.caseQueueId 
		,cq.name
ORDER BY cqct.caseQueueId;

-- use left join sub-query to keep zero-case queues in summary
SELECT  cq.caseQueueId 
		,cq.name AS case_queue_name
		,count(ucs.ucaseId) AS case_count
  FROM  fastlane.usercase.CaseQueue cq
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseQueueId = cq.caseQueueId
  --JOIN  (SELECT caseTypeId, min(caseQueueId) AS caseQueueId FROM fastlane.usercase.CaseQueueCaseType GROUP BY caseTypeId) cqct ON cqct.caseTypeId = cq.caseTypeId
  LEFT  JOIN (SELECT uc.ucaseId
  					,uc.caseTypeId
  					--,uc.workflowStateId
  				FROM fastlane.usercase.[UCase] uc 
  				JOIN fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId AND ws.isClosing = 0
  			 ) ucs ON ucs.caseTypeId = cqct.caseTypeId
 WHERE  cq.isDefunct = 0	-- not defunct
   --AND  ucs.workflowStateId = 1
GROUP BY cq.caseQueueId 
		,cq.name
ORDER BY cq.caseQueueId;

-- early case queues get entirely duplicated to later queues...
SELECT  q.queues
		,count(*) AS cases_with_dupes
  FROM  (SELECT uc.ucaseId 
				,string_agg(cqct.caseQueueId, ';') WITHIN GROUP (ORDER BY cqct.caseQueueId) AS queues
				,count(*) AS queue_count
		  FROM  fastlane.usercase.[UCase] uc
		  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId
		  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
		 WHERE  ws.isClosing = 0
		GROUP BY uc.ucaseId  
		HAVING count(*) > 1
		) q
GROUP BY q.queues;

-- this is likely because the case types themselves are duplicated across queues:
SELECT  cqct.caseTypeId 
		,ect.description 
		,string_agg(cqct.caseQueueId, ';') WITHIN GROUP (ORDER BY cqct.caseQueueId) AS queues
  FROM  fastlane.usercase.CaseQueueCaseType cqct
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = cqct.caseTypeId 
  JOIN  fastlane.usercase.CaseQueue cq ON cq.caseQueueId = cqct.caseQueueId
 WHERE  cq.isDefunct = 0	-- only non-defunct queues
GROUP BY cqct.caseTypeId
		,ect.description 
ORDER BY cqct.caseTypeId;

SELECT * FROM fastlane.usercase.EnuCaseType ect;
SELECT * FROM fastlane.usercase.CaseQueue cq WHERE isDefunct = 0;

-- also look at their proc fastlane.usercase.SP_CasesByCaseQueueId(@caseQueueId int, @offset INT, @fetch_next INT, @getCount bit)

-- now look to identify correct cases in queues 1015 and higher

----- final determinations
-- queues 1015 and higher serve a different purpose than the others and should not be included the monthly CSR case closure summary
-- queues 1019-1021 seem to use the usercase.CaseQueueTypeCaseSource table to limit queues certain sources
-- 1015 incoming document closed-processed - this is workflowStateId = 25 in queue 1015 (originated from queue 1012), and these are also closed (not open)
-- 1019 escalation - appears to be based on uc.caseEscalationId, links to usercase.EnuCaseEscalation - 1=normal,2=high,3=highest
--       alternatively based on CaseQueueTypeCaseAttribute, which only has one entry for queue 1019, caseAttributeId=0? doesn't check out yet
-- 1020 awaiting customer input - workflowStateId = 1, in queue 1020, caseSource 0, 2, or 6 (not others)
-- 1021 call center - caseSource 2 (from CaseQueueTypeCaseSource) in workflowid 1021 and NOT awaiting customer action

-- tables/columns of interest:
-- usercase.UCase.casePriorityId - not useful
-- usercase.UCase.caseEscalationId - appears to give queue 1019
-- usercase.UCase.workflowStateId - workflowStateId = 1 in queue 1020 gets close, but is always 8 cases higher than web portal
-- usercase.CaseAttribute.caseAttributeId - also gives escalation but does not match; unsure how it is used
-- usercase.CaseQueueCaseType.caseQueueId - with workflowStateId = 1 (closed), this is about 13 higher than web portal
-- usercase.UCase.workflowStateId - workflowStateId = 25 - gives about 51 higher than web portal, but correct number if checking only in queue 1015
-- usercase.UCase.caseSourceId - caseSourceId = 2 (phone) in queue 1021: 88, 7 higher than web portal
SELECT  --uc.casePriorityId 
		--uc.caseEscalationId 
		--uc.workflowStateId 
		--ca.caseAttributeId, cqtca.caseQueueId
		--cqct.caseQueueId
		uc.workflowStateId, cqct.caseQueueId, uc.caseSourceId
		--uc.workflowStateId, cqct.caseQueueId, cqct.caseTypeId
		--uc.caseSourceId, cqct.caseQueueId
		--uc.caseSourceId, cqct.caseQueueId, cqct.caseTypeId 
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  --JOIN  fastlane.usercase.CaseAttribute ca ON ca.ucaseId = uc.ucaseId 
  --JOIN  fastlane.usercase.CaseQueueTypeCaseAttribute cqtca ON cqtca.caseAttributeId = ca.caseAttributeId 
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
  JOIN  fastlane.usercase.CaseQueueTypeCaseSource cqtcs ON cqtcs.caseQueueId = cqct.caseQueueId AND cqtcs.caseSourceId = uc.caseSourceId 	-- only for queues 1019-1021
 WHERE  ws.isClosing = 0	-- open cases only
 --WHERE  ws.isClosing = 1
   --AND  uc.caseSourceId = 2	-- phone only
GROUP BY --uc.casePriorityId 
		--uc.caseEscalationId 
		--uc.workflowStateId 
		--ca.caseAttributeId, cqtca.caseQueueId
		--cqct.caseQueueId
		uc.workflowStateId, cqct.caseQueueId, uc.caseSourceId ORDER BY uc.workflowStateId, cqct.caseQueueId, uc.caseSourceId
		--uc.workflowStateId, cqct.caseQueueId, cqct.caseTypeId ORDER BY uc.workflowStateId, cqct.caseQueueId, cqct.caseTypeId
		--uc.caseSourceId, cqct.caseQueueId ORDER BY uc.caseSourceId, cqct.caseQueueId
		--uc.caseSourceId, cqct.caseQueueId, cqct.caseTypeId ORDER BY uc.caseSourceId, cqct.caseQueueId, cqct.caseTypeId
;

-- get case mis-matches in queues 1020 and 1021
SELECT  uc.ucaseId 
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
 WHERE  ws.isClosing = 0	-- open cases only
   --AND  uc.workflowStateId = 1 AND cqct.caseQueueId = 1020
   AND  uc.caseSourceId = 2 AND cqct.caseQueueId = 1021
;

SELECT  uc.ucaseId 
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
  JOIN  fastlane.usercase.CaseQueueTypeCaseAttribute cqtca ON cqtca.caseQueueId = cqct.caseQueueId 
  JOIN  fastlane.usercase.CaseAttribute ca ON ca.caseAttributeId = cqtca.caseAttributeId 
 WHERE  ws.isClosing = 0	-- open cases only
;

SELECT  cqct.caseQueueId, cqct.caseTypeId, ect.shortCode, ect.description 
  FROM  fastlane.usercase.CaseQueueCaseType cqct
  JOIN  fastlane.usercase.EnuCaseType ect ON ect.caseTypeId = cqct.caseTypeId
ORDER BY cqct.caseQueueId, cqct.caseTypeId;

-- single query to pull counts to match case queues page
--2022-07-27:  migrated to report arch
SELECT  cqct.caseQueueId
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId
--  JOIN  fastlane.usercase.CaseQueue cq ON cq.caseQueueId = cqct.caseQueueId 
 WHERE  ws.isClosing = 0	-- open cases only
-- AND  uc.isDefunct = 0	-- case not defunct (comment out; they appear to count open but defunct cases on that page)
   AND  cqct.caseQueueId < 1015
GROUP BY cqct.caseQueueId 
UNION ALL	-- queue 1015
SELECT  cqct.caseQueueId
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
 WHERE  ws.isClosing = 1	-- closed cases only
   AND  cqct.caseQueueId = 1015
   AND  uc.workflowStateId = 25
GROUP BY cqct.caseQueueId
UNION ALL	-- queues 1019-1021
SELECT  --uc.workflowStateId, cqct.caseQueueId, uc.caseSourceId
		CASE WHEN cqct.caseQueueId = 1019 AND uc.caseEscalationId > 1 THEN 1019
			 WHEN cqct.caseQueueId = 1020 AND uc.workflowStateId = 1 THEN 1020
		     WHEN cqct.caseQueueId = 1021 AND uc.workflowStateId = 0 AND uc.caseSourceId = 2 THEN 1021
		     ELSE NULL END AS caseQueueId
		,count(*) AS case_count
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.WorkflowState ws ON ws.workflowStateId = uc.workflowStateId
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId 
  JOIN  fastlane.usercase.CaseQueueTypeCaseSource cqtcs ON cqtcs.caseQueueId = cqct.caseQueueId AND cqtcs.caseSourceId = uc.caseSourceId 
 WHERE  ws.isClosing = 0	-- open cases only
GROUP BY CASE WHEN cqct.caseQueueId = 1019 AND uc.caseEscalationId > 1 THEN 1019
			 WHEN cqct.caseQueueId = 1020 AND uc.workflowStateId = 1 THEN 1020
		     WHEN cqct.caseQueueId = 1021 AND uc.workflowStateId = 0 AND uc.caseSourceId = 2 THEN 1021
		     ELSE NULL END
;

/********** PAYG NEGATIVE ACCOUNT BALANCE **********/
-- 2022-07-21 increase in pay-as-you-go negative account balance?
SELECT  vca.acct_type_desc 
		,count(*) AS acct_count
  FROM  fastlane.xact.TransactionsBalance tb 
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
 WHERE  tb.isCurrentActivity = 1
   AND  tb.signValue = -1
   --AND  datediff(day, tb.lastSignChangeDate, getdate()) >= 30
GROUP BY vca.acct_type_desc;

/********** NEGATIVE ACCOUNT BALANCE CONTACT INFO **********/
-- 2022-07-21 request from Tyler for account balances older than 15 days which are over $1k negative ($10k for pay-by-mail)

-- sanity check - only one current activity for each account? yes
SELECT  tb.customerAccountId 
		,count(*) AS acct_count
  FROM  fastlane.xact.TransactionsBalance tb 
 WHERE  tb.isCurrentActivity = 1
GROUP BY tb.customerAccountId 
HAVING count(*) > 1;

-- get data
SELECT  tb.customerAccountId 
		,tb.amount 
		,datediff(day, tb.lastSignChangeDate, getdate()) AS days_since_negative
		,vca.acct_type_desc
		,vca.postingDate AS account_creation_date
		,ci.*
--SELECT  vca.acct_type_desc
--		,count(*) AS acct_count
  FROM  fastlane.xact.TransactionsBalance tb 
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info ci ON ci.customerAccountId = tb.customerAccountId 
 WHERE  tb.isCurrentActivity = 1
   AND  tb.signValue = -1	-- balance is negative
   AND  datediff(day, tb.lastSignChangeDate, getdate()) >= 15	-- balance at least 15 days old
   AND  tb.customerAccountId >= 1000	-- not a system account
   --AND  tb.amount < -1000
   AND  ((vca.acct_short_code NOT LIKE 'PAYBYMAIL%' AND tb.amount < -1000) OR (vca.acct_short_code LIKE 'PAYBYMAIL%' AND tb.amount < -10000))
--GROUP BY vca.acct_type_desc
ORDER BY tb.amount
;

SELECT * FROM fastlane.info.EnuAccountType;

/********** TRIPS POSTED AFTER FY2022 END **********/
-- 2022-07-27 trips from June sent in July with amounts
-- start from usual trip posting summary
SELECT  cast(tp.laneTime AS date) AS trip_date
		,cast(t.created AS date) AS posting_date
		,vfl.wsdot_roadway AS roadway
		,ertt.shortCode AS roadside_toll_type
		,CASE WHEN d.tollId IS NOT NULL THEN 1 ELSE 0 END AS duplicate_trips	-- added to filter duplicates
		,sum(ta.amount) AS sum_roadside_toll_amount
		--,count(d.tollId) AS dup_count
		,count(*) AS trip_count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = tp.roadsideTollType
  JOIN  fastlane.lance.TripAmount ta ON ta.tripId = t.tripId -- added: get roadside toll amount
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  LEFT  JOIN (SELECT DISTINCT tollId FROM fastlane.xact.TollLedgerEntry WHERE ledgeraccountId IN (16,120)) d ON d.tollId = t.tripId 
 WHERE  tp.laneTime < convert(datetime2, '2022-07-01')	-- added: trips occuring prior to new fiscal year 
   AND  t.created >= convert(datetime2, '2022-07-01')	-- added: trips posted after start of new fiscal year 
GROUP BY cast(tp.laneTime AS date)
        ,cast(t.created AS date)
        ,vfl.wsdot_roadway
        ,ertt.shortCode
        ,CASE WHEN d.tollId IS NOT NULL THEN 1 ELSE 0 END	-- added: filter by duplicates rather than count
ORDER BY cast(t.created AS date)
		,cast(tp.laneTime AS date)
        ,count(*) DESC;


/********** NEGATIVE BALANCE COMMUNICATION PAYMENTS **********/
-- 2022-08-01 payment detail for Catherine Larson for customers who were sent negative balance communications

-- first investigate payments
-- p.fundingSourceId can be NULL, in fact most are, 31,508,994 payments vs 4,546,221 with a fundingId
-- also appear to be more than one paymentRequestQ for some payments
SELECT  *
  FROM  fastlane.payment.Payment p 
  LEFT  JOIN fastlane.payment.FundingSource fs ON fs.fundingSourceId = p.fundingSourceId
  LEFT  JOIN fastlane.util.CustomerAccount ca ON ca.customerId = fs.customerId 
  LEFT  JOIN fastlane.payment.PaymentRequestQ prq ON prq.paymentId = p.paymentId AND prq.isDefunct = 0
 WHERE  p.paymentId = 21473364
;

-- use view instead? pretty slow, but seems to be better link to accounts - 31,062,139 vs 31,062,179 non-null
SELECT  *
  FROM  fastlane.payment.IV_PaymentDetails ipd
-- WHERE  ipd.customerAccountid IS NOT NULL
 WHERE  ipd.paymentId = 21473364
;

-- account detail with payment summary
-- be careful about unapply and reapply - talk to Ami if needed, but using isCancelled should address this
-- prefixes:
--  A - auto
--  C - CSC
--  L - lockbox (mail)
--  M - mobile (eagle pass/global agility)
--  O - ocala
--  W - web
SELECT  cp.communication_date
		,cp.customerAccountId 
		,vca.acct_short_code 
		,tb.amount AS current_acct_balance
		,tb.signValue AS acct_balance_sign
		,tb.lastSignChangeDate AS last_sign_change_date
		,cp.payment_prefixes
		,cp.min_payment_date
		,cp.max_payment_date
		,cp.payments_count
		,cp.sum_payment_amount
		--,ipd.paymentId 
		--,ipd.postingDate AS payment_date
		--,ipd.amount AS payment_amount
		--,ipd.transactionDate 	-- appears to be UTC time of payment
		--,ipd.paymentShortCode 
		--,count(DISTINCT ca.customerAccountId) AS acct_count
		--,count(ipd.paymentId) AS payment_count
  FROM  (SELECT ca.customerAccountId
  				,cast(c.createdAt AS date) AS communication_date
  				,string_agg(ipd.prefix,  ',') WITHIN GROUP (ORDER BY ipd.prefix) AS payment_prefixes
				,min(convert(date, ipd.postingDate)) AS min_payment_date
				,max(convert(date, ipd.postingDate)) AS max_payment_date
				,count(ipd.amount) AS payments_count
				,sum(ipd.amount) AS sum_payment_amount
		   FROM fastlane.comm.Communication c
		   JOIN fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
		   LEFT JOIN fastlane.payment.IV_PaymentDetails ipd	ON ipd.customerAccountid = ca.customerAccountId AND ipd.postingDate > c.createdAt AND ipd.isCancelled = 0 AND ipd.isDefunct = 0
		  WHERE c.communicationTypeId = 1407
			AND c.createdAt > convert(datetime2, '2022-07-01')
		GROUP BY ca.customerAccountId
  				,cast(c.createdAt AS date)
		) cp
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = cp.customerAccountId AND tb.isCurrentActivity = 1
  LEFT JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = cp.customerAccountId
;

-- look at just payments made
SELECT  d.communication_date
		,vca.acct_short_code 
		--,d.earliest_payment
		,d.earliest_payment_no_auto
		,count(d.customerAccountId) AS acct_count
  FROM  (SELECT ca.customerAccountId
				,cast(c.createdAt AS date) AS communication_date
				--,ipd.prefix AS payment_prefix
				--,convert(date, ipd.postingDate) AS payment_date
				--,min(datediff(day, c.createdAt, ipd.postingDate)) AS min_days_to_pay
				--,min(CASE WHEN ipd.prefix = 'A' THEN NULL ELSE datediff(day, c.createdAt, ipd.postingDate) END) AS min_days_to_pay_no_auto
				,min(convert(date, ipd.postingDate)) AS earliest_payment
				,min(CASE WHEN ipd.prefix = 'A' THEN NULL ELSE convert(date, ipd.postingDate) END) AS earliest_payment_no_auto
				--,ipd.amount AS payment_amount
		  FROM  fastlane.comm.Communication c
		  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
		  LEFT  JOIN fastlane.payment.IV_PaymentDetails ipd	ON ipd.customerAccountid = ca.customerAccountId AND ipd.postingDate > c.createdAt AND ipd.isCancelled = 0 AND ipd.isDefunct = 0
		 WHERE  c.communicationTypeId = 1407
		   AND  c.createdAt > convert(datetime2, '2022-07-01')
		GROUP BY ca.customerAccountId
				,cast(c.createdAt AS date)
		) d
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = d.customerAccountId
GROUP BY d.communication_date
		,vca.acct_short_code 
		--,d.earliest_payment
		,d.earliest_payment_no_auto
ORDER BY d.communication_date
		,vca.acct_short_code 
		--,d.earliest_payment
		,d.earliest_payment_no_auto
;

-- 2022-08-05 now get count by statement anniversary date
-- in web BOS view it looks like account cycle date
-- looks like it is in util.CustomeraccountCycleDay 
SELECT * FROM fastlane.util.CustomeraccountCycleDay ccd WHERE ccd.customeraccountid IN (1185, 1251, 1327, 1524, 1961, 1143778, 440967, 9778038,2333190, 2333205, 9025172, 9263510, 9276406, 9260525, 8963252, 8955951, 2323572, 9289363, 9289697, 9297842, 8963252, 8955951, 2323572, 9289363, 9289697, 9297842);

-- summarize accounts by type and cycle date ("anniversary date")
SELECT  tb.customerAccountId,
		vca.acct_type_desc
		,ccd.CycleDay
		--,datediff(day, tb.lastSignChangeDate, getdate()) AS days_overdue
		--,count(*) AS acct_count
  FROM  fastlane.xact.TransactionsBalance tb 
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = tb.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId   
 WHERE  tb.isCurrentActivity = 1
   AND  tb.signValue = -1	-- balance is negative
   AND  datediff(day, tb.lastSignChangeDate, getdate()) >= 15	-- balance at least 15 days old
   --AND  ((vca.acct_short_code NOT LIKE 'ZBA%' AND datediff(day, tb.lastSignChangeDate, getdate()) >= 15) 
   -- OR   (vca.acct_short_code LIKE 'ZBA%' AND datediff(day, tb.lastSignChangeDate, getdate()) >= 30))
   AND  tb.customerAccountId >= 1000	-- not a system account
   --AND  vca.acct_short_code NOT LIKE 'Pay As You Go%'	-- exclude PAYG on detail
GROUP BY vca.acct_type_desc
		,ccd.CycleDay
ORDER BY vca.acct_type_desc
		,ccd.CycleDay;

-- 2022-09-07 update for Ami - get all accounts by cycle date
-- add status for active vs pending close, but also look into how to tell if an account is actually closed (e.g., acct 1143778)
-- use logic from F_AcctCond_IsCloseAccountCaseProcessed to get account closure status from IV_AccountTerminationState
-- workflow states appear to be NEW, WAITINGFORUSERINPUT, AWAITING_SETTLEMENT, CLOSEDRESOLVED, CLOSEDPROCESSED
-- also looks like V_AccountStatus is garbage? create new view to replace:
SELECT  --ccd.customerAccountId,
		vca.acct_type_desc
		,vas.status AS vas_status
		,CASE WHEN ats.Workflowstate IN ('CLOSEDPROCESSED', 'CLOSEDRESOLVED') THEN 'closed' -- closed states from F_AcctCond_IsCloseAccountCaseProcessed
			  WHEN ats.expectedStateTransitionDate > getdate() THEN 'future_close'
			  WHEN ats.Workflowstate IS NOT NULL THEN 'pending'
			  ELSE NULL END AS account_termination
		,ccd.CycleDay
		,count(*) AS acct_count
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = ca.customerAccountId 
--  JOIN  fastlane.info.V_AccountStatus vas ON vas.customerAccountId = ccd.customeraccountid 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ccd.customeraccountid
  LEFT  JOIN fastlane.util.IV_AccountTerminationState ats
		  ON ats.customeraccountid = ccd.customeraccountid
--		 AND ats.expectedStateTransitionDate <= getdate()
--		 AND ats.Workflowstate IN ('CLOSEDPROCESSED', 'CLOSEDRESOLVED')	
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ccd.customerAccountId 
 WHERE  ccd.customerAccountId >= 1000	-- not a system account
   AND  ca.isDefunct = 0		-- not defunct
--   AND  ccd.customeraccountid = 1143778
--   AND  ats.Workflowstate = 'NEW' --AND vas.status = 'Active' AND vca.acct_type_desc = 'Pay By Mail (Regular) Individual'
--   AND  vas.status = 'Pending Close' AND ats.Workflowstate IS NULL
GROUP BY vca.acct_type_desc
		,vas.status
		,CASE WHEN ats.Workflowstate IN ('CLOSEDPROCESSED', 'CLOSEDRESOLVED') THEN 'closed'
			  WHEN ats.expectedStateTransitionDate > getdate() THEN 'future_close'
			  WHEN ats.Workflowstate IS NOT NULL THEN 'pending'
			  ELSE NULL END
		,ccd.CycleDay
ORDER BY vca.acct_type_desc
		,ccd.CycleDay
		,vas.status;

-- potential account closed functions:
SELECT fastlane.util.F_AccountFlag_EligibleClosure(1143778);	-- 0
SELECT fastlane.util.F_AcctCond_IsCloseAccountCaseProcessed(1143778, getdate());	-- 1
SELECT fastlane.util.F_AcctCond_TerminationRequestedCompleted(1143778, getdate());	-- 0
SELECT * FROM fastlane.util.IV_AccountTerminationState WHERE customeraccountid = 1143778;

/********** ETAN CHANGED PROCEDURE USP_ACCOUNT_TYPES_ALL **********/
-- 2022-08-01 nightly run failed since it expected @asOf parameter

-- logs show:
-- Executed as user: G2G\ravery. Procedure or function 'USP_AccountTypesAll' expects parameter '@asOf', which was not supplied. [SQLSTATE 42000] 
--   (Error 201)  Warning: Null value is eliminated by an aggregate or other SET operation. [SQLSTATE 01003] (Message 8153).  The step failed.
-- The job failed.  The Job was invoked by Schedule 23 (NIGHTLY).  The last step to run was step 1 (run WSDOT reporting).

-- incidentally, a common warning also appears:
-- Executed as user: G2G\ravery. Warning: Null value is eliminated by an aggregate or other SET operation. [SQLSTATE 01003] (Message 8153).  The step succeeded.
-- however, according to https://stackoverflow.com/a/18719852 they may be ignored (expected behavior)

-- first summarize current data by type 
-- as-of 2022-07-25 12:18:12.460
SELECT  at2.account_type_id 
		,count(*) AS acct_count
  FROM  WSDOT_STAGE.util.account_type at2
GROUP BY at2.account_type_id 
ORDER BY at2.account_type_id;

-- now get historic data
CREATE TABLE #types (
customerAccountId INT not NULL PRIMARY KEY
,revenueTypeId INT not NULL
,customerTypeId INT not NULL
,primaryPoC char not NULL
,isPrepaid bit not NULL
,hasReplenishment bit not NULL
,isExpiryGreaterThan90Days bit not NULL

,isSystemGenerated bit not NULL
,accountTypeId INT not NULL
,shortcode VARCHAR(34) not NULL index IDX_#types_shortcode
);

-- can't test since session was killed, perhaps try early AM
DECLARE @asof datetime=convert(datetime, '2022-07-25 12:18:12.460')
INSERT #types
EXEC util.USP_AccountTypesAll  @asOf  =@asof;

SELECT  t.accountTypeId 
		,count(*) AS acct_count
  FROM  #types t
GROUP BY t.accountTypeId 
ORDER BY t.accountTypeId;

-- nevertheless, solution appears to be to add parameter to procedure call 
-- since using an as-of data may cause issues, simply pass NULL to use current time (procedure adjusted)
-- re-run just the account type update
EXEC WSDOT_STAGE.util.usp_update_account_types;

/********** OBJECT MODIFIED DATES **********/
-- 2022-08-01 look at ways to detect if an object (table, view, procedure) has been modified since we depended upon it
-- see https://stackoverflow.com/questions/85036/

-- USP_AccountTypesAll was modified on 2022-07-26 04:33:55.253!
USE WSDOT_STAGE;
USE fastlane;
SELECT  schema_name(schema_id) AS schema_name, name, type, type_desc, create_date, modify_date
SELECT  *
  FROM  sys.objects
-- WHERE  name LIKE '%V_AccountStatus%'
ORDER BY modify_date DESC;

sp_help fastlane.info.V_AccountStatus;

-- question: should we track table and/or view changes?

/********** RUNNING ON PROD? **********/
-- 2022-08-02 how to tell if we are running on production instance?

-- code always runs on WSXSFCLSP9002 (see history of runs; also matches @@SERVERNAME)
SELECT @@servername;

-- we should be on PRIMARY instance of AG1 (WSDOT_STAGE is writeable)
-- we should be on SECONDARY instance of AG3 (fastlane is read-only)
SELECT WSDOT_STAGE.util.f_get_ag_role('MCLUSTR9002AG1');	-- should be PRIMARY
SELECT WSDOT_STAGE.util.f_get_ag_role('MCLUSTR9002AG3');	-- should be SECONDARY, but is PRIMARY!

-- query with more details - migrated to report_arch with is_local = 1
SELECT  ag.name AS ag_name, ar.replica_server_name, ars.is_local, ars.role_desc, ars.operational_state_desc
  FROM  sys.dm_hadr_availability_replica_states ars
  JOIN  sys.availability_replicas ar ON ar.replica_id = ars.replica_id 
  JOIN  sys.availability_groups ag ON ars.group_id = ag.group_id
-- WHERE  is_local = 1
ORDER BY ag.name, ars.role_desc, ar.replica_server_name;

-- also look at history of job runs on WSXSFCLSP9002 vs WSXSFCLSP9003
-- 2022-08-03: does appear to run on both now, with secondary exiting in 11-12 seconds
SELECT  h.instance_id, h.step_id, h.step_name, h.sql_message_id, h.sql_severity, h.message, h.run_status, h.run_date, h.run_time, h.run_duration, h.server
  FROM  WSXSFCLSP9003.msdb.dbo.sysjobs j
  JOIN  WSXSFCLSP9003.msdb.dbo.sysjobhistory h ON h.job_id = j.job_id
 WHERE  j.name = 'WSDOT_REPORTING'
ORDER BY h.run_date DESC, h.instance_id;

-- look at jobs on other servers (WSXSFCLSP9003, WSXSFCLSP9002, WSXSFCLSR9002, WSXSFCLSR9003)
SELECT  j.name, j.enabled, j.version_number		-- job info
		,st.step_id, st.step_name, st.subsystem, st.command, st.last_run_outcome, st.last_run_duration, st.last_run_date, st.last_run_time	-- job step info
		,js.next_run_date, js.next_run_time, s.name AS schedule_name	-- job schedule & schedule info
		,hs.latest_run_date, hs.hist_msg_count	-- history info
--SELECT *
  FROM  WSXSFCLSP9003.msdb.dbo.sysjobs j
  JOIN  WSXSFCLSP9003.msdb.dbo.sysjobsteps st ON st.job_id = j.job_id
  LEFT  JOIN WSXSFCLSP9003.msdb.dbo.sysjobschedules js ON js.job_id = j.job_id
  LEFT  JOIN WSXSFCLSP9003.msdb.dbo.sysschedules s ON s.schedule_id = js.schedule_id
  LEFT  JOIN (SELECT h.job_id
					 ,max(h.run_date) AS latest_run_date
					 ,count(*) AS hist_msg_count
				FROM WSXSFCLSP9003.msdb.dbo.sysjobhistory h
				GROUP BY h.job_id
			) hs ON hs.job_id = j.job_id
 WHERE  j.name = 'WSDOT_REPORTING';

-- use dynamic sql to fill in server info
-- TODO: create util functions to encapsulate in report arch
DECLARE @server varchar(32) = 'WSXSFCLSP9002'
DECLARE @job varchar(32) = '''WSDOT_REPORTING'''
DECLARE @sql varchar(1000)
SET @sql = 'SELECT * FROM ' + @server + '.msdb.dbo.sysjobs j
  JOIN  ' + @server + '.msdb.dbo.sysjobsteps st ON st.job_id = j.job_id
  LEFT  JOIN ' + @server + '.msdb.dbo.sysjobschedules js ON js.job_id = j.job_id
  LEFT  JOIN ' + @server + '.msdb.dbo.sysschedules s ON s.schedule_id = js.schedule_id
  LEFT  JOIN (SELECT h.job_id
					 ,max(h.run_date) AS latest_run_date
					 ,count(*) AS hist_msg_count
				FROM ' + @server + '.msdb.dbo.sysjobhistory h
				GROUP BY h.job_id
			) hs ON hs.job_id = j.job_id
 WHERE  j.name = ' + @job
--SELECT @sql;
EXEC (@sql);

-- test build function and overall result
SELECT util.f_build_query_server_job_info('WSXSFCLSP9002', default);
EXEC WSDOT_STAGE.util.usp_get_server_job_info;

-- try to cheat getting empty result, but no way to create table with result from EXEC
DECLARE @sql nvarchar(max)
SELECT @sql = WSDOT_STAGE.util.f_build_query_server_job_detail(@@servername, '''WSDOT_REPORTING''') + ' AND 0=1'
SELECT column_ordinal, name, system_type_name FROM sys.dm_exec_describe_first_result_set(@sql,NULL,0);
EXEC (@sql);
--SELECT * INTO #tmp FROM (EXEC sp_execute (@sql));

SELECT * FROM #tmp;

-- testing sp_executesql
EXECUTE sp_executesql
N'SELECT * FROM util.v_ag_servers WHERE replica_server_name = @name',
N'@name nvarchar(256)',
@name = 'WSXSFCLSP9002';

/********** MODIFY RUNINFO TABLE **********/
-- 2022-08-03 add process and server fields to runinfo
-- DROP TABLE WSDOT_STAGE.tmp.runinfo;
SELECT  r.message
		,CASE WHEN r.message LIKE '%tracking%' THEN 'tracking_tables'
			  WHEN r.message LIKE '%daily%' THEN 'daily_reporting'
			  WHEN r.message LIKE '%weekly%' THEN 'weekly_reporting'
			  WHEN r.message LIKE '%month%' THEN 'monthly_reporting'
			  WHEN r.message LIKE '%complete%' THEN 'reporting_completed'
			END AS process_name
		,CASE WHEN r.system_time > convert(datetime2,'2022-08-03') THEN 'WSXSFCLSP9003' ELSE 'WSXSFCLSP9002' END AS server_name
		,r.system_time
  INTO  WSDOT_STAGE.tmp.runinfo
  FROM  WSDOT_STAGE.log.runinfo r 
ORDER BY r.system_time;

-- load data into new table
INSERT INTO WSDOT_STAGE.log.runinfo SELECT * FROM WSDOT_STAGE.tmp.runinfo r ORDER BY r.system_time;

-- verify result
SELECT * FROM WSDOT_STAGE.log.runinfo r ORDER BY r.system_time DESC;

/********** SR99 TOLL RATE INCREASE CHECK **********/
-- 2022-08-08 quick check for Harika, Joe, and Liz
--		see if SR99 reflects toll rate increases effective 2022-07-01 (since my forecasting summary didn't show an increase in average rate)
-- yes, rates do seem to be updated
SELECT  * FROM fastlane.lance.trip t 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.lance.tripAmount ta ON ta.tripId = t.tripId 
 WHERE  tp.roadGantryId IN (160128,160130,160132,160134,160118,160120,160122,160124,160126)
--   AND  tp.laneTime > convert(date, '2022-07-01');
   AND  tp.laneTime > convert(datetime2, '2022-07-06 15:30');

/*######################################################################*/
/*##########              TRAC TOLL EQUITY DATA               ##########*/
/*######################################################################*/
-- 2022-08-11 toll equity study data export for TRAC

-- write "slow" data to tmp table
SELECT  ts.trip_id
		,vts.customerAccountid AS customer_account_id
		,vts.amount AS current_toll_amount
		,vts.transactionTypeId
		,tle.pricingPlanId
  INTO  WSDOT_STAGE.tmp.toll_equity_trips
  FROM  WSDOT_STAGE.util.trip_state ts
  JOIN  fastlane.lance.Trip t ON t.tripId = ts.trip_id 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_trip_txn_id 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ts.last_trip_txn_id AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  tp.laneTime BETWEEN convert(datetime2, '2021-07-01') AND convert(datetime2, '2022-07-01');

-- 61,252,785 trips; 2,358,136 distinct accounts
SELECT  count(*) --DISTINCT customer_account_id)
  FROM  WSDOT_STAGE.tmp.toll_equity_trips;

-- add index for faster join
CREATE NONCLUSTERED INDEX ix_trip_id ON tmp.toll_equity_trips (trip_id);
CREATE NONCLUSTERED INDEX ix_customer_account_id ON tmp.toll_equity_trips (customer_account_id);
CREATE NONCLUSTERED INDEX ix_transactionTypeId ON tmp.toll_equity_trips (transactionTypeId);
CREATE NONCLUSTERED INDEX ix_pricingPlanId ON tmp.toll_equity_trips (pricingPlanId);

-- build table of trip info
DECLARE @key varchar(64) = (SELECT hashkey FROM WSDOT_STAGE.wsdot.hashkeys WHERE id = 1)
SELECT  t.tollId AS trip_id
		,t.externalId AS external_trip_id
		,t.amount AS roadside_amount
		,t.axles
		,t.entryTime AS entry_datetime
		,t.exitTime AS exit_datetime
		,t.roadsideTollType
		,vfl.wsdot_facility AS facility
		,vfl.wsdot_roadway AS roadway
		,vflen.explazaCode AS entry_plaza
		,vfl.explazaCode AS exit_plaza
		--,ertt.description AS roadside_toll_type
		,CASE WHEN t.transponderId IS NOT NULL 
			THEN hashbytes('SHA2_256', concat(@key, convert(varchar, fa.kapschCode), '-', trn.transponderNumber))
			ELSE NULL END AS hashed_tag_transponder
		,CASE WHEN t.licensePlateId IS NOT NULL
			THEN hashbytes('SHA2_256', concat(@key, elj.shortCode, '-', lp.lpnumber))
			ELSE NULL END AS hashed_state_lp_number
		,t.transponderStatusId 
		--,ets.description AS transponder_status
		,t.tagRead AS tag_read_flag
		,t.occupancyCodeId AS occupancy_flag
		,vi.modelYear AS veh_model_year
		,WSDOT_STAGE.util.f_csv_sanitize(upper(vi.make),';') AS veh_make
		,WSDOT_STAGE.util.f_csv_sanitize(upper(vi.model),';') AS veh_model
		--,hashbytes('SHA2_256', concat(@key, te.customer_account_id)) AS hashed_customer_account_id
		--,te.current_toll_amount
		--,ett.description AS toll_type
		--,epp.shortCode AS pricing_plan
  INTO  WSDOT_STAGE.tmp.toll_equity_trip_info
  FROM  fastlane.rtoll.Toll t
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = t.exitPlazaCodeId 
--  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = t.roadsideTollType 
--  JOIN  WSDOT_STAGE.tmp.toll_equity_trips te ON te.trip_id = t.TollID 
--  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = te.transactionTypeId 
--  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = te.pricingPlanId 
  LEFT  JOIN WSDOT_STAGE.wsdot.v_facility_lanes vflen ON vflen.roadGantryId = t.entryPlazaCodeId 
  LEFT  JOIN fastlane.vehicle.Transponder trn ON trn.transponderId = t.transponderId 
--  LEFT  JOIN fastlane.vehicle.EnuTransponderStatus ets ON ets.transponderStatusId = t.transponderStatusId 
  LEFT  JOIN fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = trn.facilityAgencyId 
  LEFT  JOIN fastlane.util.LicensePlate lp ON lp.licensePlateId = t.licensePlateId 
  LEFT  JOIN fastlane.util.EnuLpJurisdiction elj ON elj.lpJurisdictionId = lp.lpJurisdictionId 
  LEFT  JOIN fastlane.vehicle.VehicleInfo vi ON vi.licensePlateId = t.licensePlateId 
 WHERE  t.exitTime BETWEEN convert(datetime2, '2021-07-01') AND convert(datetime2, '2022-07-01')
END;

-- add index on trip_id for faster join
CREATE NONCLUSTERED INDEX ix_trip_id ON tmp.toll_equity_trip_info (trip_id);
CREATE NONCLUSTERED INDEX ix_roadway ON tmp.toll_equity_trip_info (roadway);
CREATE NONCLUSTERED INDEX ix_facility ON tmp.toll_equity_trip_info (facility);
CREATE NONCLUSTERED INDEX ix_exit_datetime ON tmp.toll_equity_trip_info (exit_datetime);
CREATE NONCLUSTERED INDEX ix_roadsideTollType ON tmp.toll_equity_trip_info (roadsideTollType);
CREATE NONCLUSTERED INDEX ix_transponderStatusId ON tmp.toll_equity_trip_info (transponderStatusId);

SELECT count(*) FROM WSDOT_STAGE.tmp.toll_equity_trip_info;

-- correct issues in data
-- find any commas or newlines (char 10 & 13) in make or model
--SELECT  ti.veh_make, ti.veh_model, WSDOT_STAGE.util.f_csv_sanitize(ti.veh_make,';'), WSDOT_STAGE.util.f_csv_sanitize(ti.veh_model,';')
SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.toll_equity_trip_info ti
-- WHERE  ti.veh_make LIKE '%;%';
 WHERE  ti.veh_make LIKE '%' + char(10) + '%'
;

/*
 * facility	
167N	1651552
167S	2078545
405BL	5652071
405LB	6809367
520E	9399036
520W	10204157
99N		6386517
99S		6814551
TNB		12,256,989
 */
SELECT facility, count(*) FROM WSDOT_STAGE.tmp.toll_equity_trip_info ti GROUP BY facility ORDER BY facility;

-- fix data (and adjust query so it doesn't happen again)
UPDATE  WSDOT_STAGE.tmp.toll_equity_trip_info
   SET  veh_make = WSDOT_STAGE.util.f_csv_sanitize(veh_make,';')
   		,veh_model = WSDOT_STAGE.util.f_csv_sanitize(veh_model,';')

-- pull data by facility
--DECLARE @key varchar(64) = (SELECT hashkey FROM WSDOT_STAGE.wsdot.hashkeys WHERE id = 1)
SELECT  ti.*
		,ertt.description AS roadside_toll_type_desc
		,ets.description AS transponder_status
		,hashbytes('SHA2_256', concat(k.hashkey, te.customer_account_id)) AS hashed_customer_account_id
		,te.current_toll_amount
		,ett.description AS toll_type
		,epp.shortCode AS pricing_plan
  FROM  WSDOT_STAGE.tmp.toll_equity_trip_info ti 
  JOIN  WSDOT_STAGE.tmp.toll_equity_trips te ON te.trip_id = ti.trip_id 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = te.transactionTypeId 
  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = te.pricingPlanId 
  JOIN  fastlane.rtoll.EnuRoadsideTollType ertt ON ertt.roadsideTollType = ti.roadsideTollType 
  LEFT  JOIN fastlane.vehicle.EnuTransponderStatus ets ON ets.transponderStatusId = ti.transponderStatusId
  JOIN  (SELECT hashkey FROM WSDOT_STAGE.wsdot.hashkeys WHERE id = 1) k ON 1 = 1
 WHERE  ti.facility = '167S'		-- I405, SR167, SR520, SR99, TNB
;		--167N, -167S, -405BL, -405LB, -520E, -520W, -99N, -99S, -TNB

-- look at amounts
-- no positive roadside, 740962 positive current
SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.toll_equity_trip_info ti 
  JOIN  WSDOT_STAGE.tmp.toll_equity_trips te ON te.trip_id = ti.trip_id 
-- WHERE  ti.roadside_amount > 0
 WHERE  te.current_toll_amount > 0
   AND  te.current_toll_amount + ti.roadside_amount NOT IN (-2, 0, 2)
;

----- address processing
-- is there any address linked to from more than one account? 216,905 + 83,350 NULL addresses
-- how about matching addresses? 648,684 addresses have the same binary checksum; 653,475 by hashbytes
SELECT  sum(add_count) FROM (
SELECT  ci.postal_address_id 
		,binary_checksum(ci.address1,ci.address2,ci.city,ci.state,ci.postalCode,ci.country) AS chk
		,hashbytes('SHA2_256', concat(ci.address1,ci.address2,ci.city,ci.state,ci.postalCode,ci.country)) AS hash
		--,count(*) AS add_count
  FROM  (SELECT DISTINCT tet.customer_account_id FROM WSDOT_STAGE.tmp.toll_equity_trips tet) c
  JOIN  WSDOT_STAGE.util.v_acct_contact_info ci ON ci.customerAccountId = c.customer_account_id
 WHERE  postal_address_id IS NOT NULL		-- 83,350 NULL addresses
GROUP BY --ci.postal_address_id
		--binary_checksum(ci.address1,ci.address2,ci.city,ci.state,ci.postalCode,ci.country)
		hashbytes('SHA2_256', concat(ci.address1,ci.address2,ci.city,ci.state,ci.postalCode,ci.country))
HAVING count(*) > 1
) d;
  
-- address data investigation
-- create function to trim and hash address fields
-- get address updated date
-- 2,358,118 non-system accounts
-- 2,274,631 that have non-null addresses in the US
-- commas and carriage returns (char(10), char(13): 
--   address1	3821, 3, 0
--   address2	 135, 0, 0
--   city 		  13, 0, 0
-- if csv is exported quoting strings, it is okay to leave as-is
-- carriage returns seem to only be in international addresses
-- otherState appears to mostly be NULL and otherwise it is unreliable; don't use
-- country should exclude canada and international but accept US and other 
-- mexico has one address, should be missouri instead of monterrey
-- pull 'bad' addresses with comma in city (allow in address), returns, and no country id
SELECT  c.customer_account_id, ci.postal_address_id, ci.address_source, ci.postal_address_updated_at 
		,ci.address1, ci.address2, ci.city, ci.state, ci.postalCode, ci.country 
--SELECT  count(*)
  FROM  (SELECT DISTINCT tet.customer_account_id FROM WSDOT_STAGE.tmp.toll_equity_trips tet) c
  JOIN  WSDOT_STAGE.util.v_acct_contact_info ci ON ci.customerAccountId = c.customer_account_id
 WHERE  c.customer_account_id >= 1000
   AND  ci.postal_address_id IS NOT NULL
   AND  (ci.address1 LIKE '%' + char(10) + '%' OR ci.city LIKE '%,%' OR ci.country_id IN (0,3))
--   AND  ci.country_id < 2	-- not in Canada, Mexico, or International
--   AND  ci.address1  LIKE '%' + char(10) + '%'
;

-- process 2,274,632 non-null addresses in the US:
-- do not worry about sanitizing fields - leave commas in address and city to detect fixes
--   also, only international addresses appear to have line breaks
-- uppercase address fields to also reduce duplicates
-- coalesce NULLs in address 2 into empty strings to avoid hashes with differing NULL vs blank fields
-- to avoid address edits during pull, pull all data into temp table
SELECT  c.customer_account_id
		,ci.postal_address_id
		,ci.postal_address_updated_at 
		,hashbytes('SHA2_256', upper(concat(ci.address1,ci.address2,ci.city,ci.state,ci.postalCode,ci.country))) AS address_hash
		,upper(ci.address1) AS address1
		,upper(coalesce(ci.address2,'')) AS address2
		,upper(ci.city) AS city
		,upper(ci.state) AS state
		,upper(ci.postalCode) AS postal_code
		,upper(ci.country) AS country
--SELECT  count(*)
  INTO  WSDOT_STAGE.tmp.address_info
  FROM  (SELECT DISTINCT tet.customer_account_id FROM WSDOT_STAGE.tmp.toll_equity_trips tet) c
  JOIN  WSDOT_STAGE.util.v_acct_contact_info ci ON ci.customerAccountId = c.customer_account_id
 WHERE  c.customer_account_id >= 1000		-- only customer accounts
   AND  ci.postal_address_id IS NOT NULL	-- only those with an address
   AND  ci.country_id < 2	-- not in Canada, Mexico, or International (but try to code unknown country)
;

SELECT count(*) FROM WSDOT_STAGE.tmp.address_info2 ai WHERE address2 IS NULL;

-- investigate distinct address data for geocoding
-- difference could stem from trailing spaces which hashbytes removes?
-- 1,880,269 vs 1,879,163
-- upper: 1,865,179 vs 1,879,164
-- upper with NULLs removed: 1,865,177 vs 1,865,172, 1,865,180
SELECT count(DISTINCT address_hash) FROM WSDOT_STAGE.tmp.address_info ai;
SELECT count(*) FROM (
SELECT  DISTINCT ai.address_hash, ai.address1, ai.address2, ai.city, ai.state, ai.postal_code, ai.country
  FROM  WSDOT_STAGE.tmp.address_info ai) d;

-- check variation - 680,352 records having hash with more than one address
-- differences mostly stem from some address2 being NULL vs ''
-- conclusion: use the hash, it's good!
-- after NULL fix, only 3 hashes with 2 addresses each have an issue with multiple addresses for the unique hash
SELECT  * FROM (
SELECT  d.address_hash, d.cnt, ai2.address1, ai2.address2, ai2.city, ai2.state, ai2.postal_code, ai2.country, count(*) AS add_cnt
--SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.address_info ai2
  JOIN  (SELECT  address_hash, count(*) AS cnt FROM WSDOT_STAGE.tmp.address_info ai
		GROUP BY address_hash HAVING count(*) > 1
		) d ON d.address_hash = ai2.address_hash
-- WHERE  cnt > 50
GROUP BY d.address_hash, d.cnt, ai2.address1, ai2.address2, ai2.city, ai2.state, ai2.postal_code, ai2.country
) s WHERE s.cnt <> s.add_cnt
ORDER BY s.address_hash, s.add_cnt;

SELECT  address_hash, ai.address1, ai.address2, ai.city, ai.state, ai.postal_code, ai.country
  FROM  WSDOT_STAGE.tmp.address_info2 ai
GROUP BY address_hash, ai.address1, ai.address2, ai.city, ai.state, ai.postal_code, ai.country;

SELECT  address_hash, count(*) FROM WSDOT_STAGE.tmp.address_info2 ai GROUP BY address_hash HAVING count(*) > 1 ORDER BY count(*) DESC;

SELECT  hash, count(*) FROM WSDOT_STAGE.tmp.address_info ai GROUP BY hash HAVING count(*) > 1 ORDER BY count(*) DESC;

SELECT hash, ai.address1, ai.address2, ai.city, ai.state, ai.postalCode, ai.country
FROM WSDOT_STAGE.tmp.address_info ai
WHERE hash = 0x920CE4A26082A2562B73AB7B6DA0A35910A81BF65B741AA0748176F5D3E77620
   OR hash = 0x80CBC5F44ECA84B8AB792CA768854B1E081834919A192716EF04B97FB1F6B486;

SELECT address_hash, ai.address1, ai.address2, ai.city, ai.state, ai.postal_code, ai.country
FROM WSDOT_STAGE.tmp.address_info2 ai
WHERE address_hash = 0xB162342A788095B7E56F03A325038C96871141DC49EF88FB04974CF3690F8B84;

SELECT hash, count(*) FROM WSDOT_STAGE.tmp.address_info ai WHERE address1 LIKE '%Box 21508%' GROUP BY hash;

DROP TABLE WSDOT_STAGE.tmp.address_info2;

-- insert data into system-versioned address hash table
INSERT INTO WSDOT_STAGE.util.account_postal_address_hash
SELECT  ai.customer_account_id 
		,ai.postal_address_id 
		,ai.postal_address_updated_at 
		,ai.address_hash
  FROM  WSDOT_STAGE.tmp.address_info ai;
 
SELECT count(*) FROM WSDOT_STAGE.util.account_postal_address_hash ah;

-- then pull addresses with distinct hash (smaller set) for geocod.io
-- manually review the three duplicate hashes from above
-- need to concat address1 and address2 for geocodio
-- also remove unknown country
SELECT  DISTINCT ai.address_hash
		,concat(ai.address1, ' ', ai.address2) AS address
		,ai.city
		,ai.state
		,ai.postal_code
		,CASE WHEN ai.country = 'UNKNOWN / NON APPLICABLE' THEN '' ELSE ai.country END AS country
  INTO  WSDOT_STAGE.tmp.final_address
  FROM  WSDOT_STAGE.tmp.address_info ai
ORDER BY address_hash;

-- import hash, coordinates, and desired identifiers into table
-- first create temp table to store results

-- DROP TABLE WSDOT_STAGE.tmp.geocodio_results;
-- NOTE: lost most work from system crash on 2022-08-22 - see general notes
-- for future import, set delimiter to " and change escape char to | instead of \
-- also set empty strings to NULL
CREATE TABLE WSDOT_STAGE.tmp.geocodio_results (
	address_hash varbinary(MAX) NOT NULL
	,address varchar(256)
	,city varchar(100)
	,state varchar(10)				-- 13
	,postal_code varchar(100)		-- 10
	,country varchar(100)
	,latitude varchar(100)			-- numeric(9,6)
	,longitude varchar(100)			-- numeric(9,6)
	,accuracy_score varchar(100)	-- numeric(4,3)
	,accuracy_type varchar(100)		-- 32
	,gc_number varchar(100)			-- 64, 32 after removing 'Could not geocode address...'
	,gc_street varchar(100)
	,gc_unit_type varchar(100)		-- 16
	,gc_unit_number varchar(100)	-- 64
	,gc_city varchar(100)			-- 96
	,gc_state varchar(100)			-- 16
	,gc_county varchar(100)			-- 96
	,gc_zip varchar(100)			-- 16
	,gc_country varchar(100)		-- 16
	,gc_source varchar(100)
	,census_year varchar(100)		-- smallint
	,fips_state varchar(100)		-- 2
	,fips_county varchar(100)		-- 5
	,place_name varchar(128)
	,fips_place varchar(100)		-- 7
	,census_tract_code varchar(100)	-- 6
	,census_block_code varchar(100)	-- 4
	,census_block_group varchar(100)	-- 1
	,fips_block varchar(100)		-- 15
	,fips_tract varchar(100)		-- 11
	,metro_micro_stat_area_name varchar(128)
	,metro_micro_stat_area_code varchar(100)	-- 5
	,metro_micro_stat_area_type varchar(64)		-- 32
	,combined_stat_area_name varchar(128)
	,combined_stat_area_code varchar(100)	-- 3
	,metro_division_area_name varchar(128)
	,metro_division_area_code varchar(100)	-- 5
);

SELECT count(*) FROM WSDOT_STAGE.tmp.geocodio_results;
SELECT max(len(metro_division_area_code)) FROM WSDOT_STAGE.tmp.geocodio_results;
SELECT gr.gc_number FROM WSDOT_STAGE.tmp.geocodio_results gr WHERE len(gr.gc_number) > 16;

-- lost work on fixing errors - there were five rows with '\' escape character, only one required fixing

-- insert statements for census data

-- fips_places
INSERT INTO WSDOT_STAGE.wsdot.census_fips_places
SELECT  DISTINCT gr.fips_place 
		,cast(gr.census_year AS smallint) AS census_year 
		,gr.place_name 
  FROM  WSDOT_STAGE.tmp.geocodio_results gr
 WHERE  gr.fips_place <> ''
ORDER BY gr.fips_place;

-- metro/micro statistical areas
INSERT INTO WSDOT_STAGE.wsdot.census_metro_micro_statistical_areas
SELECT  DISTINCT gr.metro_micro_stat_area_code AS code 
		,cast(gr.census_year AS smallint) AS census_year
		,gr.metro_micro_stat_area_name AS name
		,gr.metro_micro_stat_area_type AS area_type
  FROM  WSDOT_STAGE.tmp.geocodio_results gr
 WHERE  gr.metro_micro_stat_area_code <> ''
ORDER BY gr.metro_micro_stat_area_code;

-- combined statistical areas
INSERT INTO WSDOT_STAGE.wsdot.census_combined_statistical_areas
SELECT  DISTINCT gr.combined_stat_area_code AS code 
		,cast(gr.census_year AS smallint) AS census_year
		,gr.combined_stat_area_name AS name
  FROM  WSDOT_STAGE.tmp.geocodio_results gr
 WHERE  gr.combined_stat_area_code <> ''
ORDER BY gr.combined_stat_area_code;

-- metropolitan division areas
INSERT INTO WSDOT_STAGE.wsdot.census_metropolitan_division_areas
SELECT  DISTINCT gr.metro_division_area_code AS code 
		,cast(gr.census_year AS smallint) AS census_year
		,gr.metro_division_area_name AS name
  FROM  WSDOT_STAGE.tmp.geocodio_results gr
 WHERE  gr.metro_division_area_code <> ''
ORDER BY gr.metro_division_area_code;

-- insert data into final geocoding results table
-- convert 'varcharized' binary back to binary - see https://stackoverflow.com/a/40776527
-- get address data from original table to avoid issues with missing '\'
-- convert empty strings to NULL and convert others to proper data type
INSERT INTO WSDOT_STAGE.wsdot.geocoding_results 
SELECT  fa.address_hash
		,0 AS provider_id
		,convert(date, '2022-08-20') AS lookup_date
		,fa.address
		,fa.city
		,fa.state
		,fa.postal_code
		,fa.country
		,CASE WHEN gr.latitude = '0' THEN NULL ELSE cast(gr.latitude AS numeric(9,6)) END AS latitude 
		,CASE WHEN gr.longitude = '0' THEN NULL ELSE cast(gr.longitude AS numeric(9,6)) END AS longitude 
		,cast(accuracy_score AS numeric(4,3)) AS accuracy_score 
		,CASE WHEN gr.accuracy_type = '' THEN 0 ELSE gat.accuracy_type_id END AS accuracy_type_id
		,CASE WHEN gr.gc_number = 'Could not geocode address. Postal code or city required.' THEN NULL 
			  WHEN gr.gc_number = '' THEN NULL ELSE gr.gc_number END AS gc_number
		,CASE WHEN gr.gc_street = '' THEN NULL ELSE gr.gc_street END AS gc_street
		,CASE WHEN gr.gc_unit_type = '' THEN NULL ELSE gr.gc_unit_type END AS gc_unit_type
		,CASE WHEN gr.gc_unit_number = '' THEN NULL ELSE gr.gc_unit_number END AS gc_unit_number
		,CASE WHEN gr.gc_city = '' THEN NULL ELSE gr.gc_city END AS gc_city
		,CASE WHEN gr.gc_state = '' THEN NULL ELSE gr.gc_state END AS gc_state
		,CASE WHEN gr.gc_county = '' THEN NULL ELSE gr.gc_county END AS gc_county
		,CASE WHEN gr.gc_zip = '' THEN NULL ELSE gr.gc_zip END AS gc_zip
		,CASE WHEN gr.gc_country = '' THEN NULL ELSE gr.gc_country END AS gc_country
		,CASE WHEN gr.gc_source = '' THEN NULL ELSE gr.gc_source END AS gc_source
		,CASE WHEN gr.census_year = '' THEN NULL ELSE cast(gr.census_year AS smallint) END AS census_year
		,CASE WHEN gr.fips_state = '' THEN NULL ELSE gr.fips_state END AS fips_state
		,CASE WHEN gr.fips_county = '' THEN NULL ELSE gr.fips_county END AS fips_county
		,CASE WHEN gr.fips_place = '' THEN NULL ELSE gr.fips_place END AS fips_place
		,CASE WHEN gr.census_tract_code = '' THEN NULL ELSE gr.census_tract_code END AS census_tract_code
		,CASE WHEN gr.census_block_code = '' THEN NULL ELSE gr.census_block_code END AS census_block_code
		,CASE WHEN gr.census_block_group = '' THEN NULL ELSE gr.census_block_group END AS census_block_group
		,CASE WHEN gr.fips_block = '' THEN NULL ELSE gr.fips_block END AS fips_block
		,CASE WHEN gr.fips_tract = '' THEN NULL ELSE gr.fips_tract END AS fips_tract
		,CASE WHEN gr.metro_micro_stat_area_code = '' THEN NULL ELSE gr.metro_micro_stat_area_code END AS metro_micro_statistical_area_code
		,CASE WHEN gr.combined_stat_area_code = '' THEN NULL ELSE gr.combined_stat_area_code END AS combined_statistical_area_code
		,CASE WHEN gr.metro_division_area_code = '' THEN NULL ELSE gr.metro_division_area_code END AS metropolitan_division_area_code
--SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.final_address fa 
  JOIN  WSDOT_STAGE.tmp.geocodio_results gr ON convert(varbinary(64), cast(gr.address_hash AS varchar(100)), 1) = fa.address_hash 
  LEFT  JOIN WSDOT_STAGE.wsdot.geocoding_accuracy_types gat ON gat.short_code = gr.accuracy_type;

-- join with census data
SELECT  gp.name AS provider_name
		,gr.*
		,gat.short_code AS accuracy_type
		,cfp.place_name 
		,msa.name AS metro_micro_statistical_area_name 
		,msa.area_type AS metro_micro_statistical_area_type 
		,csa.name AS combined_statistical_area_name 
		,mda.name AS metropolitan_division_area_name 
--SELECT  count(*)
  FROM  WSDOT_STAGE.wsdot.geocoding_results gr 
  JOIN  WSDOT_STAGE.wsdot.geocoding_providers gp ON gp.provider_id = gr.provider_id 
  JOIN  WSDOT_STAGE.wsdot.geocoding_accuracy_types gat ON gat.accuracy_type_id = gr.accuracy_type_id AND gat.provider_id = gr.provider_id 
  LEFT  JOIN WSDOT_STAGE.wsdot.census_fips_places cfp ON cfp.fips_place = gr.fips_place AND cfp.census_year = gr.census_year 
  LEFT  JOIN WSDOT_STAGE.wsdot.census_metro_micro_statistical_areas msa ON msa.code = gr.metro_micro_statistical_area_code AND msa.census_year = gr.census_year
  LEFT  JOIN WSDOT_STAGE.wsdot.census_combined_statistical_areas csa ON csa.code = gr.combined_statistical_area_code AND csa.census_year = gr.census_year
  LEFT  JOIN WSDOT_STAGE.wsdot.census_metropolitan_division_areas mda ON mda.code = gr.metropolitan_division_area_code AND mda.census_year = gr.census_year;

-- export data for TRAC equity
-- convert varbinary to char - see https://stackoverflow.com/a/12016386
SELECT  lower(convert(char(66), hashbytes('SHA2_256', concat(k.hashkey, ah.customer_account_id)), 1)) AS hashed_customer_account_id
		,gr.accuracy_score 
		,gat.short_code AS accuracy_type
		,gr.gc_city 
		,gr.gc_state 
		,gr.gc_zip 
		,gr.gc_country 
		,gr.census_year 
		,gr.fips_state 
		,gr.fips_county 
		,gr.fips_place 
		,gr.fips_tract 
		,concat(gr.fips_tract, gr.census_block_group) AS fips_block_group
		,gr.metro_micro_statistical_area_code 
		,gr.combined_statistical_area_code 
		,gr.metropolitan_division_area_code 
--SELECT  count(*)
  FROM  WSDOT_STAGE.util.account_postal_address_hash ah
  JOIN  WSDOT_STAGE.wsdot.geocoding_results gr ON gr.address_hash = ah.address_hash 
  JOIN  WSDOT_STAGE.wsdot.geocoding_providers gp ON gp.provider_id = gr.provider_id 
  JOIN  WSDOT_STAGE.wsdot.geocoding_accuracy_types gat ON gat.accuracy_type_id = gr.accuracy_type_id AND gat.provider_id = gr.provider_id 
  JOIN  (SELECT hashkey FROM WSDOT_STAGE.wsdot.hashkeys WHERE id = 1) k ON 1 = 1;
 
-- test match - works
SELECT  ti.trip_id, ti.facility, tet.customer_account_id 
		,lower(convert(char(66), hashbytes('SHA2_256', concat(k.hashkey, tet.customer_account_id)), 1)) AS hashed_customer_account_id
  FROM  WSDOT_STAGE.tmp.toll_equity_trips tet 
  JOIN  WSDOT_STAGE.tmp.toll_equity_trip_info ti ON ti.trip_id = tet.trip_id 
  JOIN  (SELECT hashkey FROM WSDOT_STAGE.wsdot.hashkeys WHERE id = 1) k ON 1 = 1
 WHERE  ti.trip_id = 247081889;

-- investigate trips with no account or matching address
-- 61,252,785
SELECT  --WSDOT_STAGE.util.f_get_month(ti.exit_datetime) AS trip_month
		ti.facility 
		--,tet.customer_account_id 
		,sum(CASE WHEN tet.customer_account_id < 1000 THEN 1 ELSE 0 END) AS sys_account_trips
		,sum(CASE WHEN tet.customer_account_id >= 1000 AND ah.postal_address_id IS NULL THEN 1 ELSE 0 END) AS no_address_trips
		,count(*) AS trip_count 
  FROM  WSDOT_STAGE.tmp.toll_equity_trips tet 
  JOIN  WSDOT_STAGE.tmp.toll_equity_trip_info ti ON ti.trip_id = tet.trip_id
  LEFT  JOIN WSDOT_STAGE.util.account_postal_address_hash ah ON ah.customer_account_id = tet.customer_account_id 
-- WHERE  tet.customer_account_id < 1000	-- system accounts only
GROUP BY ti.facility --, tet.customer_account_id 
ORDER BY ti.facility --, tet.customer_account_id 
;

SELECT min(customer_account_id) FROM WSDOT_STAGE.util.account_postal_address_hash ah;	-- 1001

-- drop tables
DROP TABLE WSDOT_STAGE.tmp.final_address;
DROP TABLE WSDOT_STAGE.tmp.address_info;
DROP TABLE WSDOT_STAGE.tmp.geocodio_results;
DROP TABLE WSDOT_STAGE.tmp.toll_equity_trip_info;
DROP TABLE WSDOT_STAGE.tmp.toll_equity_trips;

-- end trac toll equity data export
/*######################################################################*/

/********** PASS SALES INVESTIGATION **********/
-- 2022-08-12 pass sales investigation for DeeAnn (via Ami)
SELECT  *
--SELECT  count(*)
  FROM  fastlane.xact.ledger l 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  --JOIN  fastlane.gl.GLPostingRule gpr ON gpr.glPostingRuleId = mt.glPostingRuleId 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
  JOIN  fastlane.gl.EnuGlFund egf ON egf.glFundId = coa.glFundId 
  JOIN  fastlane.xact.Annotation a ON a.annotationid = l.annotationid 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = l.transactionTypeId 
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
 WHERE  l.inputUserId = 2737	--eer.shortCode = 'C-PLSFD'
   --AND  l.customerAccountId = 201
   AND  l.updatedAt  >= convert(datetime2, '2022-07-01')
--   AND  mt.postingDate > convert(date, '2022-07-01')
-- WHERE  coa.glAccountNumber = '0405 73'
--   AND  vts.customerAccountid = mt.customeraccountid
--   AND  tp.laneTime >= convert(datetime2, '2021-01-01')
ORDER BY l.updatedAt

/********** SR167 INVESTIGATION **********/
-- 2022-08-15 look into Type 92s for DeeAnn (via Ami)

SELECT  ti.roadsideTollType
		,coa.glAccountNumber 
		,*
		--,count(*)
  FROM  WSDOT_STAGE.tmp.toll_equity_trip_info ti
  JOIN  WSDOT_STAGE.tmp.toll_equity_trips te ON te.trip_id = ti.trip_id 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = te.transactionTypeId 
  JOIN  WSDOT_STAGE.util.trip_state ts ON ts.trip_id = ti.trip_id 
  JOIN  fastlane.xact.ledger l ON l.transactionId = ts.last_trip_txn_id 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
 WHERE  ti.roadway = 'SR167'
   AND  ti.exit_datetime BETWEEN convert(datetime2, '2021-09-01') AND convert(datetime2, '2021-10-01')
   AND  ti.roadsideTollType = 92
GROUP BY ti.roadsideTollType, coa.glAccountNumber 
ORDER BY count(*) DESC;

-- summarize by travel_date, post_date, trans_code (TPASO), count of trips in system account 14
-- also look for TPASD (once was in acct 14)
-- run since 2021-06-01

-- count of type 92's: 374,884
SELECT  cast(tp.laneTime AS date) AS trip_date
		,cast(t.created AS date) AS post_date
		,CASE WHEN vts.customerAccountid < 1000 THEN vts.customerAccountid ELSE NULL END AS system_account_id
		,vts.tcode 
		,coa.glAccountNumber 
		,count(*) AS trip_count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.util.v_all_trip_state ts ON ts.trip_id = t.tripId 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_trip_txn_id 
  JOIN  fastlane.xact.V_TransactionSummary vts ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
 WHERE  tp.roadsideTollType = 92
GROUP BY cast(tp.laneTime AS date)
		,cast(t.created AS date)
		,CASE WHEN vts.customerAccountid < 1000 THEN vts.customerAccountid ELSE NULL END 
		,vts.tcode 
		,coa.glAccountNumber 
ORDER BY cast(tp.laneTime AS date)
		,cast(t.created AS date)
		,count(*) DESC;

-- count in acct 14: 338,352, same for unique trip ids so no multiple entries on tolls
SELECT  vts.tcode, count(*)
		--,count(DISTINCT tle.tollId)
  FROM  fastlane.xact.V_TransactionSummary vts 
--  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
--  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
--  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  vts.customerAccountid = 14
GROUP BY vts.tcode;

-- same count in ledger
SELECT  count(*)
  FROM  fastlane.xact.ledger l
 WHERE  l.customerAccountId = 14;

/*######################################################################*/
/*##########             FACILITY FREQUENCY OF USE            ##########*/
/*######################################################################*/
-- 2022-08-18 frequency of unique vehicle (tag/plate) use on SR520 (and later other facilities)
-- similar to previous pull on 2022-07-06, but this summarizes usage in a month rather than a day 
-- pull summaries for may and october from 2018 forward, by facility (direction)
-- also pull a count of total unique SR 520 users for each year to benchmark how many are not using the facility

-- TODO: consider writing procedure to build dynamic cte to select months/facilites/weekdays of interest
--		 use dynamic pivot: https://stackoverflow.com/a/10404455
-- TODO: add ability to require inclusion of a certain screenline point for trips on SR167 and I405 (complex)
-- DROP VIEW tmp.v_trip_freq;
CREATE OR ALTER VIEW tmp.v_trip_freq AS
WITH cte AS
	(SELECT d.trip_month
			,d.veh_id
			,d.facility 
			,sum(d.toll_trips) AS toll_trips
	   FROM WSDOT_STAGE.util.v_daily_facility_usage d
	  WHERE d.trip_date >= convert(date, '2018-01-01')	-- since 2018
	    AND month(d.trip_date) IN (5, 10)	-- may and october data
  	    AND roadway = 'SR520'	-- only facility data
	GROUP BY d.trip_month
			,d.veh_id
			,d.facility
	)
SELECT  trip_month,veh_id, [520W], [520E]
  FROM  cte
 PIVOT  (
		sum(toll_trips)
		FOR facility IN ([520W], [520E])
) pvt;

-- high-use TNB in May 2022: tag_3047143: 106, none: 320, lp_1000: 24136
SELECT * FROM WSDOT_STAGE.tmp.v_trip_freq WHERE [TNB] > 100;

-- look at facilities and roadways
SELECT * FROM WSDOT_STAGE.wsdot.facility f;

SELECT  f.trip_month
		,ut.user_type 
		,[520W], [520E]
		,count(DISTINCT f.veh_id) AS user_count
  FROM  WSDOT_STAGE.tmp.v_trip_freq f
  JOIN  WSDOT_STAGE.tmp.sr520_freq_user_types ut ON ut.veh_id = f.veh_id 
GROUP BY f.trip_month
		,ut.user_type
		,[520W], [520E]
ORDER BY f.trip_month
		,count(DISTINCT f.veh_id) DESC;
	
-- now get count of distinct annual users
SELECT  year(trip_month) AS travel_year
		,count(DISTINCT veh_id) AS distinct_users
		,sum(d.toll_trips) AS total_trips
  FROM  WSDOT_STAGE.util.v_daily_facility_usage d
 WHERE  d.trip_date >= convert(date, '2018-01-01')
   AND  roadway = 'SR520'	-- only SR 520
GROUP BY year(trip_month)
ORDER BY year(trip_month);

-- second analysis: look at usage over time for frequent users - those that used either direction 15 times or more in a month
-- track all users who met the frequency criteria for *any* of the months of interest
-- way to do this? build table of frequent vechile_ids (if that uniquely links to tag/plate) and add to table for each month
SELECT  DISTINCT veh_id, trip_month
  INTO  WSDOT_STAGE.tmp.sr520_freq_users
  FROM  WSDOT_STAGE.tmp.v_trip_freq tf
 WHERE  [520W] >= 15 OR [520E] >= 15;

-- delete "fake" users, e.g. lp_1000, lp_182329, lp_184145;
SELECT * FROM WSDOT_STAGE.tmp.sr520_freq_users WHERE veh_id = 'none';
DELETE FROM WSDOT_STAGE.tmp.sr520_freq_users WHERE veh_id IN ('none', 'lp_1000', 'lp_182329', 'lp_184145');

-- look at frequent users
SELECT  month_count, count(*) AS users
  FROM  (SELECT veh_id, count(*) AS month_count FROM WSDOT_STAGE.tmp.sr520_freq_users GROUP BY veh_id) m 
GROUP BY month_count
ORDER BY month_count;

SELECT count(DISTINCT veh_id) FROM WSDOT_STAGE.tmp.sr520_freq_users;

-- frequent users per month
SELECT trip_month, count(*) AS user_count FROM WSDOT_STAGE.tmp.sr520_freq_users GROUP BY trip_month ORDER BY trip_month; 

-- pivot table
SELECT  veh_id
		,CASE WHEN pre_covid = 1 AND covid = 0 THEN 'pre_covid'
			  WHEN pre_covid = 0 AND covid = 1 THEN 'covid'
			  WHEN pre_covid = 1 AND covid = 1 THEN 'constant'
			  ELSE 'other' END AS user_type
  INTO  WSDOT_STAGE.tmp.sr520_freq_user_types
  FROM  (
SELECT  veh_id
		,([2018-05] | [2018-10] | [2019-05] | [2019-10]) AS pre_covid
		,([2020-05] | [2020-10] | [2021-05] | [2021-10] | [2022-05]) AS covid
		--,([2020-05] | [2020-10] | [2021-05]) AS covid
		--,([2021-10] | [2022-05]) AS post_covid
		--,count(*) AS users
  FROM  (
--SELECT  [2018-05], [2018-10],  [2019-05], [2019-10], [2020-05], [2020-10], [2021-05], [2021-10], [2022-05], count(*) AS user_count FROM (
SELECT  veh_id, [2018-05], [2018-10],  [2019-05], [2019-10], [2020-05], [2020-10], [2021-05], [2021-10], [2022-05]
  FROM  (SELECT veh_id, cast(trip_month AS varchar(7)) AS trip_month FROM WSDOT_STAGE.tmp.sr520_freq_users) m
 PIVOT  (
		count(trip_month)
		FOR trip_month IN ([2018-05], [2018-10],  [2019-05], [2019-10], [2020-05], [2020-10], [2021-05], [2021-10], [2022-05])
) pvt
) p
) u;
--GROUP BY [2018-05], [2018-10],  [2019-05], [2019-10], [2020-05], [2020-10], [2021-05], [2021-10], [2022-05]
GROUP BY ([2018-05] | [2018-10] | [2019-05] | [2019-10])
		,([2020-05] | [2020-10] | [2021-05] | [2021-10] | [2022-05])
		--,([2020-05] | [2020-10] | [2021-05])
		--,([2021-10] | [2022-05])
ORDER BY count(*) DESC;

-- timeframe: sooner is better, forecast update in 3 weeks

-- clean up
-- DROP TABLE WSDOT_STAGE.tmp.sr520_freq_users;
-- DROP TABLE WSDOT_STAGE.tmp.sr520_freq_user_types;
-- DROP VIEW tmp.v_trip_freq;

-- end facility frequency of use
/*######################################################################*/

/********** PBM REGULAR ACCOUNTS **********/
-- 2022-09-06 request from Ami to see PBM Regular accounts (type 8)

SELECT  vca.customer_account_id 
		,vca.postingDate AS acct_created_date
		,tb.amount AS acct_balance
--SELECT  count(*)
  FROM  WSDOT_STAGE.util.v_customer_account vca 
--  JOIN  fastlane.util.CustomerAccount ca ON vca.customer_account_id = ca.customerAccountId 
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = vca.customer_account_id AND tb.isCurrentActivity = 1
 WHERE  vca.account_type_id = 8		-- PBM (Regular)
ORDER BY vca.postingDate;

SELECT * FROM fastlane.util.CustomerAccount ca WHERE ca.customerAccountId = 11191341;
SELECT * FROM fastlane.rtoll.LicensePlateCustomerAccount lpca WHERE lpca.customerAccountId = 11191341;

/********** COMPARISON OF V_ACCOUNT_STATUS **********/
-- 2022-09-07 compare my correct v_account_status to the ETAN version
SELECT  evas.status AS etan_status
		,wvas.status AS correct_status
		,count(*) AS acct_count
		,sum(cast(ca.isDefunct AS int)) AS defunct_count
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.info.V_AccountStatus evas ON evas.customerAccountId = ca.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_account_status wvas ON wvas.customerAccountId = ca.customerAccountId
GROUP BY evas.status
		,wvas.status 
ORDER BY evas.status
		,wvas.status;

/********** COMPARISON OF NEW ACCOUNTS **********/
-- 2022-09-07 new accounts by date
SELECT  ca.postingDate 
		,vca.acct_type_desc 
		,count(*) AS acct_count
  FROM  fastlane.util.CustomerAccount ca 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId
 WHERE  ca.postingDate >= convert(date,'2022-08-04')
GROUP BY ca.postingDate 
		,vca.acct_type_desc
ORDER BY ca.postingDate 
		,count(*) DESC;

/********** COUNT OF DISTINCT USER CASES **********/
-- 2022-09-14 count for Lauren McLaughlin
SELECT  count(DISTINCT uc.ucaseId) AS distinct_cases
		,count(DISTINCT cca.ucaseId) AS distinct_user_cases
		,count(DISTINCT cca.customerAccountId) AS distinct_users
		,count(*) AS link_count
  FROM  fastlane.usercase.[UCase] uc 
  LEFT  JOIN  fastlane.usercase.CaseCustomerAccount cca ON cca.ucaseId = uc.ucaseId
 WHERE  uc.isDefunct = 0		-- non-defunct cases
   AND  uc.caseSourceId <> 6	-- not system-created
   AND  uc.created BETWEEN convert(datetime2, '2021-07-01') AND convert(datetime2, '2022-06-30 23:59:59.9999')
   AND  coalesce(cca.isDefunct, 0) = 0	-- not defunct (null-safe)
--   AND  uc.created >= convert(datetime2, '2021-07-01')
--   AND  uc.created < convert(datetime2, '2021-07-01')		-- before ETAN transition there were still a lot - why?
;

SELECT * FROM fastlane.usercase.EnuCaseSource ecs;

/********** UNPAID NOCP SUMMARY (INVOICE) **********/
-- 2022-09-15 unpaid NOCP with amount >= $1000 for Ami
-- Example: Customer Account ID 8486103 has NOCP Summary 7408611 with a balance due of $1,019.30
-- statements and NOCP both descend from invoices

-- pull Customer Account ID, NOCP Summary ID, NOCP Summary Balance, License Plate Number
SELECT  i.customerAccountId 
		,i.invoiceId AS nocp_summary_id
		,concat(lpj.shortCode,'-',lp.lpnumber) AS lp_number
		,sum(vts.amount) AS nocp_balance
		,sum(CASE WHEN vts.basecode = 'TCPP' THEN 1 ELSE 0 END) AS nocp_trip_count
		,sum(CASE WHEN vts.basecode = 'FCPP' THEN 1 ELSE 0 END) AS nocp_fee_count
		,sum(CASE WHEN vts.basecode = 'FTBP' THEN 1 ELSE 0 END) AS nocp_late_fee_count
		,count(*) AS item_count
-- SELECT  *
  FROM  fastlane.xact.Invoice i 
  JOIN  fastlane.xact.EnuInvoiceLevel eil ON eil.invoiceLevelId = i.invoiceLevelId 
  JOIN  fastlane.xact.InvoiceLedgerTransaction ilt ON ilt.invoiceId = i.invoiceId 
--  JOIN  fastlane.xact.ledger l ON l.transactionId = ilt.transactionId AND l.customerAccountId = i.customerAccountId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ilt.transactionId 
  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = i.licensePlateId 
  JOIN  fastlane.util.EnuLpJurisdiction lpj on lpj.lpJurisdictionId=lp.lpJurisdictionId
--  LEFT  JOIN  fastlane.xact.[Statement] s ON s.invoiceId = i.invoiceId 
 WHERE  eil.shortCode = 'NOCP'	-- NOCP only 
   AND  vts.isUnpaid = 1		-- unpaid only 
--   AND  i.customerAccountId = 8486103
--   AND  i.invoiceId = 7408611
GROUP BY i.customerAccountId 
		,i.invoiceId
		,concat(lpj.shortCode,'-',lp.lpnumber) 
HAVING  sum(vts.amount) <= -1000
;

/********** TNB USERS **********/
-- 2022-09-15 TNB prepaid or payg users after 2022-08-01 for Ami
-- 2022-09-20 updated to add secondary email

-- first get unique TNB customers (runs in 3 minutes)
SELECT  DISTINCT tdb.customerAccountid
		INTO WSDOT_STAGE.tmp.tnb_customers
  FROM  WSDOT_STAGE.util.v_trip_detail_base tdb
 WHERE  tdb.laneTime >= convert(datetime2, '2022-08-01')		-- only since 2022-08-01
   AND  tdb.roadway = 'TNB'										-- only tnb
   AND  tdb.customerAccountId >= 1000;							-- non-SYSTEM accounts

-- create index for faster join
CREATE NONCLUSTERED INDEX ix_customer_account_id ON tmp.tnb_customers (customerAccountId);

-- now get Customer Account ID, Primary contact information: First Name/Last Name, Business Name, Address, Email Address
SELECT  uc.customerAccountId
		,aci.isCompany 
		,aci.salutation 
		,aci.firstName 
		,aci.lastNameOrCompanyName 
		,aci.displayname 
		,aci.email_address 
		,vse.secondary_email_address 
		,aci.address1 
		,aci.address2 
		,aci.city 
		,aci.state 
		,aci.postalCode 
		,aci.country 
-- SELECT  count(*)
  FROM  WSDOT_STAGE.tmp.tnb_customers uc
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = uc.customerAccountid 
  JOIN  WSDOT_STAGE.util.v_acct_contact_info aci ON aci.customerAccountId = uc.customerAccountid 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_secondary_email vse ON vse.customerAccountId = uc.customerAccountid 
 WHERE  vca.account_type_id BETWEEN 2 AND 5;	-- only prepaid and PAYG

-- clean up
DROP TABLE WSDOT_STAGE.tmp.tnb_customers;

/********** NEGATIVE BALANCE ACCOUNT UPDATES **********/
-- 2022-09-20 update on negative balances for Tim Arnold using provided customer account ids
CREATE TABLE WSDOT_STAGE.tmp.neg_balance_accounts (customerAccountId int PRIMARY KEY);

-- load data from csv and check count
-- 2022-09-20: 27,690
SELECT count(*) FROM WSDOT_STAGE.tmp.neg_balance_accounts nba;

-- now get updated account type and account balance
SELECT  nb.customerAccountId 
		,vca.acct_type_desc
		,tb.amount 
  FROM  WSDOT_STAGE.tmp.neg_balance_accounts nb
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = nb.customerAccountId AND tb.isCurrentActivity = 1
  LEFT  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = nb.customerAccountId;

DROP TABLE WSDOT_STAGE.tmp.neg_balance_accounts;

/********** LEGACY ACCT PAYG CONVERSIONS **********/
-- 2022-09-22 legacy accounts that converted to PAYG for Lauren McLaughlin

-- as-of 2022-07-01:
DECLARE @dtm datetime2(2) = convert(datetime2, '2022-07-01')
SELECT  eat.description, count(*)
  FROM  WSDOT_STAGE.util.account_type FOR SYSTEM_TIME AS OF @dtm uat
  JOIN  fastlane.info.EnuAccountType eat ON eat.accountTypeId = uat.account_type_id
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = uat.customer_account_id
 WHERE  ca.legacyAccountNumber IS NOT NULL 
   AND  eat.shortCode LIKE 'ZBA%'
GROUP BY eat.description;

-- current status
SELECT  vca.acct_type_desc, count(*)
--SELECT  *
  FROM  WSDOT_STAGE.util.v_customer_account vca 
 WHERE  vca.legacyAccountNumber IS NOT NULL 
   AND  vca.acct_short_code LIKE 'ZBA%'
GROUP BY vca.acct_type_desc;

/********** PASS FULFILLMENT ISSUE **********/
-- 2022-09-23 Ami stuff

-- pass issue with passes not being fulfilled
-- 1609 unfulfilled
SELECT  invTagRequestStatusId -- ,count(*)
  FROM  fastlane.inv.V_InvTagRequest vitr
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = vitr.customerAccountId
 WHERE  vitr.requestDate >= convert(date, '2021-07-10')
--   AND  invTagRequestStatusId <> 3
--   AND  ca.customerAccountId = 10992798
GROUP BY invTagRequestStatusId;

-- look at tagRequest table
-- 13,437 pending, 74,598 request pending
-- add customer account id, try to get posting date -- 10992798
SELECT  itr.invTagRequestId 
		,trl.customerAccountId 
		,etrt.description AS tag_request_type
		,tdt.description AS tag_type
		,eitrs.description AS request_status
		,hitr.updatedAt
		,itr.isDefunct 
		,count(*) OVER (PARTITION BY trl.customerAccountId, etrt.description, tdt.description) AS mult_request_count
  FROM  fastlane.inv.InvTagRequest itr
  JOIN  fastlane.xact.TagRequestLedger trl 
		 ON trl.invTagRequestId=itr.invTagRequestId
		AND itr.invTagRequestStatusId NOT IN (SELECT invTagRequestStatusId FROM fastlane.inv.EnuInvTagRequestStatus WHERE shortCode IN ('ASSIGNED','REQUESTPENDING'))
--  JOIN  fastlane.xact.ledger l 
--		 ON l.ledgerItemInfoId = trl.ledgerItemInfoId 
--		AND l.transactionTypeId IN (SELECT transactionTypeId FROM fastlane.xact.EnuTransactionType WHERE shortCode IN ('COTPS','COTPL'))
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = trl.customerAccountId
  JOIN  fastlane.inv.EnuInvTagRequestStatus eitrs ON eitrs.invTagRequestStatusId = itr.invTagRequestStatusId 
  JOIN  (SELECT min(updatedAt) AS updatedAt, invTagRequestId
		   FROM fastlane.inv.InvTagRequest FOR system_time ALL
		  WHERE invTagRequestStatusId = 1
		GROUP BY invTagRequestId
		) hitr ON hitr.invTagRequestId = itr.invTagRequestId
  JOIN  fastlane.inv.EnuTagRequestType etrt ON etrt.tagRequestTypeId = itr.tagRequestTypeId 
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = itr.tagDeviceTypeId
-- WHERE  itr.isDefunct = 0
-- WHERE  itr.invTagRequestStatusId <> 3
ORDER BY trl.customerAccountId 
		,hitr.updatedAt
		,itr.invTagRequestId;

-- 2022-09-26 proposed revision to pass fulfillment
-- 3,071,523 requests in table; 324,824 since 2021-07-10
SELECT  cast(hitr.req_dtm AS date) AS request_date
		,ins.shortCode AS req_source
		,etrt.description AS request_type
		,tdt.description AS pass_type
		,eitrs.description AS request_status
		--,CASE WHEN itr.invTagRequestStatusId = 3 THEN 'Fulfilled' ELSE 'Unfulfilled' END AS fulfillment_status
		,sum(CASE WHEN itr.invTagRequestStatusId = 3 THEN 1 ELSE 0 END) AS fulfilled_count
		,count(*) AS pass_count
--SELECT count(*)
  FROM  fastlane.inv.InvTagRequest itr
  JOIN  fastlane.inv.EnuTagRequestType etrt ON etrt.tagRequestTypeId = itr.tagRequestTypeId
  JOIN  fastlane.inv.TagDeviceType tdt ON tdt.tagDeviceTypeId = itr.tagDeviceTypeId
  JOIN  fastlane.inv.EnuInvTagRequestStatus eitrs ON eitrs.invTagRequestStatusId = itr.invTagRequestStatusId 
--  JOIN  fastlane.xact.TagRequestLedger trl ON trl.invTagRequestId = itr.invTagRequestId
--  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = trl.customerAccountId
  JOIN  (SELECT DISTINCT invTagRequestId
  				,first_value(updatedAt) OVER (PARTITION BY invTagRequestId ORDER BY updatedAt) AS req_dtm		-- get original request date
  				,first_value(inputSourceId) OVER (PARTITION BY invTagRequestId ORDER BY updatedAt) AS src_id	-- get original request source
		   FROM fastlane.inv.InvTagRequest FOR system_time ALL
		  WHERE invTagRequestStatusId = 1
		) hitr ON hitr.invTagRequestId = itr.invTagRequestId
  JOIN  fastlane.info.InputSource ins ON ins.inputSourceId = hitr.src_id
--  LEFT  JOIN fastlane.inv.V_InvTagRequest vitr ON vitr.invtagRequestId = itr.invTagRequestId
 WHERE  itr.isDefunct = 0		-- remove invalid/cancelled requests
   AND  hitr.req_dtm >= convert(date, '2021-07-10')
GROUP BY cast(hitr.req_dtm AS date)
		,ins.shortCode
		,etrt.description
		,tdt.description
		,eitrs.description
ORDER BY 1,2,3,4,5;

/********** RECEIVABLES AGING FOR SR 520 **********/
-- 2022-09-23 receivables aging report for DeeAnn
-- SR 520 UNPAID trips with transcodes GL T213 for PBP, PBP-Fee, PBM, as-of 2022-06-30 (since 2020-06-30)
-- include O/Y/Z
-- open items with a posted date of 2020-07-01 or higher

-- desired output from fastlane.reports.rpt_receivables_aging_report, but this report is broken
--   it in turn depends on fastlane.reports.rpt_receivable_aging_details
--   which in turn depends on fastlane.reports.rpt_receivable_aging_internals
-- fails with error about unique constraint due to duplicate value
EXEC fastlane.reports.rpt_receivable_aging_details
	 @asOf = '2022-07-01'
	,@outputTable  = 'WSDOT_STAGE.tmp.rpt_receivables_aging_detail_05312020'
	,@inputTable  = NULL;

-- 
DECLARE @dtm datetime2(2) = convert(datetime2, '2022-07-01')
SELECT  *
  FROM  fastlane.xact.ledger l
--  FROM  WSDOT_STAGE.util.trip_state ts FOR SYSTEM_TIME AS OF @dtm uat
--  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.
;

-- similar number of items
SELECT count(*) FROM fastlane.xact.ledger;
SELECT count(*) FROM fastlane.xact.V_TransactionSummary vts;

-- find trip with lots of processing
-- 224985890 (16), 215562790 (23), 261211099 (35), 274390176 (27)
SELECT  tle.tollId, count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
GROUP BY tle.tollId 
HAVING  count(*) > 10;

-- review trips with lost of reports - previous txns are always cancelled
SELECT  *
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId 
 WHERE  tle.tollid = 261211099
ORDER BY vts.postingDate;

-- find trips where all entries are isCancelled = 1? none occur!
-- NOTE: is this a new way to get trip state as-of a certain date?
--       also note that as V_Transaction_Summary is updated, past summaries will link to updated info 
--       only way to avoid is to have history on the table, as Lance was building in a different way
-- however, can't use isCancellation = 0 here since we are looking at as-on 2022-07-01, so newer txns would cancel those valid as-of then. boo
SELECT  vts.ledgerItemInfoId
		,count(*)
  FROM  fastlane.xact.V_TransactionSummary vts 
GROUP BY vts.ledgerItemInfoId 
HAVING  count(*) > 1
   AND  count(*) = sum(cast(vts.isCancelled AS smallint));

-- further check that no trip_ids are duplicated by txn_category when considering only non-cancelled trips
-- no duplicates on tolls; over 60 min to run!
SELECT  tle.tollId 
		,ett.transactionCategoryId 
		,count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
 WHERE  vts.isCancelled = 0
   AND  ett.transactionCategoryId = 1	-- only tolls
GROUP BY tle.tollId 
		,ett.transactionCategoryId 
HAVING	count(*) > 1;

SELECT * FROM fastlane.xact.EnuTransactionType ett;

-- for this query:
-- build table of latest txn prior to 2022-07-01 on SR520 for trips on or after 2020-07-01
-- then get all txns at the same time as that txn (for fees, etc)

SELECT  *
--SELECT  count(*)
  FROM  fastlane.xact.ledger l 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  --JOIN  fastlane.gl.GLPostingRule gpr ON gpr.glPostingRuleId = mt.glPostingRuleId 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
  JOIN  fastlane.gl.EnuGlFund egf ON egf.glFundId = coa.glFundId 
  JOIN  fastlane.xact.Annotation a ON a.annotationid = l.annotationid 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = l.transactionTypeId 
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
 WHERE  l.inputUserId = 2737	--eer.shortCode = 'C-PLSFD'
   --AND  l.customerAccountId = 201
   AND  l.updatedAt  >= convert(datetime2, '2022-07-01')
--   AND  mt.postingDate > convert(date, '2022-07-01')
-- WHERE  coa.glAccountNumber = '0405 73'
--   AND  vts.customerAccountid = mt.customeraccountid
--   AND  tp.laneTime >= convert(datetime2, '2021-01-01')
ORDER BY l.updatedAt;

/********** LEGACY ACCT PAYG CONVERSIONS **********/
-- 2022-09-26 legacy PBM accounts converted to G2G for Lauren McLaughlin
-- how many PBM toll bills were converted into accounts using that bill
-- per Ami, look at trips with reversal of PBM and repost as PBP/PAS/PAYG on same date as history letter/email of account conversion confirmation
-- example account: 10950196 converted 2022-08-26

-- get communication information
-- event is 204, 269417 converstions 
SELECT  c.communicationId 
		,c.createdAt
		,c.updatedAt 
		,ca.customerAccountId 
		,ect.communicationTypeId 
		,ect.shortCode AS comm_type
		,ect.description AS comm_description
		,ecc.shortCode AS channel_type
		,ecet.shortCode AS comm_event
  INTO  WSDOT_STAGE.tmp.acct_conversions
-- SELECT  count(*), min(c.createdAt)
  FROM  fastlane.comm.Communication c 
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
  JOIN  fastlane.comm.EnuCommunicationType ect ON ect.communicationTypeId = c.communicationTypeId 
  JOIN  fastlane.comm.EnuCommunicationChannel ecc ON ecc.communicationChannelId = c.communicationChannelId 
  JOIN  fastlane.comm.EnuCommunicationEventType ecet ON ecet.communicationEventTypeId = c.communicationEventTypeId 
 WHERE  c.communicationTypeId = 204
--   AND  ca.customerAccountId = 10950196
;

-- now get reversed PBM charges near same time
-- 8914 PBM tolls, 264,842 distinct customers
-- 264,838 from 2021-07-01 - 2022-09-26
-- 195,805 from 2021-07-01 - 2022-07-01
SELECT  vca.acct_type_desc
		,count(*) AS acct_count
SELECT  ca.customerAccountId, vca.acct_type_desc 		
  FROM  (SELECT  DISTINCT ac.customerAccountId
		--SELECT  DISTINCT vts2.transactionTypeId 
		  FROM  WSDOT_STAGE.tmp.acct_conversions ac 
		  JOIN  fastlane.xact.V_TransactionSummary vts
				 ON vts.customerAccountid = ac.customerAccountid 
				AND vts.tcode = 'TPBMD'			-- toll PBM type
				AND vts.transactionTypeId = 5	-- PPDEBITCANCEL type
				AND vts.postingDate BETWEEN dateadd(minute, -20, ac.createdAt) AND dateadd(minute, 5, ac.createdAt)
		  JOIN  fastlane.xact.V_TransactionSummary vts2
				 ON vts2.customerAccountid = ac.customerAccountid
				AND vts2.postingDate = vts.postingDate 	-- same time as cancel
				AND vts2.transactionTypeId = 1			-- converted to g2g account
		--  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts2.transactionTypeId AND ett.transactionCategoryId = 1
		--  JOIN  fastlane.xact.EnuTransactionCategory etc ON etc.transactionCategoryId = ett.transactionCategoryId 
		--  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId
		 WHERE  ac.updatedAt < convert(date, '2022-07-01')
		--   AND  vts.customerAccountid = 10950196
		) ca
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId
 WHERE  vca.account_type_id NOT BETWEEN 2 AND 5
GROUP BY vca.acct_type_desc;

-- clean up:
DROP TABLE WSDOT_STAGE.tmp.acct_conversions;

/********** VOLUME-WEIGHTED AVG TOLL RATE BY PEAK PERIOD AND FACILITY **********/
-- 2022-09-28 volume-weighted average toll rates by type, time and facility for annual reporting for Jim/Marie

-- look at historic holiday behavior by summarizing all trips in ETAN by facility and date 
SELECT  vfl.wsdot_facility
		,cast(tp.laneTime AS date) AS trip_date
		,sum(CASE WHEN ta.amount < 0 THEN 1 ELSE 0 END) AS charged_trips
		,sum(CASE WHEN ta.amount = 0 THEN 1 ELSE 0 END) AS zero_toll_trips
		,count(*) AS trip_count
  FROM  fastlane.lance.trip t
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  fastlane.lance.TripAmount ta ON ta.tripId = t.tripId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
GROUP BY vfl.wsdot_facility
		,cast(tp.laneTime AS date)
ORDER BY vfl.wsdot_facility
		,cast(tp.laneTime AS date);

-- TODO: need to add remaining manual paid TNB trips too!
-- for PBP, be sure to add $0.25 PBP fee since not included in amount
-- use isnull on weekend rates to avoid removing nulls from left join
SELECT  tdf.trip_month 
		,tdf.facility 
		,tp.period_name 
		,CASE WHEN tdf.pricing_plan = 'PAS' THEN 'pass'							-- tag/pass on an account
			  WHEN tdf.pricing_plan IN ('HOV','MOT','NRV') THEN 'hov/mot/nrv'	-- HOV, motorcycle or non-revenue
			  WHEN tdf.pricing_plan IN ('PBM','PBP') THEN 'plate'				-- plate-based on an account
			  WHEN tdf.transponderId IS NOT NULL THEN 'pass'					-- pending or other but with a tag
			  WHEN tdf.licensePlateId IS NOT NULL THEN 'plate'					-- pending or other with a plate
			  ELSE 'unknown' END AS category									-- unable to identify
		,sum(CASE WHEN tdf.pricing_plan = 'PBM' THEN (tdf.latest_toll_amount + 2.00) ELSE tdf.latest_toll_amount END) AS sum_toll_amount
		,sum(CASE WHEN tdf.pricing_plan = 'PBP' THEN (tdf.latest_toll_amount - 0.25) ELSE tdf.latest_toll_amount END) AS sum_full_amount
		,count(*) AS trip_count
		,sum(CASE WHEN tdf.pricing_plan = 'PBM' THEN 1 ELSE 0 END) AS pbm_trips
		,sum(CASE WHEN tdf.pricing_plan = 'PBP' THEN 1 ELSE 0 END) AS pbp_trips
		,count(DISTINCT cast(tdf.laneTime AS date)) AS num_days
  FROM  WSDOT_STAGE.util.v_trip_detail_full tdf
  JOIN  WSDOT_STAGE.wsdot.time_periods tp ON cast(tdf.laneTime AS time) BETWEEN tp.start_time AND tp.end_time 
  LEFT  JOIN WSDOT_STAGE.wsdot.holidays h ON h.holiday_date = cast(tdf.laneTime AS date) 
 WHERE  disposition_category NOT IN ('DUPLICATE_TRIP','TYPE_92','TYPE_98')		-- remove non-viable trips, add TYPE_99 here?
   AND  datename(weekday, tdf.laneTime) NOT IN ('Saturday','Sunday') 			-- remove weekend days
   AND  isnull(h.weekend_rates, 0) = 0		-- remove weekend rate holidays (use inull to avoid removing nulls from left join)
   AND  tdf.roadway IN ('I405','SR167')
   AND  tdf.laneTime >= convert(date, '2020-07-01')
GROUP BY tdf.trip_month
		,tdf.facility 
		,tp.period_name
		,CASE WHEN tdf.pricing_plan = 'PAS' THEN 'pass'
			  WHEN tdf.pricing_plan IN ('HOV','MOT','NRV') THEN 'hov/mot/nrv'
			  WHEN tdf.pricing_plan IN ('PBM','PBP') THEN 'plate'
			  WHEN tdf.transponderId IS NOT NULL THEN 'pass'
			  WHEN tdf.licensePlateId IS NOT NULL THEN 'plate'
			  ELSE 'unknown' END
ORDER BY tdf.trip_month
		,tdf.facility 
		,tp.period_name;

/********** SR520 CLA AUDIT TRIP DATES **********/
-- 2022-09-29 pull information for specific days for CLA audit for DeeAnn 
-- dates: 07/01/2021, 09/30/2021, 10/15/2021, 11/03/2021, 12/23/2021
-- info: Trip Date,Posting Date,Customer Number,Customer Name, Toll Amount

-- first build data as-of end of month for that date
-- DROP TABLE WSDOT_STAGE.tmp.trip_state_520;
DECLARE @d date = convert(date, '2021-11-03')
SELECT  DISTINCT tle.tollId AS trip_id
		,first_value(vts.transactionId) OVER (PARTITION BY tle.tollId ORDER BY vts.postingDate DESC, ttp.priority DESC, vts.transactionId) AS last_trip_txn_id
		,first_value(CASE WHEN ett.transactionCategoryId = 1 THEN vts.transactionId ELSE NULL END) OVER (PARTITION BY tle.tollId ORDER BY ett.transactionCategoryId, vts.postingDate DESC) AS last_toll_txn_id
  INTO  WSDOT_STAGE.tmp.trip_state_520
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  LEFT  JOIN  WSDOT_STAGE.wsdot.transaction_type_priority ttp ON ttp.transaction_type_id = vts.transactionTypeId
 WHERE  cast(tp.laneTime AS date) = @d
-- WHERE  cast(t.created AS date) = @d
   AND  vts.postingDate < dateadd(month, 1, WSDOT_STAGE.util.f_get_month(@d))
   AND  vfl.wsdot_roadway = 'SR520'
;

SELECT count(*) FROM WSDOT_STAGE.tmp.trip_state_520;

-- look at trip 261211099, ledgeriteminfoid 294245202
SELECT * FROM fastlane.xact.ledger l WHERE l.ledgerItemInfoId = 294245202;
SELECT * FROM fastlane.xact.V_TransactionSummary vts WHERE vts.ledgerItemInfoId = 294245202; 
SELECT * FROM fastlane.xact.ledger l JOIN fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId WHERE l.ledgerItemInfoId = 294245202;

-- so my previous query was using latest trip txn (which gets TOLLDISC) instead of latest toll txn
DECLARE @d date = convert(date, '2021-12-23')
SELECT  tp.laneTime AS trip_datetime
		,t.created AS posting_datetime 
		,vts.customerAccountid 
		,vts.amount 
		,CASE WHEN aci.isCompany = 1 THEN aci.lastNameOrCompanyName ELSE aci.displayname END AS customer_name
--SELECT  count(*) AS txns, count(DISTINCT t.tripId) AS dist_trips, sum(vts.amount) AS amount
  FROM  fastlane.xact.ledger l
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId
  JOIN  fastlane.lance.trip t ON t.ledgerItemInfoId = l.ledgerItemInfoId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.xact.Annotation a ON a.annotationid = l.annotationid 
  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info aci ON aci.customerAccountId = vts.customerAccountid 
 WHERE  cast(tp.laneTime AS date) = @d
   AND  vts.postingdate < dateadd(month, 1, WSDOT_STAGE.util.f_get_month(@d))
   AND  vfl.wsdot_roadway = 'SR520'
   AND  vts.tcode = 'TPASO'
   AND  eer.shortCode NOT IN ('R-WRLPR', 'S-TGDUP')		-- remove duplicates and wrong plates from these dates
;

-- look at a sample trip - yes, this is a TOLLDISC
SELECT  *
  FROM  fastlane.xact.ledger l
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId
  JOIN  fastlane.lance.trip t ON t.ledgerItemInfoId = l.ledgerItemInfoId
 WHERE  t.tripId = 246174659;

-- clean up 
DROP TABLE WSDOT_STAGE.tmp.trip_state_520;

/*-- work when TLE didn't seem to match, but does now
-- get trips on 2021-12-23 with multiple transactions
-- 246198727 (10)
SELECT  tle.tollId, count(*)
  FROM  fastlane.xact.TollLedgerEntry tle
  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
 WHERE  vfl.wsdot_roadway = 'SR520'
   AND  cast(tp.laneTime AS date) = convert(date, '2021-12-23')
GROUP BY tle.tollId 
HAVING  count(*) > 5;

-- code from aging internals - too many txns
-- count is too high; still counting all transactions, not unique items
DECLARE @d date = convert(date, '2021-11-03')
SELECT  count(DISTINCT t.tripId)
--SELECT  ts.*
  FROM  fastlane.xact.IV_AllocationsStart st WITH (NOEXPAND, nolock)
  JOIN  fastlane.xact.V_TransactionSummary ts ON ts.transactionId=st.debitLedgertransactionId
  JOIN  fastlane.xact.ledger l ON l.transactionId = ts.transactionId 
  JOIN  fastlane.lance.trip t ON t.ledgerItemInfoId = l.ledgerItemInfoId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.gl.M_transactiondetail_allocation tdl WITH(NOLOCK) ON tdl.allocationId = st.allocationId AND tdl.customeraccountid = ts.customeraccountid
  JOIN  fastlane.gl.M_transactiondetail td WITH(NOLOCK) ON tdl.transactiondetailId=td.transactiondetailId 
  JOIN  fastlane.gl.IV_chartofaccounts coa WITH(noexpand) ON td.glChartOfAccountsId = coa.glChartOfAccountsId
 WHERE  td.postingdate < convert(date, '2022-01-01')
--   AND  ts.postingdate < convert(date, '2022-01-01') -- dateadd(month, 1, WSDOT_STAGE.util.f_get_month(@d))
   AND  ts.balancetypeid = 1
--   AND  ts.isDebit = 1 
--   AND  tdl.isUndo = 0
   AND  cast(tp.laneTime AS date) = @d
   AND  vfl.wsdot_roadway = 'SR520'
   AND  ts.tcode = 'TPASO'
--   AND  t.tripId = 246198727
;
*/

/********** NEG BALANCE PBM ACCOUNTS **********/
-- 2022-09-29 request from Tim for Individual and Commercial PBM accounts with negative balance and a non-NULL email address

-- 938,221 neg balance pbm accts, but only 96,562 have a valid email address
SELECT  tb.customerAccountId 
		,vca.acct_type_desc 
		,tb.amount 
		,CASE WHEN vaci.isCompany = 1 THEN vaci.lastNameOrCompanyName ELSE vaci.displayname END AS customer_name
		,vaci.email_address 
		,vacse.secondary_email_address 
-- SELECT  count(*)
  FROM  fastlane.xact.TransactionsBalance tb 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info vaci ON vaci.customerAccountId = tb.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_secondary_email vacse ON vacse.customerAccountId = tb.customerAccountId 
 WHERE  tb.isCurrentActivity = 1		-- current balance only 
   AND  tb.customerAccountId >= 1000	-- no system accounts
   AND  tb.amount < 0					-- negative balance
   AND  vca.acct_short_code LIKE 'PAYBYMAIL%'	-- pay-by-mail only 
   AND  (vaci.email_address_id IS NOT NULL OR vacse.secondary_email_ddress_id IS NOT NULL);

SELECT * FROM fastlane.info.EnuAccountType eat;

/********** UNIQUE ACCTS IN NOCP CERTIFICATION QUEUE **********/
-- 2022-09-29 request from Tim for unique accts in NOCP certification queue 
-- 2022-10-06 re-run
-- 2023-02-06 re-run 

SELECT  WSDOT_STAGE.util.f_get_month(lii.postingDate) AS posting_month
		,count(DISTINCT nq.customerAccountId) AS distinct_accts
		,count(*) AS in_queue
  FROM  fastlane.xact.NOCPCertificationQueue nq
  JOIN  fastlane.xact.LedgerItemInfo lii ON lii.ledgerItemInfoId = nq.ledgerItemInfoId 
--  JOIN  fastlane.lance.trip
 WHERE  nq.isDefunct = 0
GROUP BY WSDOT_STAGE.util.f_get_month(lii.postingDate)
ORDER BY WSDOT_STAGE.util.f_get_month(lii.postingDate);

-- do trip created at times match posting date? 79,925,467 don't, 217,687,350 do
SELECT  *
  FROM  fastlane.xact.LedgerItemInfo lii
  JOIN  fastlane.lance.trip t ON t.ledgerItemInfoId = lii.ledgerItemInfoId
 WHERE  t.created <> lii.postingDate;

/********** UNPAID LATE FEES **********/
-- 2022-10-05 late fees unpaid (all from ETCC since no escalation in new system yet)
-- late fees are fund 495, gl T214

-- overall fund 495 has:
-- 44,211,188 total txns
-- 15,918,357 of which are not cancelled after 2020-07-01
--    623,328 of which are unpaid - still too many
--SELECT  *
SELECT  count(*), sum(vts.amount) AS vts_amount, sum(l.amount) AS ledger_amount
  FROM  fastlane.xact.ledger l 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  --JOIN  fastlane.gl.GLPostingRule gpr ON gpr.glPostingRuleId = mt.glPostingRuleId 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
  JOIN  fastlane.gl.EnuGlFund egf ON egf.glFundId = coa.glFundId 
--  JOIN  fastlane.xact.Annotation a ON a.annotationid = l.annotationid 
--  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = l.transactionTypeId 
--  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
 WHERE  egf.shortCode = '495'
   AND  vts.isCancelled = 0
   AND  vts.isUnpaid = 1
   AND  vts.postingDate >= convert(date, '2020-07-01');
;

SELECT * FROM fastlane.gl.EnuGlFund ;

SELECT  WSDOT_STAGE.util.f_get_month(tp.laneTime) AS trip_month
		,WSDOT_STAGE.util.f_get_month(vts.postingDate) AS posting_month
		--,WSDOT_STAGE.util.f_get_month(mt.postingDate) AS mt_post_month
		,ett.shortCode AS transaction_type
		,vfl.wsdot_facility 
		,epp.shortCode AS pricing_plan
		,egf.shortCode AS fund_code 
		,sum(vts.amount) AS vts_amount
		--,sum(mt.amount) AS mt_amount
		,count(DISTINCT mt.transactiondetailId) AS distinct_transactions
		,count(DISTINCT t.tripId) AS distinct_trips
		,count(*) AS row_count
;
SELECT  count(*)
  FROM  fastlane.xact.ledger l 
  JOIN  fastlane.gl.M_transactiondetail_ledger mtl ON mtl.transactionid = l.transactionId	-- tle.ledgertransactionId
  JOIN  fastlane.gl.M_transactiondetail mt ON mt.transactiondetailId = mtl.transactiondetailId AND mt.sequence_no = mtl.sequence_no 
  --JOIN  fastlane.gl.GLPostingRule gpr ON gpr.glPostingRuleId = mt.glPostingRuleId 
  JOIN  fastlane.gl.ChartOfAccounts coa ON coa.glChartOfAccountsId = mt.glChartOfAccountsId 
  JOIN  fastlane.gl.EnuGlFund egf ON egf.glFundId = coa.glFundId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = l.transactionId		-- tle.ledgertransactionId 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId
  LEFT  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = l.transactionId 
  LEFT  JOIN  fastlane.xact.EnuPricingPlan epp ON epp.pricingPlanId = tle.pricingPlanId
  LEFT  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  LEFT  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  LEFT  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
 WHERE  coa.glAccountNumber = '0405 73'
--   AND  vts.customerAccountid = mt.customeraccountid
--   AND  tp.laneTime >= convert(datetime2, '2021-01-01')
GROUP BY WSDOT_STAGE.util.f_get_month(tp.laneTime)
		,WSDOT_STAGE.util.f_get_month(vts.postingDate)
		--,WSDOT_STAGE.util.f_get_month(mt.postingDate)
		,ett.shortCode 
		,vfl.wsdot_facility 
		,epp.shortCode
		,egf.shortCode;

/********** ACCT TYPE ACTIVE ACCTS **********/
-- 2022-10-06 check code for Marie
SELECT  eat.description
		,count(ca.customerAccountId)
  FROM  fastlane.util.CustomerAccount ca
LEFT OUTER JOIN WSDOT_STAGE.util.account_type at2
             ON at2.customer_account_id = customerAccountId
LEFT OUTER JOIN fastlane.info.EnuAccountType eat
             ON eat.accountTypeId = at2.account_type_id
LEFT OUTER JOIN wsdot_stage.util.v_account_status vas
             ON vas.customerAccountId = ca.customerAccountId
LEFT OUTER JOIN fastlane.util.V_ah_CustomerAccountOperational vacao
             ON vacao.customerAccountId = ca.customerAccountId  
WHERE ca.isDefunct = '0'
AND vas.status = 'Active'
AND vacao.effectiveStartDate < CONVERT(datetime2,'2022-06-30 23:59:59.999')
GROUP BY eat.description
ORDER BY eat.description;

SELECT  vca.acct_type_desc 
		,count(*) AS acct_cnt
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ca.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
 WHERE  ca.isDefunct = 0	-- not defunct
   
   AND  vas.status = 'Active'
GROUP BY vca.acct_type_desc 
ORDER BY vca.acct_type_desc;


/********** TAG ASSIGNMENT DATE **********/
-- 2022-10-06 look at tag assignment dates and if tags are ever assigned to multiple accounts
SELECT tca.transponderId 
		,count(DISTINCT tca.customerAccountId)
FROM fastlane.rtoll.TransponderCustomerAccount tca 
GROUP BY tca.transponderId  
HAVING count(DISTINCT tca.customerAccountId) > 1;

-- 2394890	2
SELECT * FROM fastlane.rtoll.TransponderCustomerAccount tca WHERE tca.transponderId = 2394890;

-- also look at licenseplateid in this table!

/********** NEGATIVE ACCOUNT BALANCE UPDATE **********/
-- 2022-10-18 Chris Foster - review code on 2022-08-01
-- issue is need to get account balance as-of a previous date

-- 64843 communications
SELECT  count(DISTINCT ca.customerAccountId) AS acct_count
		,count(*) AS comm_count
SELECT  *
  FROM  fastlane.comm.Communication c
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId 
 WHERE  c.communicationTypeId = 1407
   AND  c.createdAt < convert(datetime2, '2022-08-01')
;

-- look at tables which may have account balance info?
SELECT  *
--  FROM  fastlane.xact.TransactionsBalance tb
--  FROM  fastlane.xact.hInvoice hi 
--  FROM  fastlane.xact.Statement s
--  FROM  fastlane.xact.IV_transactionsummary_base itb 
  FROM  fastlane.xact.V_tollTransaction_detail vttd 
 WHERE  customerAccountId = 1413502
;

-- this function gives balance, but does not give all transactions
SELECT * FROM  fastlane.xact.F_transaction_history(1413502);

/********** LOW BALANCE NO STATEMENT **********/
-- 2022-10-20 count of customers with no bill from WSDOT in last six months with balance less than 200

SELECT * FROM fastlane.xact.[Statement] s WHERE s.customerAccountId = 21985;
SELECT * FROM fastlane.xact.invoice i WHERE i.customerAccountId = 21985;
SELECT * FROM fastlane.xact.V_Statement vs WHERE vs.customerAccountId = 21985;

-- investigations
SELECT * FROM fastlane.xact.[Statement] s WHERE s.invoiceId = 46307381;
SELECT * FROM fastlane.xact.invoice i WHERE i.invoiceId = 46307381;
SELECT * FROM fastlane.comm.Communication c WHERE c.communicationId = 64782215;

SELECT * FROM fastlane.xact.V_Statement vs WHERE vs.statementId = 46307381;		-- slow as heck! but has returned flag
SELECT * FROM fastlane.xact.V_Notice vn WHERE vn.invoiceNumber  = 46307381;
SELECT * FROM fastlane.xact.V_tollbill_renderer_data vtrd WHERE vtrd.invoiceId = 46307381;
SELECT * FROM fastlane.xact.V_tollbill_renderer_detail vtrd WHERE vtrd.invoiceId = 46307381;

-- NOT SENT seems to correspond to NULL communicationId
SELECT * FROM fastlane.xact.V_Statement vs WHERE vs.customerAccountId = 1487102;
SELECT * FROM fastlane.xact.invoice i WHERE i.customerAccountId = 1487102;

-- statements, invoices, and communications always seem to be generated; trick is to find if not received
-- use ,CASE WHEN util.F_IsMailReturned(i.storageFileId) = 1 THEN 'Yes' ELSE 'No' END AS returned from V_Statement
SELECT  tb.customerAccountId
		--,count(s.statementId) AS statement_cnt
		,count(i.invoiceId) AS invoice_cnt
		--,count(c.communicationId) AS comm_cnt
		,sum(fastlane.util.F_IsMailReturned(i.storageFileId)) AS ret_cnt
  FROM  fastlane.xact.TransactionsBalance tb
--  LEFT  JOIN fastlane.xact.Statement s ON s.customerAccountId = tb.customerAccountId 
  LEFT  JOIN fastlane.xact.Invoice i ON i.customerAccountId = tb.customerAccountId
--  LEFT  JOIN fastlane.comm.Communication c ON c.communicationId = i.communicationId 
 WHERE  tb.isCurrentActivity = 1
   AND  tb.amount < 0		-- negative balance
   AND  tb.amount > -200	-- but not more than $200
--   AND  s.finalDate > convert(date, '2022-04-20')
   AND  i.postingDate > convert(date, '2022-04-20')
--   AND  c.createdAt > convert(date, '2022-04-20')
GROUP BY tb.customerAccountId
--HAVING  count(s.statementId) = 0
--HAVING  count(i.invoiceId) = 0
--HAVING  count(c.communicationId) = 0
HAVING sum(fastlane.util.F_IsMailReturned(i.storageFileId)) > 0
;

--   5,962 have all returned
-- 372,663 are NOT SENT (communicationId is NULL)
-- 411,147 for either, of which 407,937 are pay-by-mail
-- also look at "generated communication channel"
SELECT  d.customerAccountId
		,vca.acct_type_desc AS acct_type
		,ccd.CycleDay AS cycle_day
		,cm.comm_channels
		,vaci.email_address 
		,d.amount
		,d.unsent_comm_cnt
		,d.returned_cnt
		,d.statement_cnt
--SELECT  count(*)
  FROM  (SELECT tb.customerAccountId
				,tb.amount
				,sum(CASE WHEN i.communicationId IS NULL THEN 1 ELSE 0 END) AS unsent_comm_cnt
				,sum(fastlane.util.F_IsMailReturned(i.storageFileId)) AS returned_cnt
				,count(i.invoiceId) AS statement_cnt
		--SELECT  *
		  FROM  fastlane.xact.TransactionsBalance tb
		  LEFT  JOIN fastlane.xact.Invoice i ON i.customerAccountId = tb.customerAccountId
		 WHERE  tb.isCurrentActivity = 1
		   AND  tb.amount < 0		-- negative balance
		   AND  tb.amount > -200	-- but not more than $200
		   AND  i.postingDate > convert(date, '2021-10-20')
		   --AND  i.postingDate > convert(date, '2022-04-20')
		   --AND  tb.customerAccountId = 21985
		GROUP BY tb.customerAccountId
				,tb.amount
		--HAVING sum(util.F_IsMailReturned(i.storageFileId)) = count(i.invoiceId)
		--HAVING sum(CASE WHEN i.communicationId IS NULL THEN 1 ELSE 0 END) = count(i.invoiceId)
		--HAVING sum(util.F_IsMailReturned(i.storageFileId)) + sum(CASE WHEN i.communicationId IS NULL THEN 1 ELSE 0 END) = count(i.invoiceId)
		) AS d
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = d.customerAccountId
  LEFT  JOIN (SELECT ca.customerAccountId
  				,string_agg(ecc.description, ',') WITHIN GROUP (ORDER BY ecc.description) AS comm_channels
		   FROM fastlane.util.CustomerAccount ca 
		   LEFT JOIN fastlane.info.CommunicationPreference cp ON cp.customerId = ca.customerId 
		   LEFT JOIN fastlane.comm.EnuCommunicationChannel ecc ON ecc.communicationChannelId = cp.communicationChannelId
		  WHERE cp.communicationCategoryId = 1	-- statements
		    AND cp.optInFlag = 1				-- turned on
		GROUP BY ca.customerAccountId
  		) cm ON cm.customerAccountId = d.customerAccountId
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = d.customerAccountId
  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info vaci ON vaci.customerAccountId = d.customerAccountId
 WHERE  d.unsent_comm_cnt + d.returned_cnt = d.statement_cnt
--   AND  vca.acct_short_code LIKE 'PAYBYMAIL%'
--   AND  d.customerAccountId = 1487102
;

SELECT * FROM fastlane.xact.TransactionsBalance tb WHERE tb.customerAccountId = 1487102;

-- communication preferences
SELECT  ech.description AS comm_channel
		,ecc.description AS comm_category
		,cp.optInFlag 
		,cp.*
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.info.CommunicationPreference cp ON cp.customerId = ca.customerId 
  JOIN  fastlane.comm.EnuCommunicationCategory ecc ON ecc.communicationCategoryId = cp.communicationCategoryId 
  JOIN  fastlane.comm.EnuCommunicationChannel ech ON ech.communicationChannelId = cp.communicationChannelId 
 WHERE  ca.customerAccountId = 1487102
   AND  cp.isDefunct = 0;

/********** NEGATIVE BALANCE ACCTS WITH FAILED REPLENISHMENT **********/
-- 2022-10-28 Tim Arnold:  how many:
--     Prepaid Accounts with a negative balance greater than 30 days
--     PAYG accounts with negative balance greater than 30 days and a failed CC payment method
--   which have opted in to text messaging in their customer preferences
SELECT  vca.acct_type_desc 
		,cm.sms_comm_categories
		,count(*) AS acct_cnt
--;SELECT  count(*)
  FROM  fastlane.xact.TransactionsBalance tb
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = tb.customerAccountId 
  LEFT  JOIN (SELECT ca.customerAccountId
  				,string_agg(ecc.description, ',') WITHIN GROUP (ORDER BY ecc.description) AS sms_comm_categories
		   FROM fastlane.util.CustomerAccount ca 
		   LEFT JOIN fastlane.info.CommunicationPreference cp ON cp.customerId = ca.customerId 
		   LEFT JOIN fastlane.comm.EnuCommunicationCategory ecc ON ecc.communicationCategoryId = cp.communicationCategoryId 
		  WHERE cp.communicationChannelId = 2	-- sms
		    AND cp.optInFlag = 1				-- turned on
		GROUP BY ca.customerAccountId
  		) cm ON cm.customerAccountId = tb.customerAccountId
 WHERE  tb.customerAccountId >= 1000	-- non-system accounts
   AND  tb.isCurrentActivity = 1
   AND  tb.signValue = -1	-- negative balance 
   AND  datediff(day, tb.lastSignChangeDate, getdate()) > 30	-- older than 30 days
   AND  ((vca.account_type_id BETWEEN 2 AND 3)	-- prepaid
    OR  (vca.account_type_id BETWEEN 4 AND 5) AND fastlane.util.F_AccountFlag_CreditCardFailed(tb.customerAccountId) = 1)	-- pay-as-you-go, with failed CC payment
--   AND  cm.comm_categories IS NOT NULL
GROUP BY vca.acct_type_desc 
		,cm.sms_comm_categories
ORDER BY vca.acct_type_desc 
		,cm.sms_comm_categories;

-- check for if CC replenishment failed
SELECT  fastlane.util.F_AccountFlag_CreditCardFailed(11127);

/********** NEGATIVE BALANCE ACCTS WITH LAST CONTACT DATE **********/
-- 2022-10-28 help Marie with script for Tyler - add "last contact date" as latest sent (and not returned) toll bill
SELECT  ca.customerAccountId AS AcctNumFL
		,vca.account_type_id AS AcctType
		,vca.acct_short_code AS AcctTypeDescript
		,vas.status AS acct_status
		,bill.latest_received_bill_date
		,tb.amount AS AcctBalAsOf20221024
		,convert(datetime2, tb.LastSignChangeDate, 120) AS AcctBalSignLastChanged
		,tb.lastSignChangeDate AS AcctInNegBalAsOf
		,datediff(day, tb.lastSignChangeDate, getdate()) AS DaysSinceSignLastChanged --CONVERT(DateTime2, 'YYYY-MM-DD', 20))
		,convert(datetime2, tb.updatedAt, 120) AS AcctBalLastUpdatedAt
		,tb.updatedAt AS AcctBalSignLastUpdated
		,datediff(day, tb.updatedAt, GETDATE()) AS DaysSinceBalLastUpdated --(CONVERT(DateTime2, 'YYYY-MM-DD', 20))
		--,MAX(ifs.fundingSourceId) AS AcctFundingSource
		,ifs.paymentTypeid AS AcctPaymentType
		,ifs.paymentTypeCode AS AcctPaymentTypeDescript  
		,ifs.PaymentTypeClassid AS AcctPaymentTypeClass
		,ifs.paymentTypeClassCode AS AcctPaymentTypeClassDescript
		,convert(datetime2,ifs.expirationDate,120) AS AcctPaymentTypeExpire
		,datediff(day, ifs.expirationDate, getdate()) AS DaysSincePymtTypeExpired
		,ifs.maxAutoReplenishAmount AS MaxReplenishAmt
		,ifs.maxAutoReplenishAttmpts AS MaxAttemptsToReplenish
		,vaci.isCompany
		,vaci.firstName
		,vaci.lastNameOrCompanyName
		,vaci.mobile_phone
		,vaci.email_address
		,vaci.address1
		,vaci.address2
		,vaci.city
		,vaci.state
		,vaci.postalCode
		,vaci.country
		,vaci.address_source
  FROM  fastlane.util.CustomerAccount ca
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId
  LEFT  JOIN fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = ca.customerAccountId AND tb.isCurrentActivity = 1
  LEFT  JOIN Fastlane.Payment.IV_FundingSources ifs ON ifs.customerId = ca.customerId
--LEFT  JOIN fastlane.xact.V_Statement vs ON vs.customerAccountId = ca.customerAccountId
--LEFT  JOIN WSDOT_STAGE.util.v_trip_detail_base tdb ON tdb.customerAccountid  = ca.customerAccountId
  LEFT  JOIN WSDOT_Stage.Util.v_acct_contact_info vaci ON vaci.customerAccountId = ca.customerAccountId
  LEFT  JOIN WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ca.customerAccountId
  LEFT  JOIN (SELECT i.customerAccountId
					,max(i.postingDate) AS latest_received_bill_date
			   FROM fastlane.xact.invoice i
			  WHERE i.communicationId IS NOT NULL	-- communication about statement was sent
			    AND fastlane.util.F_IsMailReturned(i.storageFileId) = 0		-- was not returned
			GROUP BY i.customerAccountId
			) bill ON bill.customerAccountId = ca.customerAccountId
 WHERE  tb.signValue = -1 -- -1 is a Negative Balance Status, will only pull negative Accts
   AND  vca.account_type_id IN ('8','14','15','16','17','18','22')
   AND  ca.customerAccountId >= 1000 -- Elimination of System-Processing Accts
ORDER BY ca.customerAccountId;

/********** SOLD VEHICLE DISPUTES FOR BOBBIE-JEAN **********/
-- 2022-11-02 look into sold vehicles with and without new buyer info for Bobbie-Jean
-- need a list of all trips that are attached to Vehicle Sold WITH new buyer info and Vehicle Sold WITHOUT new buyer info where the disputed 
-- trip (Dispute Mitigation Status) has the status of "Disputed Eligible Resolved - Reverse" or "Disputed Eligible Days Ovr Resolved - Reverse"

-- reason codes R-SOLDR - with new buyer info
-- D-SOLDF - without new buyer info

-- include info: trip_id, transaction datetime, toll plaza, toll amount, pass/plate, acct_number, case_number
-- too slow to link to case info, go in other direction searching by case first
SELECT  tdf.trip_id 
		,tdf.laneTime AS trip_date
		,tdf.exit_plaza 
		,tdf.latest_amount AS toll_amount
		,tdf.identifiers
		--,CASE WHEN tdf.transponderId IS NOT NULL THEN concat(fa.kapschCode,'-',t.transponderNumber) AS pass_id
		,tdf.customerAccountid 
  FROM  WSDOT_STAGE.util.v_trip_detail_full tdf 
--  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = tdf.licensePlateId 
--  JOIN  fastlane.vehicle.Transponder t ON t.transponderId = tdf.transponderId 
--  JOIN  fastlane.util.FacilityAgency fa ON fa.facilityAgencyId = t.facilityAgencyId  
 WHERE  tdf.entry_reason IN ('R-SOLDR', 'D-SOLDF')
;

-- sample case 6585808, acct 9853633
-- look at CaseLedgerEntry to link to trip
SELECT  tle.tollId AS trip_id
		,tp.laneTime AS trip_date
		,vfl.wsdot_facility AS facility
		,vfl.explazaCode AS exit_plaza 
		,vts.amount AS toll_amount
		,CASE WHEN rt.licensePlateId IS NULL AND rt.transponderId IS NULL THEN 'no_lp_or_pass'
        	  WHEN rt.licensePlateId IS NULL AND rt.transponderId IS NOT NULL THEN 'pass_only'
        	  WHEN rt.licensePlateId IS NOT NULL AND rt.transponderId IS NULL THEN 'lp_only'
        	  WHEN rt.licensePlateId IS NOT NULL AND rt.transponderId IS NOT NULL THEN 'both_lp_and_pass'
        	END AS identifiers
		--,CASE WHEN tdf.transponderId IS NOT NULL THEN concat(fa.kapschCode,'-',t.transponderNumber) AS pass_id
		,vts.customerAccountid 
		--,eer.shortCode
		--,cqct.caseQueueId 
		,uc.ucaseId AS case_number
--SELECT  count(*)
  FROM  fastlane.usercase.[UCase] uc
  JOIN  fastlane.usercase.CaseQueueCaseType cqct ON cqct.caseTypeId = uc.caseTypeId
  JOIN  fastlane.usercase.CaseLedgerEntry cle ON cle.ucaseId = uc.ucaseId
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = cle.transactionId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId 
  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
  JOIN  WSDOT_STAGE.wsdot.v_facility_lanes vfl ON vfl.roadGantryId = tp.roadGantryId 
  JOIN  fastlane.rtoll.Toll rt ON rt.TollID = tle.tollId 
--  JOIN  fastlane.xact.ledger l ON l.transactionId = cle.transactionId 
  LEFT  JOIN  fastlane.xact.Annotation a ON a.annotationid = tle.EndAnnotationid 
  LEFT  JOIN  fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
  --JOIN  WSDOT_STAGE.util.v_trip_detail_full tdf ON tdf.trip_id = tle.tollId 
-- WHERE  uc.
 WHERE  cqct.caseQueueId = '1006'
   AND  eer.shortCode IN ('R-SOLDR', 'D-SOLDF')
--   AND  uc.ucaseId = 6585808;
;

-- simple test
SELECT  tle.*, eer.*
  FROM  fastlane.xact.TollLedgerEntry tle 
  LEFT  JOIN fastlane.xact.Annotation a ON a.annotationid = tle.endAnnotationId 
  LEFT  JOIN fastlane.xact.EnuEntryReason eer ON eer.entryReasonId = a.entryReasonId 
 WHERE  tle.tollId = 244920537;

-- ledgerEntryId is not always equal to transaction Id
SELECT * FROM fastlane.usercase.CaseLedgerEntry cle WHERE cle.ledgerEntryId <> transactionId;
-- 296172947, 297287894

/********** ESCALATIONS WORK SUMMARY **********/
-- 2022-11-02 create detail and summary data by various categories for communications for escalation for Tyler
-- this is for testing; see procedure in report_arch.sql

-- conditions:
--   negative balance (over 30 days for PAYG and PBM)
--   failed CC for PAYG only
-- filters:
--   account type
--   balance amount (0-200, 200-5000, or 5000+)
--   valid email (MAIN contact, not null, valid format, not returned)
--     for valid email, latest received statement (between May-Nov 2022; earlier)

-- summarize accounts for which trip is unpaid and is INVTOLL2 OR NOCP; include all, but flag accounts that show in other list later
-- get account number for unpaid INVTOLL2 and NOCP trips - use fastlane.xact.V_TransactionSummary instead of my trip_state for speed
-- look at isCurrentActivity - 146,838 vs 148,116 from last month for NOCP - seems reasonable?
SELECT  vts.transactionTypeId 
		,count(DISTINCT tle.tollId) AS trip_cnt
		,count(*) AS cnt
  FROM  fastlane.xact.V_TransactionSummary vts 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
  JOIN  fastlane.lance.trip t ON t.tripId = tle.tollId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  vts.isCurrentActivity = 1	-- current
   AND  vts.isUnpaid = 1	-- not paid
   AND  vts.transactionTypeId IN (23, 70, 83)
   AND  tp.laneTime >= convert(datetime, '2021-01-01')
GROUP BY vts.transactionTypeId;

-- get old VTOLL2 and NOCP accounts with trip counts
-- NOTE: VTOLL2 takes priority if an account has both
-- 20221107: 136,508 in 53 seconds
SELECT  vts.customerAccountid
		,vts.transactionTypeId 
		,count(*) AS trip_cnt
--  INTO  WSDOT_STAGE.tmp.neg_bal_vt2_nocp_accts
  FROM  fastlane.xact.V_TransactionSummary vts 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
 WHERE  vts.isCurrentActivity = 1	-- current
   AND  vts.isUnpaid = 1	-- not paid
   AND  vts.transactionTypeId IN (23, 70, 83)
--   AND  vts.customerAccountid = 6819550
GROUP BY vts.customerAccountid
		,vts.transactionTypeId;

-- look at overlap with current
SELECT  *
--SELECT  nb.acct_category, nb.amount_class, count(*)
  FROM  WSDOT_STAGE.tmp.neg_bal_vt2_nocp_accts leg
--  JOIN  WSDOT_STAGE.tmp.neg_bal_vt2_nocp_accts leg2 ON leg2.customerAccountid = leg.customerAccountid AND leg2.transactionTypeId = 83
  LEFT JOIN  WSDOT_STAGE.rpt.x_negative_balance_detail nb ON nb.customerAccountId = leg.customerAccountid 
-- WHERE  leg.transactionTypeId = 23
 WHERE  nb.customerAccountId IS NULL
--GROUP BY nb.acct_category, nb.amount_class
;

--***** rest of code incorporated into procedure *****

-- test running procedure (~8-15 minutes to run)
-- EXEC WSDOT_STAGE.util.usp_escalation_golive;

-- other testing
SELECT count(*) FROM fastlane.xact.TransactionsBalance tb WHERE isCurrentActivity = 1 AND signValue = -1;
SELECT max(datediff(day, tb.lastSignChangeDate, getdate())) FROM fastlane.xact.TransactionsBalance tb WHERE isCurrentActivity = 1 AND signValue = -1;
SELECT * FROM fastlane.xact.TransactionsBalance tb WHERE customerAccountId = 6819550;

SELECT * FROM fastlane.info.EnuAccountType eat 
WHERE NOT (eat.shortCode LIKE 'PAYBYMAIL%' OR eat.shortCode LIKE 'ZBA%');

/********** TAG PREVIOUSLY ASSIGNED TO ACCT? **********/
-- 2022-11-04 from Ami, see if there was ever a tag assigned to 10868590
SELECT * FROM fastlane.rtoll.TransponderCustomerAccount tca WHERE tca.customerAccountId = 10868590;

/********** DISPOSITION ISSUE **********/
-- 2022-11-14 disposition summary is incorrect - shows ~1.3 million trips instead of ~5.7 million

-- get total trips in ETAN for the month - seems correct, issue is in summary then (5684233)
SELECT  count(*) 
  FROM  fastlane.lance.Trip t 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  tp.laneTime BETWEEN convert(datetime, '2022-10-01') AND convert(datetime, '2022-11-01');

-- try using isCurrentActivity - count does not match?
SELECT  ett.shortCode
		,count(*) 
  FROM  fastlane.xact.V_TransactionSummary vts 
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = vts.transactionId AND tle.ledgeraccountId = vts.customerAccountid 
  JOIN  fastlane.xact.EnuTransactionType ett ON ett.transactionTypeId = vts.transactionTypeId 
  JOIN  fastlane.lance.Trip t ON t.tripId = tle.tollLedgerEntryId 
  JOIN  fastlane.lance.tripPassage tp ON tp.tripPassageId = t.tripPassageId 
 WHERE  vts.isCurrentActivity = 1
   AND  tp.laneTime BETWEEN convert(datetime, '2022-10-01') AND convert(datetime, '2022-11-01')
GROUP BY ett.shortCode;

-- summary: issue was with type mis-match on UNION ALL with new manual trip data; fixed now

/********** ACCOUNT FUNDING SOURCES **********/
-- 2022-11-15 accounts with cc or ach funding source for Marie
SELECT  vca.acct_type_desc
		/*,CASE WHEN cafs.cc_sources > 0 AND cafs.ach_sources > 0 THEN 'ach_and_cc'
			  WHEN cafs.cc_sources > 0 THEN 'cc_only'
			  WHEN cafs.ach_sources > 0 THEN 'ach_only'
			  ELSE 'none' END AS 'funding_sources'*/
		,sum(CASE WHEN cafs.cc_sources > 0 OR cafs.ach_sources > 0 THEN 1 ELSE 0 END) AS ach_or_cc_funding
		,count(*) AS acct_count
  FROM  fastlane.util.CustomerAccount ca 
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
--  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_info ci ON ci.customerAccountId = ca.customerAccountId
  LEFT  JOIN (SELECT ca.customerAccountId 
				,sum(CASE WHEN fs.paymentTypeid IN (1,2,3,4) THEN 1 ELSE 0 END) AS cc_sources
				,sum(CASE WHEN fs.paymentTypeid IN (5,6,8,20) THEN 1 ELSE 0 END) AS ach_sources
		   FROM fastlane.util.CustomerAccount ca 
		   JOIN fastlane.payment.FundingSource fs ON fs.customerId = ca.customerId 
		GROUP BY ca.customerAccountId 
		) cafs	-- this gives a count of ach and cc payment sources by customer account
		 ON cafs.customerAccountId = ca.customerAccountId 
 WHERE  ca.isDefunct = 0	-- assume you only want non-defunct accounts, but you may want to restrict to non-closed too
   AND  vca.acct_short_code NOT LIKE 'PAYBYMAIL%'
GROUP BY vca.acct_type_desc
		/*,CASE WHEN cafs.cc_sources > 0 AND cafs.ach_sources > 0 THEN 'ach_and_cc'
			  WHEN cafs.cc_sources > 0 THEN 'cc_only'
			  WHEN cafs.ach_sources > 0 THEN 'ach_only'
			  ELSE 'none' END*/
ORDER BY vca.acct_type_desc
		,count(*) DESC;

/********** STATEMENTS SENT ON CLOSED ACCOUNTS **********/
-- 2022-11-21 statements generated on closed accounts (like mine!)
-- use case expected transition state as closing date?
SELECT  vas.status
		,vca.acct_type_desc
		,ecc.shortCode AS comm_channel
		--,ccd.CycleDay AS cycle_day
		,i.postingDate 
		,count(i.communicationId) AS comms_sent
		,count(*) statements_generated
--SELECT  count(DISTINCT i.customerAccountId)		-- 4172592 vs 4124300
  FROM  fastlane.xact.Invoice i 
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = i.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = i.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = i.customerAccountId 
  LEFT  JOIN fastlane.comm.Communication c ON c.communicationId = i.communicationId 
  LEFT  JOIN fastlane.comm.EnuCommunicationChannel ecc ON ecc.communicationChannelId  = c.communicationChannelId 
 WHERE  i.postingDate >= convert(date,'2022-09-22')
--   AND  vas.statusId = 'CLOSED'
--   AND  vas.expectedStateTransitionDate < i.postingDate		-- statement after account closure
--   AND  i.customerAccountId = 1143778	-- 2898500
GROUP BY vas.status
		,vca.acct_type_desc 
		,ecc.shortCode 
		--,ccd.CycleDay 
		,i.postingDate 
ORDER BY vas.status
		,vca.acct_type_desc 
		,ecc.shortCode 
;

-- preference detail for closed accounts
-- re-run 2022-12-08
WITH s AS (SELECT i.customerAccountId 
		,min(i.postingDate) AS earliest_closed_statement_date
		,max(i.postingDate) AS latest_closed_statement_date
		,count(i.communicationId) AS comms_sent
		,count(*) AS statements_generated
  FROM  fastlane.xact.Invoice i 
  JOIN  WSDOT_STAGE.util.v_account_status vas1
		 ON vas1.customerAccountId = i.customerAccountId
		AND vas1.expectedStateTransitionDate < i.postingDate	-- statement after account closure
 WHERE i.postingDate >= convert(date,'2022-08-01')	-- only recent statements, ignore statements generated during transition
GROUP BY i.customerAccountId
), c AS (SELECT ca.customerAccountId
		,string_agg(ech.description, ',') WITHIN GROUP (ORDER BY ech.description) AS statement_comm_channel_prefs
  FROM  fastlane.util.CustomerAccount ca 
  LEFT  JOIN fastlane.info.CommunicationPreference cp ON cp.customerId = ca.customerId 
  LEFT  JOIN fastlane.comm.EnuCommunicationCategory ecc ON ecc.communicationCategoryId = cp.communicationCategoryId 
  LEFT  JOIN fastlane.comm.EnuCommunicationChannel ech ON ech.communicationChannelId = cp.communicationChannelId
 WHERE  cp.communicationCategoryId = 1	-- statements
   AND  cp.optInFlag = 1				-- turned on
GROUP BY ca.customerAccountId
)
SELECT  ca.customerAccountId 
		,vas.status
		,vas.expectedStateTransitionDate
		,c.statement_comm_channel_prefs
		,s.earliest_closed_statement_date
		,s.latest_closed_statement_date
		,s.comms_sent
		,s.statements_generated
--SELECT count(*)
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ca.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
  LEFT  JOIN c ON c.customerAccountId = ca.customerAccountId 
  LEFT  JOIN s ON s.customerAccountId = ca.customerAccountId 
 WHERE  vas.statusId = 'CLOSED'
   AND  c.statement_comm_channel_prefs IS NOT NULL
--   AND  ca.customerAccountId = 1143778
;

/********** ACCOUNT PLATES AND TAGS **********/
-- 2022-12-13 count of plates and tags by account and type
SELECT  ca.customerAccountId
		,vca.acct_type_desc AS account_type
		,count(lpca.licensePlateId) AS plate_count
		,count(tca.transponderId) AS tag_count
  FROM  fastlane.util.CustomerAccount ca  
  LEFT  JOIN fastlane.rtoll.LicensePlateCustomerAccount lpca
		  ON lpca.customerAccountId = ca.customerAccountId
		 AND fastlane.config.getpostingdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate
  LEFT  JOIN fastlane.rtoll.TransponderCustomerAccount tca ON tca.customerAccountId = ca.customerAccountId
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId 
GROUP BY ca.customerAccountId
		,vca.acct_type_desc AS account_type
ORDER BY ca.customerAccountId;

/********** NEGATIVE BALANCE TRIPS AND TOTALS **********/
-- 2022-12-13 summarize trips and totals by escalation category for Tyler
-- getting the total is easy, but getting number of trips is harder - look at unpaid trips on the account?
-- get total amounts by category
SELECT  nb.category_id 
		,count(*) AS acct_count
		,sum(tb.amount) AS total_balance_due
  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = nb.customerAccountId AND tb.isCurrentActivity = 1
 WHERE category_id <> '0'
GROUP BY nb.category_id
ORDER BY nb.category_id;

-- add trip counts from etan for negative balance accounts by category id
WITH trips AS (		-- trip detail
	SELECT  vts.customerAccountId
			,vts.amount
	  FROM  WSDOT_STAGE.util.trip_state ts
	  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.ledgertransactionId = ts.last_toll_txn_id 
	  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = ts.last_toll_txn_id AND vts.customerAccountid = tle.ledgeraccountId
	 WHERE  vts.isUnpaid = 1
	 --  AND  tdb.laneTime < convert(date, '2022-11-13 10:00:00')	-- trips over 30 days old (need to join trip and tripPassage to get)
), tsm AS (			-- trip summary
	SELECT  nb.category_id
			,count(t.amount) AS trip_count
			,sum(t.amount) AS total_toll_amount
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb 
	  JOIN  trips t ON t.customerAccountId = nb.customerAccountId
	 WHERE  nb.category_id <> '0'
	GROUP BY nb.category_id
), acs AS (		-- account summary
	SELECT  nb.category_id 
			,count(*) AS acct_count
			,sum(tb.amount) AS total_balance_due
	  FROM  WSDOT_STAGE.rpt.x_negative_balance_detail nb
	  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = nb.customerAccountId AND tb.isCurrentActivity = 1
	 WHERE category_id <> '0'
	GROUP BY nb.category_id
)
SELECT  tsm.category_id
		,acs.acct_count
		,tsm.trip_count
		,tsm.total_toll_amount
		,acs.total_balance_due
  FROM  tsm
  JOIN  acs ON acs.category_id = tsm.category_id
ORDER BY tsm.category_id;

/********** NEGATIVE BALANCE SUMMARY BY LAST TXN DATE **********/
-- 2022-12-22 summarize last txn for negative balance accounts for Marie, Russ, and Tyler

-- summarize negative accounts by latest transaction quarter
SELECT  WSDOT_STAGE.util.f_get_quarter(t.latest_trip) AS trip_quarter
		,nbr.category_id 
		,count(*) AS account_count
		,sum(nbr.balance_due) AS total_balance_due
  FROM  WSDOT_STAGE.rpt.v_negative_balance_reference_group nbr
  LEFT  JOIN WSDOT_STAGE.util.v_acct_latest_trip t ON t.customer_account_id = nbr.customerAccountId
GROUP BY WSDOT_STAGE.util.f_get_quarter(t.latest_trip)
		,nbr.category_id
ORDER BY WSDOT_STAGE.util.f_get_quarter(t.latest_trip)
		,nbr.category_id;

-- find login: account 356957 (KCM rideshare) - last login 11/15/2022 12:28 pm
SELECT  cca.updatedAt 
  FROM  fastlane.info.v_ah_CustomerCWPLoginAttempts cca 
  JOIN  fastlane.info.Customer c ON c.CustomerCredentialsId = cca.customerCredentialsId 
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
 WHERE  ca.customerAccountId = 356957
ORDER BY updatedAt DESC;

SELECT fastlane.util.F_AcctCond_HasCWPLogin(356957, getdate());		-- 1 is yes, 0 is no
SELECT fastlane.util.F_AcctCond_HasLoginCredentials(356957, getdate());		-- 1 is yes, 0 is no

-- detailed query with additional info - account creation date, latest login, and latest customer case date
WITH uc AS (
	SELECT  cca.customerAccountId 
			,max(u.created) AS latest_case_open
	  FROM  fastlane.usercase.CaseCustomerAccount cca 
	  JOIN  fastlane.usercase.[UCase] u ON u.ucaseId = cca.ucaseId 
	GROUP BY cca.customerAccountId
), ll AS (
	SELECT  ca.customerAccountId 
			,cca.updatedAt AS last_login_date
	  FROM  fastlane.info.CustomerCWPLoginAttempts cca 
	  JOIN  fastlane.info.Customer c ON c.CustomerCredentialsId = cca.customerCredentialsId 
	  JOIN  fastlane.util.CustomerAccount ca ON ca.customerId = c.customerId
)
SELECT  nbr.customerAccountId 
		,nbr.category_id 
		,nbr.acct_type_desc 
		,nbr.acct_status 
		,ca.postingDate AS acct_creation_date
		,nbr.balance_due 
		,t.latest_trip 
		,uc.latest_case_open
		,ll.last_login_date
		,fastlane.util.F_AcctCond_HasCWPLogin(nbr.customerAccountId, getdate()) AS has_cwp_login
		,fastlane.util.F_AcctCond_HasLoginCredentials(nbr.customerAccountId, getdate()) AS has_login_credentials
  FROM  WSDOT_STAGE.rpt.v_negative_balance_reference_group nbr
  JOIN  fastlane.util.CustomerAccount ca ON ca.customerAccountId = nbr.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_latest_trip t ON t.customer_account_id = nbr.customerAccountId
  LEFT  JOIN uc ON uc.customerAccountId = nbr.customerAccountId
  LEFT  JOIN ll ON ll.customerAccountId = nbr.customerAccountId;

/********** MONTHLY TRIP VS POST DATE SUMMARY **********/
-- 2022-12-22 monthly post date summary for Jen K.

-- does post in trip table match first VTS post?
-- no, but geneally close enough; also, when truncating to month it would be the same
SELECT  t.tripId
		,t.created 
		,vts.postingDate 
  FROM  fastlane.lance.trip t
  JOIN  fastlane.xact.TollLedgerEntry tle ON tle.tollId = t.tripId 
  JOIN  fastlane.xact.V_TransactionSummary vts ON vts.transactionId = tle.ledgertransactionId AND vts.customerAccountid = tle.ledgeraccountId
 WHERE  t.tripId > 300000000;

SELECT  WSDOT_STAGE.util.f_get_month(tps.trip_date) AS trip_month
		,WSDOT_STAGE.util.f_get_month(tps.posting_date) AS posting_month
		,tps.roadway
		,CASE WHEN WSDOT_STAGE.util.f_get_month(tps.trip_date) = WSDOT_STAGE.util.f_get_month(tps.posting_date) THEN 1 ELSE 0 END AS same_month
		,sum(tps.dup_flag) AS duplicate_trips
		,sum(CASE WHEN tps.dup_flag = 0 THEN tps.sum_roadside_amount ELSE 0 END) AS sum_roadside_amount 
		,sum(tps.trip_count) AS trip_count
  FROM  WSDOT_STAGE.rpt.v_trip_posting_summary tps
 WHERE  tps.trip_date >= convert(datetime2, '2021-07-01')
GROUP BY WSDOT_STAGE.util.f_get_month(tps.trip_date)
		,WSDOT_STAGE.util.f_get_month(tps.posting_date)
		,tps.roadway
		,CASE WHEN WSDOT_STAGE.util.f_get_month(tps.trip_date) = WSDOT_STAGE.util.f_get_month(tps.posting_date) THEN 1 ELSE 0 END
ORDER BY WSDOT_STAGE.util.f_get_month(tps.trip_date)
		,WSDOT_STAGE.util.f_get_month(tps.posting_date)
		,tps.roadway;

/********** PBM SYSTEM ACCOUNTS WITH FEES BUT NO TOLLS **********/
-- 2022-12-28 request for Ami
Active License Plate/State

-- use lastest group for this query
-- 20221228: 777,754 system accounts
WITH p AS (
	SELECT  lpca.customerAccountId 
			,count(*) AS plate_count
			,string_agg(concat(lpj.shortCode,'-',lp.lpnumber), ',') WITHIN GROUP (ORDER BY lp.licensePlateId) AS license_plates
	  FROM  fastlane.rtoll.LicensePlateCustomerAccount lpca 
	  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = lpca.licensePlateId 
	  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
	 WHERE  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate		-- only currently assigned plates
	GROUP BY lpca.customerAccountId
)
SELECT  nb.customerAccountId 
		,nb.acct_type_desc 
		,nb.acct_status 
		,nb.balance_due
		,p.plate_count
		,p.license_plates
--SELECT  count(*)
  FROM  WSDOT_STAGE.rpt.v_negative_balance_latest_group nb
  LEFT  JOIN WSDOT_STAGE.util.v_acct_latest_trip t ON t.customer_account_id = nb.customerAccountId
  LEFT  JOIN p ON p.customerAccountId = nb.customerAccountId 
 WHERE  nb.acct_type_desc LIKE '%(System)%'
   AND  t.latest_trip IS NULL
;

-- NOTE: does lpjurisdiction ever differ from state or country?
SELECT  *
  FROM  fastlane.util.EnuLpJurisdiction lpj 
  JOIN  fastlane.locale.EnuState es ON es.stateId = lpj.stateId 
 WHERE  lpj.shortCode <> es.shortCode OR lpj.countryId <> es.countryId;

-- alternate approach - look for unpaid fees in the ledger? note needed for now

/********** PBM SYSTEM ACCOUNTS WITH LONG PLATES **********/
-- 2023-01-04 get list of PBM system accounts with plate char length over 9 characters for Tim and Ami
SELECT  ca.customerAccountId 
		,vca.acct_type_desc 
		,vas.status AS acct_status
		,len(lp.lpnumber) AS lp_length
		,concat(lpj.shortCode,'-',lp.lpnumber) AS state_plate
--SELECT  count(*)
  FROM  fastlane.util.CustomerAccount ca
  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ca.customerAccountId
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ca.customerAccountId
  JOIN  fastlane.rtoll.LicensePlateCustomerAccount lpca ON lpca.customerAccountId = ca.customerAccountId
  JOIN  fastlane.util.LicensePlate lp ON lp.licensePlateId = lpca.licensePlateId 
  JOIN  fastlane.util.EnuLpJurisdiction lpj ON lpj.lpJurisdictionId = lp.lpJurisdictionId
 WHERE  getdate() BETWEEN lpca.effectiveStartDate AND lpca.effectiveExpirationDate		-- only currently assigned plates
   AND  vca.acct_type_desc LIKE '%(System)%'	-- only PBM system accounts
   AND  len(lp.lpnumber) >= 9					-- plate character length greater than 9
;

/********** GOODTOGO EMAIL ADDRESS LIST **********/
-- 2023-01-06 get list of Good To Go active account email addresses for Chris Foster
-- NOTE: v_acct_contact_info already excludes defunct addresses (including unknown@etantolling.com)
--   1,586,277 g2g accounts of which 1,350,678 are active and 1,342,285 have non-NULL email addresses
--   8,392 accounts have no main email, with 715 having a secondary email
WITH em AS (
	SELECT  ca.customer_account_id 
			,coalesce(aci.email_address, se.secondary_email_address) AS email_address
--	SELECT  count(*)
	  FROM  WSDOT_STAGE.util.v_customer_account ca 
	  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ca.customer_account_id 
	  JOIN  WSDOT_STAGE.util.v_acct_contact_info aci ON aci.customerAccountId = ca.customer_account_id 
	  LEFT  JOIN WSDOT_STAGE.util.v_acct_contact_secondary_email se ON se.customerAccountId = ca.customer_account_id 
	 WHERE  ca.account_type_id BETWEEN 2 AND 5	-- prepaid and pay-as-you-go accounts
	   AND  vas.statusId = 'ACTIVE'				-- only active accounts
	   AND  (aci.email_address_id IS NOT NULL OR se.secondary_email_address_id IS NOT NULL)	-- ensure that primary or secondary address is present
)
--SELECT  DISTINCT em.email_address
SELECT  em.customer_account_id
		,em.email_address
  FROM  em
 WHERE  em.email_address NOT LIKE '%@%.%'		-- simple validity check
;

/********** RETURNED MAIL FOR JEFF **********/
-- 2023-01-23 get contact info for accounts with returned mail
-- account, email (bad), name, address, phone)
-- 2938076 with email
-- really slow - 20 min for 100 rows
-- 2023-01-31: re-run adding last toll trip date, acct status, acct type, and last login date to CWP
--      would be faster to be able to use T_CustomerAccountList, but custom types cannot be shared across databases: https://stackoverflow.com/q/14504756/

-- instead, refactor fastlane.util.F_AccountFlag_ReturnedEmail_Many to save a table of accts with returned email:
-- 2023-02-01: ~15 minutes to run
WITH lc AS (	-- gets latest email queue id BY account
	SELECT  cm.customeraccountid
			,max(q.emailQueueId) AS max_qid
	  FROM  fastlane.comm.IV_CustomerCommunications cm WITH(noexpand)
	  JOIN  fastlane.comm.EmailQueue q ON q.communicationId = cm.communicationId 
	 WHERE  cm.communicationChannelCode = 'EMAIL'
	   AND  q.isRequested = 1
	GROUP BY cm.customeraccountid
), em AS (		-- gets main email address for each account
	SELECT  cac.customerAccountId
			,ea.emailAddress
	  FROM  fastlane.util.IV_CustomerAccountContacts cac with(noexpand)
	  JOIN  fastlane.info.EmailAddress ea ON (ea.emailAddressId = cac.emailAddressId AND ea.emailAddressId != 0 AND ea.isDefunct = 0)
     WHERE  cac.contacttype = 'MAIN'
)	-- below gets returned email based ON email queue status
SELECT  lc.customerAccountId
		,1 AS returned_email
  INTO  WSDOT_STAGE.tmp.returned_email_accts
  FROM  lc 
  JOIN  em ON em.customerAccountId = lc.customerAccountId
CROSS APPLY fastlane.monitor.F_GetEmailQueueStatus(lc.max_qid) v
  JOIN fastlane.comm.EmailCommEventRes res ON res.emailCommEventResId = v.emailCommEventResId 
  LEFT  JOIN fastlane.comm.EnuEmailCommEventError err ON err.emailCommEventErrorId = res.emailCommEventErrorId AND err.shortCode NOT IN('AUTH_ERROR','INVALID_CONFIGURATION','VALIDATION_ERROR')
 WHERE  v.status NOT IN ('QUEUED','SENT','PROCESSED','DELIVERED')
   AND  err.emailCommEventErrorId IS NULL 
   AND  em.emailAddress = v.toAddress;

-- investigate table
SELECT count(*), sum(returned_email) FROM WSDOT_STAGE.tmp.returned_email_accts;

-- now use temp table to get returned mail
SELECT  vas.status 
		,vca.acct_type_desc 
		,valt.latest_trip 
		,ll.last_cwp_login_date
		,vaci.*
		,coalesce(rm.returned_email, 0) AS retured_email_flag
--SELECT  count(*)
  FROM  WSDOT_STAGE.util.v_acct_contact_info vaci 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = vaci.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_account_last_cwp_login ll ON ll.customerAccountId = vaci.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.tmp.returned_email_accts rm ON rm.customerAccountId = vaci.customerAccountId
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = vaci.customerAccountId 
  LEFT  JOIN WSDOT_STAGE.util.v_acct_latest_trip valt ON valt.customer_account_id = vaci.customerAccountId 
 WHERE  vaci.email_address_id IS NOT NULL
   AND  (vaci.email_address NOT LIKE '%_@%_._%'	OR rm.returned_email = 1)	-- email check
--		 OR fastlane.util.F_AccountFlag_ReturnedEmail(vaci.customerAccountId) = 1)
;

-- drop tmp table
DROP TABLE WSDOT_STAGE.tmp.returned_email_accts;

WITH k AS (SELECT 'rpavery@uw.edu' AS em)
SELECT * FROM k WHERE k.em LIKE '%_@%_._%';

SELECT fastlane.util.F_AccountFlag_ReturnedEmail(11369732);

/********** PREPAID AND PAYG CUSTOMERS RECEIVING PAPER STATEMENTS **********/
-- 2023-01-26 request from Tyler/Ami for prepaid and pay-as-you-go accounts which receive paper statements
-- 2023-01-27 modified include all account types, but only those that are active and do not have a negative balance
WITH p AS (	-- this gets "turned on" statement preferences for each account
	SELECT  ca.customerAccountId 
			,string_agg(ech.description, ',') WITHIN GROUP (ORDER BY ech.description) AS statement_comm_channel_prefs
	  FROM  fastlane.util.CustomerAccount ca 
	  JOIN  fastlane.info.CommunicationPreference cp ON cp.customerId = ca.customerId 
	  JOIN  fastlane.comm.EnuCommunicationCategory ecc ON ecc.communicationCategoryId = cp.communicationCategoryId 
	  JOIN  fastlane.comm.EnuCommunicationChannel ech ON ech.communicationChannelId = cp.communicationChannelId
	 WHERE  cp.communicationCategoryId = 1	-- statements
	   AND  cp.optInFlag = 1				-- turned on
	   AND  ca.customerAccountId >= 1000	-- non-system accounts
	   --AND  cp.communicationChannelId = 3	-- US mail
	   --AND  ca.customerAccountId = 1143778
	GROUP BY ca.customerAccountId 
)
SELECT  vca.acct_type_desc
		,vaci.*
		,p.statement_comm_channel_prefs
		,CASE WHEN p.statement_comm_channel_prefs LIKE '%email%' THEN 1 ELSE 0 END AS email_statement
--SELECT  count(*)
  FROM  p
  JOIN  fastlane.xact.TransactionsBalance tb ON tb.customerAccountId = p.customerAccountId AND tb.isCurrentActivity = 1
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = p.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_acct_contact_info vaci ON vaci.customerAccountId = p.customerAccountId 
  LEFT  JOIN  WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = p.customerAccountId
-- WHERE  vca.account_type_id BETWEEN 2 AND 5		-- prepaid and pay-as-you-go
 WHERE  p.statement_comm_channel_prefs LIKE '%US%'	-- receiving US mail
   AND  tb.amount >= 0								-- zero or positive balance
   AND  vas.statusId = 'ACTIVE'						-- active account
;

/********** UPDATE DISPOSITION SUMMARY **********/
-- 2023-01-30: added columns to util.v_trip_detail_[base,full] and chased updates to dependent objects
-- added etan_received_month to disposition, forecasting summary

-- re-run disposition as-of 20230110:
-- change trip_detail_base to use trip_state instead of v_all_trip_state, then change back
INSERT INTO WSDOT_STAGE.rpt.disposition_summary
SELECT *, cast('2023-01-10' AS date) FROM WSDOT_STAGE.rpt.v_disposition_summary vds
WHERE vds.trip_month >= convert(date, '2021-01-01');

/********** ACTIVE ACCTS ON FIRST CYCLE DATE **********/
-- 2023-02-01 detail for active accounts with cycle day on the first for Ami

SELECT  ca.customerAccountId 
		,vca.acct_type_desc
--SELECT count(*)
  FROM  fastlane.util.CustomerAccount ca 
  JOIN  fastlane.util.CustomeraccountCycleDay ccd ON ccd.customeraccountid = ca.customerAccountId 
  JOIN  WSDOT_STAGE.util.v_account_status vas ON vas.customerAccountId = ccd.customeraccountid
  LEFT  JOIN WSDOT_STAGE.util.v_customer_account vca ON vca.customer_account_id = ccd.customerAccountId 
 WHERE  ca.customerAccountId >= 1000	-- not a system account
   AND  ca.isDefunct = 0		-- not defunct
   AND  vas.statusId = 'ACTIVE'	-- active accounts
   AND  ccd.CycleDay = 1		-- cycle on the first
ORDER BY ca.customerAccountId;

/********** TROUBLESHOOT SCHEMA CREATION ISSUE **********/
-- 2023-02-01 
USE WSDOT_STAGE;

CREATE SCHEMA xtest;
CREATE TABLE xtest.testing (id int);

SELECT * FROM sys.tables WHERE name LIKE '%testing%';
SELECT SCHEMA_NAME(7);

DROP TABLE xtest.testing;
DROP SCHEMA xtest;

/********** UNFULFILLED PASS REQUESTS **********/
-- 2023-02-06 - get detail on older unfulfilled pass reqeists for Bobby Jean
-- refactor pass fulfillment to create new util.v_pass_request view

SELECT * FROM WSDOT_STAGE.util.v_old_unfulfilled_pass_requests up ORDER BY up.request_datetime;

/********** DESC **********/
-- 2023-02-xx 

/*------------------------------------------------------------------------------
 * notes
-- Ryan's old account: 1143778

-- meeting 2021-06-23 with Mariana, Marie with reference excel file "ETAN db notes.xlsx":

lance.RoadsideRequestWABody contains all XML info for a trip from Kapsch and Transcore
roadsiderequestid

trippassageexternalreference.externalId is the id assigned by kapsch or transcore

xact.V_TransactionSummary - tollledgerentry.ledgertransaction = ts.transaction_id

isFullyAllocated = 1 means it is "allocated" but that also happens for when the customer pays

look for not pending (TPEN)
also look at transactiontypeid in column BF

can't look for NOCP certification this way, need to work backwards
look for procedure that may do the NOCP certification
Mariana suspects this procedure is missing some trips as there are more trips that should be certified
*/

USE fastlane;
-- search for objects by type
-- types: table ('U'), view ('V'), procedure ('P'), function ('FN', 'IF', 'FN', 'AF', 'FS', 'FT', 'TF')
SELECT  schema_name(schema_id) AS schema_name
		,o.*
  FROM  sys.objects o
 WHERE  o.name LIKE '%login%'
--   AND  o.[type] = 'U'
--   AND  o.[type] IN ('FN', 'IF', 'FN', 'AF', 'FS', 'FT', 'TF')
;

-- search for column name
SELECT  TABLE_SCHEMA
        ,TABLE_NAME
        ,COLUMN_NAME
  FROM  INFORMATION_SCHEMA.COLUMNS
 WHERE  COLUMN_NAME LIKE '%customercredentialsid%'
ORDER BY 1,2,3;