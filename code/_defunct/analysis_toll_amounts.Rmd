---
title: "Title"
subtitle: "Subtitle"
author: "Mike Gaunt"
date: "`r Sys.Date()`"
output: 
  github_document:
  toc: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  cache = FALSE, cache.lazy = FALSE, autodep = TRUE, warning = FALSE, 
  message = FALSE, echo = TRUE, dpi = 180,
  fig.width = 8, fig.height = 5, echo = FALSE
  )
```

<!--#general comments===========================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is [[insert description here - what it does/solve]]
#
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: [[insert brief readme here]]
#-------- [[insert brief readme here]]
#
# *please use 80 character margins
# *please go to https://pkgs.rstudio.com/flexdashboard/articles/layouts.html
# to explore the different layouts available for the flexdashboard framework
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

<!--#library set-up=============================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->
```{r}
library(tidyverse)
library(plotly)
library(data.table)
library(skimr)
library(validate)
library(DT)
library(lubridate)
```

<!--#source helpers/utilities===================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev -->
```{r}
source(here::here("code/helpers_general.r"))
source(here::here("code/helpers_plotly.r"))
source(here::here("code/helpers_DT.r"))
```

```{r}
make_comparison_df = function(data, position_list = c(1, 5)){
  tmp_names = data %>%  
    names() %>%  
    parse_number()
  
  tmp = data[[min(position_list)]] %>%  
    merge(data[[max(position_list)]], 
          by = c("roadway", "trip_date", "amount"), all = T) %>%  
    select(-starts_with("queried")) %>%  
    filter(count.x - count.y != 0) %>% 
    group_by(roadway, trip_date) %>% 
    summarise(`#_amount_levels` = n(), 
              total_changed_trips = sum(abs(count.x - count.y)),
              sum_of_changed_trips = abs(sum(count.x - count.y))) 
}
```


<!--#source data================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev 
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts -->
```{r}
toll_amounts = read_xlsx_allFiles(specifically = "toll_status_amounts") 
```

<!--#SECTION NAME===============================================================
#use this header to make demarcations/section in code [delete this line]
#short description -->

```{r}
toll_amounts %>%  
  skim()
```

## Count Comparison 
This section has tools which compare how bucket counts differe across two query dates.

Originally, this section compared the initially queried data to the most recently queried data. Changes have been made that allow for different query day pairs.

```{r}
#gets query dates from data file names
tmp_names = toll_amounts %>%  
  names() %>%  
  parse_number()

#makes index/name dictionary 
dict = data.frame(query_dates = tmp_names) %>%  
  rownames_to_column(var = "index")

positions_to_compare = c(1, 5)
data = toll_amounts
make_comparison_df(toll_amounts, c(4, 5)) %>%  
  dt_common(y = 500,
            sel = "multiple",
            pl = 10000,
            filter = "top",
            dom = "Bftipr") %>%  
  DT::formatDate(columns = 2)
```


```{r}
toll_amounts_bind = toll_amounts %>%  
  rbindlist() %>%  
  arrange(roadway, trip_date, amount) %>%  
  group_by(roadway, trip_date, amount) %>%  
  mutate(count_crrt0 = crrct0(count)) 
```

```{r}
toll_amounts_bind %>%
  mutate(d_color = case_when(count_crrt0>0~"Bad",T~"Good"),
         n_color = case_when(count_crrt0>0~1,T~0), 
         text = str_glue("")) %>% 
  plot_ly(x = ~trip_date, y = ~queried_at, z = ~n_color, text = ~text,
          type = 'heatmap',# mode = 'bars',
          transforms = list(
            list(type = 'filter', target = ~roadway, operation = '=',
                 value = unique(toll_amounts_bind$roadway)[1]
            )
          )) %>%
  layout(xaxis = make_range_select_buttons(
    "Trip Date",
    c(1, 3, 6, 12),
    rep("month", 4),
    rep("backward", 4)
  ),
  yaxis = list(title = "Queired Date"),
  updatemenus = make_menu_item(name_list = unique(toll_amounts_bind$roadway), filter_pos = 0,
                               direction = "right", x = 0, y = 1.2),
  showlegend = FALSE)
```

```{r}
toll_amounts_bind %>%
  mutate(d_color = case_when(count_crrt0>0~"Bad",T~"Good"),
         n_color = case_when(count_crrt0>0~1,T~0), 
         text = str_glue("")) %>% 
  plot_ly(x = ~trip_date, y = ~queried_at, z = ~amount, color = ~n_color, text = ~text,
          type = 'scatter',# mode = 'bars',
          transforms = list(
            list(type = 'filter', target = ~roadway, operation = '=',
                 value = unique(toll_amounts_bind$roadway)[1]
            )
          )) %>%
  layout(
  #   xaxis = make_range_select_buttons(  #   c(1, 3, 6, 12),
  #   rep("month", 4),
  #   rep("backward", 4)
  # ),
  yaxis = list(title = "Queired Date"),
  updatemenus = make_menu_item(name_list = unique(toll_amounts_bind$roadway), filter_pos = 0,
                               direction = "right", x = 0, y = 1.2),
  showlegend = FALSE)
```

```{r}
toll_amounts_bind %>%
  mutate(d_color = case_when(count_crrt0>0~"Bad",T~"Good"),
         n_color = case_when(count_crrt0>0~1,T~0)) %>% 
  filter(roadway == "99S") %>% 
  plot_ly(x = ~trip_date, y = ~queried_at, z = ~amount, color = ~n_color)
```

```{r}
toll_amounts_com %>%  
  skim()
```

```{r}
val_trip_amounts = validator(unequal_counts = count.x == count.y,
                             test = count.x <40,
          missing_amounts_oldest = !is.na(count.x),
          missing_amounts_newest = !is.na(count.y))
```

```{r}
val_toll_amounts = toll_amounts_com %>%  
  confront(val_trip_amounts)
```

```{r}
val_toll_amounts %>%  
  plot()
```

```{r}
val_toll_amounts %>%  
  summary()
```

```{r}
violating(toll_amounts_com, val_toll_amounts[3])
```

## Unequal Toll Amounts 
```{r}
unequal = toll_amounts_com %>%  
  filter(count.x != count.y) %>%  
  mutate(count_diff = count.x - count.y)
```

```{r}
unequal_tmp = unequal %>%  
  head(3)
```

```{r}
unequal %>% 
  group_by(roadway, trip_date) %>%  
  filter(sum(count_diff) != 0)
```

```{r}
tmp = toll_amounts_com %>% 
  filter(count.x - count.y != 0) %>% 
  group_by(roadway, trip_date) %>% 
  summarise(`#of_distinct_impacted_amounts` = n(), 
            ttl_changed_trips = sum(abs(count.x - count.y)),
            ttl_changed_trips1 = abs(sum(count.x - count.y)))

yolo = tmp %>%  
  pivot_longer(cols = c(ttl_changed_trips, ttl_changed_trips1)) %>% 
  mutate(value = sqrt(value), 
         text = str_glue("Trip Date: {ymd(trip_date)} \n Trip Count: {value^2}")) %>% 
  ggplot() + 
  geom_col(aes(trip_date, y = ifelse(test = name == "ttl_changed_trips", yes = -value, no = value), fill = name, text = text)) + 
    lemon::scale_y_symmetric(labels = abs) +
  facet_grid(cols = vars(roadway))

yolo %>%  
  ggplotly(tooltip = "text")
```


```{r}
toll_amounts_com %>%  
  mutate(count_diff = count.x - count.y) %>% 
  group_by(roadway, trip_date) %>%
  mutate(sum_count_diff = sum(count_diff, na.rm = T), 
         flag_sum_diff = case_when(sum_count_diff != 0~"No", 
                                  T~"Yes")) %>%  
  select(roadway, trip_date, sum_count_diff, flag_sum_diff)  %>%  
  unique() %>%  
  filter(roadway == "SR16") %>% 
  ggplot() + 
  geom_col(aes(trip_date, sum_count_diff)) + 
  scale_y_log10()
  
```


```{r}
unequal %>% 
  group_by(roadway, trip_date) %>%
  mutate(sum_count_diff = sum(count_diff), 
         flag_sum_diff = case_when(sum_count_diff != 0~"No", 
                                  T~"Yes")) %>%  
  select(-starts_with("que")) %>% 
  setnames(old = c("count.x", "count.y"), 
           new = c(str_glue("count_{tmp_names[1]}"), str_glue("count_{tmp_names[length(tmp_names)]}"))) %>% 
  dt_common(y = 500,
            sel = "multiple",
            pl = 10000,
            filter = "top",
            dom = "Bftipr") %>%  
  DT::formatCurrency(columns = 3) %>%  
  DT::formatDate(columns = 2)

```





<!--end-->




































