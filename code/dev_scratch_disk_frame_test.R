#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is is a scratch file to make a POC for using disk.frame.
#
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: disk.frame performs operations on data before it is brought into memory
#-------- [[insert brief readme here]]
#
# *please use 80 character margins
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#library set-up=================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
library(tidyverse)
library(data.table)
library(lubridate)
library(disk.frame)
library(nycflights13)

# this allows large datasets to be transferred between sessions
setup_disk.frame()
options(future.globals.maxSize = Inf)

#path set-up====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev

#source helpers/utilities=======================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
source(here::here("code/helpers_general.r"))
source(here::here("code/helpers_plotly.r"))
source(here::here("code/helpers_DT.r"))


#source data====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts

flights.df <- as.disk.frame(nycflights13::flights)

flights.df %>%
  filter(year == 2013) %>%
  mutate(origin_dest = paste0(origin, dest)) %>%
  head(2)

flights.df %>%
  .[year == 2013,] %>%
  .[,`:=`(yolo = 1)] %>%
  print()

#source data====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts


#trying to being object into memory the normal way
temp3 = fread("./data/toll_amounts/toll_status_trips_202108/trip_amounts_20211018.csv")

#actually doesnt seem to be a probelm with that particular data
#I loaded it three different times so im currently okay on RAM


#just trying disk.frame for fun
setup_disk.frame()
options(future.globals.maxSize = Inf)

dfm_temp = csv_to_disk.frame(
  "./data/toll_amounts/toll_status_trips_202108/trip_amounts_20211018.csv",
  outdir = file.path(tempdir(), "tmp_dfm.df"),
  overwrite = T
)


dfm_temp = csv_to_disk.frame(
  "./data/icrs_data/latest_icrs.csv",
  outdir = file.path(tempdir(), "tmp_dfm.df"),
  header = F,
  overwrite = T
)

crrnt_names = colnames(dfm_temp)
names_vec = setNames(crrnt_names, col_names)

tictoc::tic()
yolo = dfm_temp %>%
  rename(!!!names_vec) %>%
  .[,`:=`(updatedat = ymd_hms(updatedat),
          createdat = ymd_hms(createdat),
          processedat = ymd_hms(processedat))] %>%
  .[,`:=`(process_total = as_date(processedat)-as_date(trip_date),
          process_qfree = as_date(processedat)-as_date(updatedat),
          process_b4qfree = as_date(updatedat)-as_date(trip_date),
          dispositionNullFlag = case_when(dispositionNullFlag==1~"Has disposition",
                                          T~"No disposition"))]
tictoc::toc()



#EDA/data descriptions==========================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vps_20211008 %>%
  .[,.(counts = .N), by = .(tollstatuscodeid, shortcode, description)]

vps_20211013 %>%
  .[,.(counts = .N), by = .(tollstatuscodeid, shortcode, description)]


#investigating status changes===================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev

data_drift %>%
  .[status.x != status.y,.(counts = .N), by = .(status.x, status.y)] %>%
  .[order(status.x, status.y)]

#records where oldest query was posted but has since changed
data_drift_posted = data_drift %>%
  .[status.x == "4-POSTED-Posted" &
      status.x != status.y]

#subsetting OG data with data_drift_posted trippassageids
#---> nothing too interesting in this
data_drift_subset = data_drift %>%
  .[trippassageid %in% data_drift_posted$trippassageid]

data_drift_subset %>%
  select(order(colnames(data_drift_subset))) %>%
  select(tollId, trippassageid, imageReviewRequestId, everything())

broken_out_issue_records =  list(vps_20211008, vps_20211013) %>%
  map(~.x %>%
        .[trippassageid %in% data_drift_posted$trippassageid]
  ) %>%
  reduce(bind_rows) %>%
  .[order(trippassageid, imageReviewRequestId)]

#investigating IRR with IRRequestIds============================================
#want to cross-validate against IRR query

#this code grabs the review request ids that can then be queried with
{
unique(broken_out_issue_records$imageReviewRequestId) %>%
  paste(collapse = ", ")

clipr::write_last_clip()
}

#reading in table copied from SSMS
# IRR_queried_ids = clipr::read_clip_tbl()
IRR_queried_ids %>%
  mutate(across(c(updatedat, createdat, processedat), ymd_hms)) %>%
  arrange(trippassageid, desc(imageReviewRequestId), desc(imageReviewResultsId))


#conclusion (maybe)=============================================================
#it looks like the query doesn't include result and dispute information like
#how the actual IRR query does
#in addition, it looks like the timetamps don't really indicate the the sequence of when an IRR






#one record for each 'description' type per each work week
vps_irr %>%
  .[,(head(.SD, 1)), by = .(week_of = floor_date(exittime, "week"), description)] %>%
  .[order(week_of, description)]

vps_irr %>%
  .[,.(trip_count = .N), by = .( week_of = floor_date(exittime, "week"), description)]






#Correct column pivoting here===================================================
icrs_subprocess = icrs_current %>%
  mutate(txn_date = mdy(txn_date),
         across(matches("_\\d+"), as.numeric)) %>%
  pivot_longer(cols = unprocessed_cnt:icrs_2800_image_upload_failed,
               names_to = "process_stage",
               values_to = "count") %>%
  select(txn_date, process_stage, count) %>%
  mutate(process_stage = as.factor(process_stage))

icrs_summary = icrs_current %>%
  mutate(txn_date = mdy(txn_date),
         across(matches("_\\d+"), as.numeric)) %>%
  pivot_longer(cols = c(icrs_2100_tag_post_to_host:csc_rejected_cnt, -vps_icrs_diff),
               names_to = "process_stage",
               values_to = "count") %>%
  select(txn_date, process_stage, count) %>%
  mutate(process_stage = as.factor(process_stage))

icrs_summary %>%
  plot_ly(x = ~txn_date, y = ~process_stage, z = ~log10(count+1), type = "heatmap") %>%
  layout(
    title = "ICRS Stage Count Tracker",
    xaxis = make_range_select_buttons(
      "Transaction Date",
      c(1, 3, 6, 12),
      rep("month", 4),
      rep("backward", 4)
    ),
    yaxis = list(title = "ICR Stage"))


icrs_current %>%
  mutate(txn_date = mdy(txn_date),
         across(matches("_\\d+"), as.numeric)) %>%
  mutate(val_ending_disposition_subtotal = (icrs_2100_tag_post_to_host+
                   icrs_2050_posted_to_host+
                   icrs_2050_icrs_reject+
                   icrs_2000_prevps_manual_review+icrs_2000_posted_to_vps) - ending_disposition_subtotal,
         val_rtxn_cnt_edisp = rtxn_cnt-ending_disposition_subtotal,
         val_rtxn_cnt_csc = rtxn_cnt-(csc_posted_cnt+csc_rejected_cnt),
         val_edisp_csc = ending_disposition_subtotal-(csc_posted_cnt+csc_rejected_cnt)) %>%
  pivot_longer(cols = starts_with("val_"),
               names_to = "process_stage",
               values_to = "count") %>%
  select(txn_date, process_stage, count) %>%
  mutate(process_stage = as.factor(process_stage)) %>%
  plot_ly(x = ~txn_date, y = ~process_stage, z = ~log10(abs(count)+1), type = "heatmap") %>%
  layout(
    title = "ICRS Stage Count Tracker",
    xaxis = make_range_select_buttons(
      "Transaction Date",
      c(1, 3, 6, 12),
      rep("month", 4),
      rep("backward", 4)
    ),
    yaxis = list(title = "ICR Stage"))


#script end=====================================================================

foo <- function(){
  n1<-readline(prompt="Enter skip 1: " )
  n2<-readline(prompt="Enter skip 2: " )
  n1<-as.integer(n1)

  is.null(n2) %>%  print()
  n2<-as.integer(n2)
  c(n1, n2)
}

foo()
