#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is script processes the ICRS data before it is analyzed.
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: script was majorly changed
#-------- data out of ETAN was changed to be aggregated
#--------
#
# *please use 80 character margins
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#library set-up=================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
library(data.table)
library(tidyverse)
library(lubridate)
library(DBI)
library(odbc)
library(dbcooper)

#path set-up====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev

#source helpers/utilities=======================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
source(here::here("code/helpers_markdown.R"))
source(here::here("code/helpers_general.R"))

#source data====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts


#DATA QUERY=====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define db_connection
db_conn = dbConnect(odbc(), "test_dsn_ssms")
# db_conn = dbConnect(odbc(), "etan_listener")

##QUERY_#1======================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#three step query that creates image request count table by tripPassageId
message("Query 1 of 4 started.")

dbExecute(db_conn,
"with temp as(
select tpidr.tripPassageImageDownloadRequestId, tpidr.tripPassageId, tpidr.xmlnodelocation, tpidr.tollImageTypeId,
tpidr.updatedat, tpidr2.errorcode, tpidr2.errorstring, tpidr2.postingdate, tpidr2.isprocessed
,case when tpidr2.errorcode = 2 then 1 else 0 end as flag_dl
,case when tpidr2.errorcode = 1 then 1 else 0 end as flag_dlno
,case when tpidr2.errorcode is NULL then 1 else 0 end as flag_dlnull
from lance.TripPassageImageDownloadRequest tpidr
left join lance.TripPassageImageDownloadResponse tpidr2 on tpidr2.tripPassageImageDownloadRequestId = tpidr.tripPassageImageDownloadRequestId
WHERE tpidr.tripPassageId in (select trippassageid from lance.tripPassage where cast(lanetime as date) > '2021-08-01')
),
temp_1 as (
select trippassageid
,count(*) as count_req
,sum(flag_dl) as count_dl
,sum(flag_dlnull) as cnt_null
,sum(flag_dlno) as cnt_no_dl
,case when sum(flag_dl) > 0 then 1 else 0 end as flag_yes_dl
,case when sum(flag_dl) = 0 then 1 else 0 end as flag_no_dl
from temp
group by trippassageid
)
select * into #fltrd_notAgg_1
from temp_1", immediate = TRUE)

message("Query 1 of 3 completed.")

##QUERY_#2======================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#joins tip attributes back into view
message("Query 2 of 3 started.")
tictoc::tic()

dbExecute(db_conn,
"select temp_1.*
,temp_2.flag_inLance
,case when temp_2.flag_inLance is NULL then 1 else 0 end as flag_notinImage
,pcode.roadway
,toll.tollstatuscodeid
,tp.roadsideTollType
,cast(tp.lanetime as date) as date
into #fltrd_notAgg_2_joined
from #fltrd_notAgg_1 temp_1
left join
  (select DISTINCT tripPassageId
   ,1 as flag_inLance from lance.PassageImageURI)  temp_2
  on temp_2.trippassageid = temp_1.trippassageid
join lance.tripPassage tp on tp.tripPassageId = temp_1.tripPassageId
join rtoll.PlazaCodes as pcode on tp.roadgantryid = pcode.plazacodeid
join lance.trip trip on trip.tripPassageId = tp.tripPassageId
join rtoll.tollstatus toll on toll.tollid = trip.tripid", immediate = TRUE)

message("Query 2 of 3 completed.")


##QUERY_#3======================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#creates large summary table from view
message("Query 3 of 3 completed.")

combined_data =
  dbGetQuery(db_conn,
             "select
           date, roadway, roadsidetolltype, tollstatuscodeid
           ,sum(count_req) as ttl_requested
           ,sum(count_dl) as ttl_downloaded_t
           ,sum(cnt_null) as ttl_nullstatus
           ,sum(cnt_no_dl) as ttl_downloaded_f
           ,sum(flag_yes_dl) as ttl_yes_dl_image
           ,sum(flag_no_dl) as ttl_no_dl_image
           ,sum(flag_inLance) as ttl_inLance
           ,sum(flag_notinImage) as ttl_notinImage
           ,count(*) as count
           ,cast(GETDATE() as date) as queried_at
           from #fltrd_notAgg_2_joined
           group by roadway, date, roadsidetolltype, tollstatuscodeid
           order by roadway, date, roadsidetolltype, tollstatuscodeid")

name =  str_remove_all(as_date(Sys.Date()), "-") %>%
  paste0("bl_toll_status_", ., ".csv") %>%
  here::here("data/backlog", .)

write.csv(combined_data, name)

message(paste0("Query 3 of 3 completed and data saved as: \n \n", name))


#script end=====================================================================
