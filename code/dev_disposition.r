#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This is script [[insert brief readme here]]
#
# By: mike gaunt, michael.gaunt@wsp.com
#
# README: [[insert brief readme here]]
#-------- [[insert brief readme here]]
#
# *please use 80 character margins
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#library set-up=================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
library(tidyverse)
library(gauntlet)

#path set-up====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev

#source helpers/utilities=======================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev

#source data====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#content in this section should be removed if in production - ok for dev
#area to upload data with and to perform initial munging
#please add test data here so that others may use/unit test these scripts


#main header====================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

data_disposition = tar_read("data_disposition")

ds_make_reactable_table = function(file){
    # file = tar_read("file_disposition")

    #import data
    files = readxl::excel_sheets(file)
    disposition = readxl::read_xlsx(file, "data")

    reactable(disposition)

}

##sub header 1==================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

##sub header 2==================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

penguins %>%
  group_by(species, island) %>%
  na.omit(.) %>%
  summarize(across(where(is.numeric), list)) %>%
  mutate(penguin_colors = case_when(
    species == 'Adelie' ~ '#F5A24B',
    species == 'Chinstrap' ~ '#AF52D5',
    species == 'Gentoo' ~ '#4C9B9B',
    TRUE ~ 'grey'
  )) %>%
  select(-c(year,body_mass_g,flipper_length_mm)) %>%
  reactable(
    .,
    theme = pff(centered = TRUE),
    compact = TRUE,
    columns = list(
      species = colDef(maxWidth = 90),
      island = colDef(maxWidth = 85),
      penguin_colors = colDef(show = FALSE),
      bill_length_mm = colDef(
        cell = react_sparkline(
          data = .,
          height = 100,
          line_width = 1.5,
          bandline = 'innerquartiles',
          bandline_color = 'forestgreen',
          bandline_opacity = 0.6,
          labels = c('min','max'),
          label_size = '0.9em',
          highlight_points = highlight_points(min = 'blue', max = 'red'),
          margin = reactablefmtr::margin(t=15,r=2,b=15,l=2),
          tooltip_type = 2
        )
      ),
      bill_depth_mm = colDef(
        cell = react_sparkline(
          data = .,
          height = 100,
          line_width = 1.5,
          bandline = 'innerquartiles',
          bandline_color = 'forestgreen',
          bandline_opacity = 0.6,
          labels = c('min','max'),
          label_size = '0.9em',
          highlight_points = highlight_points(min = 'blue', max = 'red'),
          margin = reactablefmtr::margin(t=15,r=2,b=15,l=2),
          tooltip_type = 2
        )
      )
    )
  )

#script end=====================================================================










































